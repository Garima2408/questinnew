package co.questin.database;

import android.provider.BaseColumns;

/**
 * Created by farheen on 13/9/17
 */

public final class QuestinContract {

    private QuestinContract(){}

    public static final String DB_NAME = "questin.db";
    public static final int DB_VERSION = 1;

    public static final int CATEGORY_DATE = 0;
    public static final int CATEGORY_TEXT = 1;
    public static final int CATEGORY_IMAGE = 2;
    public static final int CATEGORY_VIDEO = 3;
    public static final int CATEGORY_DOCUMENT = 4;
    public static final int CATEGORY_LOCATION = 5;
    public static final int CATEGORY_CONTACT = 6;

    public static final int MINE_NO = 0;
    public static final int MINE_YES = 1;

    public static final int STATUS_PENDING = 0;
    public static final int STATUS_SENT = 1;
    public static final int STATUS_DELIVERED = 2;
    public static final int STATUS_READ = 3;

    public static class Chat implements BaseColumns{
        public static final String TABLE_NAME = "CHAT_TABLE";
        public static final String COL_IS_MINE = "is_mine";
        public static final String COL_SENDER_ID = "sender_id";
        public static final String COL_CATEGORY = "category";
        public static final String COL_TEXT = "text";
        public static final String COL_LINK = "link";
        public static final String COL_TIME = "time";
        public static final String COL_STATUS = "status";
    }

    public static class Conversation implements BaseColumns{
        public static final String TABLE_NAME = "CONVERSATION_TABLE";
        public static final String COL_SENDER_ID = "sender_id";
        public static final String COL_NAME = "name";
        public static final String COL_DP_URL = "dp_url";
        public static final String COL_IS_MINE = "is_mine";
        public static final String COL_LAST_MSG_CATEGORY = "last_msg_category";
        public static final String COL_LAST_MSG_TEXT = "last_msg_text";
        public static final String COL_LAST_MSG_TIME = "last_msg_time";
        public static final String COL_LAST_MSG_STATUS = "last_msg_status";
        public static final String COL_UNREAD_COUNT = "unread_count";
        public static final String COL_GROUP_ID = "group_id";
//        public static final String COL_REG_TOKEN = "reg_token";
    }


    public static class GroupConversationList implements BaseColumns{
        public static final String TABLE_NAME = "GROUPCONVERSATION_TABLE";
        public static final String GROUP_SENDER_ID = "sender_id";
        public static final String GROUP_NAME = "name";
        public static final String GROUP_DP_URL = "dp_url";
        public static final String GROUP_IS_MINE = "is_mine";
        public static final String GROUP_LAST_MSG_CATEGORY = "last_msg_category";
        public static final String GROUP_LAST_MSG_TEXT = "last_msg_text";
        public static final String GROUP_LAST_MSG_TIME = "last_msg_time";
        public static final String GROUP_LAST_MSG_STATUS = "last_msg_status";
        public static final String GROUP_UNREAD_COUNT = "unread_count";
        public static final String GROUP_GROUP_ID = "group_id";

    }




    public static class GroupChat implements BaseColumns{
        public static final String TABLE_NAME = "GROUP_CHAT_TABLE";
        public static final String GROUP_GROUP_ID = "group_id";
        public static final String GROUP_SENDER_ID = "sender_id";
        public static final String GROUP_SENDER_NAME = "group_name";
        public static final String GROUP_IS_MINE = "is_mine";
        public static final String GROUP_CATEGORY = "category";
        public static final String GROUP_TEXT = "text";
        public static final String GROUP_LINK = "link";
        public static final String GROUP_TIME = "time";
        public static final String GROUP_STATUS = "status";
    }




        public static class CalenderEvents implements BaseColumns{
        public static final String TABLE_NAME = "CALENDER_EVENT_TABLE";
        public static final String EVENT_ID = "event_id";
        public static final String EVENT_START_DATE = "event_start_date";
        public static final String EVENT_END_DATE = "event_end_date";
        public static final String EVENT_START_TIME = "event_start_time";
        public static final String EVENT_END_TIME = "event_end_time";
        public static final String EVENT_TITTLE = "event_tittle";
        public static final String EVENT_DESCRIPTION = "event_description";
        public static final String EVENT_MAINID ="event_mainid";

        }

    public static class CalenderAssignment implements BaseColumns{
        public static final String TABLE_NAME = "CALENDER_ASSIGNMENT_TABLE";
        public static final String EVENT_ID = "event_id";
        public static final String EVENT_START_DATE = "event_start_date";
        public static final String EVENT_END_DATE = "event_end_date";
        public static final String EVENT_START_TIME = "event_start_time";
        public static final String EVENT_END_TIME = "event_end_time";
        public static final String EVENT_TITTLE = "event_tittle";
        public static final String EVENT_DESCRIPTION = "event_description";
        public static final String EVENT_MAINID ="event_mainid";

    }

    public static class CalenderExams implements BaseColumns{
        public static final String TABLE_NAME = "CALENDER_EXAMS_TABLE";
        public static final String EVENT_ID = "event_id";
        public static final String EVENT_START_DATE = "event_start_date";
        public static final String EVENT_END_DATE = "event_end_date";
        public static final String EVENT_START_TIME = "event_start_time";
        public static final String EVENT_END_TIME = "event_end_time";
        public static final String EVENT_TITTLE = "event_tittle";
        public static final String EVENT_DESCRIPTION = "event_description";
        public static final String EVENT_MAINID ="event_mainid";

    }




    public static class CalenderCollageEvents implements BaseColumns{
        public static final String TABLE_NAME = "COLLAGE_CALENDER_EVENT_TABLE";
        public static final String EVENT_ID = "event_id";
        public static final String EVENT_START_DATE = "event_start_date";
        public static final String EVENT_END_DATE = "event_end_date";
        public static final String EVENT_START_TIME = "event_start_time";
        public static final String EVENT_END_TIME = "event_end_time";
        public static final String EVENT_TITTLE = "event_tittle";
        public static final String EVENT_DESCRIPTION = "event_description";
        public static final String EVENT_MAINID ="event_mainid";




    }

    /*Attandance Tabble*/


    public static class AddAttendanceOffline implements BaseColumns{
        public static final String TABLE_NAME = "OFFLINE_ATTENDANCE_TABLE";
        public static final String ATTENDANCE_ID = "attendance_id";
        public static final String ATTENDANCE_UID = "uid";
        public static final String ATTENDANCE_VALUE = "marked_value";
        public static final String ATTENDANCE_COURSE_ID = "Course_id";
        public static final String ATTENDANCE_CLASS_ID = "Classs_Id";
        public static final String ATTENDANCE_DATE = "date";
        public static final String ATTENDANCE_TYPE = "type";




    }






}
