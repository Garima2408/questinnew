package co.questin.clubandsociety;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.GroupNSocityAdapter;
import co.questin.library.StateBean;
import co.questin.models.ClubsNSocityArray;
import co.questin.network.URLS;
import co.questin.settings.FAQ;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class GroupNSocieties extends BaseAppCompactActivity {
    RecyclerView rv_ClubsNsocity;
    String Offsetvalue;
    public ArrayList<ClubsNSocityArray> mclubnsocityList;
    private RecyclerView.LayoutManager layoutManager;
    private GroupNSocityAdapter clunnsocityadapter;
    FloatingActionButton fab;
    SwipeRefreshLayout swipeContainer;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    private int currentPage = 1;
    static TextView ErrorText;
    private static ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_nsocieties);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        mclubnsocityList=new ArrayList<>();

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();
        layoutManager = new LinearLayoutManager(this);
        rv_ClubsNsocity = findViewById(R.id.rv_ClubsNsocity);
        rv_ClubsNsocity.setHasFixedSize(true);
        rv_ClubsNsocity.setLayoutManager(layoutManager);
        swipeContainer =findViewById(R.id.swipeContainer);
        ErrorText =findViewById(R.id.ErrorText);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));
        clunnsocityadapter = new GroupNSocityAdapter(rv_ClubsNsocity,mclubnsocityList,GroupNSocieties.this);
        rv_ClubsNsocity.setAdapter(clunnsocityadapter);
        fab = findViewById(R.id.fab);
        CollageClubsNSocityDisplay();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity (new Intent (GroupNSocieties.this, FAQ.class));

            }
        });



        inItView();


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        PAGE_SIZE=0;
                        isLoading=false;
                        swipeContainer.setRefreshing(true);
                        CollageClubsNSocityDisplay();


                    }
                }, 3000);
            }
        });




    }





    private void inItView(){

        swipeContainer.setRefreshing(false);

        rv_ClubsNsocity.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    clunnsocityadapter.addProgress();
                    CollageClubsNSocityDisplay();

                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }



    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");

        super.onPause();
    }



    private void CollageClubsNSocityDisplay() {
        swipeContainer.setRefreshing(true);
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_COLLAGECLUBS +"?cid="+ SessionManager.getInstance(getActivity()).getCollage().getTnid()+"&offset="+PAGE_SIZE+"&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                rv_ClubsNsocity.setVisibility(View.VISIBLE);
                                swipeContainer.setRefreshing(false);

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    ArrayList<ClubsNSocityArray> arrayList = new ArrayList<>();

                                    for (int i = 0; i < jsonArrayData.length(); i++) {

                                        ClubsNSocityArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), ClubsNSocityArray.class);
                                        arrayList.add(CourseInfo);

                                    }
                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {


                                            rv_ClubsNsocity.setVisibility(View.VISIBLE);
                                            clunnsocityadapter.setInitialData(arrayList);


                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);



                                        } else {
                                            isLoading = false;
                                            clunnsocityadapter.removeProgress();
                                            swipeContainer.setRefreshing(false);

                                            clunnsocityadapter.addData(arrayList);


                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);




                                        }
                                    }
                                    else{
                                        if (PAGE_SIZE==0){
                                            rv_ClubsNsocity.setVisibility(View.GONE);
                                            ErrorText.setVisibility(View.VISIBLE);
                                            ErrorText.setText("No Societies");
                                            swipeContainer.setVisibility(View.GONE);

                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);


                                        }else
                                            clunnsocityadapter.removeProgress();

                                    }
                                } else {
                                    swipeContainer.setRefreshing(false);
                                    mShimmerViewContainer.stopShimmerAnimation();
                                    mShimmerViewContainer.setVisibility(View.GONE);


                                }
                            } else {
                                swipeContainer.setRefreshing(false);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);



                            }
                        } catch (Exception e) {
                            swipeContainer.setRefreshing(false);


                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipeContainer.setRefreshing(false);
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(GroupNSocieties.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(GroupNSocieties.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(GroupNSocieties.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(GroupNSocieties.this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }


    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                PAGE_SIZE=0;
                isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
