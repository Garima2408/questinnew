package co.questin.clubandsociety;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TabHost;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.library.StateBean;
import co.questin.network.URLS;
import co.questin.utils.SessionManager;

import static co.questin.utils.BaseAppCompactActivity.ShowIndicator;
import static co.questin.utils.BaseAppCompactActivity.hideIndicator;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;
import static co.questin.utils.Utils.isStatusSuccess;

public class GroupNSocityDetails extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Bundle bundle;
    ImageView ClubImage;
    String Group_id, GroupTittle, groupImage,groupdetails,groupemail;
    private TabHost tabHost;
    private CollapsingToolbarLayout collapsingToolbarLayout = null;
    Double Lat,Long;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_nsocity_details);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        Bundle b = getIntent().getExtras();
        Group_id = b.getString("GROUP_ID");
        GroupTittle = b.getString("TITTLE");

        GetAllEventDetails();
        collapsingToolbarLayout =  findViewById(R.id.collapsing_toolbar);
        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        collapsingToolbarLayout.setTitle(GroupTittle);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.CollapsingToolbarLayout);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsingToolbarLayout);

        ClubImage = findViewById(R.id.ClubImage);




    }



    private void setupViewPager(ViewPager viewPager) {


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        bundle = new Bundle();
        bundle.putString("GROUP_ID", Group_id);
        bundle.putString("TITTLE",GroupTittle);
        bundle.putString("GROUP_DETAILS",groupdetails);
        bundle.putString("GROUP_EMAIL",groupemail);
        bundle.putString("LAT", String.valueOf(Lat));
        bundle.putString("LNG", String.valueOf(Long));



        GroupInformationDetails info = new GroupInformationDetails();
        info.setArguments(bundle);

        ClubMembersGroups group = new ClubMembersGroups();
        group.setArguments(bundle);

        GroupMediaDetails classlist = new GroupMediaDetails();
        classlist.setArguments(bundle);


        adapter.addFragment(info, "Info");
        adapter.addFragment(group, "Group");
        adapter.addFragment(classlist, "Media");


        viewPager.setAdapter(adapter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            viewPager.setNestedScrollingEnabled(false);
        }

    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    /*GROUPDETAILS*/

    private void GetAllEventDetails() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_COLLAGEGROUPINFO+"/"+Group_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONObject data = obj.getJSONObject("data");

                                    groupdetails =data.getString("body");
                                    groupemail =data.getString("field_team_email");


                                    JSONArray picture = data.getJSONArray("field_group_image");
                                    if(picture != null && picture.length() > 0 ) {
                                        for (int j = 0; j < picture.length(); j++) {
                                            Log.e("group pictire", picture.getString(j));
                                            groupImage =(picture.getString(0));

                                        }
                                    }


                                    if(groupImage != null && groupImage.length() > 0 ) {
                                        try {
                                            Glide.with(GroupNSocityDetails.this).load(groupImage)
                                                    .placeholder(R.mipmap.club).dontAnimate()
                                                    .skipMemoryCache(true)
                                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                    .fitCenter().into(ClubImage);




                                        } catch (Exception e) {

                                        }
                                    }else {
                                        ClubImage.setImageResource(R.mipmap.club);

                                    }


                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(GroupNSocityDetails.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(GroupNSocityDetails.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(GroupNSocityDetails.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);

    }



    @Override
    public void onBackPressed() {
        ActivityCompat.finishAfterTransition(this);
        Intent i = new Intent(GroupNSocityDetails.this, GroupNSocieties.class);
        finish();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                ActivityCompat.finishAfterTransition(this);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }



}