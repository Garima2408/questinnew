package co.questin.clubandsociety;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.GroupMembersAdapter;
import co.questin.library.StateBean;
import co.questin.models.CourseMemberLists;
import co.questin.models.SubjectCommentArray;
import co.questin.network.URLS;
import co.questin.utils.BaseFragment;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

import static co.questin.utils.BaseAppCompactActivity.ShowIndicator;
import static co.questin.utils.BaseAppCompactActivity.hideIndicator;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;
import static co.questin.utils.Utils.isStatusSuccess;

public class ClubMembersGroups extends BaseFragment {
    String Group_id;
    RecyclerView GroupMember_Lists,rv_subject_comment;
    Button JoinGroup;
    RecyclerView.LayoutManager mLayoutManager,layoutManager;
    GroupMembersAdapter mGroupMemberAdapter;
    public ArrayList<CourseMemberLists> memberList;
    public ArrayList<SubjectCommentArray> commentsList;
    static Activity activity;
    private static ProgressBar spinner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_group_member_details, container, false);
        activity = (Activity) view.getContext();
        Group_id = getArguments().getString("GROUP_ID");
        spinner= view.findViewById(R.id.progressBar);
        JoinGroup = view.findViewById(R.id.JoinGroup);
        GroupMember_Lists = view.findViewById(R.id.GroupMember_Lists);
        rv_subject_comment = view.findViewById(R.id.rv_subject_comment);
        layoutManager = new LinearLayoutManager(activity);
        mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        GroupMember_Lists.setLayoutManager(mLayoutManager);
        rv_subject_comment.setHasFixedSize(true);
        rv_subject_comment.setLayoutManager(layoutManager);
        memberList = new ArrayList<CourseMemberLists>();
        commentsList = new ArrayList<SubjectCommentArray>();

        /*GROUP MEMBER LIST CALLING*/
        GroupMemberList();






        JoinGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*JOIN ANY GROUP AND SOCITY CALLING*/
                ProgressJoinTheGroupRequest();

            }
        });


        return view;
    }





    /*GROUP MEMBER LIST CALLING METHODS*/


    private void GroupMemberList() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET,URLS.URL_GROUPMEMBERLISTS+"/"+Group_id+"/users?fields=id,etid,gid",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    memberList.clear();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {

                                        for (int i = 0; i < jsonArrayData.length(); i++) {
                                            CourseMemberLists CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CourseMemberLists.class);
                                            memberList.add(CourseInfo);



                                            if (CourseInfo.getEtid().contains(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id()))
                                            {
                                                Log.d("TAG", "myid: " + SessionManager.getInstance(getActivity()).getUser().getUserprofile_id());

                                                JoinGroup.setVisibility(View.GONE);
                                                /* CALLING LISTS OF COMMENTS*/
                                   /* subjectscommentsList = new SubjectsCommentsList();
                                    subjectscommentsList.execute();*/


                                            }



                                        }
                                        mGroupMemberAdapter = new GroupMembersAdapter(activity, R.layout.list_of_groupmemberlists,memberList);
                                        GroupMember_Lists.setAdapter(mGroupMemberAdapter);
                                    }


                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);

    }



    /*JOIN ANY GROUP AND SOCITY CALLING METHODS*/

    private void ProgressJoinTheGroupRequest() {

        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_JOINGROUP+"/"+Group_id+"/"+SessionManager.getInstance(getActivity()).getUser().userprofile_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {


                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type","subgroup");

                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);

    }




}

