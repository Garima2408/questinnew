package co.questin.alumni;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AlignmentSpan;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.activities.SignIn;
import co.questin.library.StateBean;
import co.questin.models.CollageLists;
import co.questin.models.CollageRoleArray;
import co.questin.models.CourseMainList;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static co.questin.utils.Utils.isStatusSuccess;

public class AluminiRegistrationUpdate extends BaseAppCompactActivity {
    EditText edt_bach,edt_branch,edt_selectsub;
    Button btn_go_dashboard;
    TextView collageName;
    Spinner edt_selectsubSpinner;
    String batch,branch,subject;
    private ArrayList<String> courselist;
    ArrayList<CourseMainList> listofcourse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumini_registration_update);
        GetTheListOfCourse();


        edt_bach = findViewById(R.id.edt_bach);
        edt_branch = findViewById(R.id.edt_branch);
        edt_selectsub = findViewById(R.id.edt_selectsub);
        btn_go_dashboard = findViewById(R.id.btn_go_dashboard);
        edt_selectsubSpinner = findViewById(co.questin.R.id.edt_selectsubSpinner);
        courselist = new ArrayList<String>();
        listofcourse = new ArrayList<CourseMainList>();


        collageName = findViewById(R.id.collageName);
        String Topheading = SessionManager.getInstance(getActivity()).getCollage().getTitle();
        SpannableString spString = new SpannableString(Topheading);
        AlignmentSpan.Standard aligmentSpan = new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER);
        spString.setSpan(aligmentSpan, 0, spString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        collageName.setText(spString);

        displayUserData();

        btn_go_dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptToAluminiRegistration();


            }
        });

        edt_selectsub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

    }

    private void GetTheListOfCourse() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_COURCELIST+"?cid="+ SessionManager.getInstance(getActivity()).getCollage().getTnid(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray data = obj.getJSONArray("data");

                                    for (int i=0; i<data.length();i++) {
                                        CourseMainList GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), CourseMainList.class);
                                        courselist.add(GoalInfo.getName());
                                        listofcourse.add(GoalInfo);

                                        edt_selectsubSpinner.setAdapter(new ArrayAdapter<String>(AluminiRegistrationUpdate.this,
                                                android.R.layout.simple_spinner_dropdown_item,
                                                courselist));
                                        hideIndicator();

                                        edt_selectsubSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

                                            @Override
                                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                edt_selectsub.setText(listofcourse.get(position).getTid());

                                                Log.d("TAG", "edt_selectsub: " + edt_selectsub);
                                            }

                                            @Override
                                            public void onNothingSelected(AdapterView<?> parent) {}

                                        });

                                    }


                                } else {
                                    hideIndicator();


                                }
                            } else {
                                hideIndicator();


                            }
                        } catch (Exception e) {
                            hideIndicator();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(AluminiRegistrationUpdate.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(AluminiRegistrationUpdate.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(AluminiRegistrationUpdate.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }


    private void displayUserData() {


        if (!co.questin.utils.TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole())) {
            edt_bach.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageBatch());
        }
        if (!co.questin.utils.TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageBranch())) {
            edt_branch.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageBranch());
        }



    }



    private void attemptToAluminiRegistration() {

        edt_bach.setError(null);
        edt_branch.setError(null);
        edt_selectsub.setError(null);



        // Store values at the time of the login attempt.

        batch = edt_bach.getText().toString().trim();
        branch = edt_branch.getText().toString().trim();
        subject = edt_selectsub.getText().toString().trim();


        CollageRoleArray userRoleDetail = new CollageRoleArray();
        userRoleDetail.role = "15";
        userRoleDetail.tnid = SessionManager.getInstance(getActivity()).getCollage().getTnid();
        userRoleDetail.title = SessionManager.getInstance(getActivity()).getCollage().getTitle();
        userRoleDetail.field_group_image = SessionManager.getInstance(getActivity()).getCollage().getField_group_image();
        userRoleDetail.CollageLogo =SessionManager.getInstance(getActivity()).getCollage().getField_groups_logo();
        userRoleDetail.CollageRole ="alumni";
        userRoleDetail.CollageBranch = branch;
        userRoleDetail.CollageBatch =batch;

        SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);



        boolean cancel = false;
        View focusView = null;

       if (co.questin.utils.TextUtils.isNullOrEmpty(batch)) {
            // check for First Name
            focusView = edt_bach;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));        }
        else if (co.questin.utils.TextUtils.isNullOrEmpty(branch)) {
            // check for First Name
            focusView = edt_branch;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));        }
        else if (co.questin.utils.TextUtils.isNullOrEmpty(subject)) {
            // check for First Name
            focusView = edt_selectsub;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            if (!isInternetConnected()){
                showConnectionSnackbar();
            }else {
                ShowIndicator();
                RegistrationAluminiAuthTask();
            }
        }
    }



    private void RegistrationAluminiAuthTask() {
        StringRequest mStrRequest = new StringRequest(Request.Method.PUT, URLS.URL_STUDENETREGISTRATION+"/"+SessionManager.getInstance(getActivity()).getCollage().getCollageMemberShipId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    showSnackbarSignIn("Role Change Successfully.Login Again");


                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(getActivity()).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(getActivity()).getaccesstoken().sessionName+"="+SessionManager.getInstance(getActivity()).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("entity_type","user");
                params.put("etid",SessionManager.getInstance(getActivity()).getUser().userprofile_id);
                params.put("group_type","node");
                params.put("gid",SessionManager.getInstance(getActivity()).getCollage().getTnid());
                params.put("state","1");
                params.put("field_i_am_a","alumni");
                params.put("field_branch",branch);
                params.put("field_enrollment_number","");
                params.put("field_member_course",subject);
                params.put("field_batch_year",batch);
                params.put("roles[15]","alumni");
                return params;
            }

        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);
    }







    @Override
    public void onBackPressed() {

        finish();
    }
}


