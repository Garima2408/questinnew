package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 17-08-2017.
 */

public class ClubsNSocityArray implements Serializable{




    private int type= 0;
    public String  tnid;
    public String  image;
    public String  title;
    public static final int COARSE_TYPE = 0;            //default yeh hai
    public static final int PROGRESS_ = 1;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public ClubsNSocityArray(int type) {
        this.type = type;

    }


    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



}
