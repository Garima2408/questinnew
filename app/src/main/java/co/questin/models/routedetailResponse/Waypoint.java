
package co.questin.models.routedetailResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Waypoint {

    @SerializedName("field_photo")
    @Expose
    private String fieldPhoto;
    @SerializedName("field_start_time")
    @Expose
    private String fieldStartTime;
    @SerializedName("field_departure_time")
    @Expose
    private String fieldDepartureTime;
    @SerializedName("field_way_points")
    @Expose
    private FieldWayPoints fieldWayPoints;

    private String  field_place_name;

    public String getField_place_name() {
        return field_place_name;
    }

    public void setField_place_name(String field_place_name) {
        this.field_place_name = field_place_name;
    }

    public String getFieldPhoto() {
        return fieldPhoto;
    }

    public void setFieldPhoto(String fieldPhoto) {
        this.fieldPhoto = fieldPhoto;
    }

    public String getFieldStartTime() {
        return fieldStartTime;
    }

    public void setFieldStartTime(String fieldStartTime) {
        this.fieldStartTime = fieldStartTime;
    }

    public String getFieldDepartureTime() {
        return fieldDepartureTime;
    }

    public void setFieldDepartureTime(String fieldDepartureTime) {
        this.fieldDepartureTime = fieldDepartureTime;
    }

    public FieldWayPoints getFieldWayPoints() {
        return fieldWayPoints;
    }

    public void setFieldWayPoints(FieldWayPoints fieldWayPoints) {
        this.fieldWayPoints = fieldWayPoints;
    }

}
