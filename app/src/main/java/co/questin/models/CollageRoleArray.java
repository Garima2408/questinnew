package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 04-12-2017.
 */

public class CollageRoleArray implements Serializable {

    public String role;
    public String tnid;
    public String title;
    public String field_group_image;
    public String CollageLogo;
    public String CollageRole;
    public String CollageBatch;
    public String CollageBranch;
    public String Collagedepartment;
    public String CollageEmail;
    public String CollageDesignation;
    public String CollageEnrollment;
    public String CollageStudentNamev;


    public String getCollageLogo() {
        return CollageLogo;
    }

    public void setCollageLogo(String collageLogo) {
        CollageLogo = collageLogo;
    }

    public String getCollageRole() {
        return CollageRole;
    }

    public void setCollageRole(String collageRole) {
        CollageRole = collageRole;
    }

    public String getCollageBatch() {
        return CollageBatch;
    }

    public void setCollageBatch(String collageBatch) {
        CollageBatch = collageBatch;
    }

    public String getCollageBranch() {
        return CollageBranch;
    }

    public void setCollageBranch(String collageBranch) {
        CollageBranch = collageBranch;
    }

    public String getCollagedepartment() {
        return Collagedepartment;
    }

    public void setCollagedepartment(String collagedepartment) {
        Collagedepartment = collagedepartment;
    }

    public String getCollageEmail() {
        return CollageEmail;
    }

    public void setCollageEmail(String collageEmail) {
        CollageEmail = collageEmail;
    }

    public String getCollageDesignation() {
        return CollageDesignation;
    }

    public void setCollageDesignation(String collageDesignation) {
        CollageDesignation = collageDesignation;
    }

    public String getCollageEnrollment() {
        return CollageEnrollment;
    }

    public void setCollageEnrollment(String collageEnrollment) {
        CollageEnrollment = collageEnrollment;
    }

    public String getCollageStudentNamev() {
        return CollageStudentNamev;
    }

    public void setCollageStudentNamev(String collageStudentNamev) {
        CollageStudentNamev = collageStudentNamev;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getField_group_image() {
        return field_group_image;
    }

    public void setField_group_image(String field_group_image) {
        this.field_group_image = field_group_image;
    }
}
