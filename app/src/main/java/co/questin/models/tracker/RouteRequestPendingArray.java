package co.questin.models.tracker;

import java.io.Serializable;

/**
 * Created by Dell on 23-10-2017.
 */

public class RouteRequestPendingArray implements Serializable {
    public String field_first_name;
    public String field_last_name;
    public String rid;
    public String id;
    public String etid;
    public String gid;
    public String state;
    public String picture;
    public String child_uid;
    public String child_field_first_name;
    public String child_field_last_name;
    public String child_picture;

    public String getChild_uid() {
        return child_uid;
    }

    public void setChild_uid(String child_uid) {
        this.child_uid = child_uid;
    }

    public String getChild_field_first_name() {
        return child_field_first_name;
    }

    public void setChild_field_first_name(String child_field_first_name) {
        this.child_field_first_name = child_field_first_name;
    }

    public String getChild_field_last_name() {
        return child_field_last_name;
    }

    public void setChild_field_last_name(String child_field_last_name) {
        this.child_field_last_name = child_field_last_name;
    }

    public String getChild_picture() {
        return child_picture;
    }

    public void setChild_picture(String child_picture) {
        this.child_picture = child_picture;
    }

    public String getField_first_name() {
        return field_first_name;
    }

    public void setField_first_name(String field_first_name) {
        this.field_first_name = field_first_name;
    }

    public String getField_last_name() {
        return field_last_name;
    }

    public void setField_last_name(String field_last_name) {
        this.field_last_name = field_last_name;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEtid() {
        return etid;
    }

    public void setEtid(String etid) {
        this.etid = etid;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
