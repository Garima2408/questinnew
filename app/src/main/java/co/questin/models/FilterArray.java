package co.questin.models;

import java.io.Serializable;

public class FilterArray implements Serializable {
  public String id ;
  public String label ;
  private boolean isSelected;

  public boolean isSelected() {
    return isSelected;
  }

  public void setSelected(boolean selected) {
    isSelected = selected;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }




}
