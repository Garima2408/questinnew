package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 08-02-2018.
 */

public class CalenderDatabaseArray implements Serializable {

    public String Eventtittle;
    public String EventStartDate;
    public String EventendDate;
    public String EventTimeStart;
    public String EventtimeEnd;

    public String getEventtittle() {
        return Eventtittle;
    }

    public void setEventtittle(String eventtittle) {
        Eventtittle = eventtittle;
    }

    public String getEventStartDate() {
        return EventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        EventStartDate = eventStartDate;
    }

    public String getEventendDate() {
        return EventendDate;
    }

    public void setEventendDate(String eventendDate) {
        EventendDate = eventendDate;
    }

    public String getEventTimeStart() {
        return EventTimeStart;
    }

    public void setEventTimeStart(String eventTimeStart) {
        EventTimeStart = eventTimeStart;
    }

    public String getEventtimeEnd() {
        return EventtimeEnd;
    }

    public void setEventtimeEnd(String eventtimeEnd) {
        EventtimeEnd = eventtimeEnd;
    }
}
