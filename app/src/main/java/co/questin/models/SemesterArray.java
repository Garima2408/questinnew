package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 11-01-2018.
 */

public class SemesterArray implements Serializable {
    public String id;
    public String label;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
