package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 03-08-2017.
 */

public class StudentListsArray  implements Serializable {
    public String uid;
    public String name;
    public String picture;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
