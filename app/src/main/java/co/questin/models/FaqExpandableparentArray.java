package co.questin.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 10-12-2017.
 */

public class FaqExpandableparentArray implements Serializable {
    public String parentTitle;

    public boolean isSelected = false;
    public List<FaqExpandableChildArray> childsTaskList = new ArrayList<FaqExpandableChildArray>();

}
