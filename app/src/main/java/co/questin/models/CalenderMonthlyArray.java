package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 27-10-2017.
 */

public class CalenderMonthlyArray implements Serializable {
   public String title;
    public String nid;
    public String type;
    public String body;
    public String field_class_time[];
    public String startdate;
    public String EventTimeStart;
    public String endDate;
    public String EventtimeEnd;

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEventTimeStart() {
        return EventTimeStart;
    }

    public void setEventTimeStart(String eventTimeStart) {
        EventTimeStart = eventTimeStart;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEventtimeEnd() {
        return EventtimeEnd;
    }

    public void setEventtimeEnd(String eventtimeEnd) {
        EventtimeEnd = eventtimeEnd;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String[] getField_class_time() {
        return field_class_time;
    }

    public void setField_class_time(String[] field_class_time) {
        this.field_class_time = field_class_time;
    }
}
