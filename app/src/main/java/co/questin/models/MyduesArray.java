package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 09-12-2017.
 */

public class MyduesArray implements Serializable {


    public String id;
    public String uid;
    public String enrollmentId;
    public String fee;
    public String late_fee;
    public String due_date;
    public String total_fee;
    public String transaction_id;
    public String transaction_status;
    public String transaction_date;
    public  String  Sports_fee;
    public  String  Education_Fee;
    public  String  Exam_fee;
    public  String  Hostel_Fee;
    public  String  Others;


    public String getSports_fee() {
        return Sports_fee;
    }

    public void setSports_fee(String sports_fee) {
        Sports_fee = sports_fee;
    }

    public String getEducation_Fee() {
        return Education_Fee;
    }

    public void setEducation_Fee(String education_Fee) {
        Education_Fee = education_Fee;
    }

    public String getExam_fee() {
        return Exam_fee;
    }

    public void setExam_fee(String exam_fee) {
        Exam_fee = exam_fee;
    }

    public String getHostel_Fee() {
        return Hostel_Fee;
    }

    public void setHostel_Fee(String hostel_Fee) {
        Hostel_Fee = hostel_Fee;
    }

    public String getOthers() {
        return Others;
    }

    public void setOthers(String others) {
        Others = others;
    }



    public String getTotal_fee() {
        return total_fee;
    }

    public void setTotal_fee(String total_fee) {
        this.total_fee = total_fee;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getTransaction_status() {
        return transaction_status;
    }

    public void setTransaction_status(String transaction_status) {
        this.transaction_status = transaction_status;
    }

    public String getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(String transaction_date) {
        this.transaction_date = transaction_date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEnrollmentId() {
        return enrollmentId;
    }

    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getLate_fee() {
        return late_fee;
    }

    public void setLate_fee(String late_fee) {
        this.late_fee = late_fee;
    }

    public String getDue_date() {
        return due_date;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }
}
