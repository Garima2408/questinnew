package co.questin.models.chat;

/**
 * Created by farheen on 23/10/17
 */

public class NewChatModel {

    private String id;
    private String name;
    private String dpUrl;

    public String getGroupIds() {
        return GroupIds;
    }

    public void setGroupIds(String groupIds) {
        GroupIds = groupIds;
    }

    public String GroupIds;

    public NewChatModel() {

    }

    public NewChatModel(String id, String name, String dpUrl) {
        this.id = id;
        this.name = name;
        this.dpUrl = dpUrl;
    }

    public String getId() {
        return id;
    }

    public NewChatModel setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public NewChatModel setName(String name) {
        this.name = name;
        return this;
    }

    public String getDpUrl() {
        return dpUrl;
    }

    public NewChatModel setDpUrl(String dpUrl) {
        this.dpUrl = dpUrl;
        return this;
    }
}
