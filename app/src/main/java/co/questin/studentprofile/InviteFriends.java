package co.questin.studentprofile;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.InviteFirendsAdapter;
import co.questin.adapters.SearchedInviteFirendsAdapter;
import co.questin.library.StateBean;
import co.questin.models.AllUser;
import co.questin.network.URLS;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;

import static co.questin.utils.BaseAppCompactActivity.ShowIndicator;
import static co.questin.utils.BaseAppCompactActivity.hideIndicator;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarinteger;
import static co.questin.utils.Utils.isStatusSuccess;

public class InviteFriends extends Fragment {
    static EditText searchEmail;
    String SearchedName;
    public static String MainRole,CollageId, CollageName,CollageRole,CollageBranch,CollageEnrollment,Collagedepartment,CollageStudentName;
    static RecyclerView rv_myfriends_INVITElist;
    static RecyclerView SearchedInviteList;
    public static ArrayList<AllUser> mFriendsInviteList;
    public ArrayList<AllUser> mFriendsInviteSearchList;
    private static InviteFirendsAdapter mMyfriendinviteadapter;
    private SearchedInviteFirendsAdapter searchedadapter;
    private RecyclerView.LayoutManager layoutManager, mLayoutManager;
    static Activity activity;
    Activity activityhide;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    static SwipeRefreshLayout swipeContainer;
    private static ProgressBar spinner;
    public InviteFriends() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_invite_friends, container, false);
        activity = (Activity) view.getContext();
        activityhide = (Activity) view.getContext();
        searchEmail = view.findViewById(R.id.searchEmail);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        mFriendsInviteList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext());
        mLayoutManager = new LinearLayoutManager(getContext());
        rv_myfriends_INVITElist = view.findViewById(R.id.rv_myfriends_INVITElist);
        rv_myfriends_INVITElist.setHasFixedSize(true);
        rv_myfriends_INVITElist.setLayoutManager(layoutManager);
        SearchedInviteList = view.findViewById(R.id.SearchedInviteList);
        SearchedInviteList.setHasFixedSize(true);
        SearchedInviteList.setLayoutManager(mLayoutManager);
        spinner= view.findViewById(R.id.progressBar);

        mMyfriendinviteadapter = new InviteFirendsAdapter(rv_myfriends_INVITElist,activity, mFriendsInviteList );
        rv_myfriends_INVITElist.setAdapter(mMyfriendinviteadapter);

        MyFriendsInviteListDisplay();


        searchEmail.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {

                    addnewfriends();


                    return true;
                }
                return false;
            }
        });




        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh

                        RefreshWorked();


                    }
                }, 1000);
            }
        });

        inItView();


        return view;
    }
  /*  public static void CalledFromInviteAdapter() {
        mFriendsInviteList.clear();
        searchEmail.setText("");
        myfriendsinvitelist = new MyFriendsInviteListDisplay();
        myfriendsinvitelist.execute();



    }*/


    private void addnewfriends() {
        rv_myfriends_INVITElist.setVisibility(View.GONE);
        searchEmail.setError(null);

        // Store values at the time of the login attempt.
        SearchedName = searchEmail.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(SearchedName)) {
            focusView = searchEmail;
            cancel = true;
            //showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();

        } else {
            /* ADD COMMENTS ON SUBJECTS*/
           SendInviteListDisplay();

           rv_myfriends_INVITElist.setVisibility(View.GONE);
            swipeContainer.setVisibility(View.GONE);
            activityhide.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        }
    }




    private void inItView(){


        swipeContainer.setRefreshing(true);

        rv_myfriends_INVITElist.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=10;
                    mMyfriendinviteadapter .addProgress();

                    MyFriendsInviteListDisplay();


                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }



    public static void RefreshWorked() {

        PAGE_SIZE=0;
        isLoading=false;
        swipeContainer.setRefreshing(true);
        MyFriendsInviteListDisplay();
        SearchedInviteList.setVisibility(View.GONE);
        searchEmail.setText("");


    }

    private void SendInviteListDisplay() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_SEARCHBYEMAIL+SearchedName+"&offset="+PAGE_SIZE+"&limit=50",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    mFriendsInviteSearchList  = new ArrayList<>();
                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {


                                        for (int i = 0; i < jsonArrayData.length(); i++) {


                                            AllUser FriendInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AllUser.class);

                                            JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                                            if (jsonObject.has("college")) {
                                                JSONObject CampusData = jsonObject.getJSONObject("college");

                                                if (CampusData != null && CampusData.length() > 0 ){
                                                    MainRole = CampusData.getString("role");

                                                    if (MainRole.matches("13")) {
                                                        CollageName = CampusData.getString("title");
                                                        CollageRole = CampusData.getString("field_i_am_a");
                                                        CollageBranch = CampusData.getString("field_branch");
                                                        CollageId = CampusData.getString("tnid");

                                                        FriendInfo.CollageId =CollageId;
                                                        FriendInfo.CollageName =CollageName;
                                                        FriendInfo.CollageRole =CollageRole;
                                                        FriendInfo.CollageBranch =CollageBranch;
                                                        FriendInfo.MainRole =MainRole;


                                                    } else if (MainRole.matches("14")) {

                                                        CollageId = CampusData.getString("tnid");

                                                        CollageName = CampusData.getString("title");
                                                        CollageRole = CampusData.getString("field_i_am_a");
                                                        Collagedepartment = CampusData.getString("field_department");


                                                        FriendInfo.CollageId =CollageId;
                                                        FriendInfo.CollageName =CollageName;
                                                        FriendInfo.CollageRole =CollageRole;
                                                        FriendInfo.Collagedepartment =Collagedepartment;
                                                        FriendInfo.MainRole =MainRole;


                                                    } else if (MainRole.matches("15")) {


                                                        CollageId = CampusData.getString("tnid");
                                                        CollageName = CampusData.getString("title");
                                                        CollageRole = CampusData.getString("field_i_am_a");
                                                        CollageBranch = CampusData.getString("field_branch");

                                                        FriendInfo.CollageId =CollageId;
                                                        FriendInfo.CollageName =CollageName;
                                                        FriendInfo.CollageRole =CollageRole;
                                                        FriendInfo.CollageBranch =CollageBranch;
                                                        FriendInfo.MainRole =MainRole;



                                                    } else if (MainRole.matches("16")) {

                                                        CollageId = CampusData.getString("tnid");
                                                        CollageName = CampusData.getString("title");
                                                        CollageRole = CampusData.getString("field_i_am_a");
                                                        CollageStudentName = CampusData.getString("field_student_name");
                                                        FriendInfo.CollageId =CollageId;
                                                        FriendInfo.CollageName =CollageName;
                                                        FriendInfo.CollageRole =CollageRole;
                                                        FriendInfo.CollageStudentName =CollageStudentName;
                                                        FriendInfo.MainRole =MainRole;


                                                    }
                                                    else if (MainRole.matches("3")) {


                                                        CollageName = CampusData.getString("title");
                                                        CollageRole = CampusData.getString("field_i_am_a");
                                                        Collagedepartment = CampusData.getString("field_department");
                                                        CollageId = CampusData.getString("tnid");

                                                        FriendInfo.CollageId =CollageId;
                                                        FriendInfo.CollageName =CollageName;
                                                        FriendInfo.CollageRole =CollageRole;
                                                        FriendInfo.Collagedepartment =Collagedepartment;
                                                        FriendInfo.MainRole =MainRole;


                                                    }
                                                }else {

                                                }
                                            }else {

                                            }



                                            mFriendsInviteSearchList.add(FriendInfo);
                                            searchedadapter = new SearchedInviteFirendsAdapter(activity, mFriendsInviteSearchList);
                                            SearchedInviteList.setAdapter(searchedadapter);
                                            SearchedInviteList.setVisibility(View.VISIBLE);
                                        }
                                    }else{
                                        showSnackbarinteger(R.string.nouser);
                                        }
                                        } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }


    private static void MyFriendsInviteListDisplay() {
        swipeContainer.setRefreshing(true);
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_ALLUSERS+"?offset="+PAGE_SIZE+"&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    rv_myfriends_INVITElist.setVisibility(View.VISIBLE);
                                    swipeContainer.setRefreshing(false);
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    ArrayList<AllUser> arrayList = new ArrayList<>();

                                    for (int i = 0; i < jsonArrayData.length(); i++) {


                                        AllUser FriendInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AllUser.class);

                                        JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                                        if (jsonObject.has("college")) {
                                            JSONObject CampusData = jsonObject.getJSONObject("college");

                                            if (CampusData != null && CampusData.length() > 0 ){
                                                String MainRole = CampusData.getString("role");

                                                if (MainRole.matches("13")) {
                                                    CollageName = CampusData.getString("title");
                                                    CollageRole = CampusData.getString("field_i_am_a");
                                                    CollageBranch = CampusData.getString("field_branch");
                                                    CollageId = CampusData.getString("tnid");

                                                    FriendInfo.CollageId =CollageId;
                                                    FriendInfo.CollageName =CollageName;
                                                    FriendInfo.CollageRole =CollageRole;
                                                    FriendInfo.CollageBranch =CollageBranch;
                                                    FriendInfo.MainRole =MainRole;


                                                } else if (MainRole.matches("14")) {


                                                    CollageName = CampusData.getString("title");
                                                    CollageRole = CampusData.getString("field_i_am_a");
                                                    Collagedepartment = CampusData.getString("field_department");
                                                    CollageId = CampusData.getString("tnid");

                                                    FriendInfo.CollageId =CollageId;

                                                    FriendInfo.CollageName =CollageName;
                                                    FriendInfo.CollageRole =CollageRole;
                                                    FriendInfo.Collagedepartment =Collagedepartment;
                                                    FriendInfo.MainRole =MainRole;


                                                } else if (MainRole.matches("15")) {


                                                    CollageName = CampusData.getString("title");
                                                    CollageRole = CampusData.getString("field_i_am_a");
                                                    CollageBranch = CampusData.getString("field_branch");
                                                    CollageId = CampusData.getString("tnid");

                                                    FriendInfo.CollageId =CollageId;
                                                    FriendInfo.CollageName =CollageName;
                                                    FriendInfo.CollageRole =CollageRole;
                                                    FriendInfo.CollageBranch =CollageBranch;
                                                    FriendInfo.MainRole =MainRole;



                                                } else if (MainRole.matches("16")) {

                                                    CollageName = CampusData.getString("title");
                                                    CollageRole = CampusData.getString("field_i_am_a");
                                                    CollageStudentName = CampusData.getString("field_student_name");
                                                    CollageId = CampusData.getString("tnid");

                                                    FriendInfo.CollageId =CollageId;
                                                    FriendInfo.CollageName =CollageName;
                                                    FriendInfo.CollageRole =CollageRole;
                                                    FriendInfo.CollageStudentName =CollageStudentName;
                                                    FriendInfo.MainRole =MainRole;


                                                } else if (MainRole.matches("3")) {


                                                    CollageName = CampusData.getString("title");
                                                    CollageRole = CampusData.getString("field_i_am_a");
                                                    Collagedepartment = CampusData.getString("field_department");
                                                    CollageId = CampusData.getString("tnid");

                                                    FriendInfo.CollageId =CollageId;
                                                    FriendInfo.CollageName =CollageName;
                                                    FriendInfo.CollageRole =CollageRole;
                                                    FriendInfo.Collagedepartment =Collagedepartment;
                                                    FriendInfo.MainRole =MainRole;
                                                    }

                                                    }else {
                                                }

                                                }else {
                                            }

                                        arrayList.add(FriendInfo);


                                    }

                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {


                                            rv_myfriends_INVITElist.setVisibility(View.VISIBLE);
                                            mMyfriendinviteadapter.setInitialData(arrayList);

                                        } else {
                                            isLoading = false;
                                            mMyfriendinviteadapter.removeProgress();
                                            swipeContainer.setRefreshing(false);

                                            mMyfriendinviteadapter.addData(arrayList);


                                        }
                                    }else{
                                        mMyfriendinviteadapter.removeProgress();
                                    }

                                } else {
                                    swipeContainer.setRefreshing(false);
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                swipeContainer.setRefreshing(false);
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            swipeContainer.setRefreshing(false);
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipeContainer.setRefreshing(false);
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }



    @Override
    public void onDestroy() {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        isLoading=false;
        PAGE_SIZE=0;
        super.onDestroy();
    }
}
