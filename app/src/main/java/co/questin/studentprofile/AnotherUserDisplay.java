package co.questin.studentprofile;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.AnoUserProfileCampusFeedsAdapters;
import co.questin.chat.ChatActivity;
import co.questin.chat.MyServerChatActivity;
import co.questin.database.QuestinSQLiteHelper;
import co.questin.library.StateBean;
import co.questin.models.CampusFeedArray;
import co.questin.models.chat.ConversationModel;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

import static co.questin.utils.Utils.isStatusSuccess;

public class AnotherUserDisplay extends BaseAppCompactActivity implements View.OnClickListener {
    ImageView mainheader,imageProfile,backone;
    LinearLayout thredHeader,FriendDisplayLayout;
    static String DisplayUserUid,DisplayClgId,college_email,rid,DetailClgId,DetailUserId;
    TextView tv_username,tv_useremail,PostNumber,friendsNumber,SendRequest,SendMessage,tv_userbatch,tv_usercollagename,ErrorText;
    String  username,picture,is_friendMain,is_friendpending,email,password,urlsofphoto,CollagePostcount,urlsofWallpic, MainRole,CollageId,CollageName,CollageImage,lati,longi,CollageBatch,CollageBranch,Collagedepartment,CollageRole,CollageLogo,CollageEmail,CollageDesignation,CollageEnrollment,CollageStudentName;
    private QuestinSQLiteHelper questinSQLiteHelper;
    static RecyclerView rv_mycampus_news;
    public static ArrayList<CampusFeedArray> mcampusfeedList;
    private static AnoUserProfileCampusFeedsAdapters campusfeedadapter;
    private RecyclerView.LayoutManager layoutManager;
    static Activity activity;
    static String tittle,URLDoc;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    private static ProgressBar spinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another_user_display);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        questinSQLiteHelper = new QuestinSQLiteHelper(this);
        Bundle b = getIntent().getExtras();
        DisplayUserUid = b.getString("USERPROFILE_ID");
        DisplayClgId = b.getString("COLLAGE_ID");
        NestedScrollView nsv =  findViewById(R.id.nestedScroll);
        mainheader = findViewById(R.id.mainheader);
        imageProfile = findViewById(R.id.imageProfile);
        tv_usercollagename = findViewById(R.id.tv_usercollagename);
        tv_username = findViewById(R.id.tv_username);
        tv_useremail = findViewById(R.id.tv_useremail);
        PostNumber = findViewById(R.id.PostNumber);
        friendsNumber = findViewById(R.id.friendsNumber);
        SendRequest = findViewById(R.id.SendRequest);
        SendMessage = findViewById(R.id.SendMessage);
        tv_userbatch = findViewById(R.id.tv_userbatch);
        thredHeader = findViewById(R.id.thredHeader);
        backone = findViewById(R.id.backone);
        spinner = findViewById(R.id.progressBar);
        ErrorText =findViewById(R.id.ErrorText);
        FriendDisplayLayout = findViewById(R.id.FriendDisplayLayout);
        mcampusfeedList = new ArrayList<>();
        activity = getActivity();
        layoutManager = new LinearLayoutManager(this);
        rv_mycampus_news = findViewById(R.id.rv_mycampus_news);
        rv_mycampus_news.setHasFixedSize(true);
        rv_mycampus_news.setLayoutManager(layoutManager);



        campusfeedadapter = new AnoUserProfileCampusFeedsAdapters(rv_mycampus_news, mcampusfeedList, activity);
        rv_mycampus_news.setAdapter(campusfeedadapter);
        rv_mycampus_news.setNestedScrollingEnabled(false);


        SendMessage.setOnClickListener(this);
        SendRequest.setOnClickListener(this);
        backone.setOnClickListener(this);
        FriendDisplayLayout.setOnClickListener(this);

       ProgressUserDisplayProfile(DisplayClgId,DisplayUserUid);


    }






    public static void RefreshWorkedFromMycampusAdapter() {
        PAGE_SIZE=0;
        isLoading=false;
        spinner.setVisibility(View.VISIBLE);
        MyCollageNewFeedsDisplay(DisplayUserUid);


    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.SendRequest:
                hideKeyBoard(v);
                if (is_friendpending!=null){
                    if(is_friendpending.matches("1")){

                    }else {
                        SendFriendRequest(DisplayUserUid);

                    }
                }



                break;

            case R.id.backone:
                PAGE_SIZE=0;
                isLoading=false;
                ActivityCompat.finishAfterTransition(this);


                break;

            case R.id.SendMessage:
                hideKeyBoard(v);
                try {
                    if(is_friendMain.matches("1")){
                        ConversationModel conversationModel = new ConversationModel()
                                .setSenderId(DisplayUserUid)
                                .setName(username)
                                .setDpUrl(picture);

                        Intent backIntent = new Intent(AnotherUserDisplay.this, ChatActivity.class)
                                .putExtra(Constants.EXTRA_IS_FIRST_MESSAGE, !questinSQLiteHelper.doesConversationExist(conversationModel.getSenderId()))
                                .putExtra(Constants.CONVERSATION_MODEL, conversationModel);
                        startActivity(backIntent);


                    }else if(is_friendMain.matches("")){

                        ConversationModel conversationModel = new ConversationModel()
                                .setSenderId(DisplayUserUid)
                                .setName(username)
                                .setDpUrl(picture);

                        Intent backIntent = new Intent(AnotherUserDisplay.this, MyServerChatActivity.class)
                                .putExtra(Constants.EXTRA_IS_FIRST_MESSAGE, !questinSQLiteHelper.doesConversationExist(conversationModel.getSenderId()))
                                .putExtra(Constants.CONVERSATION_MODEL, conversationModel)

                                .putExtra("USER_ID", DisplayUserUid)
                                .putExtra("USER_NAME",username)
                                .putExtra("PICTURE", picture);
                        startActivity(backIntent);



                    }

                }catch (Exception e){

                }





                break;



            case R.id.FriendDisplayLayout:
                hideKeyBoard(v);

                Intent backIntent = new Intent(AnotherUserDisplay.this, UsersFriendsDisplay.class)
                        .putExtra("EMAIL",college_email);

                startActivity(backIntent);
                break;
        }
    }


       /*
    SEND FRIEND REQUEST*/

        private void SendFriendRequest( String displayUserUid) {

            ShowIndicator();

            StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_SENDINFITE + "/" + DisplayUserUid + "/" + "relationship/friends",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i("VOLLEY", response);
                            try {
                                Gson gson = new GsonBuilder().create();
                                JsonParser jsonParser = new JsonParser();
                                JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                                StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                                if (stateResponse != null) {
                                    hideIndicator();
                                    if (Utils.isStatusSuccess(stateResponse.getStatus())) {



                                        finish();




                                    }else {
                                        hideIndicator();
                                        showSnackbarMessage(stateResponse.getMessage());


                                    }
                                } else {
                                    hideIndicator();
                                    showServerSnackbar(R.string.error_responce);


                                }
                            } catch (Exception e) {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideIndicator();
                            if (error instanceof NoConnectionError) {
                                showConnectionSnackbar();
                            } else {
                                showServerSnackbar(R.string.error_responce);
                            }
                        }
                    }) {



                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                    params.put("Accept-Language", "application/json");
                    params.put("X-CSRF-Token", SessionManager.getInstance(AnotherUserDisplay.this).getaccesstoken().accesstoken);
                    params.put("Cookie", SessionManager.getInstance(AnotherUserDisplay.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(AnotherUserDisplay.this).getaccesstoken().sessionID);

                    return params;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("entity_type","user");
                    params.put("group_type","node");
                    params.put("membership type","og_membership_type_college");
                    params.put("field_i_am_a","student");

                    return params;
                }


            };

            mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            //creating a request queue
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(mStrRequest);




        }



    public void DeleteFriendRequest(){

        ShowIndicator();

        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_REMOVE_MYFRIEND,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {

                                    startActivity(getIntent());
                                    finish();




                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(AnotherUserDisplay.this).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(AnotherUserDisplay.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(AnotherUserDisplay.this).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("rid", rid);
                params.put("reason", "delete this friend" );
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);



    }


    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        ActivityCompat.finishAfterTransition(this);
    }


    private void ProgressUserDisplayProfile(String displayClgId, String displayUserUid) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_USERPROFILEDISPLAY+"?"+"cid="+displayClgId+"&"+"uid="+displayUserUid,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                                        for (int i = 0; i < jsonArrayData.length(); i++) {

                                            username = jsonArrayData.getJSONObject(i).getString("first_name") + " " + jsonArrayData.getJSONObject(i).getString("last_name");
                                            String wall_picture = jsonArrayData.getJSONObject(i).getString("wall_picture");
                                            picture = jsonArrayData.getJSONObject(i).getString("picture");
                                            String type = jsonArrayData.getJSONObject(i).getString("type");
                                            // String course = jsonArrayData.getJSONObject(i).getString("course");
                                            String batch = jsonArrayData.getJSONObject(i).getString("batch");
                                            String branch = jsonArrayData.getJSONObject(i).getString("branch");
                                            String enrollment_number = jsonArrayData.getJSONObject(i).getString("enrollment_number");
                                            college_email = jsonArrayData.getJSONObject(i).getString("college_email");
                                            String department = jsonArrayData.getJSONObject(i).getString("department");
                                            String faculty_designation = jsonArrayData.getJSONObject(i).getString("faculty_designation");
                                            String parents_of = jsonArrayData.getJSONObject(i).getString("parents_of");
                                            is_friendMain = jsonArrayData.getJSONObject(i).getString("is_friend");
                                            String friends_count = jsonArrayData.getJSONObject(i).getString("friends_count");
                                            String post_count = jsonArrayData.getJSONObject(i).getString("post_count");
                                            String uid = jsonArrayData.getJSONObject(i).getString("uid");
                                            String college_title = jsonArrayData.getJSONObject(i).getString("college_title");
                                            String college_id = jsonArrayData.getJSONObject(i).getString("group_id");
                                            is_friendpending = jsonArrayData.getJSONObject(i).getString("is_friend_request_pending");
                                            rid = jsonArrayData.getJSONObject(i).getString("rid");

                                            tv_username.setText(username);
                                            tv_username.setSelected(true);
                                            tv_useremail.setText(type +" "+batch);
                                            tv_userbatch.setText(branch);
                                            tv_usercollagename.setText(college_title);



                                            if(post_count.matches("")){
                                                PostNumber.setText("0");

                                            }else {
                                                PostNumber.setText(post_count);

                                            }
                                            if(friends_count.matches("")){
                                                friendsNumber.setText("0");

                                            }else {
                                                friendsNumber.setText(friends_count);

                                            }

                                            if(college_id.matches(SessionManager.getInstance(getActivity()).getCollage().getTnid())){

                                                MyCollageNewFeedsDisplay(DisplayUserUid);
                                                inItView();


                                            }



                                            if(is_friendpending.matches("1")){
                                                SendRequest.setText("Request Sent");
                                                SendRequest.setCompoundDrawablesWithIntrinsicBounds( R.mipmap.ic_user_requestsend, 0, 0, 0);

                                                rv_mycampus_news.setVisibility(View.GONE);
                                                spinner.setVisibility(View.GONE);
                                                thredHeader.setVisibility(View.VISIBLE);


                                            }else
                                            {
                                                SendRequest.setText("Add Friend");
                                                rv_mycampus_news.setVisibility(View.GONE);
                                                spinner.setVisibility(View.GONE);

                                                thredHeader.setVisibility(View.VISIBLE);
                                            }

                                            if(is_friendMain.matches("1")){
                                                SendRequest.setText("Friend");
                                                SendRequest.setCompoundDrawablesWithIntrinsicBounds( R.mipmap.ic_user_checked, 0, 0, 0);

                                                thredHeader.setVisibility(View.VISIBLE);
                                                SendRequest.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        showAlertFragmentDialog(activity,"Unfriend","Remove this Friend");



                                                    }
                                                });



                                            }else if (is_friendMain.matches("")){

                                                if(uid.equals(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id())){
                                                    thredHeader.setVisibility(View.GONE);
                                                    MyCollageNewFeedsDisplay(DisplayUserUid);
                                                    inItView();


                                                }else if(uid.equals(SessionManager.getInstance(getActivity()).getUser().getParentUid())){

                                                    thredHeader.setVisibility(View.GONE);
                                                    MyCollageNewFeedsDisplay(DisplayUserUid);
                                                    inItView();

                                                }else {

                                                }

                                            }


                                            try{
                                                if(picture != null && picture.length() > 0 ) {

                                                    Glide.with(AnotherUserDisplay.this).load(picture)
                                                            .placeholder(R.mipmap.place_holder).dontAnimate()
                                                            .fitCenter().into(imageProfile);

                                                }else {
                                                    imageProfile.setImageResource(R.mipmap.place_holder);

                                                }

                                                if(wall_picture != null && wall_picture.length() > 0 ) {

                                                    Glide.with(AnotherUserDisplay.this).load(wall_picture)
                                                            .placeholder(R.mipmap.header_main).dontAnimate()
                                                            .fitCenter().into(mainheader);

                                                }else {
                                                    mainheader.setImageResource(R.mipmap.header_main);

                                                }


                                            }catch (Exception e){

                                            }




                                        }
                                    }else {
                                       /* showSnackbarMessage("User is not member of any college");*/
                                       ShowNotMemberRequest();
                                    }


                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());
                                    finish();



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(AnotherUserDisplay.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(AnotherUserDisplay.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(AnotherUserDisplay.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }




    private void inItView(){

        rv_mycampus_news.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager)layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){

                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;
                    campusfeedadapter.addProgress();
                    MyCollageNewFeedsDisplay(DisplayUserUid);

                    //fetchData(fYear,,);
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    //College Feed Display

    private static void MyCollageNewFeedsDisplay(String displayUserUid) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_MYCAMPUSFEED+"?"+"cid="+ SessionManager.getInstance(activity).getCollage().getTnid()+"&uid="+displayUserUid+"&offset="+PAGE_SIZE+"&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    rv_mycampus_news.setVisibility(View.VISIBLE);
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    ArrayList<CampusFeedArray> arrayList = new ArrayList<>();
                                    for (int i = 0; i < jsonArrayData.length(); i++) {


                                        CampusFeedArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CampusFeedArray.class);
                                        arrayList.add(CourseInfo);

                                        JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                                        if (jsonObject.has("field_comment_attachments")){

                                            JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");
                                            if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0 ){
                                                URLDoc = AttacmentsFeilds.getString("value");
                                                tittle = AttacmentsFeilds.getString("title");
                                                Log.d("TAG", "titleurl: " + tittle +URLDoc);
                                                CourseInfo.title =tittle;
                                                CourseInfo.url =URLDoc;


                                            }
                                        }


                                    }

                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {

                                            rv_mycampus_news.setVisibility(View.VISIBLE);
                                            campusfeedadapter.setInitialData(arrayList);

                                        } else {
                                            isLoading = false;
                                            campusfeedadapter.removeProgress();
                                            spinner.setVisibility(View.GONE);
                                            campusfeedadapter.addData(arrayList);


                                        }
                                    }else{

                                        if (PAGE_SIZE==0) {
                                            spinner.setVisibility(View.GONE);
                                            rv_mycampus_news.setVisibility(View.GONE);

                                        }else
                                            campusfeedadapter.removeProgress();

                                    }

                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }



    private void ShowNotMemberRequest() {

        AlertDialog.Builder builder  = new AlertDialog.Builder(activity);
        builder.setTitle("User is not member of any collage");
        builder.setMessage("Do you want to add friend ?").setCancelable(true);
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

                SendFriendRequest(DisplayUserUid);



            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int arg1) {

                // finish used for destroyed DialogInterface
                dialogInterface.dismiss();
                finish();

            }
        });


        builder.setCancelable(false);

        builder.show();




    }




    public void showAlertFragmentDialog(Context context, String title, String message) {
        AlertDialog.Builder builder  = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message).setCancelable(false);
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {


                DeleteFriendRequest();
                // CallTheMainListAgain();


            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int arg1) {

                // finish used for destroyed DialogInterface
                dialogInterface.dismiss();

            }
        });


        builder.setCancelable(true);

        builder.show();



    }



}

