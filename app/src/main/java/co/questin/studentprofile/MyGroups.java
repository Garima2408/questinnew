package co.questin.studentprofile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.MyGroupAdapters;
import co.questin.clubandsociety.GroupNSocieties;
import co.questin.library.StateBean;
import co.questin.models.MyGroupItem;

import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class MyGroups extends BaseAppCompactActivity {
    public  ArrayList<MyGroupItem> mgroups;
    private RecyclerView.LayoutManager layoutManager;
    private MyGroupAdapters groupadapter;
    RecyclerView mRecyclerView;
    TextView text_view,text_view2;
    RelativeLayout relative;
    FloatingActionMenu materialDesignFAM;
    com.github.clans.fab.FloatingActionButton floatingActionButton1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.questin.R.layout.activity_my_groups);

        mgroups=new ArrayList<>();

        mRecyclerView = findViewById(co.questin.R.id.rv_myGroups);
        mRecyclerView.setHasFixedSize(true);
        groupadapter=new MyGroupAdapters(this,mgroups);

        text_view = findViewById(R.id.text_view);
        text_view2 = findViewById(R.id.text_view2);
        materialDesignFAM = findViewById(R.id.material_design_android_floating_action_menu);
        relative = findViewById(R.id.relative);
        floatingActionButton1 = findViewById(R.id.material_design_floating_action_menu_item1);

        layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(groupadapter);
        
        MyGroupsListDisplay();
        

        ArrayList<MyGroupItem> mgroups = null;

        relative.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }
                return false;
            }
        });


        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MyGroups.this, GroupNSocieties.class);
                startActivity(intent);


            }
        });



    }

    private void MyGroupsListDisplay() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_MYGROUP+"?"+"uid="+ SessionManager.getInstance(getActivity()).getUser().userprofile_id+"&"+"cid="+ SessionManager.getInstance(getActivity()).getCollage().getTnid(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");


                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                                        for (int i = 0; i < jsonArrayData.length(); i++) {

                                            MyGroupItem GoalInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), MyGroupItem.class);
                                            mgroups.add(GoalInfo);
                                            groupadapter = new MyGroupAdapters(MyGroups.this, mgroups);
                                            mRecyclerView.setAdapter(groupadapter);
                                            text_view.setVisibility(View.GONE);
                                            text_view2.setVisibility(View.GONE);

                                        }

                                    }else{


                                        text_view.setVisibility(View.VISIBLE);
                                        text_view2.setVisibility(View.VISIBLE);
                                        materialDesignFAM.setVisibility(View.VISIBLE);

                                    }


                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(MyGroups.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(MyGroups.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(MyGroups.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }



    @Override
    public void onBackPressed() {
        finish();
    }
}









