package co.questin.studentprofile;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.PendindRequestAdapter;
import co.questin.library.StateBean;
import co.questin.models.RequestPendingArray;
import co.questin.network.URLS;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;

import static co.questin.utils.BaseAppCompactActivity.ShowIndicator;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;
import static co.questin.utils.Utils.isStatusSuccess;

public class PendingRequests extends Fragment {
    static RecyclerView PendingFriends;
    static TextView ErrorText;
    public static ArrayList<RequestPendingArray> mRequestpendingList;
    private static PendindRequestAdapter requestadapter;
    private RecyclerView.LayoutManager layoutManager;
    static Activity activity;
    static boolean isLoading= false;
    static int PAGE_SIZE = 0;
    private int currentPage = 1;
    static SwipeRefreshLayout swipeContainer;
    private static ProgressBar spinner;
    public PendingRequests() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_pending_requests, container, false);
        activity = (Activity) view.getContext();

        mRequestpendingList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext());
        PendingFriends = view.findViewById(R.id.PendingFriends);
        PendingFriends.setHasFixedSize(true);
        PendingFriends.setLayoutManager(layoutManager);
        spinner= view.findViewById(R.id.progressBar);
        swipeContainer=view.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        ErrorText =view.findViewById(R.id.ErrorText);
        requestadapter = new PendindRequestAdapter(PendingFriends, mRequestpendingList,activity);
        PendingFriends.setAdapter(requestadapter);

       FriendRequestPendingListDisplay();
      



        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        CalledFromPendingAdapter();


                    }
                }, 3000);
            }
        });

        inItView();
        return view;
    }

    private void inItView() {

        swipeContainer.setRefreshing(true);

        PendingFriends.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){

                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    requestadapter.addProgress();


                     FriendRequestPendingListDisplay();
                 

                }else{
                    Log.i("loadinghua", "im else now");

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });


    }


    public static void CalledFromPendingAdapter() {
        PAGE_SIZE=0;
        isLoading=false;
        swipeContainer.setRefreshing(true);
        mRequestpendingList.clear();
        FriendRequestPendingListDisplay();
       

        }

    private static void FriendRequestPendingListDisplay() {
        ShowIndicator();
        swipeContainer.setRefreshing(true);

        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_GETPENDINGREQUEST+"?offset="+PAGE_SIZE+"&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    PendingFriends.setVisibility(View.VISIBLE);
                                    swipeContainer.setRefreshing(false);
                                    mRequestpendingList.clear();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    ArrayList<RequestPendingArray> arrayList = new ArrayList<>();
                                    arrayList.clear();
                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {

                                        for (int i = 0; i < jsonArrayData.length(); i++) {

                                            RequestPendingArray Request = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), RequestPendingArray.class);
                                            arrayList.add(Request);

                                            }
                                            }

                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {
                                            PendingFriends.setVisibility(View.VISIBLE);
                                            requestadapter.setInitialData(arrayList);

                                        } else {
                                            isLoading = false;
                                            requestadapter.removeProgress();
                                            swipeContainer.setRefreshing(false);
                                            requestadapter.addData(arrayList);
                                            ErrorText.setVisibility(View.GONE);

                                        }
                                    }else{
                                        if (PAGE_SIZE==0){
                                            PendingFriends.setVisibility(View.GONE);
                                            ErrorText.setVisibility(View.VISIBLE);


                                        }else
                                            requestadapter.removeProgress();
                                        }

                                        } else {
                                    swipeContainer.setRefreshing(false);
                                    showSnackbarMessage(stateResponse.getMessage());
                                    }
                            } else {
                                swipeContainer.setRefreshing(false);
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            swipeContainer.setRefreshing(false);
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipeContainer.setRefreshing(false);
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }




    @Override
    public void onDestroy() {
        isLoading=false;
        PAGE_SIZE=0;
        super.onDestroy();
    }

}
