package co.questin.studentprofile;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.MyFriendsAdapter;
import co.questin.library.StateBean;
import co.questin.models.AllFriends;
import co.questin.network.URLS;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;

import static co.questin.utils.BaseAppCompactActivity.hideIndicator;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;
import static co.questin.utils.Utils.isStatusSuccess;

public class MyFriends extends Fragment {
    RecyclerView rv_myfriendslist;
    public ArrayList<AllFriends> mFriendsList;
    public String MainRole,CollageId,  CollageName,CollageRole,CollageBranch,CollageEnrollment,Collagedepartment,CollageStudentName;
    private  boolean isLoading= false;
    private  int PAGE_SIZE = 0;
    private int currentPage = 1;
    SwipeRefreshLayout swipeContainer;
    private MyFriendsAdapter mMyfriendadapter;
    private RecyclerView.LayoutManager layoutManager;
    static Activity activity;
    private ProgressBar spinner;
    static TextView ErrorText;
    private static ShimmerFrameLayout mShimmerViewContainer;


    public MyFriends() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_my_friends, container, false);
        activity = (Activity) view.getContext();
        mFriendsList = new ArrayList<>();


        layoutManager = new LinearLayoutManager(getContext());
        rv_myfriendslist = view.findViewById(R.id.rv_myfriendslist);
        spinner= view.findViewById(R.id.progressBar);
        rv_myfriendslist.setHasFixedSize(true);
        swipeContainer =view.findViewById(R.id.swipeContainer);
        rv_myfriendslist.setLayoutManager(layoutManager);
        ErrorText =view.findViewById(R.id.ErrorText);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();


        mMyfriendadapter = new MyFriendsAdapter(rv_myfriendslist,mFriendsList,activity);
        rv_myfriendslist.setAdapter(mMyfriendadapter);

        MyFriendsListDisplay();


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        PAGE_SIZE=0;
                        isLoading=false;
                        swipeContainer.setRefreshing(true);
                        MyFriendsListDisplay();


                    }
                }, 3000);
            }
        });

        inItView();
        return view;
    }



    private void inItView() {
        swipeContainer.setRefreshing(false);

        rv_myfriendslist.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){

                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    mMyfriendadapter.addProgress();
                    MyFriendsListDisplay();

                }else{
                    Log.i("loadinghua", "im else now");

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");

        super.onPause();
    }


    private void MyFriendsListDisplay() {
        swipeContainer.setRefreshing(true);
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_MYFRIENDS+"?"+"email="+ SessionManager.getInstance(activity).getUser().getEmail()+"&offset="+PAGE_SIZE+"&limit=150",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    rv_myfriendslist.setVisibility(View.VISIBLE);
                                    swipeContainer.setRefreshing(false);

                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");

                                    ArrayList<AllFriends> arrayList = new ArrayList<>();

                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {

                                        for (int i = 0; i < jsonArrayData.length(); i++) {

                                            AllFriends FriendInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AllFriends.class);

                                            JSONObject jsonObject = jsonArrayData.getJSONObject(i);

                                            if (jsonObject.has("college")) {
                                                JSONObject CampusData = jsonObject.getJSONObject("college");

                                                if (CampusData != null && CampusData.length() > 0 ){
                                                    String MainRole = CampusData.getString("role");

                                                    if (MainRole.matches("13")) {
                                                        CollageName = CampusData.getString("title");
                                                        CollageRole = CampusData.getString("field_i_am_a");
                                                        CollageBranch = CampusData.getString("field_branch");
                                                        CollageId = CampusData.getString("tnid");

                                                        FriendInfo.CollageId =CollageId;
                                                        FriendInfo.CollageName =CollageName;
                                                        FriendInfo.CollageRole =CollageRole;
                                                        FriendInfo.CollageBranch =CollageBranch;
                                                        FriendInfo.MainRole =MainRole;


                                                    } else if (MainRole.matches("14")) {
                                                        CollageName = CampusData.getString("title");
                                                        CollageRole = CampusData.getString("field_i_am_a");
                                                        Collagedepartment = CampusData.getString("field_department");
                                                        CollageId = CampusData.getString("tnid");

                                                        FriendInfo.CollageId =CollageId;
                                                        FriendInfo.CollageName =CollageName;
                                                        FriendInfo.CollageRole =CollageRole;
                                                        FriendInfo.Collagedepartment =Collagedepartment;
                                                        FriendInfo.MainRole =MainRole;


                                                    } else if (MainRole.matches("15")) {
                                                        CollageName = CampusData.getString("title");
                                                        CollageRole = CampusData.getString("field_i_am_a");
                                                        CollageBranch = CampusData.getString("field_branch");
                                                        CollageId = CampusData.getString("tnid");

                                                        FriendInfo.CollageId =CollageId;
                                                        FriendInfo.CollageName =CollageName;
                                                        FriendInfo.CollageRole =CollageRole;
                                                        FriendInfo.CollageBranch =CollageBranch;
                                                        FriendInfo.MainRole =MainRole;



                                                    } else if (MainRole.matches("16")) {
                                                        CollageName = CampusData.getString("title");
                                                        CollageRole = CampusData.getString("field_i_am_a");
                                                        CollageStudentName = CampusData.getString("field_student_name");
                                                        CollageId = CampusData.getString("tnid");

                                                        FriendInfo.CollageId =CollageId;
                                                        FriendInfo.CollageName =CollageName;
                                                        FriendInfo.CollageRole =CollageRole;
                                                        FriendInfo.CollageStudentName =CollageStudentName;
                                                        FriendInfo.MainRole =MainRole;


                                                    }else if (MainRole.matches("3")) {
                                                        CollageName = CampusData.getString("title");
                                                        CollageRole = CampusData.getString("field_i_am_a");
                                                        Collagedepartment = CampusData.getString("field_department");
                                                        CollageId = CampusData.getString("tnid");

                                                        FriendInfo.CollageId =CollageId;
                                                        FriendInfo.CollageName =CollageName;
                                                        FriendInfo.CollageRole =CollageRole;
                                                        FriendInfo.Collagedepartment =Collagedepartment;
                                                        FriendInfo.MainRole =MainRole;
                                                    }
                                                }else { }
                                            }else { }
                                            arrayList.add(FriendInfo); }
                                    }
                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {
                                            rv_myfriendslist.setVisibility(View.VISIBLE);
                                            mMyfriendadapter.setInitialData(arrayList);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);


                                        } else {
                                            isLoading = false;
                                            mMyfriendadapter.removeProgress();
                                            swipeContainer.setRefreshing(false);
                                            mMyfriendadapter.addData(arrayList);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);
                                        }
                                    }else{

                                        if (PAGE_SIZE==0){
                                            ErrorText.setVisibility(View.VISIBLE);
                                            ErrorText.setText("you have no friends");
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);

                                        }else
                                            mMyfriendadapter.removeProgress();

                                    }


                                } else {
                                    swipeContainer.setRefreshing(false);
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                swipeContainer.setRefreshing(false);
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            swipeContainer.setRefreshing(false);
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipeContainer.setRefreshing(false);
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }


    @Override
    public void onDestroy() {
        isLoading=false;
        PAGE_SIZE=0;
        super.onDestroy();
    }
}
