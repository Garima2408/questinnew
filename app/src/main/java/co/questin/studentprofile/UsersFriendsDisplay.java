package co.questin.studentprofile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.UsesFriendsDisplayAdapter;
import co.questin.library.StateBean;
import co.questin.models.AllFriends;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class UsersFriendsDisplay extends BaseAppCompactActivity {
    RecyclerView rv_myfriendslist;
    public ArrayList<AllFriends> mFriendsList;
    private UsesFriendsDisplayAdapter mMyfriendadapter;
    private RecyclerView.LayoutManager layoutManager;
    private ProgressBar spinner;
    public static   String CollageMail;
    public String MainRole,CollageId,  CollageName,CollageRole,CollageBranch,CollageEnrollment,Collagedepartment,CollageStudentName;
    TextView ErrorText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_friends_display);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Intent intent = getIntent();
        CollageMail = intent.getStringExtra("EMAIL");


        mFriendsList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        rv_myfriendslist =findViewById(R.id.rv_myfriendslist);
        spinner= findViewById(R.id.progressBar);
        ErrorText =findViewById(R.id.ErrorText);
        rv_myfriendslist.setHasFixedSize(true);
        rv_myfriendslist.setLayoutManager(layoutManager);
        MyFriendsListDisplay();



    }

    private void MyFriendsListDisplay() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_MYFRIENDS+"?"+"email="+CollageMail+"&offset="+0+"&limit=200",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                                        for (int i = 0; i < jsonArrayData.length(); i++) {

                                            AllFriends FriendInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AllFriends.class);
                                            JSONObject jsonObject = jsonArrayData.getJSONObject(i);

                                            if (jsonObject.has("college")) {
                                                JSONObject CampusData = jsonObject.getJSONObject("college");

                                                if (CampusData != null && CampusData.length() > 0 ) {
                                                    String MainRole = CampusData.getString("role");

                                                    if (MainRole.matches("13")) {
                                                        CollageName = CampusData.getString("title");
                                                        CollageRole = CampusData.getString("field_i_am_a");
                                                        CollageBranch = CampusData.getString("field_branch");
                                                        CollageId = CampusData.getString("tnid");

                                                        FriendInfo.CollageId = CollageId;
                                                        FriendInfo.CollageName = CollageName;
                                                        FriendInfo.CollageRole = CollageRole;
                                                        FriendInfo.CollageBranch = CollageBranch;
                                                        FriendInfo.MainRole = MainRole;


                                                    } else if (MainRole.matches("14")) {


                                                        CollageName = CampusData.getString("title");
                                                        CollageRole = CampusData.getString("field_i_am_a");
                                                        Collagedepartment = CampusData.getString("field_department");
                                                        CollageId = CampusData.getString("tnid");

                                                        FriendInfo.CollageId = CollageId;
                                                        FriendInfo.CollageName = CollageName;
                                                        FriendInfo.CollageRole = CollageRole;
                                                        FriendInfo.Collagedepartment = Collagedepartment;
                                                        FriendInfo.MainRole = MainRole;


                                                    } else if (MainRole.matches("15")) {


                                                        CollageName = CampusData.getString("title");
                                                        CollageRole = CampusData.getString("field_i_am_a");
                                                        CollageBranch = CampusData.getString("field_branch");
                                                        CollageId = CampusData.getString("tnid");

                                                        FriendInfo.CollageId = CollageId;
                                                        FriendInfo.CollageName = CollageName;
                                                        FriendInfo.CollageRole = CollageRole;
                                                        FriendInfo.CollageBranch = CollageBranch;
                                                        FriendInfo.MainRole = MainRole;


                                                    } else if (MainRole.matches("16")) {

                                                        CollageName = CampusData.getString("title");
                                                        CollageRole = CampusData.getString("field_i_am_a");
                                                        CollageStudentName = CampusData.getString("field_student_name");
                                                        CollageId = CampusData.getString("tnid");

                                                        FriendInfo.CollageId = CollageId;
                                                        FriendInfo.CollageName = CollageName;
                                                        FriendInfo.CollageRole = CollageRole;
                                                        FriendInfo.CollageStudentName = CollageStudentName;
                                                        FriendInfo.MainRole = MainRole;


                                                    } else if (MainRole.matches("3")) {


                                                        CollageName = CampusData.getString("title");
                                                        CollageRole = CampusData.getString("field_i_am_a");
                                                        Collagedepartment = CampusData.getString("field_department");
                                                        CollageId = CampusData.getString("tnid");

                                                        FriendInfo.CollageId = CollageId;
                                                        FriendInfo.CollageName = CollageName;
                                                        FriendInfo.CollageRole = CollageRole;
                                                        FriendInfo.Collagedepartment = Collagedepartment;
                                                        FriendInfo.MainRole = MainRole;
                                                    }
                                                }
                                            }

                                            mFriendsList.add(FriendInfo);
                                            mMyfriendadapter = new UsesFriendsDisplayAdapter(UsersFriendsDisplay.this, mFriendsList);
                                            rv_myfriendslist.setAdapter(mMyfriendadapter);
                                        }
                                    }else {
                                        ErrorText.setVisibility(View.VISIBLE);
                                        ErrorText.setText("you have no friends");

                                    }


                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(UsersFriendsDisplay.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(UsersFriendsDisplay.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(UsersFriendsDisplay.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        finish();
    }
}


