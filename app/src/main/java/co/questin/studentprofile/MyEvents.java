package co.questin.studentprofile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.MyEventsAdapters;
import co.questin.college.CollageEvent;
import co.questin.library.StateBean;
import co.questin.models.MyEventsArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

public class MyEvents extends BaseAppCompactActivity {

    static Activity activity;
    FloatingActionMenu materialDesignFAM;
    com.github.clans.fab.FloatingActionButton floatingActionButton1;
    static TextView text_view,text_view2;
    static   RelativeLayout relative,Displaytext;
    public static ArrayList<MyEventsArray> mEvent;
    static RecyclerView rv_myEvents;
    private RecyclerView.LayoutManager layoutManager;
    private static MyEventsAdapters eventadapter;
    static String eventid,eventtittle,eventlogo,eventstartdate,eventstarttime,eventlocation,endate,endtime,week;
    static TextView ErrorText;
    static ImageView ReloadProgress;

    private static ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.questin.R.layout.activity_my_events);

        activity = getActivity();
        Displaytext =findViewById(R.id.Displaytext);
        text_view = findViewById(R.id.text_view);
        text_view2 = findViewById(R.id.text_view2);
        ErrorText = findViewById(R.id.ErrorText);
        materialDesignFAM = findViewById(R.id.material_design_android_floating_action_menu);
        relative = findViewById(R.id.relative);
        floatingActionButton1 = findViewById(R.id.material_design_floating_action_menu_item1);
        mEvent=new ArrayList<>();
        ReloadProgress = findViewById(R.id.ReloadProgress);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();


        rv_myEvents = findViewById(co.questin.R.id.rv_myEvents);
        rv_myEvents.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rv_myEvents.setLayoutManager(layoutManager);


        if(SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {

            MyEventsListDisplay(SessionManager.getInstance(getActivity()).getUser().getParentUid());



        }else {
           MyEventsListDisplay(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id());


        }



        relative.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }
                return false;
            }
        });


        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MyEvents.this, CollageEvent.class);
                startActivity(intent);


            }
        });


        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RefershMyEventList();
            }
        });



    }

    public static void RefershMyEventList() {

        mEvent.clear();

        if(SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {

           MyEventsListDisplay(SessionManager.getInstance(activity).getUser().getParentUid());



        }else {
             MyEventsListDisplay(SessionManager.getInstance(activity).getUser().getUserprofile_id());


        }

    }



    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");

        super.onPause();
    }


    private static void MyEventsListDisplay(String userprofile_id) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_MYEVENTS+"?"+"uid="+ userprofile_id+"&cid="+ SessionManager.getInstance(activity).getCollage().getTnid()+"&offset="+"0"+"&limit=100",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                ErrorText.setVisibility(View.GONE);
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);

                                    JSONArray jsonArrayData = obj.getJSONArray("data");

                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {

                                        for (int i = 0; i < jsonArrayData.length(); i++) {
                                            MyEventsArray EventInfo = new MyEventsArray();

                                            eventid = jsonArrayData.getJSONObject(i).getString("nid");
                                            eventtittle = jsonArrayData.getJSONObject(i).getString("title");

                                            JSONObject jsonObject1 = jsonArrayData.optJSONObject(i);
                                            JSONArray logo = jsonObject1.getJSONArray("field_groups_logo");

                                            if(logo != null && logo.length() > 0 ) {
                                                eventlogo =logo.getString(0);
                                                EventInfo.field_groups_logo =eventlogo;
                                                Log.e("logo", logo.getString(0));


                                            }


                                            JSONArray eventdates = jsonArrayData.getJSONObject(i).getJSONArray("field_time");
                                            if (eventdates != null && eventdates.length() > 0) {

                                                for (int j = 0; j < eventdates.length(); j++) {
                                                    eventstartdate = eventdates.getJSONObject(j).getString("startDate");
                                                    endate = eventdates.getJSONObject(j).getString("endDate");
                                                    eventstarttime = eventdates.getJSONObject(j).getString("startTime");
                                                    endtime = eventdates.getJSONObject(j).getString("endTime");
                                                    week = eventdates.getJSONObject(j).getString("Week");

                                                    EventInfo.nid = eventid;
                                                    EventInfo.title = eventtittle;
                                                    EventInfo.field_dept_location = eventlocation;
                                                    EventInfo.startDate = eventstartdate;
                                                    EventInfo.endDate = endate;
                                                    EventInfo.startTime = eventstarttime;
                                                    EventInfo.endTime = endtime;
                                                    EventInfo.Week = week;

                                                    mEvent.add(EventInfo);
                                                    eventadapter = new MyEventsAdapters(activity, mEvent);
                                                    rv_myEvents.setAdapter(eventadapter);
                                                    Displaytext.setVisibility(View.INVISIBLE);

                                                    }
                                                    mShimmerViewContainer.stopShimmerAnimation();
                                                mShimmerViewContainer.setVisibility(View.GONE);
                                                }

                                                }

                                                }else{
                                        Displaytext.setVisibility(View.VISIBLE);
                                        rv_myEvents.setVisibility(View.GONE);
                                        text_view.setVisibility(View.VISIBLE);
                                        mShimmerViewContainer.stopShimmerAnimation();
                                        mShimmerViewContainer.setVisibility(View.GONE);

                                        text_view2.setVisibility(View.VISIBLE);

                                    }




                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());
                                    ErrorText.setVisibility(View.VISIBLE);
                                    mShimmerViewContainer.stopShimmerAnimation();
                                    mShimmerViewContainer.setVisibility(View.GONE);

                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                                ReloadProgress.setVisibility(View.VISIBLE);

                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                            mShimmerViewContainer.stopShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.GONE);
                            ReloadProgress.setVisibility(View.VISIBLE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);


    }


    @Override
    public void onBackPressed() {
        finish();
    }
}
