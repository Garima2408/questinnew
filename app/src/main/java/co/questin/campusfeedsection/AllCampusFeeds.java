package co.questin.campusfeedsection;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.CampusFeedsAdapters;
import co.questin.adapters.CampusFeedsFilterAdapters;
import co.questin.alumni.AluminiProfile;
import co.questin.calendersection.CalenderMain;
import co.questin.chat.ChatTabbedActivity;
import co.questin.library.StateBean;
import co.questin.models.CampusFeedArray;
import co.questin.models.FilterArray;
import co.questin.network.URLS;
import co.questin.studentprofile.ProfileDash;
import co.questin.teacher.TeacherProfile;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

import static co.questin.fcm.MessagingService.college_Allfeeds;
import static co.questin.utils.Utils.isStatusSuccess;

public class AllCampusFeeds extends BaseAppCompactActivity {
    Dialog imageDialog;
    private SharedPreferences preferences;
    private int Runfirst;
    static FloatingActionButton fab;
    ImageView campusfeed_toggle;
    ListView campusfeed_filter_List;
    ArrayList<FilterArray> listoffilter;
    static RecyclerView rv_campus_news;
    public static ArrayList<CampusFeedArray> mcampusfeedList;
    private static CampusFeedsAdapters campusfeedadapter;
    private RecyclerView.LayoutManager layoutManager;
    static String tittle,URLDoc,FilterTittle,FilterId;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    static TextView ErrorText,badge_notificationcampus,campus_feeds;
    private boolean _hasLoadedOnce= false; // your boolean field
    private static ShimmerFrameLayout mShimmerViewContainer;
    static SwipeRefreshLayout swipeContainer;
    private ArrayList<String> filterlist;
    static ImageView ReloadProgress;
    static Activity activity;
    RelativeLayout relative_layout_item,campusfeed_togglelayout;
    boolean isPressed = false;
    CampusFeedsFilterAdapters adapter;


    public static final Integer[] images = { R.mipmap.allfeed,
            R.mipmap.studentfeed, R.mipmap.campusnews, R.mipmap.buy_sell, R.mipmap.housesharing  , R.mipmap.lost_found , R.mipmap.ridesharing , R.mipmap.admission };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_campus_feeds);
        activity = getActivity();
        preferences = Utils.getSharedPreference(AllCampusFeeds.this);
        Runfirst = preferences.getInt(Constants.RUNNIN_FIRST_CAMPUSFEED, Constants.ROLE_RUNNING_FALSE_CAMPUSFEED);
        FirstOneThis();
        fab = findViewById(R.id.fab);
        campusfeed_toggle =findViewById(R.id.campusfeed_toggle);
        campusfeed_filter_List = findViewById(R.id.campusfeed_filter_List);
        ReloadProgress = findViewById(R.id.ReloadProgress);
        badge_notificationcampus =findViewById(R.id.badge_notificationcampus);
        relative_layout_item =findViewById(R.id.relative_layout_item);
        campus_feeds =findViewById(R.id.campus_feeds);
        campusfeed_togglelayout =findViewById(R.id.campusfeed_togglelayout);
        campusfeed_toggle.setBackgroundResource(R.mipmap.categoryicon);
        listoffilter = new ArrayList<FilterArray>();
        filterlist = new ArrayList<String>();
        ErrorText =findViewById(R.id.ErrorText);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();
        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));
        mcampusfeedList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        rv_campus_news = findViewById(R.id.rv_campus_news);
        campusfeedadapter = new CampusFeedsAdapters(rv_campus_news, mcampusfeedList ,activity);
        rv_campus_news.setLayoutManager(layoutManager);
        rv_campus_news.setItemAnimator(new DefaultItemAnimator());
        rv_campus_news.setAdapter(campusfeedadapter);
        inItView();
        Setuponbottombar();
        GetTheListOfFilters();
        CollageAllNewFeedsDisplay();


        if(college_Allfeeds >0){
            badge_notificationcampus.setVisibility(View.VISIBLE);
            badge_notificationcampus.setText(String.valueOf(college_Allfeeds));

        }else if (college_Allfeeds ==0){
            badge_notificationcampus.setVisibility(View.GONE);
        }


        campus_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                college_Allfeeds=0;
                badge_notificationcampus.setVisibility(View.GONE);
                Intent backIntent = new Intent(AllCampusFeeds.this, CampusFeedNotifications.class);
                startActivity(backIntent);

            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backIntent = new Intent(AllCampusFeeds.this, AddCampusFeeds.class)
                        .putExtra("open", "CameFromAllCampusFeed");
                startActivity(backIntent);




            }
        });

        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RefreshWorked();
            }
        });



        rv_campus_news.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (campusfeed_filter_List.getVisibility() == View.VISIBLE) {
                    campusfeed_filter_List.setVisibility(View.GONE);
                    campusfeed_toggle.setBackgroundResource(R.mipmap.categoryicon);
                }
                return false;
            }
        });
        campusfeed_togglelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isPressed){
                    if (campusfeed_filter_List.getVisibility() == View.VISIBLE) {
                        campusfeed_filter_List.setVisibility(View.GONE);
                        campusfeed_toggle.setBackgroundResource(R.mipmap.categoryicon);

                    } else {
                        campusfeed_filter_List.setVisibility(View.VISIBLE);
                        campusfeed_toggle.setBackgroundResource(R.mipmap.cross);

                    }


                }else if(!isPressed){

                    campusfeed_filter_List.setVisibility(View.VISIBLE);
                    campusfeed_toggle.setBackgroundResource(R.mipmap.cross);


                }

                isPressed = !isPressed; // reverse


            }


        });



        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh

                        if (!isLoading){
                            RefreshWorked();
                        }else{
                            isLoading=false;
                            swipeContainer.setRefreshing(false);
                        }


                    }
                }, 1000);
            }
        });

        rv_campus_news.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    AllCampusFeeds.fab.hide();
                    if (campusfeed_filter_List.getVisibility() == View.VISIBLE) {
                        campusfeed_filter_List.setVisibility(View.GONE);
                        campusfeed_toggle.setBackgroundResource(R.mipmap.categoryicon);

                    }
                } else if (dy < 0) {
                    AllCampusFeeds.fab.show();
                    if (campusfeed_filter_List.getVisibility() == View.VISIBLE) {
                        campusfeed_filter_List.setVisibility(View.GONE);
                        campusfeed_toggle.setBackgroundResource(R.mipmap.categoryicon);

                    }
                }
            }
        });

    }


    private void inItView(){
        rv_campus_news.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){

                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    campusfeedadapter.addProgress();


                    if (FilterId !=null){
                        if (FilterId.matches("1")){
                            CollageAllNewFeedsDisplay();



                        }else {
                            CollageNewFeedsDisplay(FilterId);

                        }
                    }else {
                        CollageAllNewFeedsDisplay();


                    }


                }else
                    Log.i("loadinghua", "im else now");
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }




    public static void RefreshWorked() {

        PAGE_SIZE=0;
        isLoading=true;
        mShimmerViewContainer.startShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        if (FilterId !=null){
            if (FilterId.matches("1")){
                CollageAllNewFeedsDisplay();

            }else {
                CollageNewFeedsDisplay(FilterId);

            }
        }else {
            CollageAllNewFeedsDisplay();


        }


    }


    private void GetTheListOfFilters() {
        ProgressSearchOfFilterList();

    }


    private void FirstOneThis() {

        switch (Runfirst){
            case Constants.ROLE_RUNNING_TRUE :
                imageDialog = new Dialog(AllCampusFeeds.this);
                imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                imageDialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(Color.TRANSPARENT));
                imageDialog.setContentView(R.layout.image_overlay_screen);
                imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT);

                imageDialog.show();
                ImageView showimage = imageDialog.findViewById(R.id.DisplayOnbondingImage);
                showimage.setImageResource(R.mipmap.onboarding_campusfeed);

                showimage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Utils.getSharedPreference(AllCampusFeeds.this).edit()
                                .putInt(Constants.RUNNIN_FIRST_CAMPUSFEED, Constants.ROLE_RUNNING_FALSE_CAMPUSFEED).apply();
                        Utils.removeStringPreferences(AllCampusFeeds.this,"0");

                        imageDialog.dismiss();

                    }
                });



                break;
            case Constants.ROLE_RUNNING_FALSE :
                break;


        }

    }

    private void Setuponbottombar() {

        TextView dashboard_child = findViewById(R.id.dashboard_child);
        final TextView calendar = findViewById(R.id.calendar);
        TextView campus_feeds = findViewById(R.id.campus_feeds);
        TextView message_feeds = findViewById(R.id.message_feeds);
        TextView profile = findViewById(R.id.profile);

        dashboard_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();


            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent p = new Intent(AllCampusFeeds.this,CalenderMain.class)
                        .putExtra("open", "AsSelectedSite");
                startActivity(p);
                finish();


            }
        });

        campus_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });

        message_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent z = new Intent(AllCampusFeeds.this,ChatTabbedActivity.class)
                        .putExtra("open", "AsSelectedSite");
                startActivity(z);
                finish();


            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SessionManager.getInstance(getActivity()).getUserClgRole()!=null) {

                    if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("13")) {
                        Intent j = new Intent(AllCampusFeeds.this,ProfileDash.class);
                        startActivity(j);
                        finish();


                    } else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("14")) {
                        Intent j = new Intent(AllCampusFeeds.this,TeacherProfile.class);
                        startActivity(j);
                        finish();


                    } else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("15")) {
                        Intent j = new Intent(AllCampusFeeds.this,AluminiProfile.class);
                        startActivity(j);
                        finish();


                    }
                    else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("3")) {
                        Intent j = new Intent(AllCampusFeeds.this,TeacherProfile.class);
                        startActivity(j);
                        finish();

                    }
                }
            }});

    }


    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");
        _hasLoadedOnce= false;
        super.onPause();
    }



    private static void CollageNewFeedsDisplay(String filterId) {
        swipeContainer.setRefreshing(false);
        ReloadProgress.setVisibility(View.GONE);
        rv_campus_news.setVisibility(View.VISIBLE);
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_SHORTCOMMENTS+"?"+"cid="+SessionManager.getInstance(activity).getCollage().getTnid()+"&"+"channel="+filterId+"&offset="+PAGE_SIZE+"&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {

                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    ArrayList<CampusFeedArray> arrayList = new ArrayList<>();
                                    arrayList.clear();
                                    for (int i = 0; i < jsonArrayData.length(); i++) {

                                        CampusFeedArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CampusFeedArray.class);
                                        arrayList.add(CourseInfo);
                                        JSONObject jsonObject = jsonArrayData.getJSONObject(i);

                                        JSONArray jsoncategoryArray = jsonArrayData.getJSONObject(i).getJSONArray("category");
                                        if (jsoncategoryArray != null && jsoncategoryArray.length() > 0) {

                                            for (int j = 0; j < jsoncategoryArray.length(); j++) {
                                                String categoryid = jsoncategoryArray.getJSONObject(j).getString("id");
                                                String categoryName = jsoncategoryArray.getJSONObject(j).getString("name");
                                                CourseInfo.categoryId =categoryid;
                                                CourseInfo.categoryName =categoryName;
                                            }
                                        }




                                        if (jsonObject.has("field_comment_attachments")){
                                            JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                            if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0 ){
                                                URLDoc = AttacmentsFeilds.getString("value");
                                                tittle = AttacmentsFeilds.getString("title");
                                                CourseInfo.title =tittle;
                                                CourseInfo.url =URLDoc;

                                            }else {

                                            }
                                        }
                                    }



                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {

                                            rv_campus_news.setVisibility(View.VISIBLE);
                                            campusfeedadapter.setInitialData(arrayList);
                                            // stop animating Shimmer and hide the layout
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);

                                        } else {

                                            campusfeedadapter.removeProgress();
                                            campusfeedadapter.addData(arrayList);
                                            swipeContainer.setRefreshing(false);
                                            rv_campus_news.setVisibility(View.VISIBLE);
                                            // stop animating Shimmer and hide the layout
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);
                                        }
                                        isLoading = false;
                                    }
                                    else{
                                        if (PAGE_SIZE==0){
                                            // stop animating Shimmer and hide the layout
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);
                                            // swipeContainer.setVisibility(View.GONE);
                                            rv_campus_news.setVisibility(View.GONE);
                                            ErrorText.setVisibility(View.VISIBLE);
                                            ErrorText.setText("No Post");
                                        }else
                                            campusfeedadapter.removeProgress();

                                    }



                                } else {
                                    showSnackbarMessage(stateResponse.getMessage());
                                    mShimmerViewContainer.stopShimmerAnimation();
                                    mShimmerViewContainer.setVisibility(View.GONE);
                                }
                            } else {

                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);
                return params;
            }
        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PAGE_SIZE=0;
    }


    private void ProgressSearchOfFilterList() {
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_FILTERLIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray data = obj.getJSONArray("data");
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                        if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {
                                            data.remove(1);

                                        } else {

                                            data.remove(0);
                                        }

                                    }
                                    FilterArray filterInfo2 =new FilterArray();
                                    filterInfo2.id ="1";
                                    filterInfo2.label="All feed";
                                    listoffilter.add(filterInfo2);
                                    filterlist.add(filterInfo2.getLabel());
                                    for (int i = 0; i < 7; i++) {


                                        FilterArray filterInfo = new Gson().fromJson(data.getJSONObject(i).toString(), FilterArray.class);
                                        listoffilter.add(filterInfo);
                                        filterlist.add(filterInfo.getLabel());

                                        adapter = new CampusFeedsFilterAdapters(AllCampusFeeds.this, filterlist,images);
                                        campusfeed_filter_List.setAdapter(adapter);
                                        campusfeed_filter_List.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

                                        // Capture ListView item click
                                        campusfeed_filter_List.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            public void onItemClick(AdapterView<?> parent, View view,
                                                                    int position, long id) {

                                                FilterId = listoffilter.get(position).getId();
                                                FilterTittle = listoffilter.get(position).getLabel();

                                                if (FilterId.matches("1")){

                                                    campusfeed_filter_List.setVisibility(View.GONE);
                                                    campusfeed_toggle.setBackgroundResource(R.mipmap.categoryicon);
                                                    mShimmerViewContainer.startShimmerAnimation();
                                                    mShimmerViewContainer.setVisibility(View.VISIBLE);
                                                    PAGE_SIZE=0;
                                                    isLoading=true;
                                                    CollageAllNewFeedsDisplay();

                                                    FilterId ="1";

                                                }else {

                                                    campusfeed_filter_List.setVisibility(View.GONE);
                                                    campusfeed_toggle.setBackgroundResource(R.mipmap.categoryicon);
                                                    mShimmerViewContainer.startShimmerAnimation();
                                                    mShimmerViewContainer.setVisibility(View.VISIBLE);
                                                    PAGE_SIZE=0;
                                                    isLoading=true;
                                                    CollageNewFeedsDisplay(FilterId);

                                                }
                                            }
                                        });

                                    }

                                }else {
                                    showSnackbarMessage(stateResponse.getMessage());
                                }
                            } else {
                                showServerSnackbar(R.string.error_responce);
                            }
                        } catch (Exception e) {

                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("vid","35");

                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(AllCampusFeeds.this);
        requestQueue.add(mStrRequest);

    }

    private static void CollageAllNewFeedsDisplay() {
        swipeContainer.setRefreshing(false);
        ReloadProgress.setVisibility(View.GONE);
        rv_campus_news.setVisibility(View.VISIBLE);
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_CAMPUSFEED+"?"+"cid="+SessionManager.getInstance(activity).getCollage().getTnid()+"&offset="+PAGE_SIZE+"&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {

                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    ArrayList<CampusFeedArray> arrayList = new ArrayList<>();
                                    arrayList.clear();


                                    for (int i = 0; i < jsonArrayData.length(); i++) {

                                        CampusFeedArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CampusFeedArray.class);
                                        arrayList.add(CourseInfo);
                                        JSONObject jsonObject = jsonArrayData.getJSONObject(i);

                                        JSONArray jsoncategoryArray = jsonArrayData.getJSONObject(i).getJSONArray("category");
                                        if (jsoncategoryArray != null && jsoncategoryArray.length() > 0) {

                                            for (int j = 0; j < jsoncategoryArray.length(); j++) {
                                                String categoryid = jsoncategoryArray.getJSONObject(j).getString("id");
                                                String categoryName = jsoncategoryArray.getJSONObject(j).getString("name");
                                                CourseInfo.categoryId =categoryid;
                                                CourseInfo.categoryName =categoryName;
                                            }
                                        }




                                        if (jsonObject.has("field_comment_attachments")){
                                            JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                            if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0 ){
                                                URLDoc = AttacmentsFeilds.getString("value");
                                                tittle = AttacmentsFeilds.getString("title");
                                                CourseInfo.title =tittle;
                                                CourseInfo.url =URLDoc;

                                            }else {

                                            }
                                        }
                                    }



                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {

                                            rv_campus_news.setVisibility(View.VISIBLE);
                                            campusfeedadapter.setInitialData(arrayList);
                                            // stop animating Shimmer and hide the layout
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);

                                        } else {
                                            rv_campus_news.setVisibility(View.VISIBLE);
                                            campusfeedadapter.removeProgress();
                                            campusfeedadapter.addData(arrayList);
                                            swipeContainer.setRefreshing(false);
                                            // stop animating Shimmer and hide the layout
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);
                                        }
                                        isLoading = false;
                                    }
                                    else{
                                        if (PAGE_SIZE==0){
                                            // stop animating Shimmer and hide the layout
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);
                                            // swipeContainer.setVisibility(View.GONE);
                                            rv_campus_news.setVisibility(View.GONE);
                                            ErrorText.setVisibility(View.VISIBLE);
                                            ErrorText.setText("No Post");
                                        }else
                                            campusfeedadapter.removeProgress();

                                    }



                                } else {
                                    showSnackbarMessage(stateResponse.getMessage());
                                    mShimmerViewContainer.stopShimmerAnimation();
                                    mShimmerViewContainer.setVisibility(View.GONE);

                                }
                            } else {
                                showServerSnackbar(R.string.error_responce);

                            }
                        } catch (Exception e) {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);
                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }


    @Override
    public void onBackPressed() {
        Utils.getSharedPreference(this).edit()
                .putInt(Constants.RUNNIN_FIRST_CAMPUSFEED, Constants.ROLE_RUNNING_FALSE_CAMPUSFEED).apply();
        Utils.removeStringPreferences(AllCampusFeeds.this,"0");

        super.onBackPressed();
    }
}
