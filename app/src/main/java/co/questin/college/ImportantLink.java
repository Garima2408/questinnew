package co.questin.college;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.ImportantLinkAdapters;
import co.questin.library.StateBean;
import co.questin.models.ListImportanLink;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class ImportantLink extends BaseAppCompactActivity {
    RecyclerView  rv_ImportantLinks;
    SwipeRefreshLayout swipeContainer;
    static ImageView ReloadProgress;
    public ArrayList<ListImportanLink> mLinkList;
    private RecyclerView.LayoutManager layoutManager;
    private ImportantLinkAdapters linkadapter;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    private static ShimmerFrameLayout mShimmerViewContainer;
    static TextView ErrorText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_important_link);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        mLinkList = new ArrayList<>();
        ReloadProgress = findViewById(R.id.ReloadProgress);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();
        ErrorText = findViewById(R.id.ErrorText);
        swipeContainer =findViewById(R.id.swipeContainer);
        layoutManager = new LinearLayoutManager(this);
        rv_ImportantLinks = findViewById(R.id.rv_ImportantLinks);
        rv_ImportantLinks.setHasFixedSize(true);
        rv_ImportantLinks.setLayoutManager(layoutManager);

        linkadapter = new ImportantLinkAdapters(ImportantLink.this, mLinkList);
        rv_ImportantLinks.setAdapter(linkadapter);
        CallTheImaportantLinks();

        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallTheImaportantLinks();
            }
        });
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        PAGE_SIZE=0;
                        isLoading=false;
                        swipeContainer.setRefreshing(true);
                        CallTheImaportantLinks();
                    }
                }, 3000);
            }
        });

        inItView();
    }

    private void CallTheImaportantLinks() {
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_IMPORTANTLINKS + "?cid=" + SessionManager.getInstance(getActivity()).getCollage().getTnid() + "&offset=" + PAGE_SIZE + "&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    ArrayList<ListImportanLink> arrayList = new ArrayList<>();

                                    for (int i = 0; i < jsonArrayData.length(); i++) {


                                        JSONArray currentDataLink = new JSONArray(jsonArrayData.getJSONObject(i).getString("link"));



                                        for (int j = 0; j < currentDataLink.length(); j++) {
                                            String t =currentDataLink.getJSONObject(j).getString("title");
                                            String p =currentDataLink.getJSONObject(j).getString("path");
                                            Log.d("TAG", "titlepath: " + t+p);

                                            ListImportanLink LinkInfo = new ListImportanLink(0);
                                            LinkInfo.title =currentDataLink.getJSONObject(j).getString("title");
                                            LinkInfo.path =currentDataLink.getJSONObject(j).getString("path");

                                            arrayList.add(LinkInfo);


                                        }


                                    }

                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {


                                            rv_ImportantLinks.setVisibility(View.VISIBLE);
                                            linkadapter.setInitialData(arrayList);

                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);


                                        } else {
                                            isLoading = false;
                                            linkadapter.removeProgress();
                                            swipeContainer.setRefreshing(false);

                                            linkadapter.addData(arrayList);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);



                                        }
                                    }

                                    else{
                                        if (PAGE_SIZE==0){
                                            rv_ImportantLinks.setVisibility(View.GONE);
                                            ErrorText.setVisibility(View.VISIBLE);
                                            ErrorText.setText("No Important Link");
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);


                                            swipeContainer.setVisibility(View.GONE);
                                        }else
                                            linkadapter.removeProgress();

                                    }



                                }else {
                                    mShimmerViewContainer.stopShimmerAnimation();
                                    mShimmerViewContainer.setVisibility(View.GONE);
                                    swipeContainer.setRefreshing(false);

                                }



                            }else {
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                                swipeContainer.setRefreshing(false);
                            }



                        } catch (Exception e) {
                            swipeContainer.setRefreshing(false);
                            ReloadProgress.setVisibility(View.VISIBLE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(ImportantLink.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(ImportantLink.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(ImportantLink.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(ImportantLink.this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);






    }

    private void inItView(){

        swipeContainer.setRefreshing(false);

        rv_ImportantLinks.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    linkadapter.addProgress();
                    CallTheImaportantLinks();
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }



    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();

        super.onPause();
    }





    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                PAGE_SIZE=0;
                isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


}
