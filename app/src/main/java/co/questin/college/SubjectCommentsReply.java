package co.questin.college;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.activities.UserDisplayProfile;
import co.questin.adapters.SubjectReplyAdapter;
import co.questin.buttonanimation.LikeButton;
import co.questin.buttonanimation.OnAnimationEndListener;
import co.questin.buttonanimation.OnLikeListener;
import co.questin.library.StateBean;
import co.questin.models.ReplyOnCommentArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

import static co.questin.utils.Utils.isStatusSuccess;

public class SubjectCommentsReply extends BaseAppCompactActivity {
    String parent_id, subject_id, parentname, Pimage, Pdate, parentcomment, comments, TransferId, noOfComments, noOfLikes, IsLiked, Content_url;
    static String PARENTID;
    static String SUBJECTID;
    ImageView circleView, comment, options, inaprooptions, imagePost, imagepreview, share;
    private TextView blog_content, publisher_name, dateTime, commentNo, likeNo, tv_comment;
    LikeButton like;
    static RecyclerView rv_reply_comment;
    public static ArrayList<ReplyOnCommentArray> commentsList;
    static SubjectReplyAdapter mcommentAdapter;
    private RecyclerView.LayoutManager layoutManager;
    EditText editText_Commment;
    ImageButton Send;
    Bundle extras;
    static TextView ErrorText;
    static Activity activity;
    RelativeLayout previewlayout;
    static String tittle, URLDoc;
    String BackScreen;
    private static boolean isLoading= true;
    private static int PAGE_SIZE = 0;
    String Extension;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_comments_reply);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        activity = getActivity();
        Intent intent = getIntent();
        extras = intent.getExtras();

        commentsList = new ArrayList<ReplyOnCommentArray>();

        subject_id = intent.getStringExtra("SUBJECT_ID");
        parent_id = intent.getStringExtra("PARENT_ID");
        SUBJECTID = intent.getStringExtra("SUBJECT_ID");
        PARENTID = intent.getStringExtra("PARENT_ID");
        parentname = intent.getStringExtra("NAME");
        Pimage = intent.getStringExtra("IMAGE_URL");
        Pdate = intent.getStringExtra("DATE");
        parentcomment = intent.getStringExtra("BLOG_CONTENT");
        TransferId = intent.getStringExtra("USERSELECTED_ID");
        noOfComments = intent.getStringExtra("NOCOMMENT");
        noOfLikes = intent.getStringExtra("NOLIKED");
        IsLiked = intent.getStringExtra("IS_LIKED");
        Content_url = intent.getStringExtra("POSTCONENT");

        if (extras.containsKey("open")) {
            if (extras.getString("open").equals("CameFromTeacherAdapter")) {
                BackScreen = "CameFromTeacherAdapter";


            } else if (extras.getString("open").equals("CameFromStudentAdapter")) {
                BackScreen = "CameFromStudentAdapter";


            }
        }



        /*CameFromTeacherAdapter*/

       /* editText_Commment = findViewById(R.id.editText_Commment);
        Send =  findViewById(R.id.Send);*/
        blog_content = findViewById(R.id.blog_content);
        tv_comment = findViewById(R.id.tv_comment);
        dateTime = findViewById(R.id.dateTime);
        publisher_name = findViewById(R.id.publisher_name);
        circleView = findViewById(R.id.circleView);
        rv_reply_comment = findViewById(R.id.rv_reply_comment);
        layoutManager = new LinearLayoutManager(this);
        rv_reply_comment.setHasFixedSize(true);
        rv_reply_comment.setLayoutManager(layoutManager);
        ErrorText = findViewById(R.id.ErrorText);
        options = findViewById(R.id.options);
        like = findViewById(R.id.like);
        share = findViewById(R.id.share);
        comment = findViewById(R.id.comment);
        commentNo = findViewById(R.id.commentNo);
        likeNo = findViewById(R.id.likeNo);
        imagePost = findViewById(R.id.imagePost);
        imagepreview = findViewById(R.id.imagepreview);
        previewlayout = findViewById(R.id.previewlayout);
        inaprooptions = findViewById(R.id.inaprooptions);
        blog_content.setText(Html.fromHtml(parentcomment));
        publisher_name.setText(parentname);
        dateTime.setText(Pdate);
        commentNo.setText(noOfComments);
        likeNo.setText(noOfLikes);

        if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {
            tv_comment.setVisibility(View.VISIBLE);
        }


        mcommentAdapter = new SubjectReplyAdapter(rv_reply_comment, commentsList ,activity);
        rv_reply_comment.setAdapter(mcommentAdapter);
        rv_reply_comment.setNestedScrollingEnabled(false);

        if (Pimage != null) {

            Glide.with(this).load(Pimage)
                    .placeholder(R.mipmap.place_holder).dontAnimate()
                    .fitCenter().into(circleView);

        } else {
            circleView.setImageResource(R.mipmap.place_holder);

        }



           /*CALLING LISTS OF REPLY COMMENTS*/

         SubjectsCommentsList(SUBJECTID);

        inItView();

        if (Content_url != null) {

            Extension = Content_url.substring(Content_url.lastIndexOf("."));

            if (Extension.matches(".jpeg")) {

                previewlayout.setVisibility(View.VISIBLE);
                imagePost.setVisibility(View.VISIBLE);
                Glide.clear(imagePost);
                Glide.with(this).load(Content_url)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(imagePost);
            }else if (Extension.matches(".png"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagePost.setVisibility(View.VISIBLE);
                Glide.clear(imagePost);
                Glide.with(this).load(Content_url)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(imagePost);
            }
            else if (Extension.matches(".jpg"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagePost.setVisibility(View.VISIBLE);

                Glide.clear(imagePost);
                Glide.with(this).load(Content_url)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(imagePost);
            }else if (Extension.matches(".doc"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_doc_download);
            }
            else if (Extension.matches(".docx"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_doc_download);
            }
            else if (Extension.matches(".pdf"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_pdf_download);
            }
            else if (Extension.matches(".txt"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_text_download);
            }
            else if (Extension.matches(".xls"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_xls_download);
            }
            else if (Extension.matches(".xlsx"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_xls_download);
            }


        }else {
            previewlayout.setVisibility(View.GONE);



        }



        if (TransferId.equals(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id())) {
            options.setVisibility(View.VISIBLE);
            inaprooptions.setVisibility(View.INVISIBLE);
        } else {
            options.setVisibility(View.INVISIBLE);
            inaprooptions.setVisibility(View.VISIBLE);
        }


        if(IsLiked.contains("False")){
            like.setLiked(false);
        }else if(IsLiked.contains("True"))  {
            like.setLiked(true);
        }


        like.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                if (!IsLiked.isEmpty()) {
                    if (IsLiked.contains("False")) {
                        like.setLiked(true);

                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) + 1));
                        String NewLikeNo = likeNo.getText().toString();
                        LikeFeedComments(parent_id,"flag");

                        IsLiked ="True";
                        noOfLikes =NewLikeNo;
                        //lists.get(position).setIs_liked("True");

                        /* lists.get(position).setLikes(NewLikeNo);
                         */
                    } else if (IsLiked.contains("True")) {
                        like.setLiked(false);
                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) - 1));
                        String NewLikeNo = likeNo.getText().toString();
                        LikeFeedComments(parent_id,"unflag");

                        IsLiked ="False";
                        noOfLikes =NewLikeNo;
                       /* lists.get(position).setIs_liked("False");
                        lists.get(position).setLikes(NewLikeNo);*/
                    }
                }
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                if (!IsLiked.isEmpty()) {
                    if (IsLiked.contains("False")) {
                        like.setLiked(true);

                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) + 1));
                        String NewLikeNo = likeNo.getText().toString();
                        LikeFeedComments(parent_id,"flag");

                        IsLiked ="True";
                        noOfLikes =NewLikeNo;
                        //lists.get(position).setIs_liked("True");

                        /* lists.get(position).setLikes(NewLikeNo);
                         */
                    } else if (IsLiked.contains("True")) {
                        like.setLiked(false);
                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) - 1));
                        String NewLikeNo = likeNo.getText().toString();
                        LikeFeedComments(parent_id,"unflag");
                        IsLiked ="False";
                        noOfLikes =NewLikeNo;
                       /* lists.get(position).setIs_liked("False");
                        lists.get(position).setLikes(NewLikeNo);*/
                    }
                }
            }
        });


        like.setOnAnimationEndListener(new OnAnimationEndListener() {
            @Override
            public void onAnimationEnd(LikeButton likeButton) {

            }
        });


        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String shareBody = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "APP NAME (Open it in Google Play Store to Download the Application)");

                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody + parentcomment);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));


            }
        });


        publisher_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent backIntent = new Intent(SubjectCommentsReply.this, UserDisplayProfile.class)
                        .putExtra("USERPROFILE_ID", TransferId);
                startActivity(backIntent);


            }
        });


        circleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent backIntent = new Intent(SubjectCommentsReply.this, UserDisplayProfile.class)
                        .putExtra("USERPROFILE_ID", TransferId);
                startActivity(backIntent);


            }
        });


        tv_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent backIntent = new Intent(SubjectCommentsReply.this, StudentCommentScreen.class)
                        .putExtra("COURSE_ID", subject_id)
                        .putExtra("PARENT_ID", parent_id)
                        .putExtra("open", "CameFromStudentCommentreply");
                startActivity(backIntent);


            }
        });


        inaprooptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), android.app.AlertDialog.THEME_HOLO_DARK)
                        .setTitle("Inappropriate")
                        .setMessage(R.string.Inappropriate)
                        .setCancelable(false)
                        .setPositiveButton("Yes", new Dialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                                  /*  DELETE COMMENTS ON POST*/
                                InappropriateFeedComments(PARENTID);



                            }
                        })
                        .setNegativeButton("No", new Dialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                builder.create().show();


            }
        });

        options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPopupMenu();


            }

            private void showPopupMenu() {


                PopupMenu popup = new PopupMenu(getActivity(), options);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.optionpopupmenu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.Editoption:


                                Intent backIntent = new Intent(SubjectCommentsReply.this, UpdateSubjectComment.class)
                                        .putExtra("PARENT_ID", parent_id)
                                        .putExtra("NAME", parentname)
                                        .putExtra("BLOG_CONTENT", parentcomment)
                                        .putExtra("DATE", Pdate)
                                        .putExtra("IMAGE_URL", Pimage)
                                        .putExtra("USERSELECTED_ID", TransferId)
                                        .putExtra("POSTCONENT", Content_url)
                                        .putExtra("open", BackScreen);
                                startActivity(backIntent);
                                finish();

                                return true;
                            case R.id.deleteoption:

                                AlertDialog.Builder builder = new AlertDialog.Builder(SubjectCommentsReply.this, android.app.AlertDialog.THEME_HOLO_DARK)
                                        .setTitle("Delete My Feed")
                                        .setMessage(R.string.delete)
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                  /*  DELETE COMMENTS ON POST*/
                                                DeleteSubjectComments(PARENTID);


                                            }
                                        })
                                        .setNegativeButton("No", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        });
                                builder.create().show();


                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popup.show();

            }
        });


    }

    public static void RefreshWorked() {
        PAGE_SIZE=0;
        commentsList.clear();
        /* CALLING LISTS OF COMMENTS*/
      SubjectsCommentsList(SUBJECTID);



    }


    private void inItView(){
        rv_reply_comment.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    mcommentAdapter.addProgress();
                    SubjectsCommentsList(SUBJECTID);


                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }








    /*LISTS OF REPLY COMMENTS ON SUBJECTS*/

    private static void SubjectsCommentsList(String subjectid) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_SUBJECTSREPLYCOMMENTS +"?&nid=" + subjectid+"&pid=" + PARENTID+"&offset="+PAGE_SIZE+"&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    rv_reply_comment.setVisibility(View.VISIBLE);
                                    JSONObject obj = new JSONObject(response);
                                    ArrayList<ReplyOnCommentArray> arrayList = new ArrayList<>();

                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    if (jsonArrayData != null && jsonArrayData.length() > 0) {


                                        for (int i = 0; i < jsonArrayData.length(); i++) {

                                            ReplyOnCommentArray subjects = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), ReplyOnCommentArray.class);
                                            arrayList.add(subjects);

                                            JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                                            if (jsonObject.has("field_comment_attachments")) {
                                                JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                                if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0) {
                                                    URLDoc = AttacmentsFeilds.getString("value");
                                                    tittle = AttacmentsFeilds.getString("title");
                                                    subjects.title = tittle;
                                                    subjects.url = URLDoc;

                                                } else {

                                                }
                                            }

                                            commentsList.add(subjects);
                                            mcommentAdapter.notifyDataSetChanged();
                                            rv_reply_comment.setVisibility(View.VISIBLE);
                                            mcommentAdapter.setInitialData(commentsList);
                                            ErrorText.setVisibility(View.GONE);



                                        }

                                    }
                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {


                                            rv_reply_comment.setVisibility(View.VISIBLE);
                                            mcommentAdapter.setInitialData(arrayList);

                                        } else {
                                            isLoading = false;
                                            mcommentAdapter.removeProgress();

                                            mcommentAdapter.addData(arrayList);


                                        }
                                    } else{
                                        if (PAGE_SIZE==0){
                                            rv_reply_comment.setVisibility(View.GONE);
                                            ErrorText.setVisibility(View.VISIBLE);
                                            ErrorText.setText("No Post");
                                        }else
                                            mcommentAdapter.removeProgress();

                                    }



                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }





    /*ADD LIKES ON COMMENTS */

    private void LikeFeedComments(final String id, final String flag) {
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_LIKEDCOMMENTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {

                                }else {

                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {

                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {

                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("flag_name","comments_like");
                params.put("entity_id",id);
                params.put("action",flag);
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);

    }



    /*MARKED IN APPROPRIATE ON COMMENTS */

    private void InappropriateFeedComments(final String id) {

        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_MARKEDINAPPROPRIATECOMMENT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    Check();
                                    finish();
                                }else {

                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {

                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {

                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("flag_name","comments_inappropriate");
                params.put("entity_id",id);
                params.put("action","flag");
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);

    }




    /*LISTS OF DELETE COMMENTS ON POSTS*/

    private void DeleteSubjectComments(String id) {

        StringRequest mStrRequest = new StringRequest(Request.Method.DELETE, URLS.URL_DELETECOMMENTS+"/"+id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    Check();
                                    finish();

                                }
                            } else {
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            showServerSnackbar(R.string.error_responce);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                        } else {
                        }
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }




        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);
    }









    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                PAGE_SIZE=0;
                isLoading=false;
                Check();
                ActivityCompat.finishAfterTransition(this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


    private void Check() {

        if (extras.containsKey("open")) {
            if (extras.getString("open").equals("CameFromTeacherAdapter")) {

                CourseModuleGroup.RefreshWorkedStudent();


            } else if (extras.getString("open").equals("CameFromStudentAdapter")) {

                CourseModuleGroup.RefreshWorkedStudent();


            }
        }


    }

    @Override
    public void onBackPressed() {

        PAGE_SIZE=0;
        isLoading=false;
        Check();
        ActivityCompat.finishAfterTransition(this);
    }


}
