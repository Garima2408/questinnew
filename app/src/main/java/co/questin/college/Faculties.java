package co.questin.college;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.CollageFacultyAdapter;
import co.questin.library.StateBean;
import co.questin.models.FacultyArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;


public class Faculties extends BaseAppCompactActivity {

    private RecyclerView.LayoutManager layoutManager;
    private CollageFacultyAdapter collagefacultyadapter;
    RecyclerView rv_collagefaculty;
    static SwipeRefreshLayout swipeContainer;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2, floatingActionButton3,floatingActionButton4;
    public ArrayList<FacultyArray> mFacultyList;
    static TextView ErrorText;
    private static ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.questin.R.layout.activity_faculties);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);


        swipeContainer = findViewById(R.id.swipeContainer);
        ErrorText =findViewById(R.id.ErrorText);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        mFacultyList = new ArrayList<>();
        collagefacultyadapter = new CollageFacultyAdapter(Faculties.this, mFacultyList);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();


        layoutManager = new LinearLayoutManager(this);

        rv_collagefaculty = findViewById(co.questin.R.id.rv_collagefaculty);
        rv_collagefaculty.setHasFixedSize(true);
        rv_collagefaculty.setLayoutManager(layoutManager);
        rv_collagefaculty.setAdapter(collagefacultyadapter);
        materialDesignFAM = findViewById(R.id.material_design_android_floating_action_menu);
        floatingActionButton1 = findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = findViewById(R.id.material_design_floating_action_menu_item2);
        floatingActionButton3 = findViewById(R.id.material_design_floating_action_menu_item3);
        floatingActionButton4 = findViewById(R.id.material_design_floating_action_menu_item4);

        if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("14")) {



            materialDesignFAM.setVisibility(View.VISIBLE);


        }else {
            materialDesignFAM.setVisibility(View.GONE);

        }



        /*Schools*/
        CollageFacultiesList();


        inItView();


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        PAGE_SIZE = 0;
                        isLoading = false;
                        swipeContainer.setRefreshing(true);
                        rv_collagefaculty.setVisibility(View.GONE);
                        CollageFacultiesList();


                    }
                }, 3000);
            }
        });


        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                if(SessionManager.getInstance(getActivity()).getCollage().getType() !=null){
                    if(SessionManager.getInstance(getActivity()).getCollage().getType().matches("Schools")){
                       /* Intent intent = new Intent(Faculties.this, CreateStudentInClass.class);
                        startActivity(intent);
*/

                    }else {
                       /* Intent intent = new Intent(Faculties.this, CreateStudentInClass.class);
                        startActivity(intent);
*/

                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_SUBJECT,"insert subject here");
                        String app_url = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";
                        intent.putExtra(Intent.EXTRA_TEXT,app_url);
                        startActivity(Intent.createChooser(intent,"share via"));
                    }
                }

                }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT,"insert subject here");
                String app_url = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";
                intent.putExtra(Intent.EXTRA_TEXT,app_url);
                startActivity(Intent.createChooser(intent,"share via"));
            }
        });
        floatingActionButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT,"insert subject here");
                String app_url = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";
                intent.putExtra(Intent.EXTRA_TEXT,app_url);
                startActivity(Intent.createChooser(intent,"share via"));

            }
        });
        floatingActionButton4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT,"insert subject here");
                String app_url = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";
                intent.putExtra(Intent.EXTRA_TEXT,app_url);
                startActivity(Intent.createChooser(intent,"share via"));

            }
        });


    }


    private void inItView(){

        swipeContainer.setRefreshing(false);

        rv_collagefaculty.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    collagefacultyadapter.addProgress();
                    CollageFacultiesList();

                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");

        super.onPause();
    }



    private void CollageFacultiesList() {
        swipeContainer.setRefreshing(true);
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_COLLAGEFACULTIES+"?"+"cid="+SessionManager.getInstance(Faculties.this).getCollage().getTnid() +"&offset="+PAGE_SIZE+"&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                rv_collagefaculty.setVisibility(View.VISIBLE);
                                swipeContainer.setRefreshing(false);
                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    ArrayList<FacultyArray> arrayList = new ArrayList<>();

                                    for (int i = 0; i < jsonArrayData.length(); i++) {

                                        FacultyArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), FacultyArray.class);
                                        arrayList.add(CourseInfo);

                                    }
                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {
                                            rv_collagefaculty.setVisibility(View.VISIBLE);
                                            collagefacultyadapter.setInitialData(arrayList);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);


                                        } else {
                                            isLoading = false;
                                            collagefacultyadapter.removeProgress();
                                            swipeContainer.setRefreshing(false);
                                            collagefacultyadapter.addData(arrayList);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);


                                        }
                                    }

                                    else{
                                        if (PAGE_SIZE==0){
                                            rv_collagefaculty.setVisibility(View.GONE);
                                            ErrorText.setVisibility(View.VISIBLE);
                                            ErrorText.setText("No Faculty");
                                            swipeContainer.setVisibility(View.GONE);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);

                                        }else
                                            collagefacultyadapter.removeProgress();

                                    }


                                } else {
                                    swipeContainer.setRefreshing(false);
                                    mShimmerViewContainer.stopShimmerAnimation();
                                    mShimmerViewContainer.setVisibility(View.GONE);

                                }
                            } else {
                                swipeContainer.setRefreshing(false);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            }
                        } catch (Exception e) {
                            swipeContainer.setRefreshing(false);


                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipeContainer.setRefreshing(false);
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(Faculties.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(Faculties.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(Faculties.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(Faculties.this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);

    }


    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                PAGE_SIZE=0;
                isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

}


