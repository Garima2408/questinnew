package co.questin.college;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.soundcloud.android.crop.Crop;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.library.StateBean;
import co.questin.models.FilterArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

public class StudentCommentScreen extends BaseAppCompactActivity implements View.OnClickListener {
    ImageView profilepic;
    ImageView Camera, Smilely, AddGallary, AddAttachments,imagepreview,cancelPreview;
    ImageButton button_send;
    TextView use_name;
    private Uri currentImageUri;
    private static final int RC_TAKE_PICTURE = 1;
    private static final int RC_SELECT_IMAGE = 2;
    private static final int REQUEST_PATH = 3;
    Bundle extras;
    Bitmap thumbnail = null;
    RelativeLayout previewlayout;
    //  EditText blog_content;
    Spinner filterSpinner;
    String PostContent, Course_id,parent_id;
    private ArrayList<String> filterlist;
    ArrayList<FilterArray> listoffilter;
    RelativeLayout loading_view;
    private ProgressDialog progressDialog;
    View rootView;
    static String strFile = null;
    String Fileimagename,fullPath;
    private static final int REQUEST_CODE = 2;
    EditText emojiconEditText;
    private File selectedFile;
    Uri imageUri;
    /*new cam/gallary*/
    private static final int ACTIVITY_START_CAMERA_APP = 0;
    private String mImageFileLocation = "";
    private int SELECT_FILE = 1;
    public static final int RequestPermissionCode = 1;
    boolean ExternslWriteAccepted,ExternslreadAccepted,cameraAccepted;
    Boolean CallingCamera,CallingGallary,CallingAttachment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_comment_screen);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        rootView = findViewById(R.id.root_view);


        Intent intent = getIntent();
        extras = intent.getExtras();

        Course_id = intent.getStringExtra("COURSE_ID");
        parent_id = intent.getStringExtra("PARENT_ID");

        Log.d("TAG", "Course_id: " + Course_id);


        filterlist = new ArrayList<String>();
        listoffilter = new ArrayList<FilterArray>();
        emojiconEditText = findViewById(R.id.emojicon_edit_text);
        profilepic = findViewById(R.id.profilepic);
        use_name = findViewById(R.id.use_name);

        filterSpinner = findViewById(R.id.filterSpinner);
        button_send = findViewById(R.id.button_send);
        Camera = findViewById(R.id.Camera);
        Smilely = findViewById(R.id.Smilely);
        AddGallary = findViewById(R.id.AddGallary);
        AddAttachments = findViewById(R.id.AddAttachments);
        imagepreview  =findViewById(R.id.imagepreview);
        cancelPreview  =findViewById(R.id.cancelPreview);
        previewlayout  =findViewById(R.id.previewlayout);


        button_send.setOnClickListener(this);
        Camera.setOnClickListener(this);
        Smilely.setOnClickListener(this);
        AddGallary.setOnClickListener(this);
        AddAttachments.setOnClickListener(this);
        cancelPreview.setOnClickListener(this);


        displayUserData();
    }


    private void displayUserData() {
        if (getUser() != null) {
            if (!co.questin.utils.TextUtils.isNullOrEmpty(getUser().FirstName)) {
                use_name.setText(getUser().FirstName + " " + getUser().LastName);
            }
            if (!co.questin.utils.TextUtils.isNullOrEmpty(getUser().LastName)) {
            }

            if (!co.questin.utils.TextUtils.isNullOrEmpty(getUser().photo)) {

                Glide.with(this).load(getUser().photo)
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(profilepic);

            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.button_send:
                hideKeyBoard(v);
                AttempToAddComments();
                break;

            case R.id.Camera:
                hideKeyBoard(v);
                CallingCamera =true;
                CallingGallary =false;
                CallingAttachment =false;
                if(checkPermission()){
                    CallCamera();
                }



                break;

            case R.id.Smilely:
              /*  emojIcon = new EmojIconActions(this, rootView, emojiconEditText, Smilely);
                emojIcon.ShowEmojIcon();
                emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.mipmap.smilelyimage);
                emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
                    @Override
                    public void onKeyboardOpen() {
                        Log.e("TAG", "Keyboard opened!");
                    }

                    @Override
                    public void onKeyboardClose() {
                        Log.e("TAG", "Keyboard closed");
                    }
                });*/

                break;


            case R.id.AddGallary:
                hideKeyBoard(v);
                imagepreview.setImageDrawable(null);
                CallingGallary =true;
                CallingAttachment =false;
                CallingCamera =false;

                if(checkPermission()){
                    galleryIntent();
                }


                break;


            case R.id.AddAttachments:
                hideKeyBoard(v);
                CallingAttachment =true;
                CallingCamera =false;
                CallingGallary =false;


                if(checkPermission()){
                    AddValueFromTheBrows();
                }
                break;

            case R.id.cancelPreview:
                hideKeyBoard(v);
                previewlayout.setVisibility(View.GONE);
                strFile ="";
                Fileimagename="";


                break;



        }
    }


    private void CallCamera() {
        imagepreview.setImageDrawable(null);
        Intent callCameraApplicationIntent = new Intent();
        callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        File photoFile = null;
        try {
            photoFile = createImageFile();

        } catch (IOException e) {
            e.printStackTrace();
        }
        imageUri = FileProvider.getUriForFile(StudentCommentScreen.this, "co.questin.fileprovider",photoFile);

        callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(callCameraApplicationIntent, ACTIVITY_START_CAMERA_APP);
    }


    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }


    private void AddValueFromTheBrows() {

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        String[] mimeTypes = {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                "text/plain",
                "application/pdf"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, REQUEST_CODE);



    }

    private void galleryIntent()
    {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(Intent.createChooser(galleryIntent, "Select File"),SELECT_FILE);
    }



    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if(requestCode == ACTIVITY_START_CAMERA_APP && resultCode == RESULT_OK) {
            beginCrop(imageUri);

        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
        }
        if (requestCode == Crop.REQUEST_CROP) {

            handleCrop(resultCode, data);

        }

        if (requestCode == REQUEST_CODE &&resultCode == RESULT_OK) {

            Uri FileURI = data.getData();
            String uriString = FileURI.toString();
            selectedFile = new File(uriString);
            try {
                fullPath = Utils.getPath(this,FileURI);
            }catch (NumberFormatException e){

                System.out.println("not a number");

            } catch (Exception e){

                System.out.println(e);
            }


            String FileName = Utils.getDataColumn(this,FileURI,null,null);

            Log.d("TAG", "onActivityResult: " + FileName + FileName );

            String path = selectedFile.toString();

            String filepath = path;

            String displayName = null;

            if (uriString.startsWith("content://")) {

                Cursor cursor = null;

                try {

                    cursor = getContentResolver().query(FileURI, null, null, null, null);

                    if (cursor != null && cursor.moveToFirst()) {

                        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);

                        Fileimagename = cursor.getString(nameIndex);

                        Log.d("TAG", "onActivityResult: " + Fileimagename + filepath + fullPath);

                        /*Log.d("TAG", "onActivityResult2: " + Fileimagename);*/


                            if (fullPath != null) {


                                if (filepath.matches("(.*)providers(.*)")) {

                                    String fullfilePath = fullPath + "/" + Fileimagename;

                                    convertFileToString(fullfilePath, Fileimagename);

                                    Log.d("TAG", "onActivityResult3: " + fullfilePath);
                                } else if (filepath.matches("(.*)externalstorage(.*)")) {

                                    fullPath = fullPath;
                                    convertFileToString(fullPath, Fileimagename);
                                    Log.d("TAG", "onActivityResult4: " + fullPath);
                                }


                            } else {
                                Toast.makeText(this, "File not found !! Get it from internal/external storage", Toast.LENGTH_SHORT).show();
                            }

                        }



                }catch (Exception e )
                {
                    e.printStackTrace();
                }

            }




        }



    }




    public String convertFileToString(String pathOnSdCard, String curFileName){

        File file = new File(pathOnSdCard);

        try {

            byte[] data = FileUtils.readFileToByteArray(file);//Convert any file, image or video into byte array


            // byte[] data = FileUtils.readFileToByteArray(file); //Convert any file, image or video into byte array

            strFile = Base64.encodeToString(data, Base64.NO_WRAP);

            //Convert byte array into string

            System.out.println("file in bitmap first method " + strFile);

            Fileimagename =curFileName;

            Log.d("TAG", "onActivityResult2: " + strFile);

            System.out.println("filename in bitmap first method " + Fileimagename);

            // CreateFileMsg(strFile,curFileName);

            String Extension = Fileimagename.substring(Fileimagename.lastIndexOf("."));

            imagepreview.setImageDrawable(null);
            imagepreview.setBackgroundResource(R.mipmap.file);
            previewlayout.setVisibility(View.VISIBLE);



        } catch (Exception e) {

            e.printStackTrace();

        }

        return strFile;

    }




    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                beginCrop(data.getData());
                Uri uri = data.getData();
                String[] projection = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                Log.d("TAG", DatabaseUtils.dumpCursorToString(cursor));

                int columnIndex = cursor.getColumnIndex(projection[0]);
                Fileimagename = cursor.getString(columnIndex); // full path of image

                cursor.close();

            }catch (Exception e)
            {
                e.printStackTrace();
            }


        }

    }





    File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        Fileimagename = image.getName();
        return image;

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(result));
                thumbnail = getResizedBitmap(thumbnail,1000);
                strFile = encodeImageTobase64(thumbnail);
                System.out.println("camera " + strFile);
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setImageBitmap(thumbnail);

            } catch (IOException e) {
                e.printStackTrace();
            }




        } else if (resultCode == Crop.RESULT_ERROR) {
            //  Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }



    private void AttempToAddComments() {

        emojiconEditText.setError(null);
        if(strFile ==null){
            strFile ="";
        }
        if(Fileimagename ==null){
            Fileimagename ="";
        }
        PostContent = emojiconEditText.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (android.text.TextUtils.isEmpty(PostContent)) {
            focusView = emojiconEditText;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            if (!isInternetConnected()){
                showConnectionSnackbar();
            }else {
                ShowIndicator();
                CourseAddComments();


            }
        }
    }





    /*ADD COMMENTS ON SUBJECTS*/


    private void CourseAddComments() {
        StringRequest mStrRequest = new StringRequest(Request.Method.POST,  URLS.URL_SUBJECTSADDCOMMENTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    emojiconEditText.setText("");
                                    Utils.hideKeyBoard(StudentCommentScreen.this, emojiconEditText);
                                    strFile="";
                                    Fileimagename="";
                                    Check();
                                    finish();


                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());

                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        } else {
                            hideIndicator();
                            showConnectionSnackbar();

                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                String Cookies= SessionManager.getInstance(StudentCommentScreen.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(StudentCommentScreen.this).getaccesstoken().sessionID;
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(StudentCommentScreen.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("nid", Course_id);
                        params.put("pid", parent_id);
                        params.put("comment_body", PostContent);
                        params.put("field_comment_attachments[file]",strFile);
                        params.put("field_comment_attachments[filename]",Fileimagename);
                        params.put("field_comment_attachments[description]","SubjectAttach");


                return params;
            }



        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);



    }

    private void Check() {

        if (extras.containsKey("open")) {
            if (extras.getString("open").equals("CameFromStudentCommentreply")) {

                SubjectCommentsReply.RefreshWorked();


            } else if (extras.getString("open").equals("CameFromStudentCommentreplyAdapter")) {

                SubjectCommentsReply.RefreshWorked();


            }
        }
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
               // TeacherCourceGroup.RefreshWorked();
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
    @Override
    public void onBackPressed() {
        finish();
    }

    private  boolean checkPermission() {
        int camerapermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionLocation = ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), RequestPermissionCode);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("TAG", "Permission callback called-------");
        switch (requestCode) {
            case RequestPermissionCode: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("TAG", "sms & location services permission granted");
                        if (CallingCamera ==true){
                            CallingGallary=false;
                            CallCamera();

                        }else if (CallingGallary ==true) {
                            CallingCamera=false;

                            galleryIntent();


                        }else if (CallingAttachment==true){
                            AddValueFromTheBrows();

                        }
                    } else {
                        Log.d("TAG", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Service Permissions are required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkPermission();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    finish();
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
    private void explain(String msg){
        final android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        //  permissionsclass.requestPermission(type,code);
                        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:com.exampledemo.parsaniahardik.marshmallowpermission")));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        finish();
                    }
                });
        dialog.show();
    }
}
