package co.questin.college;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.MediaDetailsAdapter;
import co.questin.adapters.MediaGridAdapter;
import co.questin.library.StateBean;
import co.questin.models.MediaArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class CollageMediaDetails extends BaseAppCompactActivity {
    String Media_id,MediaTittle,mediaDetails;
    TextView text_title,textDetails ;
    ImageView iv_vedio_icon;
    LinearLayout barlayout;
    YouTubeThumbnailView youTubeThumbnailView;
    Dialog imageDialog,vedioDialog;
    String img_url;
    int logos[];
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    ViewPager pagerImage;
    PagerAdapter Imageadapter;
    public RecyclerView rv_mediagrid;
    //  GridView rv_mediagrid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage_media_details);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        Bundle b = getIntent().getExtras();
        Media_id = b.getString("MEDIA_ID");
        MediaTittle = b.getString("MEDIA_TITLE");


        textDetails = findViewById(R.id.textDetails);
        text_title = findViewById(R.id.text_title);
        iv_vedio_icon = findViewById(R.id.iv_vedio_icon);
        youTubeThumbnailView = findViewById(R.id.thumbnail);
        rv_mediagrid = findViewById(R.id.rv_mediagrid);
        CollageMediaDetailsShow();
       


    }

    private void CollageMediaDetailsShow() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_COLLAGMEDIADETAILS+"/"+Media_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONObject data = obj.getJSONObject("data");

                                    String tnd = data.getString("nid");
                                    MediaTittle = data.getString("title");
                                    mediaDetails= data.getString("body");
                                    String mediaType = data.getString("field_media_type");

                                    text_title.setText(MediaTittle);
                                    textDetails.setText(Html.fromHtml(mediaDetails));
                                    //  setGridViewHeaderAndFooter(MediaTittle,mediaDetails);

                                    final MediaArray medialists = new MediaArray(0);

                                    if (mediaType.equals("image")){

                                        final JSONArray picture = data.getJSONArray("field_forum_images");

                                        for(int j=0; j<picture.length(); j++){
                                            Log.e("Media pictire", picture.getString(j));

                                            medialists.setField_forum_images(picture.getString(j));

                                            SetValueTogird(picture);



                                        }



                                    }else if (mediaType.equals("video"))
                                    {
                                        rv_mediagrid.setVisibility(View.GONE);
                                        JSONArray vedio = data.getJSONArray("field_ffile");
                                        for(int j=0; j<vedio.length(); j++){

                                            Log.e("Media vedio", vedio.getString(0));

                                            try {
                                                final String videoId=extractYoutubeId(vedio.getString(0));
                                                Log.e("VideoId is->","" + videoId);
                                                img_url="http://img.youtube.com/vi/"+videoId+"/0.jpg"; // this is link which will give u thumnail image of that video


                                                Glide.with(CollageMediaDetails.this).load(img_url)
                                                        .placeholder(R.color.black).dontAnimate()
                                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                        .into(youTubeThumbnailView);


                                                youTubeThumbnailView.setVisibility(View.VISIBLE);
                                                text_title.setVisibility(View.VISIBLE);
                                                textDetails.setVisibility(View.VISIBLE);
                                                iv_vedio_icon.setVisibility(View.VISIBLE);
                                                iv_vedio_icon.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {

                                                        Intent intent = new Intent(CollageMediaDetails.this, CollageMediaDisplay.class);
                                                        intent.putExtra("url",videoId);
                                                        startActivity(intent);


                                                    }
                                                });




                                            } catch (MalformedURLException e) {
                                                e.printStackTrace();
                                            }


                                        }
                                    }
                                    else{
                                        showSnackbarMessage("No Media is Availabe here");

                                    }



                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(CollageMediaDetails.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(CollageMediaDetails.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(CollageMediaDetails.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);

    }

    private void SetValueTogird(final JSONArray picture) {


        List<String> list = new ArrayList<>();
        for(int i=0; i<picture.length(); i++){
            try {
                list.add(picture.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
      //  rv_mediagrid.setAdapter(new CollagemediaGrirdAdapter(getActivity(), list));
        rv_mediagrid.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        rv_mediagrid.setLayoutManager(layoutManager);

        rv_mediagrid.setAdapter(new MediaGridAdapter(getActivity(), list));



    }

    public String extractYoutubeId(String url) throws MalformedURLException {
        String query = new URL(url).getQuery();
        String[] param = query.split("&");
        String id = null;
        for (String row : param) {
            String[] param1 = row.split("=");
            if (param1[0].equals("v")) {
                id = param1[1];
            }
        }
        return id;
    }








    private void imageDialogmultiple(JSONArray data){


        imageDialog = new Dialog(CollageMediaDetails.this);
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.BLACK));
        imageDialog.setContentView(R.layout.multiple_images_display);
        // dialogLogin.setCancelable(false);
        imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        imageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        imageDialog.show();
        pagerImage = imageDialog. findViewById(R.id.pager);
        List<String> list = new ArrayList<>();
        for(int i=0; i<data.length(); i++){
            try {
                list.add(data.getString(i));
                pagerImage.setOnClickListener(onChagePageClickListener(i));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Imageadapter = new MediaDetailsAdapter(CollageMediaDetails.this, list);
        pagerImage.setAdapter(Imageadapter);

    }

    private View.OnClickListener onChagePageClickListener(final int i) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pagerImage.setCurrentItem(i);
            }
        };
    }



    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

               // PAGE_SIZE=0;
                // isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


}








