package co.questin.college;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.CourseMembersAdapter;
import co.questin.adapters.SubjectsCommentsAdapter;
import co.questin.campusfeedsection.AllCampusFeeds;
import co.questin.library.StateBean;
import co.questin.models.CourseMemberLists;
import co.questin.models.SubjectCommentArray;
import co.questin.network.URLS;
import co.questin.studentprofile.ProfileDash;
import co.questin.teacher.TeacherCourseModule;
import co.questin.teacher.TeacherCourseModuleDetails;
import co.questin.teacher.TeacherProfile;
import co.questin.utils.BaseFragment;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

import static co.questin.utils.BaseAppCompactActivity.ShowIndicator;
import static co.questin.utils.BaseAppCompactActivity.hideIndicator;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;
import static co.questin.utils.Utils.isStatusSuccess;

public class CourseModuleGroup extends BaseFragment {
    String Course_id,comments;
    static String Course_idMain,Adminuser,Join_Value,is_Student;
    RecyclerView GroupMember_Lists;
    static RecyclerView rv_subject_comment;
    static Button JoinGroup;
    CardView card_view1;
    RecyclerView.LayoutManager mLayoutManager,layoutManager;
    CourseMembersAdapter mGroupMemberAdapter;
    static SubjectsCommentsAdapter mcommentAdapter;
    public ArrayList<CourseMemberLists> memberList;
    public static ArrayList<SubjectCommentArray> commentsList;
    static TextView ErrorText;
    static Activity activity;
    static String tittle,URLDoc;
    static SwipeRefreshLayout swipeContainer;
    private static ProgressBar spinner;
    static FloatingActionButton fab;
    private static boolean isLoading= true;
    private boolean _hasLoadedOnce= false; // your boolean field
    private static ShimmerFrameLayout mShimmerViewContainer;
    private static int PAGE_SIZE = 0;

    public CourseModuleGroup() {


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(isFragmentVisible_);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                Log.e("view", "CollegeNewFeed");
                PAGE_SIZE = 0;
                isLoading = false;
                swipeContainer.setRefreshing(true);
                if (Join_Value.equals("member")) {
                    mShimmerViewContainer.startShimmerAnimation();
                    SubjectsCommentsList(Course_idMain);


                }


                _hasLoadedOnce = true;
            }
             GroupMemberList(Course_idMain);


        }
    }

    @Override
    public void onPause() {

        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");
        _hasLoadedOnce= false;
        super.onPause();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_course_module_group, container, false);
        activity = (Activity) view.getContext();

        Course_id = getArguments().getString("COURSE_ID");
        Course_idMain= getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        Join_Value = getArguments().getString("JOINED");

        swipeContainer = view.findViewById(R.id.swipeContainer);

        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        Log.d("TAG", "Course_idMain: " + Course_idMain);
        JoinGroup = view.findViewById(R.id.JoinGroup);
        card_view1 = view.findViewById(R.id.card_view1);
        GroupMember_Lists = view.findViewById(R.id.GroupMember_Lists);
        spinner= view.findViewById(R.id.progressBar);
        rv_subject_comment = view.findViewById(R.id.rv_subject_comment);
        mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        layoutManager = new LinearLayoutManager(activity);
        GroupMember_Lists.setLayoutManager(mLayoutManager);
        rv_subject_comment.setHasFixedSize(true);
        rv_subject_comment.setLayoutManager(layoutManager);
        fab = view. findViewById(R.id.fab);
        ErrorText= view. findViewById(R.id.ErrorText);
        memberList = new ArrayList<CourseMemberLists>();
        commentsList = new ArrayList<SubjectCommentArray>();
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mcommentAdapter = new SubjectsCommentsAdapter(rv_subject_comment, commentsList ,activity);
        rv_subject_comment.setAdapter(mcommentAdapter);

        /*CALLING LISTS OF STUDENTS*/
         GroupMemberList(Course_idMain);




        JoinGroup.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("14")) {
                    ProgressJoinTheGroupRequest(Course_idMain);


                }else {
                    ProgressJoinTheGroupRequestStudent(Course_idMain);
                }



            }
        });

        if (Adminuser.equals("TRUE")) {


        } else if (Adminuser.equals("FALSE")) {


        }

        if (Join_Value.equals("member")) {
            SubjectsCommentsList(Course_idMain);
            mShimmerViewContainer.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
            JoinGroup.setVisibility(View.GONE);
            mShimmerViewContainer.startShimmerAnimation();

            rv_subject_comment.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (dy > 0) {
                        fab.hide();
                    } else if (dy < 0) {
                        fab.show();
                    }
                }
            });


            Animation makeInAnimation = AnimationUtils.makeInAnimation(activity,false);
            makeInAnimation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) { }

                public void onAnimationRepeat(Animation animation) { }

                public void onAnimationStart(Animation animation) {
                    fab.setVisibility(View.VISIBLE);
                }
            });

            Animation makeOutAnimation = AnimationUtils.makeOutAnimation(activity,true);
            makeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationEnd(Animation animation) {
                    fab.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) { }

                @Override
                public void onAnimationStart(Animation animation) { }
            });


            if (fab.isShown()) {
                fab.startAnimation(makeOutAnimation);
            }

            if (!fab.isShown()) {
                fab.startAnimation(makeInAnimation);
            }


        } else if (Join_Value.equals("pending")) {

            fab.setVisibility(View.VISIBLE);
            JoinGroup.setVisibility(View.GONE);
            mShimmerViewContainer.stopShimmerAnimation();
            mShimmerViewContainer.setVisibility(View.GONE);
            swipeContainer.setVisibility(View.GONE);
        }
        else if (Join_Value.equals("nomember")) {

            swipeContainer.setVisibility(View.GONE);
            JoinGroup.setText("Join");
            JoinGroup.setVisibility(View.VISIBLE);
            fab.setVisibility(View.GONE);
            mShimmerViewContainer.stopShimmerAnimation();
            mShimmerViewContainer.setVisibility(View.GONE);

        }



        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent backIntent = new Intent(activity, AddSubjectComment.class)
                        .putExtra("COURSE_ID",Course_id)
                        .putExtra("SAME","1")
                        .putExtra("open","StudentCourceGroup");
                startActivity(backIntent);




            }
        });






        return view;
    }




    private void inItView(){
        swipeContainer.setVisibility(View.VISIBLE);
        swipeContainer.setRefreshing(false);
        CallTheSpin();
        rv_subject_comment.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    mcommentAdapter.addProgress();

                   SubjectsCommentsList(Course_idMain);

                    //fetchData(fYear,,);
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }








    public static void RefreshWorkedStudent() {
        PAGE_SIZE=0;
        isLoading=true;
        /* CALLING LISTS OF COMMENTS*/
        SubjectsCommentsList(Course_idMain);



    }



    /*STUDENTS ON THE SUBJECTS*/



    private void GroupMemberList(String course_idMain) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET,URLS.URL_COLLAGECOURCEMEMBERLISTS+course_idMain+"/users?fields=id,etid,%20gid,%20state&parameters=state=1",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    memberList.clear();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {


                                        for (int i = 0; i < jsonArrayData.length(); i++) {

                                            CourseMemberLists CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CourseMemberLists.class);
                                            memberList.add(CourseInfo);
                                            if (CourseInfo.getEtid().equals(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id()))
                                            {
                                                Log.d("TAG", "myid: " + SessionManager.getInstance(getActivity()).getUser().getUserprofile_id());

                                                JoinGroup.setVisibility(View.GONE);
                                                inItView();


                                            }else {

                                            }


                                        }
                                        mGroupMemberAdapter = new CourseMembersAdapter(activity, R.layout.list_of_groupmemberlists,memberList);
                                        GroupMember_Lists.setAdapter(mGroupMemberAdapter);
                                        card_view1.setVisibility(View.VISIBLE);

                                    }else {


                                    }


                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);

    }



    @Override
    public void onResume() {
        super.onResume();

    }
    private void CallTheSpin()
    {

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {


                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        if (!isLoading){
                            RefreshWorkedStudent();
                        }else{
                            isLoading=false;
                            swipeContainer.setRefreshing(false);
                        }




                    }
                }, 3000);
            }
        });


    }

    /*LISTS OF COMMENTS ON SUBJECTS*/


    private static void SubjectsCommentsList(String course_idMain) {
        swipeContainer.setRefreshing(true);
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_SUBJECTSCOMMENTS+"?"+"sid"+"="+course_idMain+"&offset="+PAGE_SIZE+"&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    swipeContainer.setRefreshing(false);
                                    JSONObject obj = new JSONObject(response);
                                    ArrayList<SubjectCommentArray> arrayList = new ArrayList<>();
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {



                                        for (int i = 0; i < jsonArrayData.length(); i++) {

                                            SubjectCommentArray subjects = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), SubjectCommentArray.class);

                                            JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                                            if (jsonObject.has("field_comment_attachments")){
                                                JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                                if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0 ){
                                                    URLDoc = AttacmentsFeilds.getString("value");
                                                    tittle = AttacmentsFeilds.getString("title");
                                                    subjects.title =tittle;
                                                    subjects.url =URLDoc;

                                                }else {

                                                }
                                            }
                                            arrayList.add(subjects);



                                        }

                                    }

                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {


                                            rv_subject_comment.setVisibility(View.VISIBLE);
                                            mcommentAdapter.setInitialData(arrayList);
                                            swipeContainer.setRefreshing(false);

                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);


                                        } else {
                                            mcommentAdapter.removeProgress();
                                            swipeContainer.setRefreshing(false);
                                            mcommentAdapter.addData(arrayList);


                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);



                                        }

                                        isLoading = false;

                                    } else{
                                        if (PAGE_SIZE==0){
                                            rv_subject_comment.setVisibility(View.GONE);
                                            ErrorText.setVisibility(View.VISIBLE);
                                            ErrorText.setText("No Post");
                                            swipeContainer.setVisibility(View.GONE);
                                            swipeContainer.setRefreshing(false);

                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);

                                        }else
                                            mcommentAdapter.removeProgress();

                                    }


                                } else {
                                    swipeContainer.setRefreshing(false);
                                    mShimmerViewContainer.stopShimmerAnimation();
                                    mShimmerViewContainer.setVisibility(View.GONE);

                                }
                            } else {
                                swipeContainer.setRefreshing(false);

                            }
                        } catch (Exception e) {
                            swipeContainer.setRefreshing(false);


                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipeContainer.setRefreshing(false);
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);


    }




    /*SEND REQUEST FOR JOINING SUBJECTS*/




    private void ProgressJoinTheGroupRequestStudent(final String courseid) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_JOININSUBJECTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    fab.setVisibility(View.VISIBLE);
                                    JoinGroup.setVisibility(View.GONE);
                                    mShimmerViewContainer.startShimmerAnimation();
                                    CallAboveMethods();


                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("entity_type", "user");
                params.put("etid", SessionManager.getInstance(getActivity()).getUser().userprofile_id);
                params.put("group_type", "node");
                params.put("gid", courseid);
                params.put("state", "1");

                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);

    }


    private void ProgressJoinTheGroupRequest(String courseid) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_ADDSTUDENTINSUBJECT+"/"+courseid+"/"+SessionManager.getInstance(getActivity()).getUser().userprofile_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    fab.setVisibility(View.VISIBLE);
                                    JoinGroup.setVisibility(View.GONE);
                                    mShimmerViewContainer.startShimmerAnimation();
                                    CallAboveMethods();


                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type","subject");
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);

    }



    private void CallWorked() {
        JoinGroup.setText("Request Sent");
        JoinGroup.setOnClickListener(null);
    }

    public static void CallAboveMethods() {

        JoinGroup.setVisibility(View.GONE);
        commentsList.clear();
        /* CALLING LISTS OF COMMENTS*/
         SubjectsCommentsList(Course_idMain);



    }

    @Override
    public void onDestroy() {
        isLoading=false;
        PAGE_SIZE=0;
        activity.finish();
        super.onDestroy();
    }


}

