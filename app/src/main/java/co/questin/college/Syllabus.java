package co.questin.college;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import co.questin.R;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;

public class Syllabus extends BaseAppCompactActivity {
    private WebView wv1;
    private ProgressBar progressBar;
    String UniversityName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syllabus);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        wv1= findViewById(R.id.webView);

        wv1.setWebViewClient(new MyBrowser());

        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        ShowIndicator();
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        if(SessionManager.getInstance(getActivity()).getCollage().getAffiliation() !=null){


            UniversityName = SessionManager.getInstance(getActivity()).getCollage().getAffiliation().replaceAll("\\s", "-");

            Log.d("TAG", "UniversityName: " + UniversityName);
        }

        wv1.loadUrl(URLS.QUESTIN_BASE_URL+"/university-syllabus/"+UniversityName);
        wv1.setWebViewClient(new WebViewClient(){

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                hideIndicator();

            }
        });

    }



    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    @Override
    public void onBackPressed() {

        finish();
    }


}
