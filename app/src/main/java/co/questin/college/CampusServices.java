package co.questin.college;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.CampusServicesAdapter;
import co.questin.library.StateBean;
import co.questin.models.CampusServiceArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class CampusServices extends BaseAppCompactActivity {


    public ArrayList<CampusServiceArray> mServiceList;
    private RecyclerView.LayoutManager layoutManager;
    private CampusServicesAdapter serviceadapter;
    RecyclerView rv_CampusServices;
    SwipeRefreshLayout swipeContainer;
    static TextView ErrorText;
    private  boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    private static ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.questin.R.layout.activity_campus_services);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        mServiceList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(CampusServices.this);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();


        rv_CampusServices = findViewById(co.questin.R.id.rv_CampusServices);
        ErrorText =findViewById(R.id.ErrorText);

        swipeContainer =findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        rv_CampusServices.setHasFixedSize(true);
        rv_CampusServices.setLayoutManager(layoutManager);
        serviceadapter = new CampusServicesAdapter(rv_CampusServices, mServiceList ,CampusServices.this);
        rv_CampusServices.setAdapter(serviceadapter);
        CampusServicesDisplay();
        inItView();





        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        PAGE_SIZE=0;
                        isLoading=false;
                        swipeContainer.setRefreshing(true);
                        CampusServicesDisplay();
                    }
                }, 3000);
            }
        });






    }

    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");

        super.onPause();
    }



    private void inItView() {


        swipeContainer.setRefreshing(false);

        rv_CampusServices.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){

                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    serviceadapter.addProgress();
                    CampusServicesDisplay();

                }else{
                    Log.i("loadinghua", "im else now");

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });


    }

    private void CampusServicesDisplay() {
        swipeContainer.setRefreshing(true);
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_CAMPUSSERVICES+"?"+"cid="+SessionManager.getInstance(getActivity()).getCollage().getTnid() +"&offset="+PAGE_SIZE+"&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                swipeContainer.setRefreshing(false);
                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    ArrayList<CampusServiceArray> arrayList = new ArrayList<>();

                                    for (int i = 0; i < jsonArrayData.length(); i++) {

                                        CampusServiceArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CampusServiceArray.class);
                                        arrayList.add(CourseInfo);

                                    }
                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {


                                            rv_CampusServices.setVisibility(View.VISIBLE);
                                            serviceadapter.setInitialData(arrayList);


                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);



                                        } else {
                                            isLoading = false;
                                            serviceadapter.removeProgress();
                                            swipeContainer.setRefreshing(false);

                                            serviceadapter.addData(arrayList);

                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);

                                            }

                                    }else{
                                        if (PAGE_SIZE==0){
                                            rv_CampusServices.setVisibility(View.GONE);
                                            ErrorText.setVisibility(View.VISIBLE);

                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);



                                            ErrorText.setText("No Services");
                                            swipeContainer.setVisibility(View.GONE);
                                        }else
                                            serviceadapter.removeProgress();

                                    }
                                    // view.setVisibility(View.GONE);


                                } else  {
                                    showSnackbarMessage(stateResponse.getMessage());
                                    swipeContainer.setRefreshing(false);
                                    mShimmerViewContainer.stopShimmerAnimation();
                                    mShimmerViewContainer.setVisibility(View.GONE);



                                }
                            }
                        } catch (Exception e) {
                            swipeContainer.setRefreshing(false);


                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipeContainer.setRefreshing(false);
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(CampusServices.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(CampusServices.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(CampusServices.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(CampusServices.this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }

    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                PAGE_SIZE=0;
                isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

}


