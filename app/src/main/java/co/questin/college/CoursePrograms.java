package co.questin.college;

import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.CourseOfferedAdapter;
import co.questin.library.StateBean;
import co.questin.models.CourseOfferedList;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class CoursePrograms extends BaseAppCompactActivity {
    GridView course_information;
    ArrayList<CourseOfferedList> CourseInformationList;
    public static int[] CourseInformationImages = {
            R.mipmap.accomodation,
            R.mipmap.club_society,
            R.mipmap.mba,
            R.mipmap.bba,
            R.mipmap.bca,
            R.mipmap.mca,
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_programs);
          GetTheListOfCourse();
        CourseInformationList = new ArrayList<CourseOfferedList>();
        course_information = findViewById(R.id.course_information);

    }



    private void GetTheListOfCourse() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_COLLAGEPROVIDED+"?"+"cid="+SessionManager.getInstance(getActivity()).getCollage().getTnid(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");


                                    for (int i=0; i<jsonArrayData.length();i++) {
                                        CourseOfferedList GoalInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CourseOfferedList.class);
                                        CourseInformationList.add(GoalInfo);
                                        course_information.setAdapter(new CourseOfferedAdapter(getActivity(),CourseInformationList, CourseInformationImages));

                                    }


                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(CoursePrograms.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(CoursePrograms.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(CoursePrograms.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);

    }




    @Override
    public void onBackPressed() {
        finish();
    }
}








