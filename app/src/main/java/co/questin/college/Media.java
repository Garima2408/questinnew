package co.questin.college;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.MediaAdapter;
import co.questin.library.StateBean;
import co.questin.models.MediaArray;
import co.questin.network.URLS;
import co.questin.settings.FAQ;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class Media extends BaseAppCompactActivity {

    public ArrayList<MediaArray> mmediaList;
    private RecyclerView.LayoutManager layoutManager;
    private MediaAdapter mediaeadapter;
    RecyclerView rv_Collagemedia;
    FloatingActionButton fab;
    SwipeRefreshLayout swipeContainer;
    private static boolean isLoading = false;
    private static int PAGE_SIZE = 0;
    static TextView ErrorText;
    private static ShimmerFrameLayout mShimmerViewContainer;
    String gallery, video_url, media_type_demo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        ErrorText =findViewById(R.id.ErrorText);

        rv_Collagemedia = findViewById(R.id.rv_Collagemedia);
        swipeContainer =findViewById(R.id.swipeContainer);
        mmediaList = new ArrayList<>();
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
         mShimmerViewContainer.startShimmerAnimation();
        mediaeadapter = new MediaAdapter(this, mmediaList);
        layoutManager = new LinearLayoutManager(this);
        fab = findViewById(R.id.fab);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Media.this, FAQ.class));

            }
        });

        rv_Collagemedia.setHasFixedSize(true);
        rv_Collagemedia.setLayoutManager(layoutManager);
        rv_Collagemedia.setAdapter(mediaeadapter);
        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        inItView();
        CollageMediaShow();
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh

                        PAGE_SIZE = 0;
                        isLoading = false;
                        swipeContainer.setRefreshing(true);
                        CollageMediaShow();


                    }
                }, 3000);
            }
        });




    }

    @Override
    public void onResume() {
        super.onResume();

          mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","media");

        super.onPause();
    }


    private void inItView() {

        swipeContainer.setRefreshing(false);

        rv_Collagemedia.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if (!isLoading) {


                    Log.i("loadinghua", "im here now");
                    isLoading = true;
                    PAGE_SIZE += 20;

                    mediaeadapter.addProgress();
                    CollageMediaShow();

                } else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    private void CollageMediaShow() {
        swipeContainer.setRefreshing(true);
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_COLLAGMEDIA + "?" + "cid" + "=" + SessionManager.getInstance(getActivity()).getCollage().getTnid() + "&offset=" + PAGE_SIZE + "&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    rv_Collagemedia.setVisibility(View.VISIBLE);
                                    swipeContainer.setRefreshing(false);
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    ArrayList<MediaArray> arrayList = new ArrayList<>();

                                    for (int i = 0; i < jsonArrayData.length(); i++) {

                                        MediaArray CourseInfo = new MediaArray(0);
                                        arrayList.add(CourseInfo);
                                        JSONObject jsonObject1 = jsonArrayData.optJSONObject(i);
                                        String tnd = jsonObject1.optString("tnid");
                                        String title = jsonObject1.optString("title");


                                        JSONArray mediaType = jsonObject1.getJSONArray("media_type");
                                        if(mediaType != null && mediaType.length() > 0 ) {
                                            for (int j = 0; j < mediaType.length(); j++) {

                                                media_type_demo =mediaType.getString(j);


                                                Log.e("mediaType", mediaType.getString(j));

                                            }
                                        }

                                        if (media_type_demo.equals("Image Gallery")){
                                            if(jsonObject1.has("gallery")){
                                                try{
                                                    JSONArray picture = jsonObject1.getJSONArray("gallery");
                                                    if(picture != null && picture.length() > 0 ) {
                                                        for (int j = 0; j < picture.length(); j++) {
                                                            Log.e("Media pictire", picture.getString(j));
                                                            CourseInfo.setGallery(picture.getString(0));

                                                        }
                                                    }


                                                }catch (Exception e){

                                                }

                                            }




                                        }else if (media_type_demo.equals("Embedded Video"))
                                        {
                                            if(jsonObject1.optString("video") !=null && jsonObject1.optString("video").length() > 0){
                                                video_url = jsonObject1.optString("video");
                                                CourseInfo.setVideo(video_url);
                                            }



                                        }

                                        CourseInfo.setTitle(title);
                                        CourseInfo.setMedia_type(media_type_demo);
                                        CourseInfo.setTnid(tnd);

                                    }
                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {


                                            rv_Collagemedia.setVisibility(View.VISIBLE);
                                            mediaeadapter.setInitialData(arrayList);

                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);


                                        } else {
                                            isLoading = false;
                                            mediaeadapter.removeProgress();
                                            swipeContainer.setRefreshing(false);
                                            mediaeadapter.addData(arrayList);

                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);



                                        }
                                    }

                                    else{
                                        if (PAGE_SIZE==0){
                                            rv_Collagemedia.setVisibility(View.GONE);
                                            ErrorText.setVisibility(View.VISIBLE);
                                            ErrorText.setText("No media");
                                            swipeContainer.setVisibility(View.GONE);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);

                                        }else
                                            mediaeadapter.removeProgress();

                                    }
                                } else {
                                    swipeContainer.setRefreshing(false);
                                    mShimmerViewContainer.stopShimmerAnimation();
                                    mShimmerViewContainer.setVisibility(View.GONE);
                                }
                            } else {
                                swipeContainer.setRefreshing(false);



                            }
                        } catch (Exception e) {
                            swipeContainer.setRefreshing(false);


                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipeContainer.setRefreshing(false);
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(Media.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(Media.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(Media.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);


    }


    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                PAGE_SIZE=0;
                isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


}


