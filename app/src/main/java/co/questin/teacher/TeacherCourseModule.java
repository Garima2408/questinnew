package co.questin.teacher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.TeacherCourseModuleAdapter;
import co.questin.library.StateBean;
import co.questin.models.CoursemoduleArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;


import static co.questin.utils.Utils.isStatusSuccess;

public class TeacherCourseModule extends BaseAppCompactActivity implements SearchView.OnQueryTextListener {

    public static ArrayList<CoursemoduleArray> mCouseList;
    FloatingActionButton fab;
    private static TeacherCourseModuleAdapter coursemoduleadapter;
    static RecyclerView rv_teachercoursemodule;
    private RecyclerView.LayoutManager layoutManager;
    static Activity activity;
    public static ProgressBar progressBar2;
    public static TextView ErrorText;
    public static SwipeRefreshLayout swipeContainer;
    public static boolean isLoading= false;
    public static int PAGE_SIZE = 0;
    public static ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_course_module);
        activity = getActivity();
        fab = findViewById(R.id.fab);
        ErrorText =findViewById(R.id.ErrorText);

        mCouseList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        rv_teachercoursemodule = findViewById(R.id.rv_teachercoursemodule);
        rv_teachercoursemodule.setHasFixedSize(true);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();

        swipeContainer = findViewById(R.id.swipeContainer);
        coursemoduleadapter = new TeacherCourseModuleAdapter(rv_teachercoursemodule, mCouseList ,activity,"TeacherCource");
        rv_teachercoursemodule.setAdapter(coursemoduleadapter);

        rv_teachercoursemodule.setLayoutManager(layoutManager);
        CourceModuleList();

        inItView();



        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backIntent = new Intent(activity, CreateCourse.class)
                        .putExtra("open", "CameFromCourseModuleTeacher");
                startActivity(backIntent);


            }
        });

        Animation makeInAnimation = AnimationUtils.makeInAnimation(activity,false);
        makeInAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) { }

            public void onAnimationRepeat(Animation animation) { }

            public void onAnimationStart(Animation animation) {
                fab.setVisibility(View.VISIBLE);
            }
        });

        Animation makeOutAnimation = AnimationUtils.makeOutAnimation(activity,true);
        makeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                fab.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) { }

            @Override
            public void onAnimationStart(Animation animation) { }
        });


        if (fab.isShown()) {
            fab.startAnimation(makeOutAnimation);
        }

        if (!fab.isShown()) {
            fab.startAnimation(makeInAnimation);
        }

        rv_teachercoursemodule.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    fab.hide();
                } else if (dy < 0) {

                    fab.show();
                }
            }
        });
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh

                        if (!isLoading){

                            PAGE_SIZE=0;
                            isLoading=false;

                            swipeContainer.setRefreshing(true);

                            /* CALLING LISTS OF COMMENTS*/
                             CourceModuleList();

                            //CalledFromAddCourse();

                        }else{
                            isLoading=true;
                            swipeContainer.setRefreshing(false);
                        }
                    }
                }, 3000);
            }
        });




    }

    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();

    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");


        super.onPause();
    }



    public static  void CalledFromAddCourse() {
        PAGE_SIZE=0;
        isLoading=true;

        swipeContainer.setRefreshing(true);

        /* CALLING LISTS OF COMMENTS*/
        CourceModuleList();


    }




    private void inItView(){

        swipeContainer.setRefreshing(false);

        rv_teachercoursemodule.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    coursemoduleadapter.addProgress();
                    CourceModuleList();

                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }


    private static void CourceModuleList() {
        swipeContainer.setRefreshing(true);
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_COLLAGECOURCEMODULE+"?"+"cid="+SessionManager.getInstance(activity).getCollage().getTnid() +"&offset="+PAGE_SIZE+"&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    rv_teachercoursemodule.setVisibility(View.VISIBLE);
                                    swipeContainer.setRefreshing(false);
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");

                                    ArrayList<CoursemoduleArray> arrayList = new ArrayList<>();
                                    for (int i = 0; i < jsonArrayData.length(); i++) {

                                        CoursemoduleArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CoursemoduleArray.class);
                                        arrayList.add(CourseInfo);

                                    }
                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {

                                            rv_teachercoursemodule.setVisibility(View.VISIBLE);
                                            coursemoduleadapter.setInitialData(arrayList);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);


                                        } else {
                                            coursemoduleadapter.removeProgress();
                                            swipeContainer.setRefreshing(false);
                                            coursemoduleadapter.addData(arrayList);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);
                                            }

                                        isLoading = false;

                                    }

                                    else{
                                        if (PAGE_SIZE==0){
                                            rv_teachercoursemodule.setVisibility(View.GONE);
                                            ErrorText.setVisibility(View.VISIBLE);
                                            ErrorText.setText("No Course");
                                            swipeContainer.setVisibility(View.GONE);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);


                                        }else
                                            coursemoduleadapter.removeProgress();

                                    }

                                } else {
                                    swipeContainer.setRefreshing(false);
                                    mShimmerViewContainer.stopShimmerAnimation();
                                    mShimmerViewContainer.setVisibility(View.GONE);


                                }
                            } else {
                                swipeContainer.setRefreshing(false);



                            }
                        } catch (Exception e) {
                            swipeContainer.setRefreshing(false);


                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipeContainer.setRefreshing(false);
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);
    }

    private void CourceModuleSearchedList(String query) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_COLLAGECOURCEMODULE+"?"+"cid="+SessionManager.getInstance(getActivity()).getCollage().getTnid()+"&"+"search="+query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");

                                    if (jsonArrayData != null && jsonArrayData.length() > 0) {
                                        for (int i = 0; i < jsonArrayData.length(); i++) {

                                            CoursemoduleArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CoursemoduleArray.class);
                                            mCouseList.add(CourseInfo);
                                            coursemoduleadapter = new TeacherCourseModuleAdapter(rv_teachercoursemodule, mCouseList ,activity,"TeacherCource");
                                            rv_teachercoursemodule.setAdapter(coursemoduleadapter);

                                        }
                                    } else {
                                        showSnackbarinteger(R.string.No_course);

                                    }




                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);
    }







    @Override
    public void onBackPressed() {

        PAGE_SIZE=0;
        isLoading=false;
        finish();

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));

        searchView.setOnQueryTextListener(this);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {

            PAGE_SIZE= 0;
            isLoading=false;
            finish();
            return true;
        }




        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        Log.d("TAG", "query:"+ query);
        mCouseList.clear();
        CourceModuleSearchedList(query);


        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {



        return true;
    }


}

