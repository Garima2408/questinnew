package co.questin.teacher;

import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AlignmentSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.activities.SelectYourType;
import co.questin.library.StateBean;
import co.questin.models.CollageLists;
import co.questin.models.CollageRoleArray;
import co.questin.network.URLS;
import co.questin.questinsitecontent.QuestinSite;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

public class TeacherRegistration extends BaseAppCompactActivity {
    EditText edt_department,edt_degisnation,edt_email_id;
    Button btn_go_dashboard;
    TextView collageName;
    String department,degisnation,email,Membershipid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_registration);
        edt_department = findViewById(R.id.edt_department);
        edt_degisnation = findViewById(R.id.edt_degisnation);
        edt_email_id = findViewById(R.id.edt_email_id);
        btn_go_dashboard = findViewById(R.id.btn_go_dashboard);
        collageName = findViewById(R.id.collageName);
        String Topheading = SessionManager.getInstance(getActivity()).getCollage().getTitle();
        SpannableString spString = new SpannableString(Topheading);
        AlignmentSpan.Standard aligmentSpan = new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER);
        spString.setSpan(aligmentSpan, 0, spString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        collageName.setText(spString);



        btn_go_dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptToTeacherRegistration();


            }
        });
    }

    private void attemptToTeacherRegistration() {
        edt_department.setError(null);
        edt_degisnation.setError(null);
        edt_email_id.setError(null);


        // Store values at the time of the login attempt.
        department= edt_department.getText().toString().trim();
        degisnation = edt_degisnation.getText().toString().trim();
        email = edt_email_id.getText().toString().trim();

        CollageRoleArray userRoleDetail = new CollageRoleArray();
        userRoleDetail.role = "14";
        userRoleDetail.tnid = SessionManager.getInstance(getActivity()).getCollage().getTnid();
        userRoleDetail.title = SessionManager.getInstance(getActivity()).getCollage().getTitle();
        userRoleDetail.field_group_image = SessionManager.getInstance(getActivity()).getCollage().getField_group_image();
        userRoleDetail.CollageLogo =SessionManager.getInstance(getActivity()).getCollage().getField_groups_logo();
        userRoleDetail.CollageRole ="faculty";
        userRoleDetail.CollageEmail = email;
        userRoleDetail.CollageDesignation = degisnation;
        userRoleDetail.Collagedepartment = department;

        SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);



        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            focusView = edt_email_id;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        } else if (!Utils.isValidEmailAddress(email)) {
            focusView = edt_email_id;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_invalid_email));
            // Check for a valid password, if the user entered one.
        }else if (co.questin.utils.TextUtils.isNullOrEmpty(degisnation)) {
            // check for First Name
            focusView = edt_degisnation;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        else if (co.questin.utils.TextUtils.isNullOrEmpty(department)) {
            // check for First Name
            focusView = edt_department;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            if (!isInternetConnected()){
                showConnectionSnackbar();
            }else {
                ShowIndicator();
                callRequestForAPI();


            }


        }
    }

    private void callRequestForAPI() {


        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_STUDENETREGISTRATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONObject data = obj.getJSONObject("data");

                                    if (data != null && data.length() > 0) {
                                        Membershipid = data.getString("membership_id");

                                        Log.d("TAG", "membership_id: " + Membershipid );

                                    } else {

                                    }
                                    CollageLists lists = new CollageLists();
                                    lists.setTitle(SessionManager.getInstance(getActivity()).getCollage().getTitle());
                                    lists.setTnid(SessionManager.getInstance(getActivity()).getCollage().getTnid());
                                    lists.setField_group_image(SessionManager.getInstance(getActivity()).getCollage().getField_group_image());
                                    lists.setField_groups_logo(SessionManager.getInstance(getActivity()).getCollage().getField_groups_logo());
                                    lists.setLat(SessionManager.getInstance(getActivity()).getCollage().getLat());
                                    lists.setLng(SessionManager.getInstance(getActivity()).getCollage().getLng());
                                    lists.setMultiple(SessionManager.getInstance(getActivity()).getCollage().getMultiple());
                                    lists.setType(SessionManager.getInstance(getActivity()).getCollage().getType());

                                    lists.setCollageMemberShipId(Membershipid);
                                    SessionManager.getInstance(getActivity()).saveCollage(lists);

                                    Intent i = new Intent(TeacherRegistration.this, QuestinSite.class);
                                    Utils.getSharedPreference(TeacherRegistration.this).edit()
                                            .putInt(Constants.USER_ROLE, Constants.ROLE_FACULTY).apply();
                                    Utils.getSharedPreference(TeacherRegistration.this).edit()
                                            .putInt(Constants.RUNNIN_FIRST, Constants.ROLE_RUNNING_TRUE).apply();


                                    Utils.getSharedPreference(TeacherRegistration.this).edit()
                                            .putInt(Constants.RUNNIN_FIRST_CAMPUSFEED, Constants.ROLE_RUNNING_TRUE_CAMPUSFEED).apply();

                                    Utils.getSharedPreference(TeacherRegistration.this).edit()
                                            .putInt(Constants.RUNNIN_FIRST_PROFILE, Constants.ROLE_RUNNING_TRUE_PROFILE).apply();

                                    Utils.getSharedPreference(TeacherRegistration.this).edit()
                                            .putInt(Constants.RUNNIN_FIRST_CHAT, Constants.ROLE_RUNNING_TRUE_CHAT).apply();


                                    startActivity(i);

                                    finish();


                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(getActivity()).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(getActivity()).getaccesstoken().sessionName+"="+SessionManager.getInstance(getActivity()).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("entity_type","user");
                params.put("group_type","node");
                params.put("membership type","og_membership_type_college");
                params.put("field_i_am_a","faculty");
                params.put("state","1");
                params.put("etid",SessionManager.getInstance(getActivity()).getUser().userprofile_id);
                params.put("gid",SessionManager.getInstance(getActivity()).getCollage().getTnid());
                params.put("field_college_email",email);
                params.put("field_department",department);
                params.put("field_faculty_designation",degisnation);
                params.put("field_name","field_college_member");
                params.put("roles[14]","faculty");
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);
    }




    @Override
    public void onBackPressed() {
        Intent i = new Intent(TeacherRegistration.this, SelectYourType.class);
        startActivity(i);

        finish();
    }
}

