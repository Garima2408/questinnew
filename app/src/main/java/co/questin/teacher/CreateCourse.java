package co.questin.teacher;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.ResourceAddAdapter;
import co.questin.library.StateBean;
import co.questin.models.ResourceAddList;
import co.questin.models.SemesterArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

public class CreateCourse extends BaseAppCompactActivity {
    EditText CourseTittle,Details,field_deparment,subjectCode,subjectCredit,Acadamicyear;
    String course_tittle,courseDetails,teacherdepartment,coursecode,coursecradit,coursesem,courseadamic,jsonResource,SemID;
    TextView Addresource;
    Spinner Semester;
    ListView resourcelist;
    private int RC_NEW_LIST = 1;
    Bundle b;
    ArrayList<ResourceAddList> dataList;
    ResourceAddAdapter adapter;
    ArrayList<SemesterArray> listofSem;
    private ArrayList<String> semlist;
    Bundle extras;
    private int year, month, day, week;
    private int startDay, startMonth, startYear;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_course);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Intent intent = getIntent();
        extras = intent.getExtras();
        String action = intent.getAction();

        CourseTittle  = findViewById(R.id.CourseTittle);
        /* Teacher  = findViewById(R.id.Teacher);*/
        Details  = findViewById(R.id.Details);
        field_deparment  = findViewById(R.id.field_deparment);
        subjectCode  = findViewById(R.id.subjectCode);
        subjectCredit  = findViewById(R.id.subjectCredit);
        Semester  = findViewById(R.id.Semester);
        Acadamicyear  = findViewById(R.id.Acadamicyear);
        Addresource  = findViewById(R.id.Addresource);
        resourcelist  = findViewById(R.id.resourcelist);
        dataList = new ArrayList<ResourceAddList>();

        semlist = new ArrayList<String>();
        listofSem = new ArrayList<SemesterArray>();


        if(SessionManager.getInstance(getActivity()).getCollage().getType() !=null){
            if(SessionManager.getInstance(getActivity()).getCollage().getType().matches("Schools")){

                Semester.setVisibility(View.GONE);
                subjectCode.setVisibility(View.GONE);
                subjectCredit.setVisibility(View.GONE);
                SemID = "846";
                coursesem=SemID;

            }else {
                CourseSemesterAuthTask();


            }

        }


        Addresource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent newChatIntent = new Intent(CreateCourse.this, AddResourceByTeacher.class);
                startActivityForResult(newChatIntent, RC_NEW_LIST);

            }
        });

    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_NEW_LIST) {
            if (resultCode == RESULT_OK) {

                dataList = (ArrayList<ResourceAddList>) data.getSerializableExtra("Arraylist");
                Log.d("TAG", "resultArr: " +   dataList);
                AddDataToDisplay();



            }
        }
    }
    private void AddDataToDisplay() {
        adapter = new ResourceAddAdapter(CreateCourse.this, R.layout.list_of_addresource,dataList);
        resourcelist.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        Gson  gson = new Gson();
        jsonResource = gson.toJson(dataList);
        System.out.println("jsonStudents = " + jsonResource);

    }


    private void CourseSemesterAuthTask() {
        ShowIndicator();

        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_SEMSTERID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray data = obj.getJSONArray("data");
                                    for (int i=0; i<data.length();i++) {
                                        SemesterArray GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), SemesterArray.class);

                                        semlist.add(GoalInfo.getLabel());
                                        listofSem.add(GoalInfo);

                                        Semester.setAdapter(new ArrayAdapter<String>(CreateCourse.this,
                                                android.R.layout.simple_spinner_dropdown_item,
                                                semlist));


                                        Semester.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

                                            @Override
                                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                                if (position==0)
                                                    position++;

                                                SemID = listofSem.get(position).getId();
                                                Log.d("TAG", "edt_selectsub: " + SemID);
                                                coursesem=SemID;

                                            }

                                            @Override
                                            public void onNothingSelected(AdapterView<?> parent) {}

                                        });


                                    }




                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(CreateCourse.this).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(CreateCourse.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(CreateCourse.this).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("vid","46");
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.submit_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Submit:
                AddCourseInCollageProcess();

                return true;
            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void AddCourseInCollageProcess() {


        CourseTittle.setError(null);
        Details.setError(null);
        field_deparment.setError(null);
        subjectCode.setError(null);
        subjectCredit.setError(null);
        Acadamicyear.setError(null);



        // Store values at the time of the login attempt.
        course_tittle = CourseTittle.getText().toString().trim();
        courseDetails = Details.getText().toString().trim();
        teacherdepartment = field_deparment.getText().toString().trim();
        coursecode = subjectCode.getText().toString().trim();
        coursecradit = subjectCredit.getText().toString().trim();
        coursesem = SemID;
        courseadamic = Acadamicyear.getText().toString().trim();


        boolean cancel = false;
        View focusView = null;


        if (android.text.TextUtils.isEmpty(course_tittle)) {
            focusView = CourseTittle;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }

        else if (co.questin.utils.TextUtils.isNullOrEmpty(courseDetails)) {
            // check for First Name
            focusView = Details;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            if (!isInternetConnected()){
                showConnectionSnackbar();
            }else {
                ShowIndicator();
                CourseCreateTeacherAuthTask();


            }
        }
    }





    private void CourseCreateTeacherAuthTask() {
        StringRequest mStrRequest = new StringRequest(Request.Method.POST,  URLS.URL_TEACHERCREATECOURSE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    Check();



                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());

                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        } else {
                            hideIndicator();
                            showConnectionSnackbar();

                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                String Cookies= SessionManager.getInstance(CreateCourse.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(CreateCourse.this).getaccesstoken().sessionID;
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(CreateCourse.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type","courses");
                params.put("og_group_ref", SessionManager.getInstance(getActivity()).getCollage().getTnid());
                params.put("title",course_tittle);
                params.put("body",courseDetails);
                params.put("field_teacher",SessionManager.getInstance(getActivity()).getUser().email+" [uid:"+SessionManager.getInstance(getActivity()).getUser().userprofile_id+"]");
                params.put("field_deparment",teacherdepartment);
                params.put("field_job_title",coursecode);
                params.put("field_credit",coursecradit);
                params.put("field_academic_year",courseadamic);
                params.put("field_semester",coursesem);

                return params;
            }



        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);



    }





    private void Check() {

        if (extras.containsKey("open")) {
            if (extras.getString("open").equals("CameFromMyCourseTeacher")) {

                TeacherMyCource.CalledFromAddCourse();
                finish();

            } else if (extras.getString("open").equals("CameFromCourseModuleTeacher")) {

                TeacherCourseModule.CalledFromAddCourse();
                finish();

            }

            else if (extras.getString("open").equals("CameFromFacultyPage")) {



            }


        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }



}
