package co.questin.teacher;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.TeacherCourseModuleAdapter;
import co.questin.library.StateBean;
import co.questin.models.CoursemoduleArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class TeacherMyCource extends BaseAppCompactActivity {
    public static ArrayList<CoursemoduleArray> mCouseList;
    private static SwipeRefreshLayout swipeContainer;
    static FloatingActionButton fab;
    static FloatingActionMenu materialDesignFAM;
    com.github.clans.fab.FloatingActionButton floatingActionButton1,fab2;
    static TextView text_view;
    static TextView text_view2;
    RelativeLayout relative;
    private static TeacherCourseModuleAdapter coursemoduleadapter;
    static RecyclerView rv_teachercoursemodule;
    private RecyclerView.LayoutManager layoutManager;
    static Activity activity;
    private static ProgressBar spinner;
    static TextView ErrorText;
    private static boolean isLoading= false;
    static RelativeLayout Displaytext;
    private static int PAGE_SIZE = 0;
    private static ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_my_cource);
        activity = getActivity();
        swipeContainer = findViewById(R.id.swipeContainer);
        fab = findViewById(R.id.fab);
        fab2 =findViewById(R.id.fab2);
        spinner= findViewById(R.id.progressBar);
        mCouseList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);


        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();


        rv_teachercoursemodule = findViewById(R.id.rv_teachercoursemodule);
        rv_teachercoursemodule.setHasFixedSize(true);
        final LinearLayoutManager mLayoutMan= new LinearLayoutManager(this);
        rv_teachercoursemodule.setLayoutManager(layoutManager);
        coursemoduleadapter = new TeacherCourseModuleAdapter(rv_teachercoursemodule, mCouseList ,activity,"TeacherMyCource");
        rv_teachercoursemodule.setAdapter(coursemoduleadapter);
        Displaytext = findViewById(R.id.Displaytext);
        relative = findViewById(R.id.relative);
        text_view = findViewById(R.id.text_view);
        text_view2 = findViewById(R.id.text_view2);
        materialDesignFAM = findViewById(R.id.material_design_android_floating_action_menu);
        ErrorText = findViewById(R.id.ErrorText);
        floatingActionButton1 = findViewById(R.id.material_design_floating_action_menu_item1);

        MyCoursesListDisplay();


        inItView();


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent backIntent = new Intent(activity, CreateCourse.class)
                        .putExtra("open", "CameFromMyCourseTeacher");
                activity.startActivity(backIntent);



            }
        });
        relative.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }
                return false;
            }
        });


        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(TeacherMyCource.this, TeacherCourseModule.class);
                startActivity(intent);


            }
        });
        fab2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent backIntent = new Intent(activity, CreateCourse.class)
                        .putExtra("open", "CameFromMyCourseTeacher");
                activity.startActivity(backIntent);


            }
        });


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        if (!isLoading){
                            CalledFromAddCourse();

                        }else{
                            isLoading=false;
                            swipeContainer.setRefreshing(false);
                        }


                    }
                }, 3000);
            }
        });



    }



    public static void CalledFromAddCourse() {

        PAGE_SIZE=0;
        isLoading=true;
        mShimmerViewContainer.startShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        swipeContainer.setRefreshing(true);
        MyCoursesListDisplay();



    }

    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.e("onPause","CampusNewFeeds");
        mShimmerViewContainer.stopShimmerAnimation();
    }


    private void inItView() {


        swipeContainer.setRefreshing(false);

        rv_teachercoursemodule.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){

                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    coursemoduleadapter.addProgress();
                    MyCoursesListDisplay();

                }else{
                    Log.i("loadinghua", "im else now");

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });

    }


    public static void RefershMyCourseList() {
        mCouseList.clear();
        MyCoursesListDisplay();

    }



    private static void MyCoursesListDisplay() {
        swipeContainer.setRefreshing(true);
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_MYCOURSES+"?"+"uid"+"="+SessionManager.getInstance(activity).getUser().userprofile_id+"&"+"cid="+ SessionManager.getInstance(activity).getCollage().getTnid()+"&offset="+PAGE_SIZE+"&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                rv_teachercoursemodule.setVisibility(View.VISIBLE);
                                swipeContainer.setRefreshing(false);
                                Displaytext.setVisibility(View.GONE);
                                fab.setVisibility(View.VISIBLE);

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    ArrayList<CoursemoduleArray> arrayList = new ArrayList<>();

                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {

                                        for (int i = 0; i < jsonArrayData.length(); i++) {

                                            CoursemoduleArray courseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CoursemoduleArray.class);
                                            arrayList.add(courseInfo);
                                        }
                                    }
                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {


                                            rv_teachercoursemodule.setVisibility(View.VISIBLE);
                                            coursemoduleadapter.setInitialData(arrayList);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);


                                        } else {
                                            coursemoduleadapter.removeProgress();
                                            swipeContainer.setRefreshing(false);
                                            coursemoduleadapter.addData(arrayList);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);
                                        }

                                        isLoading = false;

                                    }else{
                                        if (PAGE_SIZE==0){
                                            Displaytext.setVisibility(View.VISIBLE);
                                            fab.setVisibility(View.GONE);
                                            swipeContainer.setVisibility(View.GONE);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);

                                        }else
                                            coursemoduleadapter.removeProgress();
                                    }
                                } else {
                                    swipeContainer.setRefreshing(false);
                                    mShimmerViewContainer.stopShimmerAnimation();
                                    mShimmerViewContainer.setVisibility(View.GONE);
                                }
                            } else {
                                swipeContainer.setRefreshing(false);
                            }
                        } catch (Exception e) {
                            swipeContainer.setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipeContainer.setRefreshing(false);
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);
    }




    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        PAGE_SIZE=0;
        isLoading=false;

    }

}
