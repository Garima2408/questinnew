package co.questin.teacher;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.soundcloud.android.crop.Crop;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.activities.UserProfileEdit;
import co.questin.calendersection.CalenderMain;
import co.questin.campusfeedsection.AllCampusFeeds;
import co.questin.campusfeedsection.MyCampusFeeds;
import co.questin.chat.ChatTabbedActivity;
import co.questin.library.StateBean;
import co.questin.models.UserDetail;
import co.questin.network.URLS;
import co.questin.studentprofile.FriendsSection;
import co.questin.studentprofile.MyEvents;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.TextUtils;
import co.questin.utils.Utils;
import okhttp3.RequestBody;

public class TeacherProfile extends BaseAppCompactActivity implements View.OnClickListener{
    LinearLayout ll_my_feeds,ll_my_courses, ll_my_group, ll_my_upcoming_events, ll_friends;
    TextView tv_username, tv_useremail,tv_userbatch;
    ImageButton imgbtn_edit_profile;
    ImageView mainheader,profileImage;
    Bitmap thumbnail = null;
    private static final int EXTERNAL_STORAGE_PERMISSION_REQUEST = 23;
    RequestBody body;
    String Fileimagename;
    private Class mFragmentClass = null;
    BottomNavigationView mBottomNavigationView;
    Dialog imageDialog;
    private SharedPreferences preferences;
    private int Runfirst;
    private Dialog profileDialog,imageDialog1;
    boolean isPressed = false;
    String userphoto;
    int i=0;
    Bundle extras;
    private int SELECT_FILE = 1;
    static String strFile = null;
    private String mImageFileLocation = "";
    public static final int RequestPermissionCode = 1;
    boolean ExternslWriteAccepted,ExternslreadAccepted,cameraAccepted;
    Boolean CallingCamera,CallingGallary;
    Uri imageUri;
    private static final int ACTIVITY_START_CAMERA_APP = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_profile);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        preferences = Utils.getSharedPreference(TeacherProfile.this);
        Runfirst = preferences.getInt(Constants.RUNNIN_FIRST_PROFILE, Constants.ROLE_RUNNING_FALSE_PROFILE);
        FirstOneThis();


        ll_my_feeds = findViewById(R.id.ll_my_feeds);
        ll_my_courses = findViewById(R.id.ll_my_courses);
        ll_my_group = findViewById(R.id.ll_my_group);
        ll_my_upcoming_events = findViewById(R.id.ll_my_upcoming_events);
        ll_friends = findViewById(R.id.ll_friends);
        tv_username = findViewById(R.id.tv_username);
        tv_useremail = findViewById(R.id.tv_useremail);
        profileImage = findViewById(R.id.imageProfile);
        mainheader = findViewById(R.id.mainheader);
        tv_userbatch = findViewById(R.id.tv_userbatch);
        imgbtn_edit_profile = findViewById(R.id.imgbtn_edit_profile);
        imgbtn_edit_profile.setVisibility(View.VISIBLE);
        ll_my_feeds.setOnClickListener(this);
        ll_my_courses.setOnClickListener(this);
        ll_my_group.setOnClickListener(this);
        ll_my_upcoming_events.setOnClickListener(this);
        ll_friends.setOnClickListener(this);
        profileImage.setOnClickListener(this);
        imgbtn_edit_profile.setOnClickListener(this);
        mainheader.setOnClickListener(this);


        displayUserData();
        /*setupNavigationView();*/
        Setuponbottombar();

    }

    private void FirstOneThis() {
        switch (Runfirst){
            case Constants.ROLE_RUNNING_TRUE :
                imageDialog = new Dialog(TeacherProfile.this);
                imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                imageDialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(Color.TRANSPARENT));
                imageDialog.setContentView(R.layout.image_overlay_screen);
                // dialogLogin.setCancelable(false);
                imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT);
                imageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                imageDialog.show();
                ImageView showimage = imageDialog.findViewById(R.id.DisplayOnbondingImage);
                showimage.setImageResource(R.mipmap.onboarding_profile);

                showimage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imageDialog.dismiss();

                        Utils.getSharedPreference(TeacherProfile.this).edit()
                                .putInt(Constants.RUNNIN_FIRST_PROFILE, Constants.ROLE_RUNNING_FALSE_PROFILE).apply();
                        Utils.removeStringPreferences(TeacherProfile.this,"0");

                    }
                });



                break;
            case Constants.ROLE_RUNNING_FALSE :
                break;


        }




    }




    private void Setuponbottombar() {

        TextView dashboard_child = findViewById(R.id.dashboard_child);
        final TextView calendar = findViewById(R.id.calendar);
        TextView campus_feeds = findViewById(R.id.campus_feeds);
        TextView message_feeds = findViewById(R.id.message_feeds);
        TextView profile = findViewById(R.id.profile);

        dashboard_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent y = new Intent(TeacherProfile.this,TeacherMain.class);
                startActivity(y);
                finish();


            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent p = new Intent(TeacherProfile.this,CalenderMain.class)
                        .putExtra("open", "AsSelectedSite");
                startActivity(p);
                finish();


            }
        });

        campus_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent y = new Intent(TeacherProfile.this,AllCampusFeeds.class);
                startActivity(y);
                finish();


            }
        });

        message_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent z = new Intent(TeacherProfile.this,ChatTabbedActivity.class)
                        .putExtra("open", "AsSelectedSite");
                startActivity(z);
                finish();


            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent j = new Intent(TeacherProfile.this,TeacherProfile.class);
                startActivity(j);
*/
            }
        });


    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_my_feeds:
                startActivity (new Intent(this, MyCampusFeeds.class));

                break;


            case R.id.ll_my_courses:

                startActivity (new Intent(TeacherProfile.this, TeacherMyCource.class));

                break;





            case R.id.ll_my_group:
                //  startActivity (new Intent (this, MyGroups.class));
                Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                break;

                case R.id.ll_my_upcoming_events:
                startActivity (new Intent (this, MyEvents.class));
                // Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                break;



            case R.id.ll_friends:
                startActivity (new Intent (this, FriendsSection.class));
                //Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                break;
            case R.id.imageProfile:
                showImageDialog();

                break;
            case R.id.imgbtn_edit_profile:
                startActivity (new Intent (this , UserProfileEdit.class));
                break;

            case R.id.mainheader:

                //  showImageDialog2();
                break;


        }
    }





    private void showImageDialog() {
        final CharSequence[] items = { "View Profile","Take Photo", "Choose from Gallery", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(TeacherProfile.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("View Profile")){

                    showProfilepic(getUser().photo);

                }

                else if (items[item].equals("Take Photo")) {
                    profileImage.setImageDrawable(null);
                    CallingCamera =true;
                    CallingGallary =false;
                    if(checkPermission()){
                        CallCamera();
                    }

                } else if (items[item].equals("Choose from Gallery")) {
                    profileImage.setImageDrawable(null);

                    CallingGallary =true;
                    CallingCamera =false;
                    if(checkPermission()){
                        galleryIntent();
                    }


                } else if (items[item].equals("Cancel")) {
                    if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUser().photo)) {
                        Glide.with(TeacherProfile.this).load(getUser().photo)
                                .placeholder(R.mipmap.place_holder).dontAnimate()
                                .fitCenter().into(profileImage);

                    }
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void CallCamera() {
        Intent callCameraApplicationIntent = new Intent();
        callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        File photoFile = null;
        try {
            photoFile = createImageFile();

        } catch (IOException e) {
            e.printStackTrace();
        }
        imageUri = FileProvider.getUriForFile(TeacherProfile.this, "co.questin.fileprovider",photoFile);


        callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(callCameraApplicationIntent, ACTIVITY_START_CAMERA_APP);
    }


    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }


    private void galleryIntent()
    {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(Intent.createChooser(galleryIntent, "Select File"),SELECT_FILE);
    }



    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if(requestCode == ACTIVITY_START_CAMERA_APP && resultCode == RESULT_OK) {

            beginCrop(imageUri);

        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)


                onSelectFromGalleryResult(data);
        }
        if (requestCode == Crop.REQUEST_CROP) {

            handleCrop(resultCode, data);

        }
        if (resultCode == Activity.RESULT_CANCELED) {
            if (!TextUtils.isNullOrEmpty(getUser().photo))
                Glide.with(TeacherProfile.this).load(getUser().photo)
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(profileImage);

        }

    }


    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(result));
                thumbnail = getResizedBitmap(thumbnail,1000);
                strFile = encodeImageTobase64(thumbnail);
                System.out.println("camera " + strFile);
                profileImage.setImageBitmap(thumbnail);
                if (strFile !=null){
                    ProgressUpdateUserProfile(strFile);
                    ShowIndicator();

                }else {
                    showSnackbarMessage("There seems to be some problem.please select again");

                }

            } catch (IOException e) {
                e.printStackTrace();
            }




        } else if (resultCode == Crop.RESULT_ERROR) {
            //  Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }




    private void ImageDialogOpen() {

        final ImageView backone,showimage;
        TextView senderName;

        imageDialog1 = new Dialog(TeacherProfile.this);
        imageDialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog1.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.BLACK));
        imageDialog1.setContentView(R.layout.image_preview_show);
        // dialogLogin.setCancelable(false);
        imageDialog1.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        imageDialog1.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        imageDialog1.show();
         showimage = imageDialog1.findViewById(R.id.showimage);
        senderName = imageDialog1.findViewById(R.id.senderName);
        backone = imageDialog1.findViewById(R.id.backone);

        ImageMatrixTouchHandler imageMatrixTouchHandler = new ImageMatrixTouchHandler(TeacherProfile.this);

        showimage.setOnTouchListener(imageMatrixTouchHandler);



        Glide.with(TeacherProfile.this).load(getUser().photo)
                .placeholder(R.mipmap.header_image).dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .fitCenter().into(showimage);


        backone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageDialog1.dismiss();
            }
        });



    }


    private void showProfilepic( final String photo){
        profileDialog = new Dialog(TeacherProfile.this);
        profileDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        profileDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        profileDialog.setContentView(R.layout.show_user_profilepic);
        profileDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        profileDialog.show();

        final   ImageView  profilepic = profileDialog.findViewById(R.id.profilepic);


        Glide.with(this).load(photo)
                .placeholder(R.mipmap.header_image).dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .fitCenter().into(profilepic);




        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileDialog.dismiss();
                ImageDialogOpen();

            }
        });
    }








    private void displayUserData() {
        if (getUser()!=null){
            if (!TextUtils.isNullOrEmpty(getUser().username)) {
                tv_username.setText(getUser().username);
            }

            if (SessionManager.getInstance(getActivity()).getUserClgRole()!=null) {

                if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole()) && !TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageDesignation())) {
                    tv_useremail.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole() + " " + SessionManager.getInstance(getActivity()).getUserClgRole().getCollageDesignation());
                }
            }
            if (SessionManager.getInstance(getActivity()).getUserClgRole().getCollagedepartment()!=null) {
                if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollagedepartment())) {
                    tv_userbatch.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollagedepartment());
                }

            }
            if (!TextUtils.isNullOrEmpty(getUser().photo)) {
                Glide.with(this).load(getUser().photo)
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .fitCenter().into(profileImage);


            }
            if (!TextUtils.isNullOrEmpty(getUser().Userwallpic)) {
                Glide.with(this).load(getUser().Userwallpic)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .fitCenter().into(mainheader);


            }
        }
    }


    /*New Galley Code*/



    //Choose From Gallery
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                beginCrop(data.getData());
                Uri uri = data.getData();
                String[] projection = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                Log.d("TAG", DatabaseUtils.dumpCursorToString(cursor));

                int columnIndex = cursor.getColumnIndex(projection[0]);
                Fileimagename = cursor.getString(columnIndex); // full path of image

                cursor.close();
            }catch (Exception e)
            {
                e.printStackTrace();
            }


        }
    }



    File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        Fileimagename = image.getName();
        return image;

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }




    private void ProgressUpdateUserProfile(final String strFile) {

        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_AVATAR+"/"+ SessionManager.getInstance(getActivity()).getUser().getUserprofile_id(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    String urlsofphoto = obj.getString("data");
                                    UserDetail userDetail = new UserDetail();
                                    userDetail.userprofile_id = SessionManager.getInstance(getActivity()).getUser().userprofile_id;
                                    userDetail.username = SessionManager.getInstance(getActivity()).getUser().getFirstName() + " " + SessionManager.getInstance(getActivity()).getUser().getLastName();
                                    userDetail.email = SessionManager.getInstance(getActivity()).getUser().email;
                                    userDetail.FirstName = SessionManager.getInstance(getActivity()).getUser().getFirstName();
                                    userDetail.LastName = SessionManager.getInstance(getActivity()).getUser().getLastName();
                                    userDetail.photo = urlsofphoto;
                                    userDetail.Userwallpic = SessionManager.getInstance(getActivity()).getUser().getUserwallpic();

                                    SessionManager.getInstance(getActivity()).saveUser(userDetail);


                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(getActivity()).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(getActivity()).getaccesstoken().sessionName+"="+SessionManager.getInstance(getActivity()).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("file",strFile);
                params.put("filename",Fileimagename);
                return params;
            }


        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);
    }


    @Override
    public void onBackPressed() {
        Utils.getSharedPreference(TeacherProfile.this).edit()
                .putInt(Constants.RUNNIN_FIRST_PROFILE, Constants.ROLE_RUNNING_FALSE_PROFILE).apply();
        Utils.removeStringPreferences(TeacherProfile.this,"0");
        finish();
    }


    private  boolean checkPermission() {
        int camerapermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionLocation = ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), RequestPermissionCode);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("TAG", "Permission callback called-------");
        switch (requestCode) {
            case RequestPermissionCode: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("TAG", "sms & location services permission granted");
                        if (CallingCamera ==true){
                            CallingGallary=false;
                            CallCamera();

                        }else if (CallingGallary ==true) {
                            CallingCamera=false;

                            galleryIntent();


                        }
                    } else {
                        Log.d("TAG", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Service Permissions are required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkPermission();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    finish();
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
    private void explain(String msg){
        final android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:com.exampledemo.parsaniahardik.marshmallowpermission")));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        finish();
                    }
                });
        dialog.show();
    }
}
