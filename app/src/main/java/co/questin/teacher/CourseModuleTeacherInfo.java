package co.questin.teacher;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.soundcloud.android.crop.Crop;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.FacultyListInSubjectAdapter;
import co.questin.adapters.SubjectInfoResourceAdapter;
import co.questin.library.StateBean;
import co.questin.models.InfoArray;
import co.questin.models.SubjectFacultyArray;
import co.questin.network.URLS;
import co.questin.utils.BaseFragment;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

import static android.app.Activity.RESULT_OK;
import static co.questin.utils.BaseAppCompactActivity.ShowIndicator;
import static co.questin.utils.BaseAppCompactActivity.encodeImageTobase64;
import static co.questin.utils.BaseAppCompactActivity.hideIndicator;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarinteger;
import static co.questin.utils.Utils.isStatusSuccess;


public class CourseModuleTeacherInfo extends BaseFragment {
    static String Course_id, Adminuser,Join_Value,CourseTittle,CourseImage,Tittle,body,field_job_title,field_deparment,field_academic_year,field_semester,field_teacher,field_credit,subjectphoto,CourseValue;
    RelativeLayout MainLayout;
    static String Course_idMain;
    private static ArrayList<String> selectedResourceSend;
    static ImageView subjectlogo;
    static TextView subjectCode, Acadamic, Samester, subjectCredit, tittle, about, Teacher, Department, questionpaper, attachment, resource, syllabus, vedio,Addresource;
    static RecyclerView resourceList;
    private RecyclerView.LayoutManager layoutManager,mlayoutManager;
    public static ArrayList<InfoArray> mInfoList;
    private static SubjectInfoResourceAdapter infoadapter;
    public static ArrayList<SubjectFacultyArray> mfacultyList;
    private ProgressDialog pDialog;
    ImageView my_image;
    static Activity activity;
    private static ProgressBar spinner;
    Bitmap thumbnail = null;
    String Fileimagename;
    MenuItem item;
    private String mImageFileLocation = "";
    Boolean CallingCamera,CallingGallary;
    public static final int RequestPermissionCode = 1;
    Uri imageUri;
    /*new cam/gallary*/
    private static final int ACTIVITY_START_CAMERA_APP = 0;
    private int SELECT_FILE = 1;
    public CourseModuleTeacherInfo() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_course_module_teacher_info, container, false);

        activity = (Activity) view.getContext();


        Course_id = getArguments().getString("COURSE_ID");
        Course_idMain = getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        CourseTittle = getArguments().getString("TITTLE");
        CourseImage  = getArguments().getString("COURSE_IMAGE");
        CourseValue = getArguments().getString("VALUECHANGED");
        Join_Value = getArguments().getString("JOINED");

        if (Course_id == null) {
        }


        mInfoList = new ArrayList<>();
        mfacultyList = new ArrayList<>();
        selectedResourceSend =new ArrayList<>();

        subjectCode = view.findViewById(R.id.subjectCode);
        subjectlogo = view.findViewById(R.id.subjectlogo);
        MainLayout = view.findViewById(R.id.MainLayout);
        about = view.findViewById(R.id.about);
        Acadamic = view.findViewById(R.id.Acadamic);
        Samester = view.findViewById(R.id.Samester);
        subjectCredit = view.findViewById(R.id.subjectCredit);
        tittle = view.findViewById(R.id.tittle);
        layoutManager = new LinearLayoutManager(activity);
        mlayoutManager = new LinearLayoutManager(activity);
        spinner= view.findViewById(R.id.progressBar);
        resourceList = view.findViewById(R.id.resourceList);
        resourceList.setHasFixedSize(true);
        resourceList.setLayoutManager(layoutManager);
        spinner = view.findViewById(R.id.progressBar);
        Addresource = view.findViewById(R.id.Addresource);
        GetAllSubjectDetails();


        if (CourseImage!=null) {
            Glide.with(activity).load(CourseImage)
                    .placeholder(R.mipmap.clasrrominfo).dontAnimate()
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .fitCenter().into(subjectlogo);



        }


        if (Join_Value.equals("member")) {
            setHasOptionsMenu(true);
            Addresource.setVisibility(View.VISIBLE);

            setHasOptionsMenu(true);
            Addresource.setVisibility(View.VISIBLE);
            AddLISner();

            resourceList.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (dy > 0) {

                    } else if (dy < 0) {

                    }
                }
            });

        } else if (Join_Value.equals("pending")) {
            setHasOptionsMenu(false);
            Addresource.setVisibility(View.GONE);

        }
        else if (Join_Value.equals("nomember")) {

            setHasOptionsMenu(false);
            Addresource.setVisibility(View.GONE);
        }





        if (Adminuser.equals("TRUE")) {



            setHasOptionsMenu(true);
            Addresource.setVisibility(View.VISIBLE);
            AddLISner();

            resourceList.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (dy > 0) {

                    } else if (dy < 0) {

                    }
                }
            });




            NestedScrollView nsv =  view.findViewById(R.id.nestedScroll);
            nsv.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (scrollY > oldScrollY) {


                    } else {

                    }
                }
            });




        } else if (Adminuser.equals("FALSE")) {



        }

        return view;
    }






    private void AddLISner() {

        subjectlogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showImageDialog();

            }
        });


    }


    private void showImageDialog() {
        final CharSequence[] items = { "Take Photo", "Choose from Gallery", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    CallingCamera =true;
                    CallingGallary =false;
                    if(checkPermission()){
                        CallCamera();
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    CallingGallary =true;
                    CallingCamera =false;
                    if(checkPermission()){
                        galleryIntent();
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void CallCamera() {
        Intent callCameraApplicationIntent = new Intent();
        callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        File photoFile = null;
        try {
            photoFile = createImageFile();

        } catch (IOException e) {
            e.printStackTrace();
        }
        imageUri = FileProvider.getUriForFile(activity, "co.questin.fileprovider",photoFile);
        callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(callCameraApplicationIntent, ACTIVITY_START_CAMERA_APP);
    }






    private void galleryIntent()
    {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(galleryIntent, "Select File"),SELECT_FILE);
    }


    private void GetAllSubjectDetails() {
       ProgressSubjectInfo();



    }




    public static void RefreshWorkedFaculty() {

        mfacultyList.clear();


    }

    public static void RefreshWorkedsubjectresource() {

        mInfoList.clear();
        selectedResourceSend.clear();
        ProgressSubjectInfo();


    }





    private static void ProgressSubjectInfo() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_COLLAGESUBJECTINFO+"/"+Course_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONObject data = obj.getJSONObject("data");
                                    Tittle = data.getString("title");
                                    body = data.getString("body");
                                    field_job_title = data.getString("field_job_title");
                                    field_deparment = data.getString("field_deparment");
                                    field_academic_year = data.getString("field_academic_year");
                                    field_semester = data.getString("field_semester");
                                    field_teacher = data.getString("field_teacher");
                                    field_credit = data.getString("field_credit");

                                    JSONArray picture = data.getJSONArray("field_logo");
                                    if(picture != null && picture.length() > 0 ){
                                        String strings[] = new String[picture.length()];
                                        for(int i=0;i<strings.length;i++) {
                                            strings[i] = picture.getString(i);
                                            Glide.with(activity).load(strings[i])
                                                    .placeholder(R.mipmap.clasrrominfo).dontAnimate()
                                                    .fitCenter().into(subjectlogo);

                                            Log.d("TAG", "photo: " +  strings[i]);
                                            //  Log.d("TAG", "photo: " +  strings[i]);
                                        }
                                    }else
                                    {
                                        subjectlogo.setImageResource(R.mipmap.clasrrominfo);


                                    }



                                    JSONArray syllabus = data.getJSONArray("field_sub_syllabus");
                                    if (syllabus != null && syllabus.length() > 0) {



                                        for (int j = 0; j < syllabus.length(); j++) {
                                            String t = syllabus.getJSONObject(j).getString("value");
                                            String p = syllabus.getJSONObject(j).getString("title");
                                            //  Log.d("TAG", "recource: " + t + p);
                                            InfoArray LinkInfo = new InfoArray();
                                            LinkInfo.title = syllabus.getJSONObject(j).getString("title");
                                            LinkInfo.value = syllabus.getJSONObject(j).getString("value");
                                            LinkInfo.pos = syllabus.getJSONObject(j).getString("pos");
                                            mInfoList.add(LinkInfo);


                                        }
                                    }

                                    JSONArray QuestionPaperArray = data.getJSONArray("field_question_paper");
                                    if (QuestionPaperArray != null && QuestionPaperArray.length() > 0) {



                                        for (int j = 0; j < QuestionPaperArray.length(); j++) {
                                            String t = QuestionPaperArray.getJSONObject(j).getString("value");
                                            String p = QuestionPaperArray.getJSONObject(j).getString("title");
                                            //  Log.d("TAG", "recource: " + t + p);
                                            InfoArray LinkInfo = new InfoArray();
                                            LinkInfo.title = QuestionPaperArray.getJSONObject(j).getString("title");
                                            LinkInfo.value = QuestionPaperArray.getJSONObject(j).getString("value");
                                            LinkInfo.pos = QuestionPaperArray.getJSONObject(j).getString("pos");
                                            mInfoList.add(LinkInfo);


                                        }
                                    }





                                    JSONArray AttachmentsArray = data.getJSONArray("field_attachments");
                                    if (AttachmentsArray != null && AttachmentsArray.length() > 0) {



                                        for (int j = 0; j < AttachmentsArray.length(); j++) {
                                            String t = AttachmentsArray.getJSONObject(j).getString("value");
                                            String p = AttachmentsArray.getJSONObject(j).getString("title");
                                            //  Log.d("TAG", "recource: " + t + p);
                                            InfoArray LinkInfo = new InfoArray();
                                            LinkInfo.title = AttachmentsArray.getJSONObject(j).getString("title");
                                            LinkInfo.value = AttachmentsArray.getJSONObject(j).getString("value");
                                            LinkInfo.pos = AttachmentsArray.getJSONObject(j).getString("pos");
                                            mInfoList.add(LinkInfo);


                                        }
                                    }




                                    JSONArray resourceArrays = data.getJSONArray("field_course_resource");
                                    if (resourceArrays != null && resourceArrays.length() > 0) {



                                        for (int j = 0; j < resourceArrays.length(); j++) {
                                            String t = resourceArrays.getJSONObject(j).getString("value");
                                            String p = resourceArrays.getJSONObject(j).getString("title");
                                            Log.d("TAG", "recource: " + t + p);
                                            InfoArray LinkInfo = new InfoArray();
                                            LinkInfo.title = resourceArrays.getJSONObject(j).getString("title");
                                            LinkInfo.value = resourceArrays.getJSONObject(j).getString("value");
                                            LinkInfo.pos = resourceArrays.getJSONObject(j).getString("pos");
                                            LinkInfo.resource ="True";
                                            mInfoList.add(LinkInfo);
                                            selectedResourceSend.add(p+"$"+t);


                                        }


                                    }
                                    infoadapter = new SubjectInfoResourceAdapter(activity, mInfoList,Course_id,Adminuser);
                                    resourceList.setAdapter(infoadapter);
                                    resourceList.setNestedScrollingEnabled(false);
                                    calltheresourcemethod(selectedResourceSend);

                                    subjectCode.setText(field_job_title);
                                    Acadamic.setText(field_academic_year);
                                    Samester.setText(field_semester);
                                    subjectCredit.setText(field_credit);
                                    tittle.setText(Tittle);
                                    about.setText(body);

                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);

    }

    private static void calltheresourcemethod(final ArrayList<String> selectedResourceSend) {
        Addresource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent newIntent = new Intent(activity, AddResourceByTeacher.class);
                newIntent.putExtra("COURSE_ID",Course_id);
                newIntent.putExtra("array_list", selectedResourceSend);
                Log.e("array_list", String.valueOf(selectedResourceSend));
                activity.startActivity(newIntent);

                }
        });


        }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        super.onActivityResult(requestCode, resultCode, result);
        if(requestCode == ACTIVITY_START_CAMERA_APP && resultCode == RESULT_OK) {

            beginCrop(imageUri);

        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)


                onSelectFromGalleryResult(result);
        }
        if (requestCode == Crop.REQUEST_CROP) {

            handleCrop(resultCode, result);

        }

    }


    //Choose From Gallery
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {

            try {
                beginCrop(data.getData());
                Uri uri = data.getData();
                String[] projection = {MediaStore.Images.Media.DATA};

                Cursor cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                Log.d("TAG", DatabaseUtils.dumpCursorToString(cursor));

                int columnIndex = cursor.getColumnIndex(projection[0]);
                Fileimagename = cursor.getString(columnIndex); // full path of image

                cursor.close();

            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }


    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(activity.getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(getContext(), CourseModuleTeacherInfo.this, Crop.REQUEST_CROP);


    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {

            try {
                thumbnail = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), Crop.getOutput(result));
                thumbnail = getResizedBitmap(thumbnail,110);

                subjectlogo.setImageBitmap(thumbnail);
                ProgressUpdateUserProfile(thumbnail);


            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(getActivity(), Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        Fileimagename = image.getName();
        Log.d("TAG", "createImageFile: "+Fileimagename);

        return image;

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.optionpopupmenu, menu);
        item = menu.findItem(R.id.deleteoption);
        super.onCreateOptionsMenu(menu, menuInflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Editoption:

                Intent i = new Intent(getActivity(), UpdateCourse.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("COURSE_TITTLE", Tittle);
                bundle.putString("COURSE_BODY", body);
                bundle.putString("COURSE_CODE", field_job_title);
                bundle.putString("COURSE_DEPARTMENT", field_deparment);
                bundle.putString("COURSE_YEAR", field_academic_year);
                bundle.putString("COURSE_SEM", field_semester);
                bundle.putString("COURSE_TEACHER", field_teacher);
                bundle.putString("COURSE_CREDIT", field_credit);
                i.putExtras(bundle);
                startActivity(i);


                return true;

            case R.id.deleteoption:

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), android.app.AlertDialog.THEME_HOLO_DARK)
                        .setTitle("Delete User")
                        .setMessage(R.string.Delete)
                        .setCancelable(false)
                        .setPositiveButton("Yes", new Dialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                DeleteCollageSubject(Course_id);

                            }
                        })
                        .setNegativeButton("No", new Dialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                builder.create().show();


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* DELETE  SUBJECTS*/

    private void DeleteCollageSubject(String course_id) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.DELETE, URLS.URL_DELETESUBJECTS+"/"+course_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    if(CourseValue.matches("TeacherCource")){
                                        TeacherCourseModule.CalledFromAddCourse();
                                        activity.finish();
                                    }else if (CourseValue.matches("TeacherMyCource")){
                                        TeacherMyCource.CalledFromAddCourse();
                                        activity.finish();

                                    }
                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {

                            showServerSnackbar(R.string.error_responce);

                        } else {

                            showConnectionSnackbar();

                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(getActivity()).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(getActivity()).getaccesstoken().sessionName+"="+SessionManager.getInstance(getActivity()).getaccesstoken().sessionID);

                return params;
            }




        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);
    }



    private void ProgressUpdateUserProfile(final Bitmap strFile) {
             ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_TEACHERADDLOGO+"/"+ Course_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONObject picture = obj.getJSONObject("data");
                                    subjectphoto = picture.getString("icon_url");

                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(getActivity()).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(getActivity()).getaccesstoken().sessionName+"="+SessionManager.getInstance(getActivity()).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("file",encodeImageTobase64(thumbnail));
                params.put("filename",Fileimagename);
                return params;
            }


        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);
    }





    private  boolean checkPermission() {
        int camerapermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionLocation = ContextCompat.checkSelfPermission(activity,Manifest.permission.READ_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), RequestPermissionCode);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("TAG", "Permission callback called-------");
        switch (requestCode) {
            case RequestPermissionCode: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("TAG", "sms & location services permission granted");
                        if (CallingCamera ==true){
                            CallingGallary=false;
                            CallCamera();

                        }else if (CallingGallary ==true) {
                            CallingCamera=false;

                            galleryIntent();


                        }
                    } else {
                        Log.d("TAG", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Service Permissions are required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkPermission();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    activity. finish();
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
    private void explain(String msg){
        final android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(activity);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        //  permissionsclass.requestPermission(type,code);
                        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:com.exampledemo.parsaniahardik.marshmallowpermission")));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        activity. finish();
                    }
                });
        dialog.show();
    }



}