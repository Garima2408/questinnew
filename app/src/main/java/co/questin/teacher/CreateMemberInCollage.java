package co.questin.teacher;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.library.StateBean;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import co.questin.utils.TextUtils;
import co.questin.utils.Utils;

public class CreateMemberInCollage extends BaseAppCompactActivity {
    String Course_id,CourseTittle;
    EditText edt_first_name,edt_last_name,edt_email_address,edt_enrollment_address;
    Button btn_create_student;
    String   NewUser_id,New_User;
    String  email,enrollment,firstName,LastName;
    JSONArray studentselected;
    JSONObject shareAttandanceJson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_member_in_collage);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();





        edt_first_name = findViewById(R.id.edt_first_name);
        edt_last_name = findViewById(R.id.edt_last_name);
        edt_email_address = findViewById(R.id.edt_email_address);
        edt_enrollment_address = findViewById(R.id.edt_enrollment_address);
        btn_create_student = findViewById(R.id.btn_create_student);

        if(extras.containsKey("CameFrominvite")){
            if(extras.getString("CameFrominvite").equals("CameFrominvite")) {
                Course_id = extras.getString("COURSE_ID");
                CourseTittle = extras.getString("TITTLE");


            }
        }
        btn_create_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptToRegister();

            }
        });

        }

        private void attemptToRegister() {

        // Reset errors.
        edt_first_name.setError(null);
        edt_last_name.setError(null);
        edt_enrollment_address.setError(null);


        // Store values at the time of the login attempt.
        email = edt_email_address.getText().toString().trim();
        enrollment = edt_enrollment_address.getText().toString().trim();
        firstName = edt_first_name.getText().toString().trim();
        LastName = edt_last_name.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isNullOrEmpty(enrollment)) {
            // Check for a valid password, if the user entered one.
            focusView = edt_enrollment_address;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }  else if (TextUtils.isNullOrEmpty(firstName)) {
            // check for First Name
            focusView = edt_first_name;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        } else if (TextUtils.isNullOrEmpty(LastName)) {
            // check for Contact No
            focusView = edt_last_name;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        if (cancel) {
            // There was an error; don't attempt Registration and focus the first
            // form field with an error.
            if (focusView != null) {
                focusView.requestFocus();
            }
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.

            studentselected = new JSONArray();

            shareAttandanceJson = new JSONObject();
            try {
                shareAttandanceJson.put("first_name", firstName);
                shareAttandanceJson.put("last_name", LastName);
                shareAttandanceJson.put("enroll", enrollment);
                shareAttandanceJson.put("cid", SessionManager.getInstance(getActivity()).getCollage().getTnid());
                shareAttandanceJson.put("parent_email", email);
                studentselected.put(shareAttandanceJson);


                Log.d("TAG", "studentselected: " + studentselected);
                Log.d("TAG", "sharefriendJson: " + shareAttandanceJson);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            ProgressCreateMemberInCollage(studentselected.toString());



        }
    }

    private void ProgressCreateMemberInCollage(final String SlectedStudent) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_CREATE_STUDENTINCOLLAGE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);

                                    JSONObject newData = obj.getJSONObject("data");
                                    if (newData != null && newData.length() > 0) {

                                        New_User = newData.getString("is_new");
                                        NewUser_id = newData.getString("uid");

                                        SearchedInviteStudent(Course_id,NewUser_id);

                                        }
                                    }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(CreateMemberInCollage.this).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(CreateMemberInCollage.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(CreateMemberInCollage.this).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("records",SlectedStudent);

                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);
    }

    private void SearchedInviteStudent(String course_id, String newUser_id) {

        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_ADDSTUDENTINSUBJECT+"/"+course_id+"/"+newUser_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {

                                    finish();



                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(CreateMemberInCollage.this).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(CreateMemberInCollage.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(CreateMemberInCollage.this).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type","subject");
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.createstudent_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.Upload_Csv:


                return true;



            case android.R.id.home:
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }


    @Override
    public void onBackPressed() {
        finish();
    }

}
