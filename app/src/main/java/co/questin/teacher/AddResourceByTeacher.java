package co.questin.teacher;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.soundcloud.android.crop.Crop;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.ResourceAddAdapter;
import co.questin.library.StateBean;
import co.questin.models.ResourceAddList;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

public class AddResourceByTeacher extends BaseAppCompactActivity {
    ArrayList<ResourceAddList> dataList;
    String Flag;
    RelativeLayout previewlayout;
    Bitmap thumbnail = null;
    String Fileimagename;
    String path;
    ImageView imagepreview,cancelPreview;
    String ComeTittle,ComeValue;
    EditText Titttle, Value;
    TextView Addresource,Add;
    ListView resourcelist;
    ResourceAddAdapter adapter;
    String versionstr, osstr, curFileName, filepath;
    Dialog AttachmentDialog;
    String data, Course_id, jsonResource;
    ArrayList<String> arr = null;
    ProgressDialog progressDialog;
    private File selectedFile;
    private ArrayList<String> selectedResourceSend;
    static String strFile = null;
    private static final int REQUEST_PATH = 3;
    /*new cam/gallary*/
    private static final int ACTIVITY_START_CAMERA_APP = 0;
    private String mImageFileLocation = "";
    private int SELECT_FILE = 1;
    private static final int REQUEST_CODE = 3;
    String fullPath;
    public static final int RequestPermissionCode = 1;
    boolean ExternslWriteAccepted,ExternslreadAccepted,cameraAccepted;
    Boolean CallingCamera,CallingGallary,CallingAttachment;
    Uri imageUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_resource_by_teacher);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Flag ="FLASE";
        dataList = new ArrayList<ResourceAddList>();

        Bundle b = getIntent().getExtras();
        // path = b.getString("path");
        Course_id = b.getString("COURSE_ID");
        arr = b.getStringArrayList("array_list");
        Log.i("List", "Passed Array List :: " + arr);
        Log.i("path", "Passed Array List :: " + path);



        Addresource = findViewById(R.id.Addresource);
        resourcelist = findViewById(R.id.resourcelist);
        Titttle = findViewById(R.id.Titttle);
        Value = findViewById(R.id.Value);
        imagepreview  =findViewById(R.id.imagepreview);
        cancelPreview  =findViewById(R.id.cancelPreview);
        previewlayout  =findViewById(R.id.previewlayout);
        Add =findViewById(R.id.Add);

        Value.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("http://")){
                    Value.setText("http://");
                    Selection.setSelection(Value.getText(), Value.getText().length());

                }

            }
        });

        Addresource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AttachmentDialogOpen();

            }
        });

        cancelPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                previewlayout.setVisibility(View.GONE);
                strFile ="";
                curFileName="";
                Addresource.setVisibility(View.VISIBLE);
                Add.setVisibility(View.VISIBLE);

            }
        });
    }

    private void AttachmentDialogOpen() {

        AttachmentDialog = new Dialog(this);
        AttachmentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        AttachmentDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.WHITE));
        AttachmentDialog.setContentView(R.layout.list_of_resourcemenu);

        AttachmentDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        AttachmentDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        AttachmentDialog.show();

        TextView Drive = AttachmentDialog.findViewById(R.id.Drive);
        TextView Link = AttachmentDialog.findViewById(R.id.Link);
        TextView File = AttachmentDialog.findViewById(R.id.File);
        TextView Youtube = AttachmentDialog.findViewById(R.id.Youtube);
        TextView Photo = AttachmentDialog.findViewById(R.id.Photo);
        TextView Camera = AttachmentDialog.findViewById(R.id.Camera);


        Drive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Value.setVisibility(View.VISIBLE);
                Flag ="TRUE";
                AttachmentDialog.dismiss();
                Addresource.setVisibility(View.GONE);
            }
        });


        Link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Value.setVisibility(View.VISIBLE);
                Flag ="FAlSE";
                AttachmentDialog.dismiss();
                Addresource.setVisibility(View.GONE);

            }
        });

        File.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CallingAttachment =true;
                CallingCamera =false;
                CallingGallary =false;


                if(checkPermission()){
                    AddValueFromTheBrows();
                }


                Flag ="FALSE";


            }
        });

        Youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Value.setVisibility(View.VISIBLE);
                Flag ="TRUE";
                AttachmentDialog.dismiss();
                Addresource.setVisibility(View.GONE);
            }
        });

        Photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Flag ="FLASE";
                imagepreview.setImageDrawable(null);
                CallingGallary =true;
                CallingAttachment =false;
                CallingCamera =false;

                if(checkPermission()){
                    galleryIntent();
                }


                AttachmentDialog.dismiss();
            }
        });
        Camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Flag ="FLASE";
                CallingCamera =true;
                CallingGallary =false;
                CallingAttachment =false;
                if(checkPermission()){
                    CallCamera();
                }
            }
        });





    }


    private void CallCamera() {
        imagepreview.setImageDrawable(null);
        Intent callCameraApplicationIntent = new Intent();
        callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        File photoFile = null;
        try {
            photoFile = createImageFile();

        } catch (IOException e) {
            e.printStackTrace();
        }
        imageUri = FileProvider.getUriForFile(AddResourceByTeacher.this, "co.questin.fileprovider",photoFile);

        callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(callCameraApplicationIntent, ACTIVITY_START_CAMERA_APP);
        AttachmentDialog.dismiss();
    }


    private void AddValueFromTheBrows() {

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        String[] mimeTypes = {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                "text/plain",
                "application/pdf"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, REQUEST_CODE);

        AttachmentDialog.dismiss();
        Flag ="FALSE";

    }


    private void AddValueToList() {


        Titttle.setError(null);
        Value.setError(null);

        // Store values at the time of the login attempt.
        versionstr = Titttle.getText().toString().trim();
        osstr = Value.getText().toString().trim();


        boolean cancel = false;
        View focusView = null;


        if (android.text.TextUtils.isEmpty(versionstr)) {
            focusView = Titttle;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            ResourceAddList version = new ResourceAddList();
            version.setTitle(versionstr);
            version.setValue(osstr);
            dataList.add(version);
            resourcelist.setAdapter(adapter);
            adapter = new ResourceAddAdapter(AddResourceByTeacher.this, R.layout.list_of_addresource, dataList);
            adapter.notifyDataSetChanged();
            Gson gson = new Gson();
            jsonResource = gson.toJson(dataList);
            System.out.println("json = " + jsonResource);




        }




    }
    private void galleryIntent()
    {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(galleryIntent, "Select File"),SELECT_FILE);
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if(requestCode == ACTIVITY_START_CAMERA_APP && resultCode == RESULT_OK) {
            beginCrop(imageUri);

        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
        }
        if (requestCode == Crop.REQUEST_CROP) {

            handleCrop(resultCode, data);

        }
        if (requestCode == REQUEST_CODE &&resultCode == RESULT_OK) {

            Uri FileURI = data.getData();
            String uriString = FileURI.toString();
            selectedFile = new File(uriString);
            try {
                fullPath = Utils.getPath(this,FileURI);

            }catch (NumberFormatException e){

                System.out.println("not a number");

            } catch (Exception e){

                System.out.println(e);
            }


            String FileName = Utils.getDataColumn(this,FileURI,null,null);

            Log.d("TAG", "onActivityResult: " + FileName + FileName );

            String path = selectedFile.toString();

            String filepath = path;

            String displayName = null;

            if (uriString.startsWith("content://")) {

                Cursor cursor = null;

                try {

                    cursor = getContentResolver().query(FileURI, null, null, null, null);

                    if (cursor != null && cursor.moveToFirst()) {

                        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);

                        Fileimagename = cursor.getString(nameIndex);

                        Log.d("TAG", "onActivityResult: " + Fileimagename + filepath + fullPath);

                        /*Log.d("TAG", "onActivityResult2: " + Fileimagename);*/


                        if (fullPath != null) {


                            if (filepath.matches("(.*)providers(.*)")) {

                                String fullfilePath = fullPath + "/" + Fileimagename;

                                File file = new File(fullfilePath);
                                int fulllsize = (int) file.length()/1024;

                                Log.d("TAG", "onActivityResult4: " + fulllsize);

                                if (fulllsize/1024<8) {
                                    convertFileToString(fullfilePath, Fileimagename);
                                }else{
                                    Toast.makeText(this, "file size is greater than 8mb it can not be upload", Toast.LENGTH_SHORT).show();

                                }
                                Log.d("TAG", "onActivityResult3: " + fullfilePath);
                            } else if (filepath.matches("(.*)externalstorage(.*)")) {

                                fullPath = fullPath;
                                File file = new File(fullPath);
                                int fulllsize = (int) file.length() / 1024;

                                if (fulllsize / 1024 < 8)

                                {

                                    convertFileToString(fullPath, Fileimagename);
                                    Log.d("TAG", "onActivityResult4: " + fullPath);

                                } else {
                                    Toast.makeText(this, "file size is greater than 8mb it can not be upload", Toast.LENGTH_SHORT).show();

                                }


                            }


                        } else {
                            Addresource.setVisibility(View.VISIBLE);
                            Toast.makeText(this, "File not found !! Get it from internal/external storage", Toast.LENGTH_SHORT).show();
                        }

                    }






                }catch (Exception e )
                {
                    e.printStackTrace();
                }

            }




        }


    }


    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(result));
                thumbnail = getResizedBitmap(thumbnail,1000);
                strFile = encodeImageTobase64(thumbnail);
                System.out.println("camera " + strFile);
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setImageBitmap(thumbnail);
                Addresource.setVisibility(View.GONE);
                Add.setVisibility(View.GONE);




            } catch (IOException e) {
                e.printStackTrace();
            }




        } else if (resultCode == Crop.RESULT_ERROR) {
            //  Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }



    public String convertFileToString(String pathOnSdCard, String curFileName){

        File file = new File(pathOnSdCard);

        try {

            byte[] data = FileUtils.readFileToByteArray(file);//Convert any file, image or video into byte array


            // byte[] data = FileUtils.readFileToByteArray(file); //Convert any file, image or video into byte array

            strFile = Base64.encodeToString(data, Base64.NO_WRAP);

            //Convert byte array into string

            System.out.println("file in bitmap first method " + strFile);

            Fileimagename =curFileName;

            Log.d("TAG", "onActivityResult2: " + strFile);

            System.out.println("filename in bitmap first method " + Fileimagename);

            // CreateFileMsg(strFile,curFileName);

            String Extension = Fileimagename.substring(Fileimagename.lastIndexOf("."));

            if (Extension.matches(".jpeg") || Extension.matches(".jpg"))
            {
                imagepreview.setImageBitmap(BitmapFactory.decodeFile(fullPath));//convert file to bitmap

                previewlayout.setVisibility(View.VISIBLE);

            }else if (Extension.matches(".png") )
            {
                imagepreview.setImageBitmap(BitmapFactory.decodeFile(fullPath));//convert file to bitmap

                previewlayout.setVisibility(View.VISIBLE);

            }else{

                imagepreview.setBackgroundResource(R.mipmap.file);
                Addresource.setVisibility(View.GONE);
                previewlayout.setVisibility(View.VISIBLE);

            }

            /*imagepreview.setBackgroundResource(R.mipmap.file);

            previewlayout.setVisibility(View.VISIBLE);*/

        } catch (Exception e) {

            e.printStackTrace();

        }

        return strFile;

    }



    //Choose From Gallery
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {

            try {
                beginCrop(data.getData());
                Uri uri = data.getData();
                String[] projection = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                Log.d("TAG", DatabaseUtils.dumpCursorToString(cursor));

                int columnIndex = cursor.getColumnIndex(projection[0]);
                Fileimagename = cursor.getString(columnIndex); // full path of image

                cursor.close();

            }catch (Exception e)
            {
                e.printStackTrace();
            }


        }


    }


    File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        Fileimagename = image.getName();
        return image;

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.submit_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Submit:
                AddFileInSubjectProcess();
                AddValueToList();

                return true;
            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void AddFileInSubjectProcess() {

        Titttle.setError(null);


        // Store values at the time of the login attempt.
        versionstr = Titttle.getText().toString().trim();


        if(strFile ==null){
            showSnackbarMessage(getString(R.string.error_field_attac));

            strFile ="";
        }

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (android.text.TextUtils.isEmpty(versionstr)) {
            focusView = Titttle;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }
        else  if (android.text.TextUtils.isEmpty(strFile)) {
            focusView = Titttle;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_attac));

            // Check for a valid email address.
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.
            CourseFileCreateTeacherAuthTask();




        }
    }



    @Override
    public void onBackPressed() {
        finish();
    }


    private void CourseFileCreateTeacherAuthTask() {
        ShowIndicator();

        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_UPLOADFILETOSERVER+"/"+Course_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {

                                    CourseModuleTeacherInfo.RefreshWorkedsubjectresource();
                                    finish();




                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(AddResourceByTeacher.this).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(AddResourceByTeacher.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(AddResourceByTeacher.this).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("field","field_attachments");
                params.put("file",strFile);
                params.put("filename",Fileimagename);
                params.put("description",versionstr);

                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);


    }


    private  boolean checkPermission() {
        int camerapermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionLocation = ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), RequestPermissionCode);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("TAG", "Permission callback called-------");
        switch (requestCode) {
            case RequestPermissionCode: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("TAG", "sms & location services permission granted");
                        if (CallingCamera ==true){
                            CallingGallary=false;
                            CallCamera();

                        }else if (CallingGallary ==true) {
                            CallingCamera=false;

                            galleryIntent();


                        }else if (CallingAttachment==true){
                            AddValueFromTheBrows();

                        }
                    } else {
                        Log.d("TAG", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Service Permissions are required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkPermission();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    finish();
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
    private void explain(String msg){
        final android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        //  permissionsclass.requestPermission(type,code);
                        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:com.exampledemo.parsaniahardik.marshmallowpermission")));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        finish();
                    }
                });
        dialog.show();
    }
}
