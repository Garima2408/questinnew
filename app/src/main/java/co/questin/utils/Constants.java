package co.questin.utils;

import android.net.Uri;

import okhttp3.MediaType;

/**
 * Created by farheen on 8/9/17
 */

public class Constants {
    public static final String DATA_FILE = "DATA_FILE";
    public static final String USER_EMAIL = "USER_EMAIL";

    // Replace this key with your's
    public static final String DEVELOPER_KEY = "AIzaSyBzpxEmbDMrC4ybF69KYNj9Xo9zrlRxn6w";

    // YouTube video id
    public static final String YOUTUBE_VIDEO_CODE = "_oEA18Y8gM0";

    public static final String USER_ROLE = "USER_ROLE";

    public static  String USER_INTENT = "TRUE";
    public static  String USER_BACKINTENT = "TRUE";

    public static final String STORAGE_PATH_UPLOADS = "uploads/";
    public static final String DATABASE_PATH_UPLOADS = "uploads";


    public static final int ROLE_NULL = 0;
    public static final int ROLE_STUDENT = 1;
    public static final int ROLE_ALUMNI = 2;
    public static final int ROLE_FACULTY = 3;
    public static final int ROLE_PARENT = 4;
    public static final int ROLE_VISITOR = 5;
    public static final int ROLE_DRIVER =6;


    public static final String IS_LOGGED_IN = "IS_LOGGED_IN";
    public static final String IS_COLLEGE_ADDED = "IS_COLLEGE_ADDED";
    public static final String IS_LOGGED_OUT = "IS_LOGGED_OUT";

    // Intent Actions
    public static final String ACTION_LOCATION_UPDATE = "ACTION_LOCATION_UPDATE";
    public static final String ACTION_GEOFENCE_EXIT = "ACTION_GEOFENCE_EXIT";
    public static final String ACTION_MESSAGE_RECEIVED = "ACTION_MESSAGE_RECEIVED";
    public static final String ACTION_CHAT_MESSAGE_RECEIVED = "ACTION_CHAT_MESSAGE_RECEIVED";
    public static final String ACTION_SERVER_CHAT_MESSAGE_RECEIVED = "ACTION_CHAT_MESSAGE_RECEIVED";
    public static final String ACTION_GROUPCHAT_MESSAGE_RECEIVED = "ACTION_GROUPCHAT_MESSAGE_RECEIVED";
    public static final String ACTION_IMAGE_DOWNLOADED = "ACTION_IMAGE_DOWNLOADED";

    public static final String ACTION_CHAT_LOCATION= "ACTION_CHAT_LOCATION";
    public static final String ACTION_CHAT_PDF = "ACTION_CHAT_PDF";
    public static final String ACTION_CHAT_CONTACT = "ACTION_CHAT_CONTACT";







    // Intent EXTRAs
    public static final String EXTRA_ROUTE_NAME = "EXTRA_ROUTE_NAME";
    public static final String EXTRA_ROUTE_ID = "EXTRA_ROUTE_ID";
    public static final String EXTRA_MY_STOP = "EXTRA_MY_STOP";
    public static final String EXTRA_GEOFENCE_NAME = "EXTRA_GEOFENCE_NAME";
    public static final String EXTRA_BUS_NO = "EXTRA_BUS_NO";
    public static final String EXTRA_COLLEGE_ID = "EXTRA_COLLEGE_ID";
    public static final String EXTRA_LOCATION = "EXTRA_LOCATION";
    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    public static final String EXTRA_IS_FIRST_MESSAGE = "EXTRA_IS_FIRST_MESSAGE";

    public static final String GOOGLE_STATUS_DENIED = "REQUEST_DENIED";
    public static final String GOOGLE_STATUS_OK = "OK";
    //public static final String MAPS_API_KEY = "AIzaSyCW8YG-yc3JpScHQazMwK0Yy2Avkxjdpu4";

    //test api key for map
    public static final String MAPS_API_KEY = "AIzaSyCHa6KfGH5a5jZErMjRa6lfYVRNF6XUgec";


    //live
    //   public static final String MAPS_API_KEY = "AIzaSyBWuKIiuNn6L9gt5ovdH3GWdoyszFVqg0c";

   // public static final String MAPS_API_KEY = "AIzaSyDElS5pWnGuhLujdEd9MzW3bvVOU7BcjH0";
    public static final String DISTANCEMATRIX_API_KEY = "AIzaSyCi0mMYmfauuFhMEvwDLhe91ML4n2wAD5g";



    public static final float GEOFENCE_RADIUS =500.0f;
    public static final long ETA_UPDATE_INTERVAL = 20000; // in milli-seconds = 20 secs

    // Notification Channels for Android Oreo
    public static final String CH_TRACKING_NOTIFICATION = "CH_TRACKING_NOTIFICATION";
    public static final String CH_CHAT_MESSAGE = "CH_CHAT_MESSAGE";

    // Notification Ids
    public static final int ID_TRACKING_NOTIFICATION = 1;
    public static final int ID_CHAT_NOTIFICATION = 2;

    public static final String DP_URL = "DP_URL";
    public static final String USER_NAME = "USER_NAME";
    public static final String GROUP_ID = "GROUP_ID";
    public static final String SENDER_ID = "SENDER_ID";
    public static final String FCM_REG_TOKEN = "FCM_REG_TOKEN";
    public static final String CHAT_MODEL = "CHAT_MODEL";
    public static final String GROUP_MODEL = "GROUP_MODEL";
    public static final String CONVERSATION_MODEL = "CONVERSATION_MODEL";
    public static final String IS_FCM_TOKEN_REGISTERED = "IS_FCM_TOKEN_REGISTERED";
    public static final String JOIN_GROUP = "JOIN_GROUP";

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    // FCM message types
    public static final int FCM_TYPE_CHAT = 1;

   public  static final String CART_FRIENDS_LIST="card_friends_lsit";


    // FCM message types
    public static final int FCM_TYPE_GROUPCHAT = 2;

    // PendingIntent Request Codes
    public static final int RC_PI_CHAT_MESSAGE = 1;


    public static final String RUNNIN_FIRST = "RUNNIN_FIRST";

    public static final int ROLE_RUNNING_TRUE = 1;
    public static final int ROLE_RUNNING_FALSE= 0;



    public static final String RUNNIN_FIRST_PROFILE = "RUNNIN_FIRST_PROFILE";

    public static final int ROLE_RUNNING_TRUE_PROFILE = 1;
    public static final int ROLE_RUNNING_FALSE_PROFILE= 0;


    public static final String RUNNIN_FIRST_CAMPUSFEED = "RUNNIN_FIRST_CAMPUSFEED";
    public static final int ROLE_RUNNING_TRUE_CAMPUSFEED = 1;
    public static final int ROLE_RUNNING_FALSE_CAMPUSFEED= 0;





    public static final String RUNNIN_FIRST_CHAT = "RUNNIN_FIRST_CHAT";

    public static final int ROLE_RUNNING_TRUE_CHAT = 1;
    public static final int ROLE_RUNNING_FALSE_CHAT= 0;

// The constants below are less interesting than those above.

    // Path for the DataItem containing the last geofence id entered.
    public static final String GEOFENCE_DATA_ITEM_PATH = "/geofenceid";
    public static final Uri GEOFENCE_DATA_ITEM_URI =
            new Uri.Builder().scheme("wear").path(GEOFENCE_DATA_ITEM_PATH).build();
    public static final String KEY_GEOFENCE_ID = "geofence_id";

/*Constants For GeoFence*/

    public static final String GEOFENCE_ID_STAN_UNI = "STAN_UNI";
    public static final float GEOFENCE_RADIUS_IN_METERS = 150;

    /**
     * Map for storing information about stanford university in the Stanford.
     */





}
