package co.questin.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.questin.models.AccessDeviceToken;
import co.questin.models.AllNotificationArray;
import co.questin.models.CampusNotificationArray;
import co.questin.models.CollageLists;
import co.questin.models.CollageRoleArray;
import co.questin.models.UserDetail;
import co.questin.models.chat.SendMessageModel;


public class SessionManager {

    private static SessionManager instance;
    private final String PREFERENCE_NAME = "QUESTIN_PREFERENCES";
    private final Context mContext;
    public static final String KEY_NAME = "tokenxcess";
    private SharedPreferences prefs;
    public static final String ADMIN_NOTIFICATION = "admin_notification";
    public static final String FEED_NOTIFICATION = "feed_notification";
    public static final String All_NOTIFICATION = "All_notification";
    private final String PREF_NAME = "QUESTIN_Admin";
    ArrayList<String> arrPackage;
    /**
     * Method to get single instance of this class
     *
     * @return SessionManager instance of class
     */

    public static SessionManager getInstance(Context context) {
        if (instance == null) {
            instance = new SessionManager(context);
        }
        return instance;
    }

    public SessionManager(Context context) {

        mContext = context;
    }

    private ComplexPreferences getPreference() {
        return ComplexPreferences.getComplexPreferences(mContext, PREFERENCE_NAME, Context.MODE_PRIVATE);
    }



    public void saveUser(UserDetail user) {
        ComplexPreferences preferences = getPreference();
        preferences.putObject(UserDetail.class.getName(), user);
        preferences.commit();
    }


    public UserDetail getUser() {
        ComplexPreferences preferences = getPreference();
        UserDetail object = preferences.getObject(UserDetail.class.getName(), UserDetail.class, null);
        return object;
    }




    public void saveUserClgRole(CollageRoleArray userrole) {
        ComplexPreferences preferences = getPreference();
        preferences.putObject(CollageRoleArray.class.getName(), userrole);
        preferences.commit();
    }


    public CollageRoleArray getUserClgRole() {
        ComplexPreferences preferences = getPreference();
        CollageRoleArray role = preferences.getObject(CollageRoleArray.class.getName(), CollageRoleArray.class, null);
        return role;
    }



    public void saveAccesstoken(AccessDeviceToken tokenacess) {
        ComplexPreferences preferences = getPreference();
        preferences.putObject(AccessDeviceToken.class.getName(), tokenacess);
        preferences.commit();
    }
    public AccessDeviceToken getaccesstoken() {
        ComplexPreferences preferences = getPreference();
        AccessDeviceToken object = preferences.getObject(AccessDeviceToken.class.getName(), AccessDeviceToken.class, null);
        return object;
    }





    public void saveCollage(CollageLists clglist) {
        ComplexPreferences preferences = getPreference();
        preferences.putObject(CollageLists.class.getName(), clglist);
        preferences.commit();
    }


    public CollageLists getCollage() {
        ComplexPreferences preferences = getPreference();
        CollageLists object = preferences.getObject(CollageLists.class.getName(), CollageLists.class, null);
        return object;
    }









    public void deleteUser() {
        getPreference().clear().commit();
    }

    /**
     * put string preferences
     *
     * @param key     The name of the preference to modify
     * @param value   The new value for the preference
     * @return True if the new values were successfully written to persistent storage.
     */
    public boolean putString(String key, String value) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        return editor.commit();
    }


    /**
     * get string preferences
     *
     * @param key          The name of the preference to retrieve
     * @param defaultValue Value to return if this preference does not exist
     * @return The preference value if it exists, or defValue. Throws ClassCastException if there is a preference with
     * this name that is not a string
     */
    public String getString(String key, String defaultValue) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getString(key, defaultValue);
    }

    /**
     * remove Key from preferences
     *
     * @param key     The name of the preference to modify
     * @return True if the new values were successfully written to persistent storage.
     */
    public boolean removeKey(String key) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        return editor.commit();
    }


    // This four methods are used for maintaining Admin Notification log.


    public void saveAdminNotification(Context context, List<SendMessageModel> notice) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(notice);

        editor.putString(ADMIN_NOTIFICATION, jsonFavorites);

        editor.commit();
    }

    public void addAdminNotification(Context context, SendMessageModel product) {
        List<SendMessageModel> favorites = getAdminNotification(context);
        if (favorites == null)
            favorites = new ArrayList<SendMessageModel>();
        favorites.add(product);
        saveAdminNotification(context, favorites);
    }

    public void removenotification(Context context) {
        ArrayList<SendMessageModel> favorites = getAdminNotification(context);


        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.clear();
        editor.commit();


    }

    public ArrayList<SendMessageModel> getAdminNotification(Context context) {
        SharedPreferences settings;
        List<SendMessageModel> favorites;

        settings = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

        if (settings.contains(ADMIN_NOTIFICATION)) {
            String jsonFavorites = settings.getString(ADMIN_NOTIFICATION, null);
            Gson gson = new Gson();
            SendMessageModel[] favoriteItems = gson.fromJson(jsonFavorites,
                    SendMessageModel[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<SendMessageModel>(favorites);
        } else
            return null;

        return (ArrayList<SendMessageModel>) favorites;
    }



    /*All Questin App Notification*/


    public void saveAllAppNotification(Context context, List<AllNotificationArray> notice) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(notice);

        editor.putString(All_NOTIFICATION, jsonFavorites);

        editor.commit();
    }

    public void addAllAppNotification(Context context, AllNotificationArray product) {
        List<AllNotificationArray> favorites = getAllAppNotification(context);
        if (favorites == null)
            favorites = new ArrayList<AllNotificationArray>();
        favorites.add(product);
        saveAllAppNotification(context, favorites);
    }

    public void removeAllAppnotification(Context context) {
        ArrayList<AllNotificationArray> favorites = getAllAppNotification(context);


        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.clear();
        editor.commit();


    }

    public ArrayList<AllNotificationArray> getAllAppNotification(Context context) {
        SharedPreferences settings;
        List<AllNotificationArray> favorites;

        settings = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

        if (settings.contains(All_NOTIFICATION)) {
            String jsonFavorites = settings.getString(All_NOTIFICATION, null);
            Gson gson = new Gson();
            AllNotificationArray[] favoriteItems = gson.fromJson(jsonFavorites,
                    AllNotificationArray[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<AllNotificationArray>(favorites);
        } else
            return null;

        return (ArrayList<AllNotificationArray>) favorites;
    }


    /*All Campusfeed Notification*/


    public void saveCampusNotification(Context context, List<CampusNotificationArray> notice) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(notice);

        editor.putString(FEED_NOTIFICATION, jsonFavorites);

        editor.commit();
    }

    public void addCampusNotification(Context context, CampusNotificationArray product) {
        List<CampusNotificationArray> favorites = getCampusNotification(context);
        if (favorites == null)
            favorites = new ArrayList<CampusNotificationArray>();
        favorites.add(product);
        saveCampusNotification(context, favorites);
    }

    public void removeCampusnotification(Context context) {
        ArrayList<CampusNotificationArray> favorites = getCampusNotification(context);


        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.clear();
        editor.commit();


    }

    public ArrayList<CampusNotificationArray> getCampusNotification(Context context) {
        SharedPreferences settings;
        List<CampusNotificationArray> favorites;

        settings = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

        if (settings.contains(FEED_NOTIFICATION)) {
            String jsonFavorites = settings.getString(FEED_NOTIFICATION, null);
            Gson gson = new Gson();
            CampusNotificationArray[] favoriteItems = gson.fromJson(jsonFavorites,
                    CampusNotificationArray[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<CampusNotificationArray>(favorites);
        } else
            return null;

        return (ArrayList<CampusNotificationArray>) favorites;
    }

    /*ADD LOCATION SETTINGS*/



}
