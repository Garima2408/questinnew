package co.questin.utils;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.View;

import co.questin.activities.SignIn;
import co.questin.models.UserDetail;


public abstract class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    public void hideKeyBoard(View v) {
        ((BaseAppCompactActivity) getActivity()).hideKeyBoard(v);
    }

    public Boolean isInternetConnected() {
        return ((BaseAppCompactActivity) getActivity()).isInternetConnected();
    }



    public UserDetail getUser() {
        return ((BaseAppCompactActivity) getActivity()).getUser();
    }





}
