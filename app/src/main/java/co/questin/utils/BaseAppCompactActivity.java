package co.questin.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.client.Firebase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.activities.SignIn;
import co.questin.animationloading.AVLoadingIndicatorView;
import co.questin.campusfeedsection.AddCampusFeeds;
import co.questin.database.QuestinSQLiteHelper;
import co.questin.library.StateBean;
import co.questin.models.FilterArray;
import co.questin.models.UserDetail;
import co.questin.network.URLS;

import static co.questin.fcm.MessagingService.All_App_Notification;
import static co.questin.fcm.MessagingService.college_Allfeeds;
import static co.questin.fcm.MessagingService.college_assignment;
import static co.questin.fcm.MessagingService.college_class;
import static co.questin.fcm.MessagingService.college_classroom;
import static co.questin.fcm.MessagingService.college_exam;
import static co.questin.fcm.MessagingService.college_feeds;
import static co.questin.fcm.MessagingService.college_news;
import static co.questin.fcm.MessagingService.counter;
import static co.questin.fcm.MessagingService.singlechat;


public class BaseAppCompactActivity extends BaseActivity {
    static RelativeLayout loading_view;
    public QuestinSQLiteHelper questinSQLiteHelper;
    SessionManager sharedPreference;
    SharedPreferences sharedLocationpreferences;
    public AVLoadingIndicatorView indicatorView;
    String  filterText,filterid;
    private ArrayList<String> filterlist;
    ArrayList<FilterArray> listoffilter;
    ListView rec_select_pic;
     Dialog dialog;
    public static final String mypreference = "mypref";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(R.layout.activity_base);
        ViewGroup contentView = findViewById(R.id.content_view);
        contentView.addView(getLayoutInflater().inflate(layoutResID, null));
        questinSQLiteHelper = new QuestinSQLiteHelper(this);
        loading_view = findViewById(R.id.loading_view);
        indicatorView= (AVLoadingIndicatorView) findViewById(R.id.indicator);
        sharedPreference = new SessionManager(this);
        sharedLocationpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        filterlist = new ArrayList<String>();
        listoffilter = new ArrayList<FilterArray>();

    }


    public static void ShowIndicator(){
        loading_view.setVisibility(View.VISIBLE);

    }



    public static void hideIndicator() {
        loading_view.setVisibility(View.GONE);
    }





    public void hideKeyBoard(View v) {
        Utils.hideKeyBoard(BaseAppCompactActivity.this, v);
    }

    public Boolean isInternetConnected() {
        return Utils.isInternetConnected(BaseAppCompactActivity.this);
    }


    public UserDetail getUser() {
        return SessionManager.getInstance(getActivity()).getUser();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    /*Connection Snackbar */


    public static void showConnectionSnackbar() {

        final Snackbar snackBar = Snackbar.make(loading_view, "Check your connection", Snackbar.LENGTH_INDEFINITE);
        snackBar.setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBar.dismiss();

            }
        });
        snackBar.show();


    }


    public  static void showSnackbarinteger(int responce) {

        final Snackbar snackBar = Snackbar.make(loading_view, responce, Snackbar.LENGTH_INDEFINITE);
        snackBar.setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBar.dismiss();

            }
        });
        snackBar.show();


    }



    /*Connection Snackbar */


    public static void showServerSnackbar(int error_responce) {

        final Snackbar snackBar = Snackbar.make(loading_view, error_responce, Snackbar.LENGTH_INDEFINITE);
        snackBar.setAction("Ok", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBar.dismiss();

            }
        });
        snackBar.show();


    }

    /*
     *   Show Snackbar
     */
    public static void showSnackbarMessage(String message) {

        final Snackbar snackBar = Snackbar.make(loading_view, message, Snackbar.LENGTH_INDEFINITE);
        snackBar.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBar.dismiss();

            }
        });
        snackBar.show();
    }


    /*
     *   Show Finish Snackbar
     */
    public void showSnackbarFinishMessage(String message) {

        final Snackbar snackBar = Snackbar.make(loading_view, message, Snackbar.LENGTH_INDEFINITE);
        snackBar.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBar.dismiss();
                finish();

            }
        });
        snackBar.show();
    }





    // method for encode bitmap image to string
    public static String encodeImageTobase64(Bitmap image) {
        Bitmap immage = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immage.compress(Bitmap.CompressFormat.JPEG, 20, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        //  Log.d("Image Log:", imageEncoded);
        return imageEncoded;
    }

    // method for decode String to bitmap image
    public Bitmap decodeImageBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }



    /*ACTIVITIES SNACKBARS */

    public void showSnackbarForgetPassWord() {

        final Snackbar snackBar = Snackbar.make(loading_view, R.string.link_sent, Snackbar.LENGTH_INDEFINITE);
        snackBar.setAction("Proceed", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent upanel = new Intent(BaseAppCompactActivity.this, SignIn.class);
                upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(upanel);
                snackBar.dismiss();

            }
        });
        snackBar.show();
    }


    /*BAck to Sign In*/


    public void showSnackbarSignIn(String message) {

        final Snackbar snackBar = Snackbar.make(loading_view,message, Snackbar.LENGTH_INDEFINITE);
        snackBar.setAction("Proceed", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent upanel = new Intent(BaseAppCompactActivity.this, SignIn.class);
                upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(upanel);
                snackBar.dismiss();

            }
        });
        snackBar.show();
    }





    /*UPDATE FCM TOKEN*/

    public void UpdateFCMTokenToServer() {
        UpdateFcmToken();
    }



    public void UpdateFcmToken() {
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.REG_TOKEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    Utils.init(BaseAppCompactActivity.this);
                                    Utils.saveFCMId(BaseAppCompactActivity.this,FirebaseInstanceId.getInstance().getToken());
                                    Log.e("token",Utils.getFCMId());

                                }else {
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(getActivity()).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(getActivity()).getaccesstoken().sessionName+"="+SessionManager.getInstance(getActivity()).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type","android");
                params.put("token", FirebaseInstanceId.getInstance().getToken());
                return params;
            }


        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);
    }





    /* Delete fcm Token*/


    public void DeleteFireBaseToken(){

        StringRequest mStrRequest = new StringRequest(Request.Method.DELETE, URLS.REG_TOKEN+"/" + Utils.getFCMId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    ProgressLogout();
                                }
                            } else {
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            showServerSnackbar(R.string.error_responce);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                        } else {
                        }
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(getActivity()).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(getActivity()).getaccesstoken().sessionName+"="+SessionManager.getInstance(getActivity()).getaccesstoken().sessionID);

                return params;
            }




        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);
    }

    /*LOGOUT */


    private void ProgressLogout() {

        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_LOGOUT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    Utils.clearData();


                                    try{
                                        //user chat delete
                                        Utils.deleteUserChatAndTable(BaseAppCompactActivity.this);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }

                                    //use for clear snappy db
                                    DB snappyDB = null;
                                    try {
                                        snappyDB = DBFactory.open(BaseAppCompactActivity.this);
                                        if (snappyDB.exists(Constants.CART_FRIENDS_LIST)) {

                                            snappyDB.destroy();

                                        }
                                    } catch (SnappydbException e) {
                                        e.printStackTrace();
                                    }
                                    sharedPreference.removeAllAppnotification(BaseAppCompactActivity.this);
                                    sharedPreference.removenotification(BaseAppCompactActivity.this);
                                    sharedPreference.removeCampusnotification(BaseAppCompactActivity.this);
                                    SessionManager.getInstance(getApplicationContext()).deleteUser();
                                    Utils.getSharedPreference(BaseAppCompactActivity.this).edit()
                                            .putInt(Constants.USER_ROLE, Constants.ROLE_NULL).apply();
                                    Utils.removeStringPreferences(BaseAppCompactActivity.this,"0");
                                    counter = 0;
                                    college_news = 0;
                                    singlechat = 0;
                                    college_class = 0;
                                    college_exam = 0;
                                    college_assignment = 0;
                                    college_feeds = 0;
                                    college_Allfeeds = 0;
                                    college_classroom = 0;
                                    All_App_Notification = 0;


                                    Intent upanel = new Intent(BaseAppCompactActivity.this,SignIn.class);
                                    upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(upanel);
                                    finish();

                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(getActivity()).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(getActivity()).getaccesstoken().sessionName+"="+SessionManager.getInstance(getActivity()).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }


        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);


    }








    public  void selectyourarea_dialog(final Context context) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.radius_dialog);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        rec_select_pic =  dialog.findViewById(R.id.rec_select_pic);
        GetTheListOfFilters();
        dialog.show();








    }
    private  void GetTheListOfFilters() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_FILTERLIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray data = obj.getJSONArray("data");

                                    for (int i=8; i<data.length();i++) {
                                        FilterArray GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), FilterArray.class);
                                        filterlist.add(GoalInfo.getLabel());
                                        listoffilter.add(GoalInfo);

                                        rec_select_pic.setAdapter(new ArrayAdapter<String>(BaseAppCompactActivity.this,
                                                android.R.layout.simple_spinner_dropdown_item,
                                                filterlist));



                                        rec_select_pic.setOnItemClickListener(new AdapterView.OnItemClickListener()
                                        {
                                            @Override
                                            public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
                                            {
                                                filterText = listoffilter.get(position).getLabel();
                                                filterid = listoffilter.get(position).getId();
                                                Log.d("TAG", "onItemSelected: "+filterText
                                                        +filterid );

                                                SharedPreferences.Editor editor = sharedLocationpreferences.edit();
                                                editor.putString("id", filterid);
                                                editor.putString("label", filterText);
                                                editor.commit();

                                                dialog.dismiss();

                                            }
                                        });

                                        }



                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(BaseAppCompactActivity.this).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(BaseAppCompactActivity.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(BaseAppCompactActivity.this).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("vid","35");

                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);



    }




}






















