package co.questin.chat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.Event.ConversationFire;
import co.questin.R;
import co.questin.library.StateBean;
import co.questin.models.chat.GroupConversationModel;
import co.questin.models.chat.MyChatGroupArray;
import co.questin.network.URLS;
import co.questin.utils.BaseFragment;
import co.questin.utils.SessionManager;

import static co.questin.utils.BaseAppCompactActivity.ShowIndicator;
import static co.questin.utils.BaseAppCompactActivity.hideIndicator;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;
import static co.questin.utils.Utils.isStatusSuccess;

public class MyChatsGroups extends BaseFragment {
    static RecyclerView MyGroupsName_list;
    FloatingActionButton btnNewChat;
    public static ArrayList<MyChatGroupArray> mchatmemberList;
    public ArrayList<GroupConversationModel> databasedataList;
    private static MyChatGroupsAdapters chatmemberadapter;
    private RecyclerView.LayoutManager layoutManager;
    public static String groupId,title;
    private int RC_NEW_GROUP_CHAT = 1;
    static Activity activity;
    static LinearLayout notChat;
    private static ProgressBar spinner;

    public MyChatsGroups() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_my_chats_groups, container, false);
        activity = (Activity)view.getContext();
        mchatmemberList=new ArrayList<>();
        databasedataList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(activity);
        MyGroupsName_list = view.findViewById(R.id.MyGroupsName_list);
        spinner= view.findViewById(R.id.progressBar);
        btnNewChat =view.findViewById(R.id.btn_new_chat);
        notChat =view.findViewById(R.id.notChat);
        MyGroupsName_list.setHasFixedSize(true);
        MyGroupsName_list.setLayoutManager(layoutManager);
        RefreshChatListWhileAdding();
        btnNewChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newChatIntent = new Intent(activity, AddGroupChatList.class);
                startActivityForResult(newChatIntent, RC_NEW_GROUP_CHAT);
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //  RefreshChatListWhileAdding();
    }

    public static void RefreshChatListWhileAdding() {
        mchatmemberList.clear();

        if (SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {

            ChatGroupMemberDisplay(SessionManager.getInstance(activity).getUser().getParentUid());



        }else {
         ChatGroupMemberDisplay(SessionManager.getInstance(activity).getUser().getUserprofile_id());

        }




    }

    private static void ChatGroupMemberDisplay(String userprofile_id) {
        ShowIndicator();
            StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_MYGROUPSMEMBER+"?"+"uid="+userprofile_id+"&limit=100&offset=0",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i("VOLLEY", response);
                            try {

                                Gson gson = new GsonBuilder().create();
                                JsonParser jsonParser = new JsonParser();
                                JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                                StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                                if (stateResponse != null) {
                                    hideIndicator();
                                    if (isStatusSuccess(stateResponse.getStatus())) {
                                        JSONObject obj = new JSONObject(response);
                                        JSONArray jsonArrayData = obj.getJSONArray("data");
                                        mchatmemberList.clear();
                                        if (jsonArrayData !=null && jsonArrayData.length()>0) {


                                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                                MyChatGroupArray memberInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), MyChatGroupArray.class);
                                                mchatmemberList.add(memberInfo);
                                                chatmemberadapter = new MyChatGroupsAdapters(activity, mchatmemberList);
                                                MyGroupsName_list.setAdapter(chatmemberadapter);
                                                notChat.setVisibility(View.GONE);
                                                MyGroupsName_list.setVisibility(View.VISIBLE);

                                            }
                                        }else {
                                            notChat.setVisibility(View.VISIBLE);
                                            MyGroupsName_list.setVisibility(View.GONE);
                                        }
                                    } else {
                                        hideIndicator();
                                        showSnackbarMessage(stateResponse.getMessage());
                                    }
                                } else {
                                    hideIndicator();
                                    showServerSnackbar(R.string.error_responce);


                                }
                            } catch (Exception e) {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideIndicator();
                            if (error instanceof NoConnectionError) {
                                showConnectionSnackbar();
                            } else {
                                showServerSnackbar(R.string.error_responce);
                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                    params.put("Accept-Language", "application/json");
                    params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                    params.put("Cookie",Cookies);

                    return params;
                }
            };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);
        }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ConversationFire event) {

        chatmemberadapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
