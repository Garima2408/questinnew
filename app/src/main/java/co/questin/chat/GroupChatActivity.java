package co.questin.chat;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.soundcloud.android.crop.Crop;

import org.apache.commons.io.FileUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.Event.LatiLng;
import co.questin.R;
import co.questin.database.QuestinContract;
import co.questin.database.QuestinSQLiteHelper;
import co.questin.library.StateBean;
import co.questin.models.chat.GroupChatModel;
import co.questin.models.chat.GroupConversationModel;
import co.questin.models.chat.SendGroupMessageModel;
import co.questin.models.fcm.FcmGroupChatModel;

import co.questin.network.URLS;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.RequestBody;

import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;

public class GroupChatActivity extends AppCompatActivity implements GroupChatListAdapter.ChatListCommunicator ,LocationListener {
    LocationManager locationManager;
    String MainLAtLong;
    String provider;
    String textMainmsg,groupname,groupid,groupImage,MyName,is_admin;
    static String strFile = null;
    private Gson gson;
    private String myId;
    private File selectedFile;
    private GroupChatListAdapter chatListAdapter;
    private SendGroupMessageModel sendMessageModel;
    private QuestinSQLiteHelper questinSQLiteHelper;
    Dialog OptionDialog;
    private Uri currentImageUri;
    Uri FileURI;
    private Uri currentImageDownloadUri;
    private static final int IMAGE_GALLERY_REQUEST = 1;
    private static final int IMAGE_CAMERA_REQUEST = 2;
    private static final int PLACE_PICKER_REQUEST = 3;
    private static final int REQUEST_PATH = 3;
    private static final int RQS_PICK_CONTACT = 4;
    private static final int RC_TAKE_PICTURE = 1;
    private static final int RC_SELECT_IMAGE = 2;
    String Fileimagename, fullpath;
    String  is_group_chat ="TRUE";
    private boolean isFirstTime;
    private ArrayList<GroupChatModel> currentChat;
    private GroupConversationModel currentConversationModel;
    private static final String TAG = "ChatActivity";
    JSONArray studentselected;
    ImageView attechments;
    ImageView imageDp;
    TextView textName;
    ImageView imageTest;
    ImageButton groupmember;
   ImageView backone;
    Bitmap thumbnail = null;
    String Extension;
    RecyclerView recyclerChat;
   ImageButton buttonSend;
   EditText editTextEnterMsg;
    public static boolean locationShareGroup=false;
    public boolean isCreated=false;
    public String admin;
    public static final int RequestPermissionCode = 1;
    boolean ExternslWriteAccepted,ExternslreadAccepted,cameraAccepted;
    Boolean CallingCamera,CallingGallary,CallingPhone,CallingAttachment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        questinSQLiteHelper = new QuestinSQLiteHelper(this);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String action = intent.getAction();
        attechments =findViewById(R.id.attechments);
        imageDp =findViewById(R.id.image_dp);
        textName =findViewById(R.id.text_name);
        imageTest =findViewById(R.id.image_test);
        recyclerChat =findViewById(R.id.list_chat);
        buttonSend =findViewById(R.id.button_send);
        editTextEnterMsg =findViewById(R.id.edit_text_enter_message);
        backone =findViewById(R.id.backone);
        groupmember =findViewById(R.id.groupmember);
        recyclerChat =findViewById(R.id.list_chat);
        buttonSend =findViewById(R.id.button_send);

        gson = new Gson();
        currentChat = new ArrayList<>();
        sendMessageModel = new SendGroupMessageModel();
        chatListAdapter = new GroupChatListAdapter(new ArrayList<GroupChatModel>(), this, this);
        recyclerChat.setAdapter(chatListAdapter);
        recyclerChat.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        questinSQLiteHelper = new QuestinSQLiteHelper(this);

        Bundle b = getIntent().getExtras();
        groupid = b.getString("GROUP_ID");
        groupname = b.getString("GROUP_NAME");
        is_admin=b.getString("is_ADMIN");
        groupImage = b.getString("GroupImage");
        admin = b.getString("ADMIN");
        is_admin = b.getString("is_ADMIN");

        myId = SessionManager.getInstance(this).getUser().getUserprofile_id()+"_"+SessionManager.getInstance(this).getUser().getUsername();

        if(extras.containsKey("open")){
            if(extras.getString("open").equals("activityMyGroupdata")){


                groupname = intent.getStringExtra("GroupName");
                groupid = intent.getStringExtra("GroupID");
                groupImage = intent.getStringExtra("GroupImage");
                admin   = intent.getStringExtra("ADMIN");
                is_admin = intent.getStringExtra("is_ADMIN");

                currentConversationModel = intent.getParcelableExtra(Constants.CONVERSATION_MODEL);
                isFirstTime = intent.getBooleanExtra(Constants.EXTRA_IS_FIRST_MESSAGE, false);
                textName.setText(groupname);

                Glide.with(this).load(groupImage)
                        .placeholder(R.mipmap.chat_group).dontAnimate()
                        .fitCenter().into(imageDp);


                //  Toast.makeText(getApplicationContext(), "activity My Groupd ata.", Toast.LENGTH_SHORT).show();
                chatListAdapter.addAllData(questinSQLiteHelper.getGroupChatList(currentConversationModel.getGroupId()));
                recyclerChat.scrollToPosition(chatListAdapter.getItemCount() - 1);

                //AddOnExistingChat();

            }else if(extras.getString("open").equals("activityaddGroupdata")){

                isCreated=true;
                currentConversationModel = intent.getParcelableExtra(Constants.CONVERSATION_MODEL);
                isFirstTime = intent.getBooleanExtra(Constants.EXTRA_IS_FIRST_MESSAGE, false);
                groupname = intent.getStringExtra("GroupName");
                groupImage = intent.getStringExtra("GroupImage");
                groupid = intent.getStringExtra("GroupID");
                admin   = intent.getStringExtra("ADMIN");
                is_admin = intent.getStringExtra("is_ADMIN");


                textName.setText(groupname);
                Glide.with(this).load(groupImage)
                        .placeholder(R.mipmap.chat_group).dontAnimate()
                        .fitCenter().into(imageDp);

                //  Toast.makeText(getApplicationContext(), "activity My add group data.", Toast.LENGTH_SHORT).show();

                chatListAdapter.addAllData(questinSQLiteHelper.getGroupChatList(currentConversationModel.getGroupId()));
                recyclerChat.scrollToPosition(chatListAdapter.getItemCount() - 1);

            }


        }


        Log.e( "unread count",""+questinSQLiteHelper.
                getConversationUnreadCount(currentConversationModel.getGroupId()));
        if (questinSQLiteHelper.getGroupConversationUnreadCount(currentConversationModel.getGroupId())>0){

            currentConversationModel.setUnreadCount(0);
            questinSQLiteHelper.updateGroupConversation(currentConversationModel);

        }

        ChatActivity.SenderId=currentConversationModel.getGroupId();
        MyName =groupname ;

        // Getting LocationManager object
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // Creating an empty criteria object
        Criteria criteria = new Criteria();

        // Getting the name of the provider that meets the criteria
        provider = locationManager.getBestProvider(criteria, false);

        if (provider != null && !provider.equals("")) {
            // Get the location from the given provider
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            Location location = locationManager.getLastKnownLocation(provider);

            locationManager.requestLocationUpdates(provider, 20000, 1, this);

            if(location!=null)
                onLocationChanged(location);
           /* else
                Toast.makeText(getBaseContext(), "Location can't be retrieved", Toast.LENGTH_SHORT).show();
*/
        }else{
            // Toast.makeText(getBaseContext(), "No Provider Found", Toast.LENGTH_SHORT).show();
        }

        imageDp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(GroupChatActivity.this,ChatGroupMemberProfile.class);
                Bundle bundle=new Bundle();
                bundle.putString("GROUP_ID",groupid);
                bundle.putString("GROUP_NAME",groupname);
                bundle.putString("GroupImage",groupImage);
                bundle.putString("ADMIN",admin);
                bundle.putString("is_ADMIN",is_admin);
                bundle.putBoolean("isCreated",true);

                i.putExtras(bundle);
                startActivity(i);
            }
        });

        textName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(GroupChatActivity.this,ChatGroupMemberProfile.class);
                Bundle bundle=new Bundle();
                bundle.putString("GROUP_ID",groupid);
                bundle.putString("GROUP_NAME",groupname);
                bundle.putString("GroupImage",groupImage);
                bundle.putString("ADMIN",admin);
                bundle.putString("is_ADMIN",is_admin);
                bundle.putBoolean("isCreated",true);

                i.putExtras(bundle);
                startActivity(i);
            }
        });


        groupmember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(GroupChatActivity.this,AddNewChatMember.class);
                Bundle bundle=new Bundle();
                bundle.putString("GROUP_ID",groupid);
                bundle.putString("ACTIVITY","CHAT_ACTIVITY");
                i.putExtras(bundle);
                startActivity(i);
            }
        });
        editTextEnterMsg.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b){
                    Log.e(TAG, "onFocusChange: focused");
                }
                else {
                    Log.e(TAG, "onFocusChange: not focused");
                }
            }
        });

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textMainmsg = editTextEnterMsg.getText().toString();
                if(!textMainmsg.isEmpty()){
                    sendTextMsg(textMainmsg);
                    editTextEnterMsg.setText("");
                }
            }
        });


        attechments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                OpenAllAttachmentsDialog();


            }
        });
        backone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent p = new Intent(ChatActivity.this,ChatTabbedActivity.class);
                startActivity(p);*/
                finish();
            }
        });

    }



    private void OpenAllAttachmentsDialog() {
        TextView Cemera, Gallary, attachments, location, contacts, Cancel;


        OptionDialog = new Dialog(this);
        OptionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        OptionDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        OptionDialog.setContentView(R.layout.list_optionon_chat);


        OptionDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        OptionDialog.show();

        Cemera = OptionDialog.findViewById(R.id.Cemera);
        Gallary = OptionDialog.findViewById(R.id.Gallary);
        attachments = OptionDialog.findViewById(R.id.attachments);
        location = OptionDialog.findViewById(R.id.location);
        contacts = OptionDialog.findViewById(R.id.contacts);
        Cancel = OptionDialog.findViewById(R.id.Cancel);



        Cemera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CallingCamera =true;
                CallingAttachment=false;
                CallingGallary =false;
                CallingPhone =false;


                if(checkPermission()){
                    CallCamera();
                }


            }
        });

        Gallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CallingGallary = true;
                CallingCamera =false;
                CallingAttachment=false;
                CallingPhone =false;


                if (checkPermission()) {
                    selectImage();
                    OptionDialog.dismiss();
                }






            }
        });

        attachments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CallingAttachment=true;
                CallingGallary = false;
                CallingCamera =false;
                CallingPhone =false;

                if(checkPermission()){
                    AddValueFromTheBrows();

                }


            }
        });

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //   location sharing

                Intent in=new Intent(GroupChatActivity.this,LocationShare.class);
                startActivity(in);



                //   CreateLocationMsg(MainLAtLong);


                OptionDialog.dismiss();

            }
        });
        contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CallingPhone=true;
                CallingAttachment=false;
                CallingGallary = false;
                CallingCamera =false;

                if(checkPermission()){
                    CallTHePhone();
                }

                OptionDialog.dismiss();


            }
        });


        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OptionDialog.dismiss();

            }
        });
    }

    private void CallTHePhone() {
        try {

            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(intent, RQS_PICK_CONTACT);
            OptionDialog.dismiss();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void CallCamera() {
        Intent takePicIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePicIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = Utils.createImageFile(this);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (photoFile != null) {
                currentImageUri = FileProvider.getUriForFile(this, "co.questin.fileprovider", photoFile);

                takePicIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri);
                startActivityForResult(takePicIntent, RC_TAKE_PICTURE);
                OptionDialog.dismiss();
            }
        } else {
            Toast.makeText(getApplicationContext(), "No Camera App present", Toast.LENGTH_SHORT).show();
            OptionDialog.dismiss();
        }
    }


    private void AddValueFromTheBrows() {

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        String[] mimeTypes = {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                "text/plain",
                "application/pdf"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, REQUEST_PATH);

        OptionDialog .dismiss();


    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LatiLng event) {
        /* Do something */
        Log.e("done","ho gya bhai"+event.getLat());
        MainLAtLong =event.getLat()+"_"+event.getLng()+"_"+event.getAddress();
        if(locationShareGroup)
            CreateLocationMsg(MainLAtLong);


        locationShareGroup=false;
        EventBus.getDefault().unregister(this);


    }

    @Override
    protected void onStop() {
        super.onStop();
        ChatActivity.SenderId=null;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RC_TAKE_PICTURE){
            if(resultCode == RESULT_OK){
//                Bitmap image = data.getParcelableExtra("data");
//                imageTest.setImageBitmap(image);
                if(currentImageUri != null){
                    Utils.addPicToGallery(this, currentImageUri);
                    Log.e(TAG, "onActivityResult: " + currentImageUri.toString());
//                    Glide.with(mContext).load(currentImageUri).into(imageTest);
                   // createImageMsg(currentImageUri);
                    beginCrop(currentImageUri);
                }
            }
        }
        else if(requestCode == RC_SELECT_IMAGE && resultCode == RESULT_OK){

            currentImageUri = data.getData();
            beginCrop(currentImageUri);
            //  createImageMsg(imageUri);
        }
        else if (requestCode == REQUEST_PATH &&resultCode == RESULT_OK) {

            Uri FileURI = data.getData();
            String uriString = FileURI.toString();
            selectedFile = new File(uriString);
            try {
                fullpath = Utils.getPath(this,FileURI);
            }catch (NumberFormatException e){

                System.out.println("not a number");

            } catch (Exception e){

                System.out.println(e);
            }


            String FileName = Utils.getDataColumn(this,FileURI,null,null);

            Log.d("TAG", "onActivityResult: " + FileName + FileName );

            String path = selectedFile.toString();

            String filepath = path;

            String displayName = null;

            if (uriString.startsWith("content://")) {

                Cursor cursor = null;

                try {

                    cursor = getContentResolver().query(FileURI, null, null, null, null);

                    if (cursor != null && cursor.moveToFirst()) {

                        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);

                        Fileimagename = cursor.getString(nameIndex);

                        Log.d("TAG", "onActivityResult: " + Fileimagename + filepath + fullpath);

                        /*Log.d("TAG", "onActivityResult2: " + Fileimagename);*/



                            if (fullpath != null) {


                                if (filepath.matches("(.*)providers(.*)")) {

                                    String fullfilePath = fullpath + "/" + Fileimagename;
                                    convertFileToString(fullfilePath, Fileimagename);
                                    CreateFileMsg(fullfilePath, Fileimagename);

                                    Log.d("TAG", "onActivityResult3: " + fullfilePath);
                                } else if (filepath.matches("(.*)externalstorage(.*)")) {

                                    fullpath = fullpath;
                                    convertFileToString(fullpath, Fileimagename);
                                    CreateFileMsg(fullpath, Fileimagename);
                                    Log.d("TAG", "onActivityResult4: " + fullpath);
                                }

                            } else {
                                Toast.makeText(this, "File not found !! Get it from internal/external storage", Toast.LENGTH_SHORT).show();
                            }


                    }





                }catch (Exception e )
                {
                    e.printStackTrace();
                }

            }





        }
        else if (requestCode == RQS_PICK_CONTACT) {
            if (resultCode == RESULT_OK) {


                Uri result = data.getData();
                Log.v("TAG", "Got a result: " + result.toString());

                // get the phone number id from the Uri
                String id = result.getLastPathSegment();

                // query the phone numbers for the selected phone number id
                Cursor c = getContentResolver().query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone._ID + "=?",
                        new String[]{id}, null);

                int phoneIdx = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                int phonenamex = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);

                if (c.getCount() == 1) { // contact has a single phone number
                    // get the only phone number
                    if (c.moveToFirst()) {
                        String phone = c.getString(phoneIdx);

                        String name = c.getString(phonenamex);
                        Log.v("TAG", "Got phone number: " + phone);
                        CreateContactMsg(phone,name);

                        //contactNumber.setText(phone);
                        //  Toast.makeText(this, name + " has number " + phone, Toast.LENGTH_LONG).show();// do something with the phone number

                    } else {
                        Log.w("TAG", "No results");
                    }
                }

            }
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }

    }

    public String convertFileToString(String pathOnSdCard, String curFileName){
        File file=new File(pathOnSdCard);

        try {

            byte[] data = FileUtils.readFileToByteArray(file);//Convert any file, image or video into byte array

            strFile = Base64.encodeToString(data, Base64.NO_WRAP);//Convert byte array into string
            System.out.println("file in bitmap first method " + strFile);
            Fileimagename =curFileName;

            System.out.println("filename in bitmap first method " + Fileimagename);

            // CreateFileMsg(strFile,curFileName);

            // imagepreview.setBackgroundResource(R.mipmap.file);
            //  previewlayout.setVisibility(View.VISIBLE);
            //  Addresource.setVisibility(View.GONE);

        } catch (IOException e) {

            e.printStackTrace();

        }

        return strFile;

    }


    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).withMaxSize(500,500).start(this);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        fullpath = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(fullpath);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            //iv_user_image.setImageURI(Crop.getOutput(result));
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(result));
                thumbnail = getResizedBitmap(thumbnail,1000);
                getImageUri(getApplicationContext(),thumbnail);
                createImageMsg(Uri.parse(fullpath));


            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }




    private BroadcastReceiver chatMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(Constants.ACTION_GROUPCHAT_MESSAGE_RECEIVED)){



                FcmGroupChatModel fcmChatModel = gson.fromJson(intent.getStringExtra(Constants.EXTRA_MESSAGE),
                        FcmGroupChatModel.class);
                if(fcmChatModel.getGroupId().equals(groupid)){



                    GroupChatModel chatModel = new GroupChatModel()
                            .setIsMine(QuestinContract.MINE_NO)
                            .setCategory(fcmChatModel.getCategory())
                            .setText(fcmChatModel.getText())
                            .setLink(fcmChatModel.getLink())
                            .setTime(fcmChatModel.getTime())
                            .setStatus(QuestinContract.STATUS_READ)
                            .setSenderId(fcmChatModel.getSenderId());

                    currentConversationModel.setMine(chatModel.getIsMine())
                            .setLastMsgCategory(chatModel.getCategory())
                            .setLastMsgText(chatModel.getText())
                            .setLastMsgTime(chatModel.getTime())
                            .setLastMsgStatus(chatModel.getStatus())
                            .setUnreadCount(0)
                            .setSenderId(chatModel.getSenderId());

                    chatListAdapter.addData(chatModel);
                    recyclerChat.scrollToPosition(chatListAdapter.getItemCount() - 1);
                    currentChat.add(chatModel);

                }
                else {
                    Utils.saveGroupChatMessage(GroupChatActivity.this, fcmChatModel);
                }
            }
        }
    };

    private BroadcastReceiver imageDownloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(Constants.ACTION_IMAGE_DOWNLOADED)){
                GroupChatModel chatModel = intent.getParcelableExtra(Constants.GROUP_MODEL);
                String senderId = intent.getStringExtra(Constants.GROUP_ID);
                if(senderId.equals(currentConversationModel.getGroupId())){
                    chatListAdapter.onImageDownloaded(chatModel);
                }
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(chatMessageReceiver,
                new IntentFilter(Constants.ACTION_GROUPCHAT_MESSAGE_RECEIVED));
        LocalBroadcastManager.getInstance(this).registerReceiver(imageDownloadReceiver,
                new IntentFilter(Constants.ACTION_IMAGE_DOWNLOADED));
    }

    @Override
    protected void onPause() {
        questinSQLiteHelper.addGroupChatData(currentChat, currentConversationModel.getGroupId());
        if(!isFirstTime) questinSQLiteHelper.updateGroupConversation(currentConversationModel);
        else {
            if(chatListAdapter.getItemCount() > 0){
                questinSQLiteHelper.addGroupConversation(currentConversationModel);
            }
        }
        currentChat.clear();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(chatMessageReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(imageDownloadReceiver);
        super.onPause();
    }

    /**
     * Method which calls the Questin send_message API to send a FCM message
     * @param sendMessageModel Object to send in the body of the request
     */
    private void sendMessage(final SendGroupMessageModel sendMessageModel){
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.GROUPMSG,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {

                                }else {
                                    showSnackbarMessage(stateResponse.getMessage());
                                }
                            } else {

                                showServerSnackbar(R.string.error_responce);
                            }
                        } catch (Exception e) {

                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(GroupChatActivity.this).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(GroupChatActivity.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(GroupChatActivity.this).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("gid",sendMessageModel.getGid());
                params.put("message",sendMessageModel.getMessage());

                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);


    }

    /**
     * Method to send a text message
     * @param text The text to send, from the editText
     */
    private void sendTextMsg(String text){
        GroupChatModel chatModel = new GroupChatModel()
                .setIsMine(QuestinContract.MINE_YES)
                .setCategory(QuestinContract.CATEGORY_TEXT)
                .setText(text)
                .setLink(null)
                .setTime(String.valueOf(System.currentTimeMillis()))
                .setStatus(QuestinContract.STATUS_SENT)
                .setSenderId(myId);

        currentConversationModel.setMine(chatModel.getIsMine())
                .setLastMsgCategory(chatModel.getCategory())
                .setLastMsgText(chatModel.getText())
                .setLastMsgTime(chatModel.getTime())
                .setLastMsgStatus(chatModel.getStatus())
                .setGroupId(groupid)
                .setSenderId(myId)
                .setUnreadCount(0);

        chatListAdapter.addData(chatModel);
        recyclerChat.scrollToPosition(chatListAdapter.getItemCount() - 1);
        currentChat.add(chatModel);

        sendMessage(sendMessageModel
                .setUids(groupid)
                .setMessage(gson.toJson(new FcmGroupChatModel(chatModel, myId,MyName,groupid, chatModel.getLink(),is_group_chat), FcmGroupChatModel.class)));
    }

    /**
     * Method to select one image to upload to send as an image message
     */
    private void selectImage(){
        Intent imageSelectIntent = new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE)
                .putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        if(imageSelectIntent.resolveActivity(getPackageManager()) != null){
            startActivityForResult(imageSelectIntent, RC_SELECT_IMAGE);
        }
    }

    /**
     * Method to create a local image message for the sender and start the upload
     * @param localUri Uri of the image to upload
     */
    private void createImageMsg(Uri localUri){
        GroupChatModel chatModel = new GroupChatModel()
                .setIsMine(QuestinContract.MINE_YES)
                .setCategory(QuestinContract.CATEGORY_IMAGE)
                .setText("")
                .setLink(localUri.toString())
                .setTime(String.valueOf(System.currentTimeMillis()))
                .setStatus(QuestinContract.STATUS_PENDING).setSenderId(myId);

        currentConversationModel.setMine(chatModel.getIsMine())
                .setLastMsgCategory(chatModel.getCategory())
                .setLastMsgText("Image")
                .setLastMsgTime(chatModel.getTime())
                .setLastMsgStatus(chatModel.getStatus())
                .setUnreadCount(0).setSenderId(myId);
        chatListAdapter.addData(chatModel);
        recyclerChat.scrollToPosition(chatListAdapter.getItemCount() - 1);
        currentChat.add(chatModel);
        uploadImage(localUri, chatModel);
    }



    private void CreateFileMsg(String filepath1, String curFileName){
        GroupChatModel chatModel = new GroupChatModel()
                .setIsMine(QuestinContract.MINE_YES)
                .setCategory(QuestinContract.CATEGORY_DOCUMENT)
                .setText(curFileName)
                .setLink(filepath1.toString())
                .setTime(String.valueOf(System.currentTimeMillis()))
                .setStatus(QuestinContract.STATUS_PENDING).setSenderId(myId);

        currentConversationModel.setMine(chatModel.getIsMine())
                .setLastMsgCategory(chatModel.getCategory())
                .setLastMsgText("Attachment")
                .setLastMsgTime(chatModel.getTime())
                .setLastMsgStatus(chatModel.getStatus())
                .setUnreadCount(0).setSenderId(myId);

        chatListAdapter.addData(chatModel);
        recyclerChat.scrollToPosition(chatListAdapter.getItemCount() - 1);
        currentChat.add(chatModel);
        uploadFile(fullpath ,curFileName, chatModel);
    }


    private void CreateContactMsg(String phone, String name){
        GroupChatModel chatModel = new GroupChatModel()
                .setIsMine(QuestinContract.MINE_YES)
                .setCategory(QuestinContract.CATEGORY_CONTACT)
                .setText(phone +","+name)
                .setLink(null)
                .setTime(String.valueOf(System.currentTimeMillis()))
                .setStatus(QuestinContract.STATUS_SENT).setSenderId(myId);

        currentConversationModel.setMine(chatModel.getIsMine())
                .setLastMsgCategory(chatModel.getCategory())
                .setLastMsgText("Contact")
                .setLastMsgTime(chatModel.getTime())
                .setLastMsgStatus(chatModel.getStatus())
                .setUnreadCount(0).setSenderId(myId);

        chatListAdapter.addData(chatModel);
        recyclerChat.scrollToPosition(chatListAdapter.getItemCount() - 1);
        currentChat.add(chatModel);

        sendMessage(sendMessageModel
                .setUids(groupid)
                .setMessage(gson.toJson(new FcmGroupChatModel(chatModel, myId,MyName,groupid, chatModel.getLink(),is_group_chat), FcmGroupChatModel.class)));
    }





    private void CreateLocationMsg(String mainLAtLong){
        GroupChatModel chatModel = new GroupChatModel()
                .setIsMine(QuestinContract.MINE_YES)
                .setCategory(QuestinContract.CATEGORY_LOCATION)
                .setText(mainLAtLong)
                .setLink(null)
                .setTime(String.valueOf(System.currentTimeMillis()))
                .setStatus(QuestinContract.STATUS_SENT).setSenderId(myId);

        currentConversationModel.setMine(chatModel.getIsMine())
                .setLastMsgCategory(chatModel.getCategory())
                .setLastMsgText("Location")
                .setLastMsgTime(chatModel.getTime())
                .setLastMsgStatus(chatModel.getStatus())
                .setUnreadCount(0).setSenderId(myId);

        chatListAdapter.addData(chatModel);
        recyclerChat.scrollToPosition(chatListAdapter.getItemCount() - 1);
        currentChat.add(chatModel);

        sendMessage(sendMessageModel
                .setUids(groupid)
                .setMessage(gson.toJson(new FcmGroupChatModel(chatModel, myId,MyName,groupid, chatModel.getLink(),is_group_chat), FcmGroupChatModel.class)));
    }











    /**
     * Method to send the FCM image message
     * @param serverUri Uri received from Firebase after successful upload
     * @param chatModel ChatModel object created inside {@see #createImageMsg()}
     */
    private void sendImageMsg(Uri serverUri, GroupChatModel chatModel){
        FcmGroupChatModel fcmChatModel = new FcmGroupChatModel(chatModel, myId,MyName,groupid,serverUri.toString(),is_group_chat);
        sendMessage(sendMessageModel
                .setMessage(gson.toJson(fcmChatModel, FcmGroupChatModel.class))
                .setUids(currentConversationModel.getGroupId()));
    }



    private void sendFileMsg(Uri serverUri, GroupChatModel chatModel){
        FcmGroupChatModel fcmChatModel = new FcmGroupChatModel(chatModel, myId,MyName,groupid, serverUri.toString(),is_group_chat);
        sendMessage(sendMessageModel
                .setMessage(gson.toJson(fcmChatModel, FcmGroupChatModel.class))
                .setUids(currentConversationModel.getGroupId()));


//        sendMessage(sendMessageModel
//                .setUids(groupid)
//                .setMessage(gson.toJson(new FcmGroupChatModel(chatModel, myId,groupid, chatModel.getLink(),is_group_chat), FcmGroupChatModel.class)));

    }









    /**
     * Method to upload a local image to Firebase
     * @param imageUri Local uri of the image to upload
     * @param chatModel ChatModel object created inside createImageMsg
     */
    private void uploadImage(Uri imageUri, final GroupChatModel chatModel){
//        final ProgressDialog progressDialog = new ProgressDialog(this);
//        progressDialog.setTitle("Uploading...");
//        progressDialog.show();
        chatListAdapter.isUpload=false;
        chatListAdapter.loaderPos=chatListAdapter.getItemCount()-1;
        chatListAdapter.notifyDataSetChanged();
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        StorageReference storageReference = firebaseStorage.getReference(myId + "/images/"
                + imageUri.getLastPathSegment());
        UploadTask uploadTask = storageReference.putFile(imageUri);
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(getApplicationContext(), "Upload Success", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "onSuccess: " + taskSnapshot.getDownloadUrl());
                currentImageDownloadUri = taskSnapshot.getDownloadUrl();
                sendImageMsg(taskSnapshot.getDownloadUrl(), chatModel);
                //    progressDialog.dismiss();
                chatListAdapter.isUpload=true;
                chatListAdapter.loaderPos=chatListAdapter.getItemCount()-1;
                chatListAdapter.notifyDataSetChanged();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Upload Failed", Toast.LENGTH_SHORT).show();
                //   progressDialog.dismiss();
                chatListAdapter.isUpload=true;
                chatListAdapter.loaderPos=chatListAdapter.getItemCount()-1;
                chatListAdapter.notifyDataSetChanged();
            }
        }).addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {
                //     progressDialog.dismiss();

            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

            }
        });
    }


    private void uploadFile(String path, String curFileName, final GroupChatModel chatModel) {
//        final ProgressDialog progressDialog = new ProgressDialog(this);
//        progressDialog.setTitle("Uploading...");
//        progressDialog.show();
        chatListAdapter.isUpload=false;
        chatListAdapter.loaderPos=chatListAdapter.getItemCount()-1;
        chatListAdapter.notifyDataSetChanged();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://questin-e31c7.appspot.com");
        StorageReference riversRef = storageRef.child(myId + "/"+Fileimagename+"/" + "/text/");

        InputStream stream = null;
        try {
            stream = new FileInputStream(String.valueOf(fullpath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e)
        {
            e.printStackTrace();
        }
        UploadTask uploadTask = riversRef.putStream(stream);


        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(getApplicationContext(), "Upload Success", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "onSuccess: " + taskSnapshot.getDownloadUrl());
                currentImageDownloadUri = taskSnapshot.getDownloadUrl();
                sendFileMsg(taskSnapshot.getDownloadUrl(), chatModel);
                //   progressDialog.dismiss();
                chatListAdapter.isUpload=true;
                chatListAdapter.loaderPos=chatListAdapter.getItemCount()-1;
                chatListAdapter.notifyDataSetChanged();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // progressDialog.dismiss();
                chatListAdapter.isUpload=true;
                chatListAdapter.loaderPos=chatListAdapter.getItemCount()-1;
                chatListAdapter.notifyDataSetChanged();
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Upload Failed", Toast.LENGTH_SHORT).show();
            }
        }).addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {

                // progressDialog.dismiss();

            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                // progressDialog.dismiss();

            }
        });
    }



    @Override
    public void startImageDownload(GroupChatModel chatModel) {
        startService(new Intent(GroupChatActivity.this, GroupImageDownload.class)
                .putExtra(Constants.GROUP_MODEL, chatModel)
                .putExtra(Constants.GROUP_ID, currentConversationModel.getGroupId())
        );
    }

    @Override
    public void startDocumnetDownload(GroupChatModel chatModel) {
        startService(new Intent(GroupChatActivity.this, FileGroupDownloadDoc.class)
                .putExtra(Constants.GROUP_MODEL, chatModel)
                .putExtra(Constants.GROUP_ID, currentConversationModel.getGroupId())
        );
    }


    @Override
    public void onLocationChanged(Location location) {

        MainLAtLong = "Longitude:" + location.getLongitude()+"Latitude:"+ location.getLatitude();


    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onBackPressed() {

        Intent in=new Intent(this,ChatTabbedActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        in.putExtra("isGroup",true);
        startActivity(in);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private  boolean checkPermission() {
        int camerapermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionLocation = ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionphone = ContextCompat.checkSelfPermission(this,Manifest.permission.READ_PHONE_STATE);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionphone != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), RequestPermissionCode);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("TAG", "Permission callback called-------");
        switch (requestCode) {
            case RequestPermissionCode: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);

                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("TAG", "sms & location services permission granted");

                        if (CallingCamera ==true){
                            CallCamera();

                        }else if (CallingGallary ==true) {

                            selectImage();



                        }else if (CallingAttachment==true){
                            AddValueFromTheBrows();



                        }

                        else if (CallingPhone ==true) {

                            CallTHePhone();


                        }


                    } else {
                        Log.d("TAG", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Service Permissions are required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkPermission();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    // finish();
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
    private void explain(String msg){
        final android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        //  permissionsclass.requestPermission(type,code);
                        // startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("co.questin")));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // finish();
                    }
                });
        dialog.show();
    }
}