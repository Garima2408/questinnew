package co.questin.chat;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.Event.ServerMemberUpdate;
import co.questin.R;
import co.questin.library.StateBean;
import co.questin.models.chat.NewChatModel;
import co.questin.models.userFriendsResponse.Datum;
import co.questin.models.userFriendsResponse.UserFriendsResponseModel;
import co.questin.network.URLS;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;

import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;
import static co.questin.utils.Utils.isStatusSuccess;

/**
 * Created by HP on 4/12/2018.
 */

public class FriendsListService extends IntentService {

    private Gson gson;
    private List<NewChatModel> frrindList;
    public FriendsListService() {
        super("FriendsListService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        //use for clear snappy db
        DB snappyDB = null;
        try {
            snappyDB = DBFactory.open(this);
            if (snappyDB.exists(Constants.CART_FRIENDS_LIST)) {

                snappyDB.destroy();

            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

       GetFriendsServerList();

    }



    private void GetFriendsServerList() {

        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_MYFRIENDS + "?" + "email=" + SessionManager.getInstance(FriendsListService.this).getUser().getEmail(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    gson = new Gson();
                                    UserFriendsResponseModel userFriendsResponseModel =
                                            gson.fromJson(String.valueOf(response), UserFriendsResponseModel.class);
                                    // saveFriendsLIstData(userFriendsResponseModel);
                                    Log.e("response services", userFriendsResponseModel.toString() + " size " + userFriendsResponseModel.getData().size());

                                    Log.e("offline save", "data");

                                    try {
                                        DB snappyDB = null;
                                        snappyDB = DBFactory.open(getApplicationContext());
                                        if (snappyDB.exists(Constants.CART_FRIENDS_LIST)) {
                                            UserFriendsResponseModel datum = null;
                                            datum = snappyDB.getObject(Constants.CART_FRIENDS_LIST, UserFriendsResponseModel.class);
                                            Log.e("offline user size", "" + datum.getData().size());

                                            datum = new UserFriendsResponseModel();
                                            List<Datum> data = new ArrayList<>();

//                                List<String> ListIds = new ArrayList<>();
//                                for (int i = 0; i < datum.getData().size(); i++) {
//                                    ListIds.add(datum.getData().get(i).getUid());
//                                }

                                            for (int j = 0; j < userFriendsResponseModel.getData().size(); j++) {
//                                    if (!ListIds.contains(userFriendsResponseModel.getData().get(j).getUid())) {
//
//
//                                    }
                                                Datum datum1 = new Datum();
                                                datum1.setUid(userFriendsResponseModel.getData().get(j).getUid());
                                                datum1.setFieldFirstname(userFriendsResponseModel.getData().get(j).getFieldFirstname());
                                                datum1.setFieldLastname(userFriendsResponseModel.getData().get(j).getFieldLastname());
                                                datum1.setPicture(userFriendsResponseModel.getData().get(j).getPicture());
                                                data.add(datum1);
//
                                            }
                                            try {
                                                datum.getData().addAll(data);
                                                snappyDB.put(Constants.CART_FRIENDS_LIST, datum);
                                            } catch (Exception e) {

                                            }


                                            //snappyDB.close();


                                        } else {

                                            try {
                                                UserFriendsResponseModel datum = new UserFriendsResponseModel();
                                                datum.setData(userFriendsResponseModel.getData());
                                                snappyDB.put(Constants.CART_FRIENDS_LIST, datum);
                                                snappyDB.close();
                                            } catch (Exception e) {

                                            }


                                        }

                                    } catch (SnappydbException e) {
                                        e.printStackTrace();
                                    } catch (NullPointerException n) {
                                        n.printStackTrace();
                                    }
                                    try {
                                        EventBus.getDefault().post(new ServerMemberUpdate(1));
                                        ConversationsActivity.serverMemberUpdate = true;
                                        stopSelf();
                                    } catch (Exception e) {

                                    }

                                } else {

                                    showSnackbarMessage(stateResponse.getMessage());
                                }
                            } else {

                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {

                            showServerSnackbar(R.string.error_responce);

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(FriendsListService.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(FriendsListService.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(FriendsListService.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);

                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(FriendsListService.this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }


}


