package co.questin.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.ChatMemeberSelectAdapter;
import co.questin.library.StateBean;
import co.questin.models.AllFriends;
import co.questin.network.URLS;
import co.questin.studentprofile.FriendsSection;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

import static co.questin.utils.Utils.isStatusSuccess;

public class AddNewChatMember extends BaseAppCompactActivity  {
    EditText searchEmail;
    String SearchedName, uid, GroupMemberId,GroupId,Redirect;
    public ArrayList<AllFriends> mFriendsList;
    ChatMemeberSelectAdapter memberAdapter; /*all friends for selections*/
    FriendMembersAdapter friendmemberAdapter;
    RecyclerView SelectedName_Lists;/*list of all selected*/
    ListView rv_FriendList;/*list of friends*/
    ArrayAdapter<String> adapter;
    private ArrayList<String> frilist;
    private ArrayList<String> selectedstudentSend;
    RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<String> UIDlists;
    TextView ErrorText;
    Button invitebutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_chat_member);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        searchEmail =findViewById(R.id.searchEmail);
        SelectedName_Lists = findViewById(R.id.SelectedName_Lists);
        ErrorText =findViewById(R.id.ErrorText);
        invitebutton =findViewById(R.id.invitebutton);
        rv_FriendList = findViewById(R.id.rv_FriendList);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        SelectedName_Lists.setLayoutManager(mLayoutManager);
        SelectedName_Lists.setHasFixedSize(true);
        UIDlists = new ArrayList<>(); /*all friend list*/
        mFriendsList=new ArrayList<>(); /*all friend list*/
        frilist = new ArrayList<String>();/*search friend list*/

        Bundle b = getIntent().getExtras();
        GroupId = b.getString("GROUP_ID");
        Redirect =b.getString("ACTIVITY");
         MyFriendsListDisplay();

        searchEmail.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER) {
                    addnewfriends();
                    return true;
                }
                return false;
            }
        });

        invitebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity (new Intent(AddNewChatMember.this, FriendsSection.class));
                finish();
            }
        });

    }


    private void addnewfriends() {
        mFriendsList.clear();
        searchEmail.setError(null);

        // Store values at the time of the login attempt.
        SearchedName = searchEmail.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(SearchedName)) {
            focusView = searchEmail;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();

        } else {

           SearchedFriendDisplay();


        }
    }



    private void MyFriendsListDisplay() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_MYFRIENDS+"?"+"email="+ SessionManager.getInstance(getActivity()).getUser().getEmail(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                                        for (int i = 0; i < jsonArrayData.length(); i++) {

                                            AllFriends CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AllFriends.class);
                                            mFriendsList.add(CourseInfo);
                                            frilist.add(CourseInfo.getField_firstname() + "" + CourseInfo.getField_lastname() + " " + CourseInfo.getPicture());
                                            memberAdapter = new ChatMemeberSelectAdapter(AddNewChatMember.this,R.layout.list_groupchat_member, mFriendsList);
                                            rv_FriendList.setAdapter(memberAdapter);
                                            rv_FriendList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);



                                            rv_FriendList.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                                                @Override
                                                public void onItemCheckedStateChanged(ActionMode mode,
                                                                                      int position, long id, boolean checked) {
                                                    // Capture total checked items
                                                    final int checkedCount = rv_FriendList.getCheckedItemCount();

                                                    Log.d("TAG", "checkedCount: " + checkedCount);
                                                    mode.setTitle(checkedCount + " Selected");
                                                    memberAdapter.toggleSelection(position);

                                                    ArrayList<String> selectedstudent = new ArrayList<String>();
                                                    SparseBooleanArray checkedd = rv_FriendList.getCheckedItemPositions();

                                                    for (int i = 0; i < checkedd.size(); i++) {
                                                        // Item position in adapter
                                                        int positio = checkedd.keyAt(i);
                                                        // Add sport if it is checked i.e.) == TRUE!
                                                        if (checkedd.valueAt(i))
                                                            /*  {0=true, 1=false, 2=true}*/

                                                            selectedstudent.add(String.valueOf(memberAdapter.getItem(positio).getUid()+","+memberAdapter.getItem(positio).getField_firstname()+","+memberAdapter.getItem(positio).getField_lastname()+","+memberAdapter.getItem(positio).getPicture()));


                                                    }

                                                    String[] outputStrArr = new String[selectedstudent.size()];
                                                    selectedstudentSend = new ArrayList<String>();
                                                    for (int x = 0; x < selectedstudent.size(); x++) {
                                                        outputStrArr[x] = selectedstudent.get(x);
                                                        Log.d("TAG", " outputprint[x]: " + outputStrArr[x]);
                                                        String[] parts =  outputStrArr[x] .split(",");
                                                        uid = parts[0];
                                                        String name = parts[1] + " " + parts[2];
                                                        String picture = parts[3];
                                                        GroupMemberId = uid;
                                                        UIDlists.add(uid);

                                                        selectedstudentSend.add(outputStrArr[x]);
                                                        friendmemberAdapter = new FriendMembersAdapter(getApplicationContext(), R.layout.list_friend_member,selectedstudentSend);
                                                        SelectedName_Lists.setAdapter(friendmemberAdapter);

                                                    }

                                                }

                                                @Override
                                                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                                                    switch (item.getItemId()) {
                                                        case R.id.Submit:
                                                            CallToAddMemberInGroup();
                                                            return true;
                                                        default:
                                                            return false;
                                                    }
                                                }

                                                @Override
                                                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                                                    mode.getMenuInflater().inflate(R.menu.submit_menu, menu);
                                                    return true;
                                                }

                                                @Override
                                                public void onDestroyActionMode(ActionMode mode) {
                                                    // TODO Auto-generated method stub
                                                    memberAdapter.removeSelection();

                                                }

                                                @Override
                                                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                                                    // TODO Auto-generated method stub
                                                    return false;
                                                }
                                            });


                                        }

                                    }
                                    else{
                                        ErrorText.setVisibility(View.VISIBLE);
                                        ErrorText.setText("you have no friends");
                                        invitebutton.setVisibility(View.VISIBLE);

                                    }


                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(AddNewChatMember.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(AddNewChatMember.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(AddNewChatMember.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }



    private void CallToAddMemberInGroup() {
        for (int i = 0; i < UIDlists.size(); i++) {
            System.out.println(UIDlists.get(i));
            CreateChatGroupMember(UIDlists.get(i));


        }


    }



    private void SearchedFriendDisplay() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_SEARCHBYEMAIL+SearchedName,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                                        for (int i = 0; i < jsonArrayData.length(); i++) {
                                            frilist.clear();
                                            AllFriends CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AllFriends.class);
                                            mFriendsList.add(CourseInfo);
                                            frilist.add(CourseInfo.getField_firstname()+""+CourseInfo.getField_lastname());

                                            adapter = new ArrayAdapter<String>(AddNewChatMember.this,
                                                    android.R.layout.simple_list_item_multiple_choice, frilist);
                                            rv_FriendList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                                            rv_FriendList.setAdapter(adapter);

                                            SparseBooleanArray checked = rv_FriendList.getCheckedItemPositions();
                                            ArrayList<String> selectedItems = new ArrayList<String>();
                                            for (int z = 0; z < checked.size(); z++) {
                                                // Item position in adapter
                                                int position = checked.keyAt(z);
                                                // Add sport if it is checked i.e.) == TRUE!
                                                if (checked.valueAt(z))
                                                    selectedItems.add(adapter.getItem(position));
                                            }

                                            String[] outputStrArr = new String[selectedItems.size()];

                                            for (int x = 0; x < selectedItems.size(); x++) {
                                                outputStrArr[x] = selectedItems.get(x);
                                            }

                                        }
                                    }
                                    else{
                                        showSnackbarinteger(R.string.Friends);


                                    }



                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(AddNewChatMember.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(AddNewChatMember.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(AddNewChatMember.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }








    private void CreateChatGroupMember(String UserId) {
        StringRequest mStrRequest = new StringRequest(Request.Method.POST,  URLS.URL_CREATEGROUPMEMBER +"/"+ GroupId +"/"+UserId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    Check();


                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());

                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        } else {
                            hideIndicator();
                            showConnectionSnackbar();

                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                String Cookies= SessionManager.getInstance(AddNewChatMember.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(AddNewChatMember.this).getaccesstoken().sessionID;
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(AddNewChatMember.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }



        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);



    }



    private void Check() {

        if (Redirect.matches("CHATMEMBER_ACTIVITY")){
            ChatGroupMemberProfile.RefershList();
            finish();
        }else if(Redirect.matches("CHAT_ACTIVITY")){
            finish();
        }


    }

    @Override
    public void onBackPressed() {
        finish();

    }


}
