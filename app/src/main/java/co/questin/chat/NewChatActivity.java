package co.questin.chat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.database.QuestinSQLiteHelper;
import co.questin.library.StateBean;
import co.questin.models.chat.ConversationModel;
import co.questin.models.chat.NewChatModel;
import co.questin.models.userFriendsResponse.Datum;
import co.questin.models.userFriendsResponse.UserFriendsResponseModel;
import co.questin.network.URLS;
import co.questin.studentprofile.FriendsSection;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;

import static co.questin.utils.BaseAppCompactActivity.ShowIndicator;
import static co.questin.utils.BaseAppCompactActivity.hideIndicator;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;
import static co.questin.utils.Utils.isStatusSuccess;

public class NewChatActivity extends AppCompatActivity {

    private Gson gson;
    private FriendsListAdapter friendsListAdapter;
    private QuestinSQLiteHelper questinSQLiteHelper;
     TextView ErrorText;
     RecyclerView recyclerFriends;
     Button invitebutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_chat);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        ErrorText =findViewById(R.id.ErrorText);

        recyclerFriends =findViewById(R.id.recycler_friends);
        invitebutton =findViewById(R.id.invitebutton);
        gson = new Gson();
        questinSQLiteHelper = new QuestinSQLiteHelper(this);
        friendsListAdapter = new FriendsListAdapter(new ArrayList<NewChatModel>(), this);
        recyclerFriends.setAdapter(friendsListAdapter);
        recyclerFriends.setLayoutManager(new LinearLayoutManager(this));
        getFriendsList();


        invitebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity (new Intent (NewChatActivity.this, FriendsSection.class));
                finish();
            }
        });

    }








    private void getFriendsList(){
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_MYFRIENDS+"?"+"email="+SessionManager.getInstance(NewChatActivity.this).getUser().getEmail(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    final UserFriendsResponseModel userFriendsResponseModel =
                                            gson.fromJson(response, UserFriendsResponseModel.class);
                                    if (userFriendsResponseModel!=null && userFriendsResponseModel.getData()!=null
                                            && userFriendsResponseModel.getData().size() >0){
                                        for(Datum data : userFriendsResponseModel.getData()){
                                            friendsListAdapter.addData(
                                                    new NewChatModel(data.getUid(),
                                                            data.getFieldFirstname() + " " + data.getFieldLastname(),
                                                            data.getPicture()));
                                        }
                                    }else {
                                        ErrorText.setVisibility(View.VISIBLE);
                                        ErrorText.setText("you have no friends");
                                        invitebutton.setVisibility(View.VISIBLE);
                                    }

                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(NewChatActivity.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(NewChatActivity.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(NewChatActivity.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);




    }






    class FriendsListAdapter extends RecyclerView.Adapter<FriendsListAdapter.FriendsListHolder>{

        private Context mContext;
        private LayoutInflater inflater;
        private ArrayList<NewChatModel> data;

        FriendsListAdapter(ArrayList<NewChatModel> data, Context context){
            this.data = data;
            mContext = context;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public FriendsListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FriendsListHolder(inflater.inflate(R.layout.new_chat_list_adapter, parent, false));
        }

        @Override
        public void onBindViewHolder(final FriendsListHolder holder, int position) {
            holder.textName.setText(data.get(position).getName());
            Glide.with(mContext).load(data.get(position).getDpUrl())
                    .placeholder(R.mipmap.single).dontAnimate()
                    .fitCenter().into(holder.imageDp);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    ConversationModel conversationModel = new ConversationModel()
                            .setSenderId(data.get(holder.getAdapterPosition()).getId())
                            .setName(data.get(holder.getAdapterPosition()).getName())
                            .setDpUrl(data.get(holder.getAdapterPosition()).getDpUrl());

                    Intent backIntent = new Intent()
                            .putExtra(Constants.EXTRA_IS_FIRST_MESSAGE, !questinSQLiteHelper.doesConversationExist(conversationModel.getSenderId()))
                            .putExtra(Constants.CONVERSATION_MODEL, conversationModel);
                    NewChatActivity.this.setResult(RESULT_OK, backIntent);
                    NewChatActivity.this.finish();
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        void addData(NewChatModel newChatModel){
            data.add(newChatModel);
            notifyItemInserted(data.size() - 1);
        }

        class FriendsListHolder extends RecyclerView.ViewHolder{

            View itemView;
            TextView textName;
            ImageView imageDp;
            FriendsListHolder(View itemView) {
                super(itemView);
                this.itemView = itemView;
                textName =itemView.findViewById(R.id.text_name);
                imageDp =itemView.findViewById(R.id.image_dp);

            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        finish();

    }
}
