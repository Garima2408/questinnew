package co.questin.chat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.greenrobot.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

import co.questin.Event.LatiLng;
import co.questin.R;
import co.questin.Retrofit.ApiInterface;
import co.questin.Retrofit.ServiceGenerator;
import co.questin.models.GeoCode.GeoCodingResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


/**
 * Created by HP on 3/28/2018.
 */

public class LocationShare extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {
    private static final int GPS_PERMISSION = 100;                  //marshmallow permission
    private static final int REQUEST_CHECK_SETTINGS = 300;               //this is for turning the gps from the dialog
    private GoogleApiClient mGoogleApiClient = null;
    private Location mLastLocation;
    private GoogleMap mMap;
    LatLng parlorLatLong;
    private double lat, lng;
    private String address;
    private LinearLayout linearLayout_distance;
    private boolean isLatLng = false;
    private View progressBar;
    private int MIN_TIME_BW_UPDATES = 5000;
    private int retry = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_share);
        setToolBar();
        linearLayout_distance = findViewById(R.id.linearLayout_distance);
        progressBar = findViewById(R.id.loading_check);
        progressBar.setVisibility(View.VISIBLE);

        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            //your method here
                            getLocation();
                        } catch (Exception e) {
                        }
                    }
                });
            }
        };


        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }

        if (lat != 0 && lng != 0)
            inItView();

        timer.schedule(doAsynchronousTask, 0, MIN_TIME_BW_UPDATES); //execute in every 5 minute


        linearLayout_distance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLatLng  && ChatActivity.locationShare &&  GroupChatActivity.locationShareGroup) {
                    EventBus.getDefault().post(new LatiLng("" + lat, "" + lng, address));
                    isLatLng = false;

                    finish();


                } else {

                    Toast.makeText(LocationShare.this, "please wait we are getting your location", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    private void inItView() {
        //22.071871, 82.158165  current lat long

        parlorLatLong = new LatLng(lat, lng);
        try {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(LocationShare.this);


        } catch (NullPointerException e) {

        }


    }

    private void getLocation() {
        final LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Log.e("locario", "Getting location");

        LocationListener locationListener = new LocationListener() {

            public void onLocationChanged(Location location) {
//
                Log.e("save", "location found" + location.getLatitude() + "DFft" + location.getLongitude());
                Log.e("locario", "location found" + location.getLatitude() + "DFft" + location.getLongitude());

                progressBar.setVisibility(View.GONE);

                lat = location.getLatitude();
                lng = location.getLongitude();


                isLatLng = true;
                ChatActivity.locationShare=true;
                GroupChatActivity. locationShareGroup=true;
                inItView();


                // inItView();

                if (ActivityCompat.checkSelfPermission(LocationShare.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LocationShare.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling

                    return;
                }
                locationManager.removeUpdates(this);

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };
//        Log.e("locario", "Listener");
        // Register the listener with the Location Manager to receive location updates

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
//        Log.e("locario", "Listener 2");

    }

    private void setToolBar() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.direction));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }
    }


    public void handleNewLocation(Location lastLocation) {
        Retrofit retrofit;
        try {

            final double lat = lastLocation.getLatitude(), lon = lastLocation.getLongitude();

            retrofit = ServiceGenerator.getClient();
            ApiInterface apiInterface = retrofit.create(ApiInterface.class);
            Call<GeoCodingResponse> call = apiInterface.getAddress(
                    "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&sensor=true" + "&key=AIzaSyDwsi-O7uCC9IgO6Jx8m8wMWoLbdfyOLT4");
            call.enqueue(new Callback<GeoCodingResponse>() {
                @Override
                public void onResponse(Call<GeoCodingResponse> call, Response<GeoCodingResponse> response) {

                    if (response.isSuccessful()) {

                        try {

                            if (response.body().getResults().size() > 0) {

                                address = response.body().getResults().get(0).getFormatted_address();
                                String localtyAddress = response.body().getResults().get(0).getAddress_components().get(1).getLong_name();
                                if (address != null && address != "") {


                                }
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<GeoCodingResponse> call, Throwable t) {

                    retry++;
                    if (retry < 2) {
                        handleNewLocation(mLastLocation);
                    }

                }
            });

        } catch (Exception e) {
            Log.e("investigatingg", e.getMessage());
        }

    }


    @Override
    protected void onStart() {

        if (mGoogleApiClient != null) {

            mGoogleApiClient.connect();
        }
        super.onStart();
    }

    @Override
    protected void onStop() {


        lat = 0;
        lng = 0;
        address = "";
        if (mGoogleApiClient != null) {

            if (mGoogleApiClient.isConnected()) {

                mGoogleApiClient.disconnect();
            }

        }
        super.onStop();
    }

    private void requestContactPermission() {

        int hasContactPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (hasContactPermission != PackageManager.PERMISSION_GRANTED) {
            Log.i("permissions", "i'm in not onReqest permis granterd");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, GPS_PERMISSION);
        } else {
            Log.i("permissions", "i'm in already onReqest permis granterd");
            checkLocation();

//            Toast.makeText(this, "Contact Permission is already granted", Toast.LENGTH_LONG).show();
        }
    }


    private void checkLocation() {

        // Create an instance of GoogleAPIClient.

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        final LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.
                checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i("investigatingg", "i'm in succes of location setting");       //idhar location get kar

                        mGoogleApiClient = new GoogleApiClient.Builder(LocationShare.this)
                                .addConnectionCallbacks(LocationShare.this)
                                .addOnConnectionFailedListener(LocationShare.this)
                                .addApi(LocationServices.API)
                                .build();
                        mGoogleApiClient.connect();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i("investigatingg", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {


                            status.startResolutionForResult(LocationShare.this, REQUEST_CHECK_SETTINGS);


                            // need to be some changes need
                            //  startService(new Intent(LocationShare.this, LocationService.class));
//
                        } catch (IntentSender.SendIntentException e) {

                            Log.i("investigatingg", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("investigatingg", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });


    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            requestContactPermission();
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

//        Log.i("investigatingg", "value in locat" + mLastLocation.getLongitude()+ "  "+ mLastLocation.getLatitude());

        if (mLastLocation == null) {

            if (Build.VERSION.SDK_INT < 23) {

                checkLocation();
            } else {
                requestContactPermission();
            }
        } else {
            handleNewLocation(mLastLocation);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
// Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:

                        Log.e("requestget", "fine");
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.e("requestnot compl", "fine");
                        checkLocation();//keep asking if imp or do whatever
                        break;
                }
                break;
        }
    }




    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera

        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(false);

        // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(parlorLatLong, 16));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(parlorLatLong, 16));
        mMap.addMarker(new MarkerOptions().position(parlorLatLong));
    }
}
