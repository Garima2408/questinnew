package co.questin.chat;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;

import co.questin.database.QuestinSQLiteHelper;
import co.questin.models.chat.ChatModel;
import co.questin.utils.Constants;
import co.questin.utils.Utils;



public class ImageDownloadService extends IntentService {

    private static final String TAG = "ImageDownloadService";

    public ImageDownloadService(){
        super("ImageDownloadThread");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        assert intent != null;
        final ChatModel chatModel = intent.getParcelableExtra(Constants.CHAT_MODEL);
        final String senderId = intent.getStringExtra(Constants.SENDER_ID);
        final QuestinSQLiteHelper questinSQLiteHelper = new QuestinSQLiteHelper(this);
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference reference = storage.getReferenceFromUrl(chatModel.getLink());

        if(Utils.isExternalStorageWritable()){
            final File imageFile = new File(Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    Utils.createNewImageFileName());

            boolean isCreated = false;
            try {
                isCreated = imageFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(isCreated){
                reference.getFile(imageFile)
                        .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                chatModel.setLink(Uri.fromFile(imageFile).toString());
                                questinSQLiteHelper.updateChatMessageLink(chatModel, senderId);
                                LocalBroadcastManager.getInstance(ImageDownloadService.this)
                                        .sendBroadcast(new Intent(Constants.ACTION_IMAGE_DOWNLOADED)
                                                .putExtra(Constants.CHAT_MODEL, chatModel)
                                                .putExtra(Constants.SENDER_ID, senderId)
                                        );
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                    }
                });
            }
            else {
                Log.e(TAG, "onHandleIntent: file not created");
            }
        }
    }
}
