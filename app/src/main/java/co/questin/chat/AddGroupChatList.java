package co.questin.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.ChatMemeberSelectAdapter;
import co.questin.library.StateBean;
import co.questin.models.AllFriends;
import co.questin.network.URLS;
import co.questin.studentprofile.FriendsSection;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class AddGroupChatList extends BaseAppCompactActivity {
    EditText searchEmail;
    String SearchedName;
    public ArrayList<AllFriends> mFriendsList;
    public ArrayList<AllFriends> mFriendsArrayList;

    ChatMemeberSelectAdapter memberAdapter; /*all friends for selections*/
    FriendMembersAdapter friendmemberAdapter;
    //  public ArrayList<ChatSelectedArray> FriendSelectedList;
    RecyclerView SelectedName_Lists;/*list of all selected*/
    ListView rv_FriendList;/*list of friends*/
    ArrayAdapter<String> adapter;
    private ArrayList<String> frilist;
    private ArrayList<String> selectedstudentSend;
    RecyclerView.LayoutManager mLayoutManager;
    TextView ErrorText;
    Button invitebutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group_chat_list);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        searchEmail =findViewById(R.id.searchEmail);
        SelectedName_Lists = findViewById(R.id.SelectedName_Lists);
        rv_FriendList = findViewById(R.id.rv_FriendList);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        SelectedName_Lists.setLayoutManager(mLayoutManager);
        SelectedName_Lists.setHasFixedSize(true);
        ErrorText =findViewById(R.id.ErrorText);
        invitebutton =findViewById(R.id.invitebutton);
        mFriendsList=new ArrayList<>(); /*all friend list*/
        frilist = new ArrayList<String>();/*search friend list*/
        mFriendsArrayList=new ArrayList<>();

        MyFriendsListDisplay();
        searchEmail.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER) {
                    addnewfriends();
                    return true;
                }
                return false;
            }
        });

        invitebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity (new Intent(AddGroupChatList.this, FriendsSection.class));
                finish();
            }
        });

        searchEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = searchEmail.getText().toString()
                        .toLowerCase(Locale.getDefault());
                if (text.length()==0){
                    memberAdapter = new ChatMemeberSelectAdapter(AddGroupChatList.this,
                            R.layout.list_groupchat_member, mFriendsArrayList);

                    rv_FriendList.setAdapter(memberAdapter);
                }
            }
        });

    }






    private void addnewfriends() {
        mFriendsList.clear();
        searchEmail.setError(null);

        // Store values at the time of the login attempt.
        SearchedName = searchEmail.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(SearchedName)) {
            focusView = searchEmail;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();

        } else {

            SearchFriends();

        }
    }


    private void MyFriendsListDisplay() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_MYFRIENDS+"?"+"email="+ SessionManager.getInstance(getActivity()).getUser().getEmail(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");


                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                                        for (int i = 0; i < jsonArrayData.length(); i++) {

                                            AllFriends CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AllFriends.class);
                                            mFriendsList.add(CourseInfo);
                                            mFriendsArrayList.add(CourseInfo);
                                            frilist.add(CourseInfo.getField_firstname() + "" + CourseInfo.getField_lastname() + " " + CourseInfo.getPicture());
                                            memberAdapter = new ChatMemeberSelectAdapter(AddGroupChatList.this,
                                                    R.layout.list_groupchat_member, mFriendsArrayList);
                                            rv_FriendList.setAdapter(memberAdapter);
                                            rv_FriendList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);



                                            rv_FriendList.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                                                @Override
                                                public void onItemCheckedStateChanged(ActionMode mode,
                                                                                      int position, long id, boolean checked) {
                                                    // Capture total checked items
                                                    final int checkedCount = rv_FriendList.getCheckedItemCount();

                                                    Log.d("TAG", "checkedCount: " + checkedCount);
                                                    mode.setTitle(checkedCount + " Selected");
                                                    memberAdapter.toggleSelection(position);

                                                    ArrayList<String> selectedstudent = new ArrayList<String>();
                                                    SparseBooleanArray checkedd = rv_FriendList.getCheckedItemPositions();

                                                    for (int i = 0; i < checkedd.size(); i++) {
                                                        // Item position in adapter
                                                        int positio = checkedd.keyAt(i);
                                                        // Add sport if it is checked i.e.) == TRUE!
                                                        if (checkedd.valueAt(i))
                                                            /*  {0=true, 1=false, 2=true}*/

                                                            selectedstudent.add(String.valueOf(memberAdapter.getItem(positio).getUid()+","+memberAdapter.getItem(positio).getField_firstname()+","+memberAdapter.getItem(positio).getField_lastname()+","+memberAdapter.getItem(positio).getPicture()));


                                                    }

                                                    String[] outputStrArr = new String[selectedstudent.size()];
                                                    selectedstudentSend = new ArrayList<String>();
                                                    for (int x = 0; x < selectedstudent.size(); x++) {
                                                        outputStrArr[x] = selectedstudent.get(x);
                                                        Log.d("TAG", " outputprint[x]: " + outputStrArr[x]);
                                                        selectedstudentSend.add(outputStrArr[x]);


                                                    }
                                                    friendmemberAdapter = new FriendMembersAdapter(AddGroupChatList.this, R.layout.list_friend_member,selectedstudentSend);
                                                    SelectedName_Lists.setAdapter(friendmemberAdapter);

                                                }

                                                @Override
                                                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                                                    switch (item.getItemId()) {
                                                        case R.id.Submit:
                                                            Intent intent = new Intent(getApplicationContext(),
                                                                    AddGroupChat.class);
                                                            Bundle b = new Bundle();
                                                            b.putStringArrayList("selectedItems", selectedstudentSend);
                                                            intent.putExtras(b);
                                                            startActivity(intent);

                                                            return true;
                                                        default:
                                                            return false;
                                                    }
                                                }

                                                @Override
                                                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                                                    mode.getMenuInflater().inflate(R.menu.submit_menu, menu);
                                                    return true;
                                                }

                                                @Override
                                                public void onDestroyActionMode(ActionMode mode) {
                                                    // TODO Auto-generated method stub
                                                    memberAdapter.removeSelection();
                                                    memberAdapter.notifyDataSetChanged();

                                                }

                                                @Override
                                                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                                                    // TODO Auto-generated method stub
                                                    return false;
                                                }
                                            });


                                        }

                                    }
                                    else{
                                        ErrorText.setVisibility(View.VISIBLE);
                                        ErrorText.setText("you have no friends");
                                        invitebutton.setVisibility(View.VISIBLE);


                                    }



                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(AddGroupChatList.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(AddGroupChatList.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(AddGroupChatList.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }




    private void SearchFriends() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_MYFRIENDS+"?search="+SearchedName,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");



                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                                        for (int i = 0; i < jsonArrayData.length(); i++) {

                                            AllFriends CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AllFriends.class);
                                            mFriendsList.add(CourseInfo);
                                            frilist.add(CourseInfo.getField_firstname() + "" + CourseInfo.getField_lastname() + " " + CourseInfo.getPicture());
                                            memberAdapter = new ChatMemeberSelectAdapter(AddGroupChatList.this,R.layout.list_groupchat_member, mFriendsList);
                                            rv_FriendList.setAdapter(memberAdapter);
                                            rv_FriendList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);



                                            rv_FriendList.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                                                @Override
                                                public void onItemCheckedStateChanged(ActionMode mode,
                                                                                      int position, long id, boolean checked) {
                                                    // Capture total checked items
                                                    final int checkedCount = rv_FriendList.getCheckedItemCount();

                                                    Log.d("TAG", "checkedCount: " + checkedCount);
                                                    mode.setTitle(checkedCount + " Selected");
                                                    memberAdapter.toggleSelection(position);

                                                    ArrayList<String> selectedstudent = new ArrayList<String>();
                                                    SparseBooleanArray checkedd = rv_FriendList.getCheckedItemPositions();

                                                    for (int i = 0; i < checkedd.size(); i++) {
                                                        // Item position in adapter
                                                        int positio = checkedd.keyAt(i);
                                                        // Add sport if it is checked i.e.) == TRUE!
                                                        if (checkedd.valueAt(i))
                                                            /*  {0=true, 1=false, 2=true}*/

                                                            selectedstudent.add(String.valueOf(memberAdapter.getItem(positio).getUid()+","+memberAdapter.getItem(positio).getField_firstname()+","+memberAdapter.getItem(positio).getField_lastname()+","+memberAdapter.getItem(positio).getPicture()));


                                                    }

                                                    String[] outputStrArr = new String[selectedstudent.size()];
                                                    selectedstudentSend = new ArrayList<String>();
                                                    for (int x = 0; x < selectedstudent.size(); x++) {
                                                        outputStrArr[x] = selectedstudent.get(x);
                                                        Log.d("TAG", " outputprint[x]: " + outputStrArr[x]);
                                                        selectedstudentSend.add(outputStrArr[x]);


                                                    }
                                                    friendmemberAdapter = new FriendMembersAdapter(AddGroupChatList.this, R.layout.list_friend_member,selectedstudentSend);
                                                    SelectedName_Lists.setAdapter(friendmemberAdapter);

                                                }

                                                @Override
                                                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                                                    switch (item.getItemId()) {
                                                        case R.id.Submit:
                                                            Intent intent = new Intent(getApplicationContext(),
                                                                    AddGroupChat.class);
                                                            Bundle b = new Bundle();
                                                            b.putStringArrayList("selectedItems", selectedstudentSend);
                                                            intent.putExtras(b);
                                                            startActivity(intent);

                                                            return true;
                                                        default:
                                                            return false;
                                                    }
                                                }

                                                @Override
                                                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                                                    mode.getMenuInflater().inflate(R.menu.submit_menu, menu);
                                                    return true;
                                                }

                                                @Override
                                                public void onDestroyActionMode(ActionMode mode) {
                                                    // TODO Auto-generated method stub
                                                    memberAdapter.removeSelection();
                                                    memberAdapter.notifyDataSetChanged();

                                                }

                                                @Override
                                                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                                                    // TODO Auto-generated method stub
                                                    return false;
                                                }
                                            });


                                        }

                                    }
                                    else{

                                        showSnackbarinteger(R.string.Friends);

                                    }


                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(AddGroupChatList.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(AddGroupChatList.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(AddGroupChatList.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();

    }

}
