package co.questin.chat;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.activities.UserDisplayProfile;
import co.questin.library.StateBean;
import co.questin.models.chat.GroupChatMemberArray;
import co.questin.network.URLS;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

import static co.questin.chat.ChatGroupMemberProfile.RefershList;
import static co.questin.utils.BaseAppCompactActivity.hideIndicator;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;

/**
 * Created by Dell on 08-11-2017.
 */

public class GroupChatMemberAdapter extends RecyclerView.Adapter<GroupChatMemberAdapter.CustomVholder> {

    private ArrayList<GroupChatMemberArray> lists;
    private Context mcontext;
    String Group_id,GroupTittle,groupImage,is_admin,admin,memberId;





    public GroupChatMemberAdapter(Context mcontext, ArrayList<GroupChatMemberArray> lists, String is_admin,String admin) {
        this.lists = lists;
        this.mcontext = mcontext;
        this.is_admin = is_admin;
        this.admin=admin;

    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_ofchatmembers, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.memberName.setText(lists.get(position).getField_first_name()+" "+lists.get(position).getField_last_name());


            if(lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0 ) {

                Glide.with(mcontext).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.single).dontAnimate()
                        .fitCenter().into(holder.circleView);

            }else {
                holder.circleView.setImageResource(R.mipmap.single);

            }



            holder.mainlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    final CharSequence  [] items= { "Remove this Member","Show Profile", "Cancel"};



                    AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);

                    builder.setTitle("Action:");
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (items[item].equals("Remove this Member")) {

                                AlertDialog.Builder builder = new AlertDialog.Builder(mcontext, android.app.AlertDialog.THEME_HOLO_DARK)
                                        .setTitle("Remove this member")
                                        .setMessage(R.string.delete_Chat_member)
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                /*  DELETE this member */

                                                if (lists.get(position).getEtid().equals(admin))

                                                {
                                                    deleteMemberfromlist(lists.get(position).getGid(), lists.get(position).getEtid());

                                                }else{

                                                    Toast.makeText(mcontext,"You are not authorized",Toast.LENGTH_SHORT).show();
                                                }



                                            }
                                        })
                                        .setNegativeButton("No", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {

                                                dialogInterface.dismiss();
                                            }
                                        });
                                builder.create().show();



                            } else if (items[item].equals("Show Profile")) {

                                Intent backIntent = new Intent(mcontext, UserDisplayProfile.class)
                                        .putExtra("USERPROFILE_ID", lists.get(position).getEtid());
                                mcontext.startActivity(backIntent);

                            } else if (items[item].equals("Cancel")) {
                                dialog.dismiss();
                            }
                        }
                    });
                    builder.show();




                }
            });


            if (lists.get(position).getEtid().equals(admin))
            {
                holder.admin.setVisibility(View.VISIBLE);
            }

            else if(lists.get(position).getEtid().equals(is_admin))
            {
                holder.admin.setVisibility(View.VISIBLE);
            }





        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    private void deleteMemberfromlist(String gid,String eid)
    {
        Group_id = gid;
        memberId = eid;

        StringRequest mStrRequest = new StringRequest(Request.Method.POST,  URLS.URL_CHATGROUPMEMBEREXIXT+"/"+Group_id+"/"+memberId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    RefershList();

                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());

                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {

                            showServerSnackbar(R.string.error_responce);
                        } else {
                            showConnectionSnackbar();

                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                String Cookies= SessionManager.getInstance(mcontext).getaccesstoken().sessionName+"="+SessionManager.getInstance(mcontext).getaccesstoken().sessionID;
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(mcontext).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }



        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        requestQueue.add(mStrRequest);

        }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
        ImageView circleView;
        private TextView memberName,admin;
        RelativeLayout mainlay;


        public CustomVholder(View itemView) {
            super(itemView);

            memberName = itemView.findViewById(R.id.memberName);
            circleView = itemView.findViewById(R.id.circleView);
            mainlay =itemView.findViewById(R.id.mainlay);
            admin =itemView.findViewById(R.id.admin);

        }

    }




}


