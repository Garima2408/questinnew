package co.questin.chat;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.soundcloud.android.crop.Crop;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.Event.CampusFeed;
import co.questin.R;
import co.questin.library.StateBean;
import co.questin.models.chat.GroupChatMemberArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import okhttp3.RequestBody;

import static co.questin.utils.Utils.isStatusSuccess;

public class ChatGroupMemberProfile extends BaseAppCompactActivity {
    static RecyclerView listMemberAdded;
    TextView existGroup ,header12;
    LinearLayout linear;
    Bitmap thumbnail = null;
    String Fileimagename;
    public static String groupName;
    String Uid= SessionManager.getInstance(getActivity()).getUser().getUserprofile_id();
    RequestBody body;
    ImageView imgbtn_edit_profile;
    ImageView circleView;
    static String  is_admin,groupImage;
    static String  groupId;
    public boolean isCreated;
    public static Activity activity;
    Uri imageUri;
    private static final int ACTIVITY_START_CAMERA_APP = 0;
    private int SELECT_FILE = 1;
    private static final int EXTERNAL_STORAGE_PERMISSION_REQUEST = 23;
    public static ArrayList<GroupChatMemberArray> mchatmemberList;
    private static GroupChatMemberAdapter chatmemberadapter;
    private RecyclerView.LayoutManager layoutManager;
    String strFile = null;
    private CollapsingToolbarLayout collapsingToolbarLayout = null;
    private String mImageFileLocation;
    public static String admin;
    public static final int RequestPermissionCode = 1;
    Boolean CallingCamera,CallingGallary;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_group_member_profile);



        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeButtonEnabled(true);


        activity =getActivity();
        mchatmemberList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        listMemberAdded = findViewById(R.id.listMemberAdded);
        existGroup =findViewById(R.id.existGroup);
        linear = findViewById(R.id.linear);
        circleView = findViewById(R.id.imageProfile);
        listMemberAdded.setHasFixedSize(true);
        imgbtn_edit_profile = findViewById(R.id.imgbtn_edit_profile);
        listMemberAdded.setLayoutManager(layoutManager);
        header12 = findViewById(R.id.header12);
        collapsingToolbarLayout =  findViewById(R.id.collapsing_toolbar);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        is_admin=extras.getString("is_ADMIN");
        Bundle b = getIntent().getExtras();
        admin = b.getString("ADMIN");
        groupId = b.getString("GROUP_ID");
        groupName = b.getString("GROUP_NAME");
        is_admin=b.getString("is_ADMIN");
        groupImage = b.getString("GroupImage");
        isCreated=  b.getBoolean("isCreated");



        if (is_admin.equals("FALSE"))
        {

            header12.setVisibility(View.GONE);
        }


        chatmemberadapter = new GroupChatMemberAdapter(ChatGroupMemberProfile.this, mchatmemberList,is_admin,admin);
        listMemberAdded.setAdapter(chatmemberadapter);
        listMemberAdded.setNestedScrollingEnabled(false);

        collapsingToolbarLayout.setTitle(groupName);


        if (groupImage != null) {

            Glide.with(this).load(groupImage)
                    .placeholder(R.mipmap.chat_group).dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .fitCenter().into(circleView);

        }

        ChatGroupMemberDisplay();


        linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog.Builder builder = new AlertDialog.Builder(ChatGroupMemberProfile.this, android.app.AlertDialog.THEME_HOLO_DARK)

                        .setMessage(R.string.exitgroup)
                        .setCancelable(false)
                        .setPositiveButton("Yes", new Dialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (is_admin.equals("TRUE") ) {
                                    Toast.makeText(getApplicationContext(), "you are not permitted to exit the group", Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {
                                        ExistChatGroup(SessionManager.getInstance(getActivity()).getUser().getParentUid());



                                    } else
                                        ExistChatGroup(SessionManager.getInstance(getActivity()).getUser().getParentUid());





                                }
                            }
                        })
                        .setNegativeButton("No", new Dialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                builder.create().show();




            }
        });
        circleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddGroupImageDialog();

            }
        });



        imgbtn_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if
                        (is_admin.equals("TRUE"))
                {
                    Intent i = new Intent(ChatGroupMemberProfile.this, EditChatGroupName.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("GROUP_ID", groupId);
                    bundle.putString("GROUP_NAME", groupName);
                    bundle.putString("ADMIN",admin);
                    bundle.putString("is_ADMIN",is_admin);

                    i.putExtras(bundle);
                    startActivity(i);}

                  /*  else if (admin==Uid) {
                        Intent i = new Intent(ChatGroupMemberProfile.this, EditChatGroupName.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("GROUP_ID", groupId);
                        bundle.putString("GROUP_NAME", groupName);
                        bundle.putString("ADMIN",admin);
                        bundle.putString("is_ADMIN",is_admin);

                        i.putExtras(bundle);
                        startActivity(i);}*/
                else {
                    Toast.makeText(getApplicationContext(),"you are not admin",Toast.LENGTH_SHORT).show();
                }
            }
        });



        header12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                Intent i = new Intent(ChatGroupMemberProfile.this,AddNewChatMember.class);
                Bundle bundle=new Bundle();
                bundle.putString("GROUP_ID",groupId);
                bundle.putString("ACTIVITY","CHATMEMBER_ACTIVITY");
                i.putExtras(bundle);
                startActivity(i);
            }
        });
    }


    @Override
    protected void onResume() {

        super.onResume();

        collapsingToolbarLayout.setTitle(groupName);
        ChatGroupMemberDisplay();






    }

    public  static void RefershList()
    {
        ChatGroupMemberDisplay();



    }

    private static void ChatGroupMemberDisplay() {

        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_CHATGROUPMEMBER+"/"+groupId+"/users"+"?offset="+"0"+"&limit=50",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {

                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    if (mchatmemberList.size()>0)
                                        mchatmemberList.clear();
                                    for (int i = 0; i < jsonArrayData.length(); i++) {

                                        GroupChatMemberArray memberInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), GroupChatMemberArray.class);
                                        mchatmemberList.add(memberInfo);
                                        chatmemberadapter = new GroupChatMemberAdapter(activity, mchatmemberList,is_admin,admin);
                                        listMemberAdded.setAdapter(chatmemberadapter);
                                        listMemberAdded.setNestedScrollingEnabled(false);

                                    }

                                } else {
                                    showSnackbarMessage(stateResponse.getMessage());

                                }
                            } else {

                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {

                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }



    private void ExistChatGroup(String UserUid) {
        ShowIndicator();

        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_CHATGROUPMEMBEREXIXT+"/"+groupId+"/"+UserUid,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (isStatusSuccess(stateResponse.getStatus())) {

                                    Intent in=new Intent(ChatGroupMemberProfile.this,ChatTabbedActivity.class);
                                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    in.putExtra("isGroup",true);
                                    startActivity(in);
                                    finish();



                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(ChatGroupMemberProfile.this).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(ChatGroupMemberProfile.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(ChatGroupMemberProfile.this).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);

    }



    private void AddGroupImageDialog() {
        final CharSequence[] items = { "Take Photo", "Choose from Gallery", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(ChatGroupMemberProfile.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    circleView.setImageDrawable(null);
                    CallingCamera =true;
                    CallingGallary =false;
                    if(checkPermission()){
                        CallCamera();
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    circleView.setImageDrawable(null);
                    CallingGallary =true;
                    CallingCamera =false;
                    if(checkPermission()){
                        galleryIntent();
                    }
                } else if (items[item].equals("Cancel")) {
                    if (items[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void CallCamera() {
        Intent callCameraApplicationIntent = new Intent();
        callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        File photoFile = null;
        try {
            photoFile = createImageFile();

        } catch (IOException e) {
            e.printStackTrace();
        }
        imageUri = FileProvider.getUriForFile(ChatGroupMemberProfile.this, "co.questin.fileprovider",photoFile);
        callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(callCameraApplicationIntent, ACTIVITY_START_CAMERA_APP);
    }


    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }


    private void galleryIntent()
    {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(Intent.createChooser(galleryIntent, "Select File"),SELECT_FILE);
    }


    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if(requestCode == ACTIVITY_START_CAMERA_APP && resultCode == RESULT_OK) {

            beginCrop(imageUri);

        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)


                onSelectFromGalleryResult(data);
        }
        if (requestCode == Crop.REQUEST_CROP) {

            handleCrop(resultCode, data);

        }
        if (resultCode == Activity.RESULT_CANCELED) {
            if (groupImage !=null)
                Glide.with(ChatGroupMemberProfile.this).load(groupImage)
                        .placeholder(R.mipmap.chat_group).dontAnimate()
                        .fitCenter().into(circleView);

        }else {
            Glide.with(ChatGroupMemberProfile.this).load(R.mipmap.chat_group)
                    .placeholder(R.mipmap.chat_group).dontAnimate()
                    .fitCenter().into(circleView);

        }

    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(result));
                thumbnail = getResizedBitmap(thumbnail,1000);
                strFile = encodeImageTobase64(thumbnail);
                System.out.println("camera " + strFile);
                circleView.setImageBitmap(thumbnail);

                if (strFile !=null){
                    ProgressUpdateUserProfile(strFile);



                }else {
                    showSnackbarMessage("There seems to be some problem.please select again");

                }

            } catch (IOException e) {
                e.printStackTrace();
            }




        } else if (resultCode == Crop.RESULT_ERROR) {
            //  Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }




    /*New Galley Code*/



    //Choose From Gallery
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                beginCrop(data.getData());
                Uri uri = data.getData();
                String[] projection = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                Log.d("TAG", DatabaseUtils.dumpCursorToString(cursor));

                int columnIndex = cursor.getColumnIndex(projection[0]);
                Fileimagename = cursor.getString(columnIndex); // full path of image

                cursor.close();
            }catch (Exception e)
            {
                e.printStackTrace();
            }


        }

    }



    File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        Fileimagename = image.getName();
        return image;

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    private void ProgressUpdateUserProfile(final String strFile) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_UPDATEGROUPPHOTO+"/"+groupId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);

                                    JSONObject picture = obj.getJSONObject("data");

                                    if (picture != null && picture.length() > 0) {
                                        groupImage = picture.getString("icon_url");
                                        Glide.with(ChatGroupMemberProfile.this).load(groupImage)
                                                .placeholder(R.mipmap.chat_group).dontAnimate()
                                                .fitCenter().into(circleView);



                                    }

                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(getActivity()).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(getActivity()).getaccesstoken().sessionName+"="+SessionManager.getInstance(getActivity()).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("file",strFile);
                params.put("filename",Fileimagename);
                return params;
            }


        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {

        EventBus.getDefault().post(new CampusFeed(1));

        finish();

    }
    private  boolean checkPermission() {
        int camerapermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionLocation = ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), RequestPermissionCode);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("TAG", "Permission callback called-------");
        switch (requestCode) {
            case RequestPermissionCode: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("TAG", "sms & location services permission granted");
                        if (CallingCamera ==true){
                            CallingGallary=false;
                            CallCamera();

                        }else if (CallingGallary ==true) {
                            CallingCamera=false;

                            galleryIntent();


                        }
                    } else {
                        Log.d("TAG", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Service Permissions are required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkPermission();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    finish();
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
    private void explain(String msg){
        final android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        //  permissionsclass.requestPermission(type,code);
                        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:com.exampledemo.parsaniahardik.marshmallowpermission")));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        finish();
                    }
                });
        dialog.show();
    }



}










