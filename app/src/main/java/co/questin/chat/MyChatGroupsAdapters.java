package co.questin.chat;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import co.questin.R;
import co.questin.database.QuestinSQLiteHelper;
import co.questin.library.StateBean;
import co.questin.models.chat.GroupChatModel;
import co.questin.models.chat.GroupConversationModel;
import co.questin.models.chat.MyChatGroupArray;
import co.questin.network.URLS;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import de.hdodenhof.circleimageview.CircleImageView;

import static co.questin.utils.BaseAppCompactActivity.ShowIndicator;
import static co.questin.utils.BaseAppCompactActivity.hideIndicator;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;

/**
 * Created by Dell on 09-11-2017.
 */

public class MyChatGroupsAdapters extends RecyclerView.Adapter<MyChatGroupsAdapters.CustomVholder> {
    private GroupConversationListAdapter conversationListAdapter;
    public ArrayList<GroupConversationModel> conversationModel;
    private ArrayList<GroupChatModel>Model;
    private ArrayList<MyChatGroupArray> lists;
    private Context mcontext;
    Dialog profileDialog,imageDialog;
    String GroupId,GroupTittle,groupImage,is_admin,admin;
    private SimpleDateFormat simpleDateFormat;
    private QuestinSQLiteHelper questinSQLiteHelper;
    Activity activity;
    String[] items;
    public MyChatGroupsAdapters(Context mcontext, ArrayList<MyChatGroupArray> lists) {
        this.lists = lists;
        this.is_admin =is_admin;
        this.admin=admin;
        this.mcontext = mcontext;
        simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_mygroupchats, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.text_name.setText(lists.get(position).getTitle());

            admin = lists.get(position).getAdmin();
            is_admin = lists.get(position).getIs_admin();

            if(lists.get(position).getIcon() != null && lists.get(position).getIcon().length() > 0 ) {

                Glide.with(mcontext).load(lists.get(position).getIcon())
                        .placeholder(R.mipmap.chat_group).dontAnimate()
                        .fitCenter().into(holder.image_dp);

            }else {
                holder.image_dp.setImageResource(R.mipmap.chat_group);

            }



            final QuestinSQLiteHelper questinSQLiteHelper = new QuestinSQLiteHelper(mcontext);
            ArrayList<GroupChatModel> Model =questinSQLiteHelper.getGroupChatList(lists.get(position).getTnid());


            for (GroupChatModel cn : Model) {
                if(cn.getText() !=  null){
                    holder.text_last_message.setText(cn.getText());
                }else {

                }
                if(cn.getTime() !=  null){
                    String time = simpleDateFormat.format(new Date(Long.valueOf(cn.getTime())));
                    holder.text_last_message_time.setText(time);
                }else {

                }

              /*  String log = "getLastMsgText: " + cn.getLastMsgText() + " ,getLastMsgTime: " + cn.getLastMsgTime();
                // Writing Contacts to log
                Log.d("getLastMsgText ", cn.getLastMsgText()+""+cn.getLastMsgTime());
*/

            }



            if (getUnreadMessgeCount(lists.get(position).getTnid())>0){
                holder.text_unread_messages.setVisibility(View.VISIBLE);
                holder.text_unread_messages.setText(""+getUnreadMessgeCount(lists.get(position).getTnid()));

            }else{
                holder.text_unread_messages.setVisibility(View.GONE);

            }

            holder.layout_conversation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {


                        GroupConversationModel conversationModel = new GroupConversationModel()

                                .setGroupId(lists.get(position).getTnid())
                                .setSenderId(SessionManager.getInstance(mcontext).getUser().getUserprofile_id())
                                .setName(lists.get(position).getTitle())
                                .setDpUrl(lists.get(position).getIcon());
                        Intent backIntent = new Intent(mcontext, GroupChatActivity.class)
                                .putExtra(Constants.EXTRA_IS_FIRST_MESSAGE, !questinSQLiteHelper.doesGroupConversationExist(conversationModel.getGroupId()))
                                .putExtra(Constants.CONVERSATION_MODEL, conversationModel)
                                .putExtra("GroupID", lists.get(position).getTnid())
                                .putExtra("GroupName", lists.get(position).getTitle())
                                .putExtra("GroupImage", lists.get(position).getIcon())
                                .putExtra("ADMIN", lists.get(position).getAdmin())
                                .putExtra("is_ADMIN", lists.get(position).getIs_admin())
                                .putExtra("open", "activityMyGroupdata");

                        Log.i("TAG","is_admin"+is_admin);
                        mcontext.startActivity(backIntent);


                    }catch (ArrayIndexOutOfBoundsException e)
                    {
                        e.printStackTrace();
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }


                }
            });


            holder.layout_conversation.setOnLongClickListener(new View.OnLongClickListener() {


                @Override
                public boolean onLongClick(View view) {
                    //  =new String[]{};

                    final CharSequence  [] items= {"Delete Group", "Clear Conversation", "Cancel"};



                    AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);

                    builder.setTitle("Action:");
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (items[item].equals("Delete Group")) {





                                if(lists.get(position).getIs_admin().equals("TRUE")){
                                    AlertDialog.Builder builder = new AlertDialog.Builder(mcontext, android.app.AlertDialog.THEME_HOLO_DARK)
                                            .setTitle("Delete Group")
                                            .setMessage(R.string.delete_Chat)
                                            .setCancelable(false)
                                            .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                  /*  DELETE MY GROUP */
                                                    AttempToDeleteGroups(lists.get(position).getTnid());
                                                    lists.remove(position);




                                                }
                                            })
                                            .setNegativeButton("No", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                    builder.create().show();

                                }else{
                                    Toast.makeText(mcontext,"You are not admin so you can not delete this group ",
                                            Toast.LENGTH_SHORT).show();

                                }






                            } else if (items[item].equals("Clear Conversation")) {
                                QuestinSQLiteHelper questinSQLiteHelper = new QuestinSQLiteHelper(mcontext);

                                AlertDialog.Builder builder = new AlertDialog.Builder(mcontext, android.app.AlertDialog.THEME_HOLO_DARK)
                                        .setTitle("Clear Conversation")
                                        .setMessage(R.string.Clear_Conversation)
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Utils.deleteGroupChatAndTable(mcontext,lists.get(position).getTnid());
                                                lists.remove(position);

                                               // RefreshChatListWhileAdding();

                                            }
                                        })
                                        .setNegativeButton("No", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        });
                                builder.create().show();



                            } else if (items[item].equals("Cancel")) {
                                dialog.dismiss();
                            }
                        }
                    });
                    builder.show();


                    return false;
                }
            });





        } catch (Exception e) {
            e.printStackTrace();
        }


        holder.image_dp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    showProfilepic(lists.get(position).getIcon(),lists.get(position).getTitle(),lists.get(position).getTnid(),lists.get(position).getAdmin(),lists.get(position).getIs_admin());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        });

    }

    private void AttempToDeleteGroups(String tnid) {
        GroupId =tnid;
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.DELETE, URLS.URL_DELETEMYGROUPS +"/"+GroupId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    showSnackbarMessage(stateResponse.getMessage());
                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {

                            showServerSnackbar(R.string.error_responce);

                        } else {

                            showConnectionSnackbar();

                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(mcontext).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(mcontext).getaccesstoken().sessionName+"="+SessionManager.getInstance(mcontext).getaccesstoken().sessionID);

                return params;
            }




        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        requestQueue.add(mStrRequest);

    }

    @Override
    public int getItemCount() {
        return lists.size();
    }




    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
        CircleImageView image_dp;
        private TextView text_name,text_last_message,text_last_message_time,text_unread_messages;
        RelativeLayout layout_conversation;


        public CustomVholder(View itemView) {
            super(itemView);
            image_dp =  itemView.findViewById(R.id.image_dp);
            text_name = itemView.findViewById(R.id.text_name);
            text_last_message = itemView.findViewById(R.id.text_last_message);
            text_last_message_time = itemView.findViewById(R.id.text_last_message_time);
            text_unread_messages = itemView.findViewById(R.id.text_unread_messages);
            layout_conversation =itemView.findViewById(R.id.layout_conversation);

        }

    }




    public void addAllData(ArrayList<GroupConversationModel> list){
        conversationModel.addAll(list);
        notifyDataSetChanged();
    }



      private int getUnreadMessgeCount(String groupId){
        final QuestinSQLiteHelper questinSQLiteHelper = new QuestinSQLiteHelper(mcontext);
        questinSQLiteHelper.getGroupConversationUnreadCount(groupId);
        // ArrayList<GroupChatModel> Model =questinSQLiteHelper.getGroupChatList(lists.get(position).getTnid());
        return  questinSQLiteHelper.getGroupConversationUnreadCount(groupId);
     }

    private void showProfilepic(final String url, final String name, final String id, final String admin, final String is_admin){

        final ImageView message,info,profilepic;
        final TextView senderName;

        questinSQLiteHelper = new QuestinSQLiteHelper(mcontext);
        profileDialog = new Dialog(mcontext);
        profileDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        profileDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        profileDialog.setContentView(R.layout.show_profile_dialog);
        profileDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation2;

        profileDialog.show();

        message = profileDialog.findViewById(R.id.messsage);
        profilepic = profileDialog.findViewById(R.id.profilepic);
        info = profileDialog.findViewById(R.id.info);
        senderName = profileDialog.findViewById(R.id.senderName);
        senderName.setText(name);

        Glide.clear(profilepic);
        Glide.with(mcontext).load(url)
                .placeholder(R.mipmap.chat_group).dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .fitCenter().into(profilepic);



        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileDialog.dismiss();
                ImageDialogOpen(url,name);

            }
        });


        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GroupConversationModel conversationModel = new GroupConversationModel()
                        .setGroupId(id)
                        .setSenderId(SessionManager.getInstance(mcontext).getUser().getUserprofile_id())
                        .setName(name)
                        .setDpUrl(url);
                Intent backIntent = new Intent(mcontext, GroupChatActivity.class)
                        .putExtra(Constants.EXTRA_IS_FIRST_MESSAGE, !questinSQLiteHelper.doesGroupConversationExist(conversationModel.getGroupId()))
                        .putExtra(Constants.CONVERSATION_MODEL, conversationModel)
                        .putExtra("GroupID", id)
                        .putExtra("GroupName", name)
                        .putExtra("GroupImage",url)
                        .putExtra ("ADMIN",admin)
                        .putExtra("is_ADMIN",is_admin)
                        .putExtra("open", "activityMyGroupdata");
                mcontext.startActivity(backIntent);
                profileDialog.dismiss();


            }
        });

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mcontext,ChatGroupMemberProfile.class);
                Bundle bundle=new Bundle();
                bundle.putString("GROUP_ID",id);
                bundle.putString("GROUP_NAME",name);
                bundle.putString("GroupImage",url);
                bundle.putString("ADMIN",admin);
                bundle.putString("is_ADMIN",is_admin);
                i.putExtras(bundle);
                mcontext.startActivity(i);
                profileDialog.dismiss();

            }
        });

    }


    private void ImageDialogOpen(String url,String name1) {

        final ImageView backone;
        TextView senderName;

        imageDialog = new Dialog(mcontext);
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.BLACK));
        imageDialog.setContentView(R.layout.image_preview_show);
        // dialogLogin.setCancelable(false);
        imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        imageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        imageDialog.show();
        ImageView showimage = imageDialog.findViewById(R.id.showimage);
        ImageMatrixTouchHandler imageMatrixTouchHandler = new ImageMatrixTouchHandler(mcontext);
        showimage.setOnTouchListener(imageMatrixTouchHandler);
        senderName = imageDialog.findViewById(R.id.senderName);
        backone = imageDialog.findViewById(R.id.backone);
        Glide.clear(showimage);

        senderName.setText(name1);
        Glide.with(mcontext).load(url)
                .placeholder(R.mipmap.header_image).dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .fitCenter().into(showimage);

        backone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageDialog.dismiss();
            }
        });




    }

}