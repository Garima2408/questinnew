package co.questin.chat;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import co.questin.R;
import co.questin.activities.UserDisplayProfile;
import co.questin.database.QuestinSQLiteHelper;
import co.questin.library.StateBean;
import co.questin.models.chat.ConversationModel;
import co.questin.models.userFriendsResponse.UserFriendsResponseModel;
import co.questin.network.URLS;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static co.questin.utils.BaseAppCompactActivity.ShowIndicator;
import static co.questin.utils.BaseAppCompactActivity.hideIndicator;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;

/**
 * Created by farheen on 13/9/17
 */

public class ConversationListAdapter extends RecyclerView.Adapter<ConversationListAdapter.ConversationListHolder>{

    private Context mContext;
    private LayoutInflater inflater;
    private String groupId;
    private ArrayList<ConversationModel> data;
    private SimpleDateFormat simpleDateFormat;
    private Dialog profileDialog,imageDialog;
    UserFriendsResponseModel userFriendsResponseModel=null;
    int posistion =0;
    private QuestinSQLiteHelper questinSQLiteHelper;
    String threadId;




    public ConversationListAdapter(ArrayList<ConversationModel> data, UserFriendsResponseModel userFriends,Context context,String threadId){
        mContext = context;
        this.data = data;
        inflater = LayoutInflater.from(mContext);
        this.userFriendsResponseModel=userFriends;
        simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        this.threadId = threadId;
    }

    @Override
    public ConversationListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ConversationListHolder(inflater.inflate(R.layout.conversation_list_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(final ConversationListHolder holder, final int position) {

        try {


            final ConversationModel currentData = data.get(position);

            if (data.get(position).getDpUrl() != null && data.get(position).getDpUrl().length() > 0) {

                Glide.with(mContext).load(currentData.getDpUrl()).placeholder(R.mipmap.single)
                        .fitCenter().into(holder.imageDp);

            } else {
                holder.imageDp.setImageResource(R.mipmap.single);

            }


            holder.textName.setText(currentData.getName());
            holder.textLastMsg.setText(currentData.getLastMsgText());
            if (currentData.getLastMsgTime() == null) {

            } else {
                String time = simpleDateFormat.format(new Date(Long.valueOf(currentData.getLastMsgTime())));
                holder.textLastMsgTime.setText(time);

            }

            if (currentData.getUnreadCount() > 0) {
                holder.textUnreadMsgs.setVisibility(View.VISIBLE);
                holder.textUnreadMsgs.setText(String.valueOf(currentData.getUnreadCount()));
            } else {
                holder.textUnreadMsgs.setVisibility(View.GONE);
            }

            holder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (getServerOrChatUser(currentData.getSenderId())) {

                        Intent chatIntent = new Intent(mContext, ChatActivity.class)
                                .putExtra(Constants.EXTRA_IS_FIRST_MESSAGE, false)
                                .putExtra(Constants.CONVERSATION_MODEL, data.get(holder.getAdapterPosition()));
                        mContext.startActivity(chatIntent);

                    } else {
                        Intent backIntent = new Intent(mContext, MyServerChatActivity.class)
                                .putExtra("USER_ID", currentData.getSenderId())
                                .putExtra("USER_NAME", currentData.getName())
                                .putExtra("PICTURE", currentData.getDpUrl());

                        mContext.startActivity(backIntent);

                    }

                }
            });

            holder.rootView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    final CharSequence[] items = { "Delete" };
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle("Action:");
                    builder.setItems(items, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int item) {
                            QuestinSQLiteHelper questinSQLiteHelper = new QuestinSQLiteHelper(mContext);
                            questinSQLiteHelper.deleteSingleChatList(currentData.getSenderId());

                            new AlertDialog.Builder(mContext)
                                    .setTitle("Delete")
                                    .setMessage("Remove Chat")
                                    .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (getServerOrChatUser(currentData.getSenderId())) {
                                                data.remove(position);
                                                notifyDataSetChanged();
                                                Utils.deleteUserChat(mContext, currentData.getSenderId());
                                            }
                                            else
                                            {
                                                data.remove(position);
                                                notifyDataSetChanged();
                                               DeleteServerChat(currentData.getThreadid());




                                            }

                                        }
                                    })
                                    .show();

                        }

                    });

                    AlertDialog alert = builder.create();

                    alert.show();
                    return false;
                }
            });



            holder.imageDp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showProfilepic(data.get(position).getDpUrl(), data.get(position).getName(), data.get(position).getSenderId());

                }
            });


        }catch (Exception e)
        {
            e.printStackTrace();
        }


    }



    private void ImageDialogOpen(String url,String name1) {

        final ImageView backone;
        TextView senderName;


        imageDialog = new Dialog(mContext);
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.BLACK));
        imageDialog.setContentView(R.layout.image_preview_show);
        // dialogLogin.setCancelable(false);
        imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        imageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        imageDialog.show();
       ImageView showimage = imageDialog.findViewById(R.id.showimage);
        ImageMatrixTouchHandler imageMatrixTouchHandler = new ImageMatrixTouchHandler(mContext);
        showimage.setOnTouchListener(imageMatrixTouchHandler);
        senderName = imageDialog.findViewById(R.id.senderName);
        backone = imageDialog.findViewById(R.id.backone);
        Glide.clear(showimage);

        senderName.setText(name1);
        Glide.with(mContext).load(url)
                .placeholder(R.mipmap.header_image).dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .fitCenter().into(showimage);


        backone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageDialog.dismiss();
            }
        });



    }


    private void showProfilepic(final String url, final String name, final String id){

        final ImageView message,info,profilepic;
        TextView senderName;

        questinSQLiteHelper = new QuestinSQLiteHelper(mContext);
        profileDialog = new Dialog(mContext);
        profileDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        profileDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        profileDialog.setContentView(R.layout.show_profile_dialog);
        profileDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation2;

        profileDialog.show();

        message = profileDialog.findViewById(R.id.messsage);
        profilepic = profileDialog.findViewById(R.id.profilepic);
        info = profileDialog.findViewById(R.id.info);
        senderName = profileDialog.findViewById(R.id.senderName);

        senderName.setText(name);

        Glide.clear(profilepic);
        Glide.with(mContext).load(url)
                .placeholder(R.mipmap.single).dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .fitCenter().into(profilepic);


        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                ConversationModel conversationModel = new ConversationModel().setSenderId(id)
                        .setName(name)
                        .setDpUrl(url);

                if (getServerOrChatUser(id)){
                    Intent backIntent = new Intent(mContext, ChatActivity.class)
                            .putExtra(Constants.EXTRA_IS_FIRST_MESSAGE, !questinSQLiteHelper.doesConversationExist(conversationModel.getSenderId()))
                            .putExtra(Constants.CONVERSATION_MODEL, conversationModel);
                    mContext.startActivity(backIntent);
                }else{
                    Intent backIntent = new Intent(mContext, MyServerChatActivity.class)
                            .putExtra("USER_ID",conversationModel.getSenderId())
                            .putExtra("USER_NAME", conversationModel.getName())
                            .putExtra("PICTURE", conversationModel.getDpUrl());

                    mContext.startActivity(backIntent);
                }



              /*  Intent intent = new Intent(mContext,ChatActivity.class)
                .putExtra("senderID",id);


                mContext. startActivity(intent);
*/
                profileDialog.dismiss();


            }
        });

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext,UserDisplayProfile.class)
                        .putExtra("USERPROFILE_ID",id);
                mContext.startActivity(intent);
                profileDialog.dismiss();

            }
        });
        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileDialog.dismiss();
                ImageDialogOpen(url,name);

            }
        });
    }







    public void addData(ConversationModel conversationModel){
        data.add(conversationModel);
        notifyItemInserted(data.size() - 1);
    }

    public void addAllData(ArrayList<ConversationModel> list){
        data.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ConversationListHolder extends RecyclerView.ViewHolder{

        CircleImageView imageDp;
        TextView textName;
        TextView textLastMsg;
        TextView textLastMsgTime;
        TextView textUnreadMsgs;

        View rootView;

        ConversationListHolder(View itemView) {
            super(itemView);
            rootView = itemView;

            imageDp =itemView.findViewById(R.id.image_dp);
            textName =itemView.findViewById(R.id.text_name);
            textLastMsg =itemView.findViewById(R.id.text_last_message);
            textLastMsgTime =itemView.findViewById(R.id.text_last_message_time);
            textUnreadMsgs =itemView.findViewById(R.id.text_unread_messages);




        }
    }


    private boolean getServerOrChatUser(String uId){
        boolean isUser=false;
        try{
            if (userFriendsResponseModel!=null){


                for (int i=0;i<userFriendsResponseModel.getData().size();i++){

                    if (userFriendsResponseModel.getData().get(i).getUid().equals(uId)){
                        isUser=true;
                    break;
                    }

                }

            }

        }catch (NullPointerException e){
            e.printStackTrace();
        }


        return  isUser;
    }



    private void DeleteServerChat(String threadid) {

            ShowIndicator();
            StringRequest mStrRequest = new StringRequest(Request.Method.DELETE, URLS.URL_DELETE_SERVER_CHAT+"/"+threadid,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i("VOLLEY", response);
                            try {
                                Gson gson = new GsonBuilder().create();
                                JsonParser jsonParser = new JsonParser();
                                JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                                StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                                if (stateResponse != null) {
                                    hideIndicator();
                                    if (Utils.isStatusSuccess(stateResponse.getStatus())) {

                                    }else {
                                        hideIndicator();
                                        showSnackbarMessage(stateResponse.getMessage());


                                    }
                                } else {
                                    hideIndicator();
                                    showServerSnackbar(R.string.error_responce);


                                }
                            } catch (Exception e) {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideIndicator();
                            if (error instanceof NoConnectionError) {

                                showServerSnackbar(R.string.error_responce);

                            } else {

                                showConnectionSnackbar();

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                    params.put("Accept-Language", "application/json");
                    params.put("X-CSRF-Token", SessionManager.getInstance(mContext).getaccesstoken().accesstoken);
                    params.put("Cookie", SessionManager.getInstance(mContext).getaccesstoken().sessionName+"="+SessionManager.getInstance(mContext).getaccesstoken().sessionID);

                    return params;
                }




            };

            //creating a request queue
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            requestQueue.add(mStrRequest);

    }




}
