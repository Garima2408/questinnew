package co.questin.chat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.MyServerChatListAdapter;
import co.questin.library.StateBean;
import co.questin.models.chat.SendServerMessage;
import co.questin.models.chat.ServerChatListArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

public class MyServerChatActivity extends BaseAppCompactActivity {
    private SendServerMessage sendMessageModel;
    String name,image,userid;
    ImageView attechments;
    private Gson gson;
    ImageView backone;
    private Context mContext;
    ImageView imageDp;
    TextView textName;
    ListView list_msg;
    ImageButton buttonSend;
    EditText editTextEnterMsg;
    private List<ServerChatListArray> ChatBubbles;
    private ArrayAdapter<ServerChatListArray> adapter;
    Handler delayhandler = new Handler();
    boolean myMessage = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_server_chat);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        attechments =findViewById(R.id.attechments);
        backone =findViewById(R.id.backone);
        imageDp =findViewById(R.id.image_dp);
        textName =findViewById(R.id.text_name);
        list_msg =findViewById(R.id.list_msg);
        buttonSend =findViewById(R.id.button_send);
        editTextEnterMsg =findViewById(R.id.edit_text_enter_message);

        gson = new Gson();
        mContext = this;
        Intent intent = getIntent();
        ChatBubbles = new ArrayList<>();
        sendMessageModel = new SendServerMessage();
        name = intent.getStringExtra("USER_NAME");
        image = intent.getStringExtra("PICTURE");
        userid= intent.getStringExtra("USER_ID");


        if(image != null && image.length() > 0 ) {

            Glide.with(this).load(image)
                    .placeholder(R.mipmap.single).dontAnimate()
                    .fitCenter().into(imageDp);

        }else {
            imageDp.setImageResource(R.mipmap.single);

        }

        textName.setText(name);

        getTheperviousDataChat();
      //  mUpdateTimeTask.run();

        backone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delayhandler.removeCallbacks(mUpdateTimeTask);
                finish();
            }
        });
        editTextEnterMsg.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    Log.e("TAG", "onFocusChange: focused");
                } else {
                    Log.e("TAG", "onFocusChange: not focused");
                }
            }
        });

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String text = editTextEnterMsg.getText().toString();
                if (!text.isEmpty()) {

                    editTextEnterMsg.setText("");

                    StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.SEND_MESSAGE_TO_SERVER,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.i("VOLLEY", response);
                                    try {
                                        Gson gson = new GsonBuilder().create();
                                        JsonParser jsonParser = new JsonParser();
                                        JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                                        StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                                        if (stateResponse != null) {

                                            if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                                getTheperviousDataChat();
                                            }else {
                                                showSnackbarMessage(stateResponse.getMessage());
                                            }
                                        } else {

                                            showServerSnackbar(R.string.error_responce);
                                        }
                                    } catch (Exception e) {

                                        showServerSnackbar(R.string.error_responce);
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    if (error instanceof NoConnectionError) {
                                        showConnectionSnackbar();
                                    } else {
                                        showServerSnackbar(R.string.error_responce);
                                    }
                                }
                            }) {



                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String>  params = new HashMap<String, String>();
                            params.put("Content-Type", "application/json");
                            params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                            params.put("Accept-Language", "application/json");
                            params.put("X-CSRF-Token", SessionManager.getInstance(MyServerChatActivity.this).getaccesstoken().accesstoken);
                            params.put("Cookie", SessionManager.getInstance(MyServerChatActivity.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(MyServerChatActivity.this).getaccesstoken().sessionID);

                            return params;
                        }

                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("recipients",userid);
                            params.put("subject",text);
                            params.put("body",text);

                            return params;
                        }


                    };

                    mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    //creating a request queue
                    RequestQueue requestQueue = Volley.newRequestQueue(MyServerChatActivity.this);
                    requestQueue.add(mStrRequest);




                }
            }
        });


    }





    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {   // Todo
            long NOTIFY_INTERVAL = 29;
            long   millis =System.currentTimeMillis();
            int seconds = (int) (millis / 1000);
            seconds = seconds % 30;
            Log.e("Msggg", "Time:"+seconds);

            // This line is necessary for the next call
            delayhandler.postDelayed(this, NOTIFY_INTERVAL);

            if (seconds==NOTIFY_INTERVAL) {
                getTheperviousDataChat();
            }

        }

    };



    private void getTheperviousDataChat(){
        ChatBubbles.clear();
       GetTheChatDataDisplay();





    }

    private void GetTheChatDataDisplay() {

        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.GET_CHAT_FROM_SERVER+"/"+userid,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");

                                    for (int i = 0; i < jsonArrayData.length(); i++) {
                                        ServerChatListArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), ServerChatListArray.class);
                                        ChatBubbles.add(CourseInfo);
                                        adapter = new MyServerChatListAdapter(MyServerChatActivity.this, R.layout.chat_text_left, ChatBubbles);
                                        list_msg.setAdapter(adapter);
                                        list_msg.setSelection(list_msg.getAdapter().getCount()-1);


                                    }

                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());

                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        } else {
                            hideIndicator();
                            showConnectionSnackbar();

                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                String Cookies= SessionManager.getInstance(MyServerChatActivity.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(MyServerChatActivity.this).getaccesstoken().sessionID;
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(MyServerChatActivity.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }



        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);



    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {


            case android.R.id.home:
                delayhandler.removeCallbacks(mUpdateTimeTask);
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }


    @Override
    public void onBackPressed() {
        delayhandler.removeCallbacks(mUpdateTimeTask);
        finish();
    }


}