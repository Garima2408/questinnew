package co.questin.chat;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.Event.ConversationFire;
import co.questin.Event.ServerMemberUpdate;
import co.questin.R;
import co.questin.database.QuestinSQLiteHelper;
import co.questin.library.StateBean;
import co.questin.models.chat.ConversationModel;
import co.questin.models.userFriendsResponse.Datum2;
import co.questin.models.userFriendsResponse.UserFriendsResponseModel;
import co.questin.models.userFriendsResponse.UserServerFriendsResponseModel;
import co.questin.network.URLS;
import co.questin.utils.BaseFragment;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

import static android.app.Activity.RESULT_OK;
import static co.questin.utils.BaseAppCompactActivity.hideIndicator;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;
import static co.questin.utils.Utils.isStatusSuccess;

public class ConversationsActivity extends BaseFragment {

    private Gson gson;
    private SharedPreferences dataFile;
    public static ArrayList<ConversationModel> mconventional;
    private ConversationListAdapter conversationListAdapter;
    private static final String TAG = "ConversationsActivity";
    private int RC_NEW_CHAT = 1;
    FloatingActionButton btnNewChat;
    RecyclerView recyclerConversations;
    QuestinSQLiteHelper questinSQLiteHelper ;
    UserFriendsResponseModel userFriendsResponseModel=null;
    String threadId;
    public static boolean serverMemberUpdate=false;
    public static boolean ChatExixtServer;
    public static boolean ChatExixtNormal;
    LinearLayout notChat;
    public static Activity activity;
    public ConversationsActivity() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_conversations, container, false);
        mconventional = new ArrayList<>();
        gson = new Gson();
        activity =getActivity();
        questinSQLiteHelper=new QuestinSQLiteHelper(getActivity());
        recyclerConversations = view.findViewById(R.id.list_conversations);
        btnNewChat = view.findViewById(R.id.btn_new_chat);
        notChat =view.findViewById(R.id.notChat);
        // inItView();
        getUserData();


        btnNewChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newChatIntent = new Intent(getActivity(), NewChatActivity.class);
                startActivityForResult(newChatIntent, RC_NEW_CHAT);


            }
        });




        return view;
    }
    private void getUserData(){

        try {
            DB snappyDB = null;

            snappyDB = DBFactory.open(getActivity());

            if (snappyDB.exists(Constants.CART_FRIENDS_LIST)) {
                userFriendsResponseModel  = snappyDB.getObject(Constants.CART_FRIENDS_LIST, UserFriendsResponseModel.class);
                // snappyDB.close();

            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }
    public void  inItView() {
        //  getFriendsList();
        questinSQLiteHelper=new QuestinSQLiteHelper(getActivity());

        //
        // if (!serverMemberUpdate)
        getActivity().startService(new Intent(getActivity(), FriendsListService.class));


        dataFile = Utils.getSharedPreference(getActivity());
        recyclerConversations.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerConversations.setHasFixedSize(true);
//        conversationListAdapter = new ConversationListAdapter(new ArrayList<ConversationModel>(),userFriendsResponseModel, getActivity());
//        conversationListAdapter.addAllData(questinSQLiteHelper.getAllCotacts());
//        recyclerConversations.setAdapter(conversationListAdapter);
//        conversationListAdapter.notifyDataSetChanged();

        Log.e(TAG, "onCreateId: " + SessionManager.getInstance(getActivity()).getUser().getUserprofile_id() + "size" + questinSQLiteHelper.getConversationsList().size());
        Log.e(TAG, "onCreateToken: " + dataFile.getString(Constants.FCM_REG_TOKEN, null));

        if (questinSQLiteHelper.getConversationsList().size()==0){
            ChatExixtNormal=true;
        }else {
            ChatExixtNormal=false;
        }

        if (ChatExixtNormal==true && ChatExixtServer==true){
            notChat.setVisibility(View.VISIBLE);
            recyclerConversations.setVisibility(View.GONE);

        }else if (ChatExixtNormal==false && ChatExixtServer==false){
            notChat.setVisibility(View.GONE);
            recyclerConversations.setVisibility(View.VISIBLE);
        }else if (ChatExixtNormal==true && ChatExixtServer==false){
            notChat.setVisibility(View.GONE);
            recyclerConversations.setVisibility(View.VISIBLE);
        }else if (ChatExixtNormal==false && ChatExixtServer==true){
            notChat.setVisibility(View.GONE);
            recyclerConversations.setVisibility(View.VISIBLE);
        }

    }




    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume","onResume");
        inItView();
        if (serverMemberUpdate=true){
            getUserData();

            getFriendsServerList();
        }

        //getFriendsServerList();
    }



    private void getFriendsServerList() {

        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.GET_LIST_FROM_SERVER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    final UserServerFriendsResponseModel userResponseModel =
                                            gson.fromJson(response, UserServerFriendsResponseModel.class);
                                    List<String> serverUser = new ArrayList<>();
                                    if (userFriendsResponseModel != null && userFriendsResponseModel.getData() != null
                                            && userFriendsResponseModel.getData().size() > 0) {

                                        for (int i = 0; i < userFriendsResponseModel.getData().size(); i++) {

                                            serverUser.add(userFriendsResponseModel.getData().get(i).getUid());

                                        }
                                    }

                                    if (userResponseModel != null && userResponseModel.getData() != null && userResponseModel.getData().size() > 0) {
                                        for (Datum2 data : userResponseModel.getData()) {

                                            if (!serverUser.contains(data.getUid())) {


                                                ConversationModel conversationModel = new ConversationModel()
                                                        .setSenderId(data.getUid())
                                                        .setName(data.getName())
                                                        .setLastMsgText(data.getMessage())
                                                        .setDpUrl(data.getPicture())
                                                        .setThreadid(data.getThreadID());


                                                if (!questinSQLiteHelper.doesConversationExist(conversationModel.getSenderId()))
                                                    questinSQLiteHelper.addConversation(conversationModel);
                                            }

                                            threadId = data.getThreadID();
                                            Log.e("dataMain", data.getUid() + "...." + data.getName() + "...." + data.getMessage() + "...." + data.getPicture() + "." + data.getThreadID());
                                            ChatExixtServer = false;


                                        }
                                    } else {
                                        /*No conversation From Server*/
                                        Log.d("TAG", "ServerConverSation: " + "No Conversation");
                                        ChatExixtServer = true;

                                    }

                                    try {
                                        conversationListAdapter = new ConversationListAdapter(new ArrayList<ConversationModel>(), userFriendsResponseModel, getActivity(), threadId);
                                        conversationListAdapter.addAllData(questinSQLiteHelper.getAllCotacts());
                                        recyclerConversations.setAdapter(conversationListAdapter);


                                    } catch (Exception e) {


                                    }

                                }else {

                                    showSnackbarMessage(stateResponse.getMessage());
                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);

                return params;
            }
        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);
    }





    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RC_NEW_CHAT) {
            if (resultCode == RESULT_OK) {

                startActivity(new Intent(getActivity(), ChatActivity.class)
                        .putExtra(Constants.EXTRA_IS_FIRST_MESSAGE, data.getBooleanExtra(Constants.EXTRA_IS_FIRST_MESSAGE, false))
                        .putExtra(Constants.CONVERSATION_MODEL, data.getParcelableExtra(Constants.CONVERSATION_MODEL)));

            }
        }



    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ServerMemberUpdate event) {

        /* Do something */
        getUserData();
        getFriendsServerList();



    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ConversationFire event) {


        /* Do something */
        conversationListAdapter = new ConversationListAdapter(new ArrayList<ConversationModel>(),userFriendsResponseModel, getActivity(),threadId);
        conversationListAdapter.addAllData(questinSQLiteHelper.getAllCotacts());
        recyclerConversations.setAdapter(conversationListAdapter);
        conversationListAdapter.notifyDataSetChanged();






    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

}
