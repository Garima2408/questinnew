package co.questin.chat;

import android.app.Dialog;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import co.questin.R;
import co.questin.database.QuestinContract;
import co.questin.models.chat.GroupChatModel;


public class GroupChatListAdapter extends RecyclerView.Adapter<GroupChatListAdapter.ChatViewHolder>{

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<GroupChatModel> data;
    private SimpleDateFormat simpleDateFormat;
    private ChatListCommunicator chatListCommunicator;
    private static final String TAG = "ChatListAdapter";
    Dialog imageDialog;
    Bitmap thumbnail = null;
    boolean isUpload=true;
    int loaderPos;
    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;
    int id = 1;
    String storeDir;
    URL url;
    String Extension;

    public GroupChatListAdapter(ArrayList<GroupChatModel> data, Context context, ChatListCommunicator chatListCommunicator){
        mContext = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
        this.chatListCommunicator = chatListCommunicator;
        simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
    }

    @Override
    public int getItemViewType(int position) {
        GroupChatModel currentData = data.get(position);
        switch(currentData.getCategory()){
            case QuestinContract.CATEGORY_TEXT :
                if(currentData.getIsMine() == QuestinContract.MINE_YES) return R.layout.chat_text_right;
                else return R.layout.chat_text_left;

            case QuestinContract.CATEGORY_IMAGE :
                if(currentData.getIsMine() == QuestinContract.MINE_YES) return R.layout.chat_image_right;
                else return R.layout.chat_image_left;

            case QuestinContract.CATEGORY_CONTACT :
                if(currentData.getIsMine() == QuestinContract.MINE_YES) return R.layout.chat_contact_right;
                else return R.layout.chat_contact_left;


            case QuestinContract.CATEGORY_DOCUMENT :
                if(currentData.getIsMine() == QuestinContract.MINE_YES) return R.layout.chat_pdf_right;
                else return R.layout.chat_pdf_left;

            case QuestinContract.CATEGORY_LOCATION :
                if(currentData.getIsMine() == QuestinContract.MINE_YES) return R.layout.chat_location_right;
                else return R.layout.chat_location_left;


            default: return 0;
        }
    }

    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChatViewHolder(inflater.inflate(viewType, parent, false), viewType);
    }

    @Override
    public void onBindViewHolder(final ChatViewHolder holder, final int position) {
        String time = simpleDateFormat.format(new Date(Long.valueOf(data.get(position).getTime())));

        switch (data.get(position).getCategory()){
            case QuestinContract.CATEGORY_TEXT :

                holder.textMsg.setText(data.get(position).getText());
                holder.textTime.setText(time);

                if (data.get(position).getIsMine()!=1){

                    String arr[]=  data.get(position).getSenderId().split("_");
                    if (arr[1]!=null){
                        holder.text_usr_name.setVisibility(View.VISIBLE);
                        holder.text_usr_name.setText(arr[1]);
                    }else{
                        holder.text_usr_name.setVisibility(View.GONE);
                    }

                }


                break;

            case QuestinContract.CATEGORY_IMAGE :
                holder.textMsg.setText(data.get(position).getText());
                holder.textTime.setText(time);


                if (data.get(position).getIsMine()!=1){

                    String arr[]=  data.get(position).getSenderId().split("_");
                    if (arr[1]!=null){
                        holder.text_usr_name.setVisibility(View.VISIBLE);
                        holder.text_usr_name.setText(arr[1]);
                    }else{
                        holder.text_usr_name.setVisibility(View.GONE);
                    }

                }

                if(data.get(position).getLink().contains("http")){

                    Glide.with(mContext).load(data.get(position).getLink())
                            .override(50, 50)
                            .centerCrop().into(holder.imageMsg);
                    Log.e(TAG, "onBindViewHolder: http");
                    holder.imageDownload.setVisibility(View.VISIBLE);
                }
                else {
                    Glide.with(mContext).loadFromMediaStore(Uri.parse(data.get(position).getLink()))
                            .centerCrop().into(holder.imageMsg);
                    Log.e(TAG, "onBindViewHolder: content"+data.get(position).getLink());
                    holder.imageDownload.setVisibility(View.GONE);
                }

                if(data.get(position).isDownloading()){
                    holder.progressBar.setVisibility(View.VISIBLE);
                }
                else {
                    holder.progressBar.setVisibility(View.GONE);
                }
                holder.imageDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        data.get(holder.getAdapterPosition()).setDownloading(true);
                        holder.progressBar.setVisibility(View.VISIBLE);
                        holder.imageDownload.setVisibility(View.GONE);
                        chatListCommunicator.startImageDownload(data.get(holder.getAdapterPosition()));
                    }
                });

                if (loaderPos==position){
                    if (!isUpload)
                        holder.progressBar.setVisibility(View.VISIBLE);
                    else
                        holder.progressBar.setVisibility(View.GONE);

                }else{
                    holder.progressBar.setVisibility(View.GONE);

                }

                holder.img_open.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (!data.get(position).getLink().contains("http")) {
                            ImageDialogOpen(Uri.parse(data.get(position).getLink()));
                        } else if (data.get(position).getLink().contains("content")) {
                            ImageDialogOpen(Uri.parse(data.get(position).getLink()));
                        }

                    }
                });


                break;
            case QuestinContract.CATEGORY_CONTACT :

                holder.text_name.setText(data.get(position).getText());
                holder.text_phone.setText(time);


                if (data.get(position).getIsMine()!=1){

                    String arr[]=  data.get(position).getSenderId().split("_");
                    if (arr[1]!=null){

                        holder.text_usr_name.setVisibility(View.VISIBLE);
                        holder.text_usr_name.setText(arr[1]);
                    }else{
                        holder.text_usr_name.setVisibility(View.GONE);
                    }

                }


                break;

            case QuestinContract.CATEGORY_DOCUMENT :

                holder.text_Filename.setText(data.get(position).getText());
                holder.text_fileSize.setText(time);

                if (data.get(position).getIsMine()!=1){

                    String arr[]=  data.get(position).getSenderId().split("_");
                    if (arr[1]!=null){

                        holder.text_usr_name.setVisibility(View.VISIBLE);
                        holder.text_usr_name.setText(arr[1]);

                    }else{

                        holder.text_usr_name.setVisibility(View.GONE);
                    }

                }

                if(data.get(position).isDownloading()){
                    holder.progress_bar_pdf.setVisibility(View.VISIBLE);
                }
                else {
                    holder.progress_bar_pdf.setVisibility(View.GONE);
                }


                if (data.get(position).getLink().contains("http")) {

                    Log.e(TAG, "onBindViewHolder: http");
                    holder.img_pdf_download.setVisibility(View.GONE);


                } else {

                    Log.e(TAG, "onBindViewHolder: content");
                    holder.img_pdf_download.setVisibility(View.GONE);

                }
                if (loaderPos==position){
                    if (!isUpload){

                        Log.e(TAG, "progressvis: content"+loaderPos+isUpload);
                        holder.progress_bar_pdf.setVisibility(View.VISIBLE);

                    }
                    else{
                        Log.e(TAG, "progressgone: content"+loaderPos+isUpload);

                        holder.progress_bar_pdf.setVisibility(View.GONE);


                    }

                }else{
                    Log.e(TAG, "progressgone: content"+loaderPos+isUpload);

                    holder.progress_bar_pdf.setVisibility(View.GONE);

                }


                holder.img_pdf_download.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        data.get(holder.getAdapterPosition()).setDownloading(true);
                        holder.progress_bar_pdf.setVisibility(View.VISIBLE);
                        holder.img_pdf_download.setVisibility(View.GONE);
                        downloadFile(data.get(position).getLink());
                        chatListCommunicator.startDocumnetDownload(data.get(holder.getAdapterPosition()));


                    }
                });

                holder.rr_pdf_opn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        GroupChatModel currentData = data.get(position);
                        if(currentData.getIsMine() == QuestinContract.MINE_YES){

                            File file = new File(Environment.getExternalStorageDirectory(), data.get(position).getLink());
                            Uri  uri = FileProvider.getUriForFile(mContext, "co.questin.fileprovider", file);
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.setDataAndType(uri, "*/*");
                            mContext.startActivity(intent);

                        }else {
                            try {


                                String file = data.get(position).getLink();
                                Log.d("TAG","kya bat hyyjhy "+file);

                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data.get(holder.getAdapterPosition()).getLink()));
                                browserIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                mContext. startActivity(browserIntent);

                            }catch (Exception e)
                            {
                                e.printStackTrace();
                            }


                        }

                    }


                });


                break;


            case QuestinContract.CATEGORY_LOCATION :

                holder.latlong.setText(data.get(position).getText());
                holder.textTime.setText(time);

                if (data.get(position).getIsMine()!=1){

                    String arr[]=  data.get(position).getSenderId().split("_");
                    if (arr[1]!=null){
                        holder.text_usr_name.setVisibility(View.VISIBLE);
                        holder.text_usr_name.setText(arr[1]);
                    }else{
                        holder.text_usr_name.setVisibility(View.GONE);
                    }

                }

                final String array1[]= data.get(position).getText().split("_");
                holder.linearLayoutLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.e("testmap","done");
                        Uri uri=null;
                        if (array1[2]!=null)
                            uri= Uri.parse("http://maps.google.com/maps?q="+ array1[0]  +"," + array1[1] +"("+ array1[2]+ ")&iwloc=A&hl=es");
                        else
                            uri= Uri.parse("http://maps.google.com/maps?q="+ array1[0]  +"," + array1[1] +"(" + ")&iwloc=A&hl=es");

                        Intent intent= new Intent(Intent.ACTION_VIEW, uri);
                        intent.setPackage("com.google.android.apps.maps");
                        mContext.startActivity(intent);
                    }
                });
                Log.e("lat",data.get(position).getText());
                break;


            default :
                break;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addData(GroupChatModel chatModel){
        data.add(chatModel);
        notifyItemInserted(data.size() - 1);
    }

    public void addAllData(ArrayList<GroupChatModel> list){
        data.addAll(list);
        notifyDataSetChanged();
    }

    void onImageDownloaded(GroupChatModel chatModel){
        for(int i=0; i<data.size(); i++){
            GroupChatModel model = data.get(i);
            if(model.getTime().equals(chatModel.getTime())){
                model.setLink(chatModel.getLink())
                        .setDownloading(false);
                notifyItemChanged(i);
                break;
            }
        }
    }

    class ChatViewHolder extends RecyclerView.ViewHolder{

        int viewType;
        TextView textMsg,text_name,text_phone,text_Filename,text_fileSize,latlong,text_usr_name;
        TextView textTime;
        RelativeLayout downdlod,linearLayoutLocation,img_open,rr_pdf_opn;
        View progress_bar_pdf;

        ImageView imageMsg,map;
        ImageButton imageDownload,img_pdf_download;
        ProgressBar progressBar;
        ChatViewHolder(View itemView, int viewType) {
            super(itemView);
            this.viewType = viewType;
            switch (viewType){
                case R.layout.chat_text_left :
                case R.layout.chat_text_right :
                    textMsg = itemView.findViewById(R.id.text_message);
                    textTime = itemView.findViewById(R.id.text_time);
                    text_usr_name=itemView.findViewById(R.id.text_usr_name);
                    break;

                case R.layout.chat_image_left :
                case R.layout.chat_image_right :
                    img_open=itemView.findViewById(R.id.img_open);
                    textMsg = itemView.findViewById(R.id.text_message);
                    textTime = itemView.findViewById(R.id.text_time);
                    imageMsg = itemView.findViewById(R.id.image_msg);
                    imageDownload = itemView.findViewById(R.id.image_download);
                    progressBar = itemView.findViewById(R.id.progress_bar);
                    text_usr_name=itemView.findViewById(R.id.text_usr_name);

                    break;

                case R.layout.chat_contact_left :
                case R.layout.chat_contact_right :
                    text_name = itemView.findViewById(R.id.text_name);
                    text_phone = itemView.findViewById(R.id.text_phone);
                    text_usr_name=itemView.findViewById(R.id.text_usr_name);


                    break;
                case R.layout.chat_pdf_left :
                case R.layout.chat_pdf_right :

                    text_Filename = itemView.findViewById(R.id.text_Filename);
                    text_fileSize = itemView.findViewById(R.id.text_fileSize);



                    rr_pdf_opn=itemView.findViewById(R.id.rr_pdf_opn);
                    text_Filename = itemView.findViewById(R.id.text_Filename);
                    text_fileSize = itemView.findViewById(R.id.text_fileSize);
                    downdlod = itemView.findViewById(R.id.downdlod);
                    progress_bar_pdf=itemView.findViewById(R.id.progress_bar_pdf);
                    img_pdf_download=itemView.findViewById(R.id.img_pdf_download);
                    text_usr_name=itemView.findViewById(R.id.text_usr_name);

                    break;

                case R.layout.chat_location_left :
                case R.layout.chat_location_right :

                    latlong = itemView.findViewById(R.id.latlong);
                    map = itemView.findViewById(R.id.map);
                    textTime = itemView.findViewById(R.id.text_time);
                    linearLayoutLocation=itemView.findViewById(R.id.linearLayoutLocation);
                    text_usr_name=itemView.findViewById(R.id.text_usr_name);

                    break;


            }
        }
    }


    interface ChatListCommunicator{
        void startImageDownload(GroupChatModel chatModel);
        void startDocumnetDownload(GroupChatModel chatModel);
    }



    private void ImageDialogOpen(final Uri parse) {

        imageDialog = new Dialog(mContext);
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        imageDialog.setContentView(R.layout.imagepreview_chat);
        imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        imageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        imageDialog.show();
        ImageView showimage = imageDialog.findViewById(R.id.showimage);
        ImageView downloadImage = imageDialog.findViewById(R.id.downloadImage);
        ImageView backone = imageDialog.findViewById(R.id.backone);

        ImageMatrixTouchHandler imageMatrixTouchHandler = new ImageMatrixTouchHandler(mContext);

        showimage.setOnTouchListener(imageMatrixTouchHandler);


        Glide.clear(showimage);
        Glide.with(mContext).loadFromMediaStore(Uri.parse(String.valueOf(parse)))
                .placeholder(R.mipmap.header_image).dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .fitCenter().into(showimage);

        downloadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), parse);
                    saveImageToExternalStorage(bitmap);
                    Toast.makeText(mContext, "Download Image Successfully ", Toast.LENGTH_SHORT).show();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        backone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageDialog.dismiss();
            }
        });

    }


    public void downloadFile(String file) {
        String DownloadUrl = file;
        String filename = DownloadUrl.substring(62);
        Extension = file.substring(file.lastIndexOf("."));
        Log.d("TAG", "Extension: " + Extension);


        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(DownloadUrl));
        request.setDescription(filename);   //appears the same in Notification bar while downloading
        request.setTitle(filename);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalFilesDir(mContext, null, filename + Extension);

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }
    private void OpenSaveDialogBox(final Bitmap s) {
        final CharSequence[] items = { "Save Image",  "Cancel" };
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Save Image")) {

                    saveImageToExternalStorage(s);
                } else if (items[item].equals("Cancel")) {

                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

   /* private void saveImageToExternalStorage(String s) {
    }*/

    private void saveImageToExternalStorage(Bitmap s) {
        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Questin Images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            s.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }


        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(mContext, new String[]{file.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                    }
                });

    }


}