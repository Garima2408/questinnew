package co.questin.library;

/**
 * Created by yash on 6/13/2017.
 */

public class StateBean {
    String status;
    String message;
    String code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }
}
