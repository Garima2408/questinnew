package co.questin.visitor;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.questin.R;
import co.questin.activities.ChangeCollage;
import co.questin.activities.ForceUpdateChecker;
import co.questin.activities.ProfileEdit;
import co.questin.questinsitecontent.QuestinSite;
import co.questin.activities.StartupGuide;
import co.questin.activities.SwitchYourType;
import co.questin.adapters.NavigationItemsAdapter;
import co.questin.models.NavItems;
import co.questin.settings.AboutUs;
import co.questin.settings.EmailUs;
import co.questin.settings.FAQ;
import co.questin.settings.PrivacyPolicy;
import co.questin.settings.Settings;
import co.questin.settings.TermsAndConditions;
import co.questin.teacher.TeacherMain;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.TextUtils;
import co.questin.utils.Utils;

public class VisitorMain extends BaseAppCompactActivity implements  NavigationView.OnNavigationItemSelectedListener,ForceUpdateChecker.OnUpdateNeededListener {
    Toolbar toolbar;
    TextView mToolbarTitle,toolbar_title;
    ExpandableListView expandableListView;
    DrawerLayout mDrawerlayout;
    private NavigationItemsAdapter mNavigationItemsAdapter;
    private List<NavItems> listNavigationItem;
    private HashMap<String, List<NavItems>> listDataChild;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private Fragment mFragment = null;
    private Class mFragmentClass = null;
    TextView tv_username, tv_useremail,tv_userbatch;
    ImageView profileImage;
    ImageButton imgbtn_edit_profile,imgbtn_account;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_main);
        Utils.setBadge(this, 0);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar_title = findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);

        if(SessionManager.getInstance(getActivity()).getCollage().getTitle() !=null){

            toolbar_title.setText(SessionManager.getInstance(getApplicationContext()).getCollage().getTitle());
            toolbar_title.setSelected(true);
            getSupportActionBar().setTitle(null);



        }
        ForceUpdateChecker.with(this).onUpdateNeeded( this).check();

        pushFragment(new VistorDashBoard());

        mDrawerlayout = findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle= new ActionBarDrawerToggle(
                this, mDrawerlayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        mDrawerlayout.setDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();

        expandableListView = findViewById(R.id.nav_expand_listview);
        prepareNavigationList ();


        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.navigation_header, null, false);
        expandableListView.addHeaderView(listHeaderView);
        tv_username = findViewById(R.id.tv_username);
        tv_useremail = findViewById(R.id.tv_useremail);
        profileImage = findViewById(R.id.imageProfile);
        tv_userbatch = findViewById(R.id.tv_userbatch);
        tv_userbatch.setVisibility(View.INVISIBLE);
        imgbtn_edit_profile = findViewById(R.id.imgbtn_edit_profile);
        imgbtn_account=findViewById(R.id.imgbtn_account);
        imgbtn_account.setVisibility(View.VISIBLE);

        displayUserData();
        Utils.init(VisitorMain.this);

        if (Utils.getFCMId()==null){
            UpdateFCMTokenToServer();
        }


        imgbtn_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent n = new Intent(VisitorMain.this,SwitchYourType.class);
                startActivity(n);
            }
        });






        imgbtn_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent n = new Intent(VisitorMain.this,ProfileEdit.class);
                startActivity(n);
            }
        });





        mNavigationItemsAdapter = new NavigationItemsAdapter (this,
                listNavigationItem,listDataChild);

        expandableListView.setAdapter (mNavigationItemsAdapter);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

                int len = mNavigationItemsAdapter.getGroupCount();
                for (int i = 0; i < len; i++) {
                    if (i != groupPosition) {
                        expandableListView.collapseGroup(i);
                    }
                }


            }
        });
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener () {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View clickedView, int groupPosition, long rowId) {
                selectFragments(groupPosition,10);
                return false;
            }
        });

        expandableListView.setOnChildClickListener (new ExpandableListView.OnChildClickListener () {
            @Override public boolean onChildClick (ExpandableListView parent, View v, int groupPosition,
                                                   int childPosition, long id) {
                selectFragments(groupPosition,childPosition);
                v.setSelected (true);
                return false;
            }
        });




    }

    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version to continue reposting.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create();
        dialog.show();
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }



    private void displayUserData() {
        if (getUser()!=null){
            if (!TextUtils.isNullOrEmpty(getUser().username)) {
                tv_username.setText(getUser().username);
            }
            if (!TextUtils.isNullOrEmpty(getUser().email)) {
                tv_useremail.setText(getUser().email);
            }
            if (!TextUtils.isNullOrEmpty(getUser().photo)) {
                Glide.with(this).load(getUser().photo)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .fitCenter().into(profileImage);


            }
        }
    }
    protected void pushFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.parent_content, fragment);
            ft.commit();
        }
    }

    private void prepareNavigationList () {

        listNavigationItem = new ArrayList<NavItems>();
        listDataChild = new HashMap<String, List<NavItems>> ();

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.community),
                getResources ().getString (R.string.questin)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.colleges),
                getResources ().getString (R.string.colleges)));


        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.need_help),
                getResources ().getString (R.string.need_help)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.read_more),
                getResources ().getString (R.string.read_more)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.setting),
                getResources ().getString (R.string.setting)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.log_out),
                getResources ().getString (R.string.log_out)));

        List<NavItems> needHelp = new ArrayList<> ();
        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.email_us)));
        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.faq)));

        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.faq1)));

        List<NavItems> readMore = new ArrayList<> ();
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.read_more),
                getResources ().getString (R.string.about_us)));
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.term_condition)));
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.privacy_policy)));

        List<NavItems> empty = new ArrayList<NavItems> ();

        listDataChild.put (listNavigationItem.get (0).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (1).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (2).getNavItemName (), needHelp);
        listDataChild.put (listNavigationItem.get (3).getNavItemName (), readMore);
        listDataChild.put (listNavigationItem.get (4).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (5).getNavItemName (), empty);
    }

    private void selectFragments(int groupPosition,int childPosition){


        switch (groupPosition){
            case 0:
                Intent i = new Intent(VisitorMain.this, QuestinSite.class);
                startActivity(i);
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);
                break;
            case 1:
                // Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();
                Intent j = new Intent(VisitorMain.this, QuestinSite.class);
                startActivity(j);
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;

            case 2:

                switch (childPosition){
                    case 0:


                        Intent backIntent = new Intent(VisitorMain.this, EmailUs.class)
                                .putExtra("TITLE"," Email Us");
                        startActivity(backIntent);

                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                    case 1:

                        startActivity (new Intent (VisitorMain.this, FAQ.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;

                    case 2:

                        startActivity (new Intent (this, StartupGuide.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                }
                break;

            case 3:
                switch (childPosition){
                    case 0:

                        startActivity (new Intent (VisitorMain.this, AboutUs.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                    case 1:

                        Intent o = new Intent(VisitorMain.this, TermsAndConditions.class);
                        startActivity(o);
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;

                    case 2:

                        startActivity (new Intent (VisitorMain.this, PrivacyPolicy.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                }
                break;

            case 4:

                startActivity (new Intent (VisitorMain.this, Settings.class));
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;

            case 5:
                showNoticeDialog ();
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;

        }


    }

    private void showNoticeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(VisitorMain.this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Logout ?");
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application

                DeleteFireBaseToken();

            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();
    }



    @Override
    protected void onResume() {
        displayUserData();
        super.onResume();

    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to close this application ?");
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                Utils.getSharedPreference(VisitorMain.this).edit()
                        .putInt(Constants.RUNNIN_FIRST, Constants.ROLE_RUNNING_FALSE).apply();
                Utils.removeStringPreferences(VisitorMain.this,"0");
                finishAffinity();

            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }




}
