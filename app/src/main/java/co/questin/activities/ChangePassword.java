package co.questin.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.library.StateBean;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

public class ChangePassword extends BaseAppCompactActivity implements View.OnClickListener{

    TextView updatepassword_save;
    String newpassword,oldpassword,reentered;
    TextView  password,con_password,re_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        password = findViewById(R.id.password);
        con_password = findViewById(R.id.con_password);
        re_password = findViewById(R.id.re_password);
        updatepassword_save = findViewById(R.id.updatepassword_save);
        updatepassword_save.setOnClickListener(this);
        updatepassword_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isInternetConnected()){
                    showConnectionSnackbar();
                }else {
                    SaveUserPassword();
                    }
                  }

            });
        }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.updatepassword_save:

                break;


        }
    }



    private void SaveUserPassword() {
        // Reset errors.
        password.setError(null);
        con_password.setError(null);
        re_password.setError(null);

        oldpassword  = password.getText().toString().trim();
        newpassword = con_password.getText().toString().trim();
        reentered  = re_password.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(oldpassword)) {
            focusView = con_password;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

        } else if (TextUtils.isEmpty(newpassword)) {
            focusView = password;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));


        }  else if (TextUtils.isEmpty(reentered)) {
            focusView = re_password;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }


        if (cancel) {

            focusView.requestFocus();
        } else {

            if (reentered.matches(newpassword)) {
                ShowIndicator();
                ProgressUpdateUserPassword();

            }else {
                showSnackbarMessage(getString(R.string.password_dont));

            }
            }
    }

    /*update user password*/

    private void ProgressUpdateUserPassword() {

        StringRequest mStrRequest = new StringRequest(Request.Method.PUT, URLS.URL_UPDATEPASSWORD + "/" + SessionManager.getInstance(getActivity()).getUser().getUserprofile_id(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    showSnackbarFinishMessage(stateResponse.getMessage());

                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());

                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        } else {
                            hideIndicator();
                            showConnectionSnackbar();

                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String Cookies = SessionManager.getInstance(ChangePassword.this).getaccesstoken().sessionName + "=" + SessionManager.getInstance(ChangePassword.this).getaccesstoken().sessionID;
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(ChangePassword.this).getaccesstoken().accesstoken);
                params.put("Cookie", Cookies);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("pass", newpassword);
                params.put("current_pass", oldpassword);

                return params;
            }


        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);

    }


    @Override
    public void onBackPressed() {
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
