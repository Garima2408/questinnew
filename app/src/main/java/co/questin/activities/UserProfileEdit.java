package co.questin.activities;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.soundcloud.android.crop.Crop;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.library.StateBean;
import co.questin.models.UserDetail;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.RequestBody;

public class UserProfileEdit extends BaseAppCompactActivity implements View.OnClickListener{
    EditText firstname,lastname;
    TextView updatename_Save,hrading_2;
    ImageView profileImage,mainheader;
    String updatefirstname,updatelastname,strFile;
    Bitmap thumbnail = null;
    RequestBody body;
    String Fileimagename,mImageFileLocation;
    String UserValue;
    public static final int RequestPermissionCode = 1;
    Boolean CallingCamera,CallingGallary,CallingAttachment;
    Uri imageUri;
    private static final int ACTIVITY_START_CAMERA_APP = 0;
    private int SELECT_FILE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile_edit);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        firstname = findViewById(R.id.firstname);
        lastname = findViewById(R.id.lastname);
        profileImage = findViewById(R.id.imageProfile);
        mainheader = findViewById(R.id.mainheader);
        updatename_Save = findViewById(R.id.updatename_Save);
        hrading_2 = findViewById(R.id.hrading_2);
        displayUserData();

        mainheader.setOnClickListener(this);
        profileImage.setOnClickListener(this);
        updatename_Save.setOnClickListener(this);
        hrading_2.setOnClickListener(this);

    }

    private void displayUserData() {
        if (getUser() != null) {
            if (!co.questin.utils.TextUtils.isNullOrEmpty(getUser().FirstName)) {
                firstname.setText(getUser().FirstName);
            }
            if (!co.questin.utils.TextUtils.isNullOrEmpty(getUser().LastName)) {
                lastname.setText(getUser().LastName);
            }
            if (!co.questin.utils.TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUser().photo)) {
                Glide.with(this).load(getUser().photo)
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(profileImage);

            }
            if (!co.questin.utils.TextUtils.isNullOrEmpty(getUser().Userwallpic)) {
                Glide.with(this).load(getUser().Userwallpic)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(mainheader);




            }
        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.updatename_Save:
                SaveUserName();
                break;

            case R.id.hrading_2:
                startActivity (new Intent (this, ChangePassword.class));
                finish();
                break;

            case R.id.mainheader:
                UserValue ="Headervalue";
                showImageDialog();
                break;

            case R.id.imageProfile:
                UserValue ="Imageprofile";
                showImageDialog();
                break;


        }
    }

    private void SaveUserName() {


        // Reset errors.
        firstname.setError(null);
        lastname.setError(null);

        // Store values at the time of the login attempt.
        updatefirstname = firstname.getText().toString().trim();
        updatelastname = lastname.getText().toString().trim();


        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(updatefirstname)) {
            focusView = firstname;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        } else if (TextUtils.isEmpty(updatelastname)) {
            focusView = lastname;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }  else {
            if (!isInternetConnected()){
                showConnectionSnackbar();
            }else {
                ShowIndicator();
                ProgressUpdateUserName();


            }
        }
    }

    /*update user first and last name*/

    private void ProgressUpdateUserName() {
        StringRequest mStrRequest = new StringRequest(Request.Method.PUT,  URLS.URL_UPDATENAME+"/"+SessionManager.getInstance(getActivity()).getUser().getUserprofile_id(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    showSnackbarSignIn("Login Again With Your New Credential");

                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);

                        } else {
                            hideIndicator();
                            showConnectionSnackbar();

                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                String Cookies= SessionManager.getInstance(UserProfileEdit.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(UserProfileEdit.this).getaccesstoken().sessionID;
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(UserProfileEdit.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("field_first_name[und][0][value]",updatefirstname);
                params.put("field_last_name[und][0][value]",updatelastname);

                return params;
            }

        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);
    }










    private void showImageDialog() {
        final CharSequence[] items = { "Take Photo", "Choose from Gallery", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(UserProfileEdit.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    mainheader.setImageDrawable(null);
                    CallingCamera =true;
                    CallingGallary =false;

                    if(checkPermission()){
                        CallCamera();
                    }

                } else if (items[item].equals("Choose from Gallery")) {
                    mainheader.setImageDrawable(null);
                    CallingGallary =true;
                    CallingCamera =false;
                    if(checkPermission()){
                        galleryIntent();
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void CallCamera() {
        Intent callCameraApplicationIntent = new Intent();
        callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        File photoFile = null;
        try {
            photoFile = createImageFile();

        } catch (IOException e) {
            e.printStackTrace();
        }
        imageUri = FileProvider.getUriForFile(UserProfileEdit.this, "co.questin.fileprovider",photoFile);

        callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(callCameraApplicationIntent, ACTIVITY_START_CAMERA_APP);
    }


    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }


    private void galleryIntent()
    {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(Intent.createChooser(galleryIntent, "Select File"),SELECT_FILE);
    }





    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if(requestCode == ACTIVITY_START_CAMERA_APP && resultCode == RESULT_OK) {

            beginCrop(imageUri);

        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)


                onSelectFromGalleryResult(data);
        }
        if (requestCode == Crop.REQUEST_CROP) {

            handleCrop(resultCode, data);

        }
        if (resultCode == Activity.RESULT_CANCELED) {
            if (!co.questin.utils.TextUtils.isNullOrEmpty(getUser().Userwallpic)) {
                Glide.with(this).load(getUser().Userwallpic)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(mainheader);


            }

        }

    }


    /*New Galley Code*/



    //Choose From Gallery
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                beginCrop(data.getData());
                Uri uri = data.getData();
                String[] projection = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                Log.d("TAG", DatabaseUtils.dumpCursorToString(cursor));

                int columnIndex = cursor.getColumnIndex(projection[0]);
                Fileimagename = cursor.getString(columnIndex); // full path of image

                cursor.close();
            }catch (Exception e)
            {
                e.printStackTrace();
            }


        }
        }


    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(result));
                thumbnail = getResizedBitmap(thumbnail,1000);
                strFile = encodeImageTobase64(thumbnail);
                System.out.println("camera " + strFile);
                if (strFile != null) {
                    if (UserValue.contains("Imageprofile")) {

                        profileImage.setImageBitmap(thumbnail);
                        ShowIndicator();
                        ProgressUpdateUserProfile(strFile);

                    } else if (UserValue.contains("Headervalue")) {
                        mainheader.setImageBitmap(thumbnail);
                        ShowIndicator();
                        ProgressUpdateUserWallPic(strFile);

                    }
                } else {
                    showSnackbarMessage("There seems to be some problem.please select again");
                }





            } catch (IOException e) {
                e.printStackTrace();
            }




        } else if (resultCode == Crop.RESULT_ERROR) {
            //  Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }




    File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        Fileimagename = image.getName();
        return image;

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void ProgressUpdateUserWallPic(final String strFile) {

        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_MYWALLLPIC+"/"+SessionManager.getInstance(getActivity()).getUser().getUserprofile_id(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    String urlsofphoto = obj.getString("data");
                                    UserDetail userDetail = new UserDetail();
                                    userDetail.userprofile_id = SessionManager.getInstance(getActivity()).getUser().userprofile_id;
                                    userDetail.username = SessionManager.getInstance(getActivity()).getUser().getFirstName() + " " + SessionManager.getInstance(getActivity()).getUser().getLastName();
                                    userDetail.email = SessionManager.getInstance(getActivity()).getUser().email;
                                    userDetail.FirstName = SessionManager.getInstance(getActivity()).getUser().getFirstName();
                                    userDetail.LastName = SessionManager.getInstance(getActivity()).getUser().getLastName();
                                    userDetail.photo = SessionManager.getInstance(getActivity()).getUser().getPhoto();
                                    userDetail.Userwallpic = urlsofphoto;
                                    SessionManager.getInstance(getActivity()).saveUser(userDetail);
                                    UserValue="";


                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(getActivity()).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(getActivity()).getaccesstoken().sessionName+"="+SessionManager.getInstance(getActivity()).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("file",strFile);
                params.put("filename",Fileimagename);
                return params;
            }


        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);
    }




    private void ProgressUpdateUserProfile(final String strFile) {

        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_AVATAR+"/"+ SessionManager.getInstance(getActivity()).getUser().getUserprofile_id(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    String urlsofphoto = obj.getString("data");

                                    UserDetail userDetail = new UserDetail();
                                    userDetail.userprofile_id = SessionManager.getInstance(getActivity()).getUser().userprofile_id;
                                    userDetail.username = SessionManager.getInstance(getActivity()).getUser().getFirstName() + " " + SessionManager.getInstance(getActivity()).getUser().getLastName();
                                    userDetail.email = SessionManager.getInstance(getActivity()).getUser().email;
                                    userDetail.FirstName = SessionManager.getInstance(getActivity()).getUser().getFirstName();
                                    userDetail.LastName = SessionManager.getInstance(getActivity()).getUser().getLastName();
                                    userDetail.photo = urlsofphoto;
                                    userDetail.Userwallpic = SessionManager.getInstance(getActivity()).getUser().getUserwallpic();
                                    SessionManager.getInstance(getActivity()).saveUser(userDetail);
                                    UserValue="";


                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(getActivity()).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(getActivity()).getaccesstoken().sessionName+"="+SessionManager.getInstance(getActivity()).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("file",strFile);
                params.put("filename",Fileimagename);
                return params;
            }


        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);
    }



    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private  boolean checkPermission() {
        int camerapermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionLocation = ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), RequestPermissionCode);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("TAG", "Permission callback called-------");
        switch (requestCode) {
            case RequestPermissionCode: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("TAG", "sms & location services permission granted");
                        if (CallingCamera ==true){
                            CallingGallary=false;
                            CallCamera();

                        }else if (CallingGallary ==true) {
                            CallingCamera=false;

                            galleryIntent();


                        }
                    } else {
                        Log.d("TAG", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Service Permissions are required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkPermission();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    finish();
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
    private void explain(String msg){
        final android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        //  permissionsclass.requestPermission(type,code);
                        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:com.exampledemo.parsaniahardik.marshmallowpermission")));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        finish();
                    }
                });
        dialog.show();
    }
}
