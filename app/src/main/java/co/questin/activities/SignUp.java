package co.questin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.library.StateBean;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.TextUtils;
import co.questin.utils.Utils;

public class SignUp extends BaseAppCompactActivity implements  View.OnClickListener {
    EditText edt_first_name,edt_last_name,edt_email_address;
    Button btn_signup,btn_send;
    CheckBox chk_term_condition;
    String  email,password,cpassword,firstName,LastName;
    boolean flag = true;
    EditText edt_password,edt_confirm_password;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        edt_first_name = findViewById(R.id.edt_first_name);
        edt_last_name = findViewById(R.id.edt_last_name);
        edt_email_address = findViewById(R.id.edt_email_address);
        edt_password = findViewById(R.id.edt_password);
        edt_confirm_password = findViewById(R.id.edt_confirm_password);
        btn_signup = findViewById(R.id.btn_signup);
        btn_send =findViewById(R.id.btn_send);

        btn_signup.setOnClickListener(this);
        btn_send.setOnClickListener(this);


    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.btn_signup:
                hideKeyBoard(v);

                attemptToRegister();

                break;

            case R.id.btn_send:
                hideKeyBoard(v);


                break;

        }
    }








    private void attemptToRegister() {

        // Reset errors.
        edt_first_name.setError(null);
        edt_last_name.setError(null);
        edt_email_address.setError(null);
        edt_password.setError(null);
        edt_confirm_password.setError(null);

        // Store values at the time of the login attempt.
        email = edt_email_address.getText().toString().trim();
        password = edt_password.getText().toString().trim();
        cpassword = edt_confirm_password.getText().toString().trim();
        firstName = edt_first_name.getText().toString().trim();
        LastName = edt_last_name.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isNullOrEmpty(email)) {
            focusView = edt_email_address;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        } else if (!Utils.isValidEmailAddress(email)) {
            focusView = edt_email_address;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_invalid_email));
        } else if (TextUtils.isNullOrEmpty(password)) {
            // Check for a valid password, if the user entered one.
            focusView = edt_password;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        } else if (TextUtils.isNullOrEmpty(cpassword)) {
            // Check for a valid cpassword, if the user entered one.
            focusView = edt_password;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        } else if (!password.equalsIgnoreCase(cpassword)) {
            // Check for a valid cpassword, if the user entered one.
            focusView = edt_confirm_password;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_password_match));
        } else if (TextUtils.isNullOrEmpty(firstName)) {
            // check for First Name
            focusView = edt_first_name;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        } else if (TextUtils.isNullOrEmpty(LastName)) {
            // check for Contact No
            focusView = edt_last_name;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        if (cancel) {
            // There was an error; don't attempt Registration and focus the first
            // form field with an error.
            if (focusView != null) {
                focusView.requestFocus();
            }
        } else {
            if (!isInternetConnected()){
                showConnectionSnackbar();
            }else {
                ShowIndicator();
                callRequestForAPI();


            }
        }
    }

    private void callRequestForAPI() {


        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_SINGUP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {

                                    Intent upanel = new Intent(SignUp.this, SignIn.class);
                                    upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(upanel);
                                    finish();


                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name",email);
                params.put("pass", password);
                params.put("mail",email);
                params.put("field_first_name[und][0][value]", firstName);
                params.put("field_last_name[und][0][value]",LastName);
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);
    }

}





