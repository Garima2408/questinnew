package co.questin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AlignmentSpan;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import co.questin.R;
import co.questin.alumni.AluminiRegistration;
import co.questin.models.CollageLists;
import co.questin.models.CollageRoleArray;
import co.questin.models.MultipleLocn;
import co.questin.questinsitecontent.QuestinSite;
import co.questin.student.StudentRegistration;
import co.questin.teacher.TeacherRegistration;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import co.questin.visitor.VisitorMain;

public class SelectYourType extends BaseAppCompactActivity implements View.OnClickListener {
    TextView tv_student_register, tv_faculty, vister,alumini,tv_college_name;
    ArrayList<MultipleLocn> multiple ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_your_type);
        tv_college_name = findViewById(R.id.tv_college_name);
        tv_student_register = findViewById(R.id.tv_student_register);
        tv_faculty = findViewById(R.id.tv_faculty);
        vister = findViewById(R.id.vister);
        alumini = findViewById(R.id.alumini);
        multiple = new ArrayList<>();
        MultipleLocn lon = new MultipleLocn();
        lon.lat ="28.508484";
        lon.lng ="77.379457";
        multiple.add(lon);

        CollageLists lists = new CollageLists();
        lists.setTitle("Questin Edutech");
        lists.setTnid("258144");
        lists.setField_group_image("https://www.questin.co/sites/default/files/Questin_College.jpg");
        lists.setField_groups_logo("https://www.questin.co/sites/default/files/styles/thumbnail/public/group-logo/cropped-app-icon-512-x-512.png?itok=5p7g0n3s");
        lists.setLat("28.508484");
        lists.setLng("77.379457");
        lists.setAffiliation("AKTU Lucknow");
        lists.setMultiple(multiple);
        lists.setType("College");
        SessionManager.getInstance(getActivity()).saveCollage(lists);
        Utils.getSharedPreference(SelectYourType.this).edit()
                .putBoolean(Constants.IS_COLLEGE_ADDED, true).apply();





        if (SessionManager.getInstance(getActivity()).getCollage().getTitle()!=null){
            String Topheading = SessionManager.getInstance(getActivity()).getCollage().getTitle();
            SpannableString spString = new SpannableString(Topheading);
            AlignmentSpan.Standard aligmentSpan = new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER);
            spString.setSpan(aligmentSpan, 0, spString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv_college_name.setText(spString);

        }


        tv_student_register.setOnClickListener(this);
        tv_faculty.setOnClickListener(this);
        vister.setOnClickListener(this);
        alumini.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.tv_student_register:

                CollageRoleArray userRoleDetail = new CollageRoleArray();
                userRoleDetail.CollageRole ="student";
                SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);

                Intent i = new Intent(SelectYourType.this, StudentRegistration.class);
                startActivity(i);


                break;

            case R.id.tv_faculty:
                CollageRoleArray userRoleDetail1 = new CollageRoleArray();
                userRoleDetail1.CollageRole ="faculty";
                SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail1);


                Intent j = new Intent(SelectYourType.this, TeacherRegistration.class);
                startActivity(j);

                break;

            case R.id.vister:

                Intent k = new Intent(SelectYourType.this, QuestinSite.class);
                Utils.getSharedPreference(SelectYourType.this).edit()
                        .putInt(Constants.USER_ROLE, Constants.ROLE_VISITOR).apply();
                startActivity(k);

                break;
            case R.id.alumini:

                CollageRoleArray userRoleDetail3 = new CollageRoleArray();
                userRoleDetail3.CollageRole ="alumni";
                SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail3);

                Intent l = new Intent(SelectYourType.this, AluminiRegistration.class);
                startActivity(l);

                break;



        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }
}