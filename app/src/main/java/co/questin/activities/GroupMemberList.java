package co.questin.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.GroupMemberAdapter;
import co.questin.library.StateBean;
import co.questin.models.CourseMemberLists;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class GroupMemberList extends BaseAppCompactActivity {
    RecyclerView CourseMemberList;
    String Subject_Id;
    GroupMemberAdapter mGroupMemberAdapter;
    public ArrayList<CourseMemberLists> memberList;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_member_list);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        Bundle b = getActivity().getIntent().getExtras();
        Subject_Id = b.getString("COURSE_ID");

        memberList=new ArrayList<>();

        layoutManager = new LinearLayoutManager(this);
        CourseMemberList = findViewById(co.questin.R.id.CourseMemberList);
        CourseMemberList.setHasFixedSize(true);
        CourseMemberList.setLayoutManager(layoutManager);

        if (!isInternetConnected()){
            showConnectionSnackbar();
        }else {
            ShowIndicator();
            GroupMemberListDisplay(Subject_Id);

        }
    }


    private void GroupMemberListDisplay(String Subject_Id) {

        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_COLLAGECOURCEMEMBERLISTS+Subject_Id+"/users?fields=id,etid,%20gid,%20state&parameters=state=1&page=0&pagesize=100",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");

                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {


                                        for (int i = 0; i < jsonArrayData.length(); i++) {

                                            CourseMemberLists CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CourseMemberLists.class);
                                            memberList.add(CourseInfo);

                                        }
                                        mGroupMemberAdapter = new GroupMemberAdapter(GroupMemberList.this, memberList);
                                        CourseMemberList.setAdapter(mGroupMemberAdapter);

                                    }else {


                                    }
                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());
                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(GroupMemberList.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(GroupMemberList.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(GroupMemberList.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);

                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(GroupMemberList.this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }




    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
