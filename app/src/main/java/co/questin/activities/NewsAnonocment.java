package co.questin.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.NewsAnounceAdapter;
import co.questin.library.StateBean;
import co.questin.models.AnouncementArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class NewsAnonocment extends BaseAppCompactActivity {
    static RecyclerView rv_NewsAndAnounce;
    public static ArrayList<AnouncementArray> mnewsList;
    private RecyclerView.LayoutManager layoutManager;
    private static NewsAnounceAdapter newsanounceadapter;
    FloatingActionButton fab;
    String Teacher;
    static Activity activity;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    static SwipeRefreshLayout swipeContainer;
    static TextView  ErrorText;
    static ImageView ReloadProgress;
    private static ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_anonocment);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        fab = findViewById(R.id.fab);
        activity = getActivity();
        mnewsList=new ArrayList<>();
        swipeContainer = findViewById(R.id.swipeContainer);
        ErrorText=findViewById(R.id.ErrorText);
        ReloadProgress = findViewById(R.id.ReloadProgress);
        layoutManager = new LinearLayoutManager(this);
        rv_NewsAndAnounce = findViewById(R.id.rv_NewsAndAnounce);
        rv_NewsAndAnounce.setHasFixedSize(true);
        rv_NewsAndAnounce.setLayoutManager(layoutManager);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();

        newsanounceadapter = new NewsAnounceAdapter(rv_NewsAndAnounce, mnewsList ,activity);
        rv_NewsAndAnounce.setAdapter(newsanounceadapter);
        GetTheNotificationList();
        inItView();

        if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("14")) {
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity (new Intent (NewsAnonocment.this, CreateNewsAnnouncement.class));

                }
            });

        }
        else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("3")) {
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity (new Intent (NewsAnonocment.this, CreateNewsAnnouncement.class));

                }
            });

        }
        else  {
            fab.setVisibility(View.GONE);

        }

        rv_NewsAndAnounce.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    fab.hide();
                } else if (dy < 0) {
                    fab.show();
                }
            }
        });



        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FromNewsAndNotification();
            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh

                        if (!isLoading){
                            mShimmerViewContainer.startShimmerAnimation();
                            FromNewsAndNotification();

                        }else{
                            isLoading=false;
                            swipeContainer.setRefreshing(false);

                        }

                    }
                }, 1000);
            }
        });




    }

    private void inItView(){

        swipeContainer.setRefreshing(false);
        rv_NewsAndAnounce.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading ){

                    Log.i("loading", "Increment");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    newsanounceadapter.addProgress();
                    GetTheNotificationList();

                }else
                    Log.i("loading", "Increment Stop");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    private static void GetTheNotificationList() {
        swipeContainer.setRefreshing(true);
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_COLLAGEANNOUNCEMENTLIST+"?"+"cid="+ SessionManager.getInstance(activity).getCollage().getTnid()+"&offset="+PAGE_SIZE+"&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");

                                    ArrayList<AnouncementArray> arrayList = new ArrayList<>();

                                    for (int i = 0; i < jsonArrayData.length(); i++) {

                                        AnouncementArray newsInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AnouncementArray.class);
                                        arrayList.add(newsInfo);

                                    }
                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {


                                            rv_NewsAndAnounce.setVisibility(View.VISIBLE);
                                            newsanounceadapter.setInitialData(arrayList);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);
                                            swipeContainer.setRefreshing(false);


                                        } else {
                                            newsanounceadapter.removeProgress();
                                            swipeContainer.setRefreshing(false);
                                            newsanounceadapter.addData(arrayList);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);

                                        }

                                        isLoading = false;

                                    }else{
                                        if (PAGE_SIZE==0){
                                            rv_NewsAndAnounce.setVisibility(View.GONE);
                                            ErrorText.setVisibility(View.VISIBLE);
                                            ErrorText.setText(R.string.No_News);
                                            swipeContainer.setVisibility(View.GONE);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);

                                        }else
                                            newsanounceadapter.removeProgress();

                                    }

                                } else {
                                    swipeContainer.setRefreshing(false);
                                    ReloadProgress.setVisibility(View.VISIBLE);

                                }
                            } else {
                                swipeContainer.setRefreshing(false);
                                ReloadProgress.setVisibility(View.VISIBLE);


                            }
                        } catch (Exception e) {
                            swipeContainer.setRefreshing(false);
                            ReloadProgress.setVisibility(View.VISIBLE);

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipeContainer.setRefreshing(false);
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }






    public static void FromNewsAndNotification() {
        PAGE_SIZE=0;
        isLoading=true;
        swipeContainer.setRefreshing(true);
        GetTheNotificationList();
    }

    @Override
    protected void onPause() {
        super.onPause();

        mShimmerViewContainer.stopShimmerAnimation();}

    @Override
    protected void onResume() {
        mShimmerViewContainer.startShimmerAnimation();
        super.onResume();



    }







    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                PAGE_SIZE=0;
                isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
