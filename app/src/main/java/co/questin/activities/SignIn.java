package co.questin.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.library.StateBean;
import co.questin.models.AccessDeviceToken;
import co.questin.models.CollageLists;
import co.questin.models.CollageRoleArray;
import co.questin.models.MultipleLocn;
import co.questin.models.UserDetail;
import co.questin.network.URLS;
import co.questin.permissions.MarshmallowPermission;
import co.questin.questinsitecontent.QuestinSite;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

public class SignIn  extends BaseAppCompactActivity implements View.OnClickListener {
    EditText editText_email;
    Button button_enter;
    String email,password,urlsofphoto,urlsofWallpic, MainRole,CollageId,CollageName,CollageImage,lati,longi,locationName,CollageMemberShipId,CollageBatch,CollageBranch,Collagedepartment,CollageRole,CollageLogo,CollageEmail,
            CollageDesignation,CollageEnrollment,CollageStudentName,ParentUserChildId,ParentUserChildImage,CollageAffiliation,CollageType;
    TextView tv_signup,tv_forgot_password;
    EditText editText_password;
    public ArrayList<CollageLists> collageListMain;
    public ArrayList<String> collageList;
    ArrayList<MultipleLocn> multiple ;
    JSONArray locate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (!MarshmallowPermission.checkAllPermissions(SignIn.this)) {
                //  showToast("All Permission are required for app");
                MarshmallowPermission.checkAllPermissions(this);
            }
        }

        collageListMain = new ArrayList<>();
        collageList = new ArrayList<>();





        editText_email = findViewById(R.id.editText_email);
        editText_password = findViewById(R.id.editText_password);
        tv_signup = findViewById(R.id.tv_signup);
        tv_forgot_password = findViewById(R.id.tv_forgot_password);
        button_enter = findViewById(R.id.button_enter);
        button_enter.setOnClickListener(this);
        tv_signup.setOnClickListener(this);
        tv_forgot_password.setOnClickListener(this);



    }





    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to close this application ?");
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                finishAffinity();
            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.button_enter:
                hideKeyBoard(v);

                attemptLogin();

                break;

            case R.id.tv_signup:
                hideKeyBoard(v);

                Intent i = new Intent(SignIn.this,SignUp.class);
                startActivity(i);
                break;

            case R.id.tv_forgot_password:
                hideKeyBoard(v);

                Intent j = new Intent(SignIn.this,ForgetsPassword.class);
                startActivity(j);
                break;


        }
    }

    private void attemptLogin() {
        // Reset errors.
        editText_email.setError(null);
        editText_password.setError(null);

        // Store values at the time of the login attempt.
        email = editText_email.getText().toString().trim();
        password = editText_password.getText().toString().trim();
        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            focusView = editText_email;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        } else if (!Utils.isValidEmailAddress(email)) {
            focusView = editText_email;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_invalid_email));
            // Check for a valid password, if the user entered one.
        } else if (TextUtils.isEmpty(password)) {
            focusView = editText_password;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            if (!isInternetConnected()){
                showConnectionSnackbar();
            }else {
                ShowIndicator();
                callRequestForAPI();


            }

        }
    }




    private void callRequestForAPI() {
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putBoolean("isExit", true);
                                    editor.commit();
                                    JSONObject data = obj.getJSONObject("data");
                                    AccessDeviceToken token = new AccessDeviceToken();
                                    token.sessionID = data.getString("sessid");
                                    token.sessionName = data.getString("session_name");
                                    token.accesstoken = data.getString("token");
                                    SessionManager.getInstance(getActivity()).saveAccesstoken(token);

                                    JSONObject user = data.getJSONObject("user");
                                    String userid = user.getString("uid");
                                    String username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                    String mail = user.getString("mail");
                                    JSONObject picture = user.getJSONObject("picture");
                                    if (picture != null && picture.length() > 0) {
                                        urlsofphoto = picture.getString("url");
                                        Log.d("TAG", "urlsofphoto: " + urlsofphoto);

                                    } else {

                                    }


                                    JSONArray Userwallpic = user.getJSONArray("field_wall_picture");
                                    if (Userwallpic != null && Userwallpic.length() > 0) {
                                        for (int j = 0; j < Userwallpic.length(); j++) {
                                            Log.e("Mediapictire", Userwallpic.getString(j));

                                            urlsofWallpic =Userwallpic.getString(j);
                                        }

                                    }

                                    JSONArray CampusData = user.getJSONArray("colleges");
                                    if (CampusData != null && CampusData.length() > 0) {


                                        MainRole = CampusData.getJSONObject(0).getString("role");


                                        for (int i=0; i<CampusData.length();i++) {
                                            try{

                                                locate = CampusData.getJSONObject(0).getJSONArray("field_grp_location");

                                                if (locate != null && locate.length() > 0 ){
                                                    multiple = new ArrayList<>();
                                                    for (int j = 0; j < locate.length(); j++) {

                                                        lati = locate.getJSONObject(j).getString("lat");
                                                        longi = locate.getJSONObject(j).getString("lng");
                                                        locationName = locate.getJSONObject(j).getString("name");

                                                        MultipleLocn  lon = new MultipleLocn();

                                                        lon.lat =lati;
                                                        lon.lng =longi;
                                                        lon.name =locationName;
                                                        multiple.add(lon);


                                                    }

                                                }

                                            }catch (JSONException e){
                                                e.printStackTrace();
                                            }



                                        }

                                        if (MainRole.matches("13") ) {

                                            CollageId = CampusData.getJSONObject(0).getString("tnid");
                                            CollageName = CampusData.getJSONObject(0).getString("title");
                                            CollageImage = CampusData.getJSONObject(0).getString("field_group_image");
                                            CollageLogo  = CampusData.getJSONObject(0).getString("field_groups_logo");
                                            CollageRole = CampusData.getJSONObject(0).getString("field_i_am_a");
                                            CollageEmail = CampusData.getJSONObject(0).getString("field_college_email");
                                            CollageBatch = CampusData.getJSONObject(0).getString("field_batch_year");
                                            CollageBranch = CampusData.getJSONObject(0).getString("field_branch");
                                            CollageEnrollment  = CampusData.getJSONObject(0).getString("field_enrollment_number");
                                            CollageMemberShipId= CampusData.getJSONObject(0).getString("membership_id");
                                            CollageAffiliation = CampusData.getJSONObject(0).getString("affiliation");
                                            CollageType = CampusData.getJSONObject(0).getString("type");
                                            Log.d("TAG", "recource: " + MainRole + CollageId + CollageName + CollageImage +"-"+lati+"-"+longi);


                                            CollageRoleArray userRoleDetail = new CollageRoleArray();
                                            userRoleDetail.role = MainRole;
                                            userRoleDetail.tnid = CollageId;
                                            userRoleDetail.title = CollageName;
                                            userRoleDetail.field_group_image = CollageImage;
                                            userRoleDetail.CollageLogo =CollageLogo;
                                            userRoleDetail.CollageRole =CollageRole;
                                            userRoleDetail.CollageEmail =CollageEmail;
                                            userRoleDetail.CollageBatch =CollageBatch;
                                            userRoleDetail.CollageBranch =CollageBranch;
                                            userRoleDetail.CollageEnrollment =CollageEnrollment;


                                            SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                            CollageLists lists = new CollageLists();
                                            lists.setTitle(CollageName);
                                            lists.setTnid(CollageId);
                                            lists.setField_group_image(CollageImage);
                                            lists.setField_groups_logo(CollageLogo);
                                            lists.setCollageMemberShipId(CollageMemberShipId);
                                            lists.setMultiple(multiple);
                                            lists.setLat(lati);
                                            lists.setLng(longi);
                                            lists.setAffiliation(CollageAffiliation);
                                            lists.setType(CollageType);
                                            SessionManager.getInstance(getActivity()).saveCollage(lists);

                                            UserDetail userDetail = new UserDetail();
                                            userDetail.userprofile_id = user.getString("uid");
                                            userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                            userDetail.email = user.getString("mail");
                                            userDetail.FirstName = user.getString("field_first_name");
                                            userDetail.LastName = user.getString("field_last_name");
                                            userDetail.photo = urlsofphoto;
                                            userDetail.Userwallpic = urlsofWallpic;

                                            SessionManager.getInstance(getActivity()).saveUser(userDetail);

                                            Log.d("TAG", "only name: " + username);



                                            Utils.setStringPreferences(SignIn.this, "login", "0");
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putInt(Constants.USER_ROLE, Constants.ROLE_STUDENT).apply();
                                            hideIndicator();
                                            Intent upanel = new Intent(SignIn.this, QuestinSite.class);
                                            startActivity(upanel);
                                            finish();



                                        } else if (MainRole.matches("14")) {


                                            CollageId = CampusData.getJSONObject(0).getString("tnid");
                                            CollageName = CampusData.getJSONObject(0).getString("title");
                                            CollageImage = CampusData.getJSONObject(0).getString("field_group_image");
                                            CollageLogo  = CampusData.getJSONObject(0).getString("field_groups_logo");
                                            CollageRole = CampusData.getJSONObject(0).getString("field_i_am_a");
                                            CollageBatch = CampusData.getJSONObject(0).getString("field_batch_year");
                                            CollageEmail = CampusData.getJSONObject(0).getString("field_college_email");
                                            CollageDesignation = CampusData.getJSONObject(0).getString("field_faculty_designation");
                                            Collagedepartment = CampusData.getJSONObject(0).getString("field_department");
                                            CollageMemberShipId= CampusData.getJSONObject(0).getString("membership_id");
                                            CollageAffiliation = CampusData.getJSONObject(0).getString("affiliation");
                                            CollageType = CampusData.getJSONObject(0).getString("type");





                                            CollageRoleArray userRoleDetail = new CollageRoleArray();
                                            userRoleDetail.role = MainRole;
                                            userRoleDetail.tnid = CollageId;
                                            userRoleDetail.title = CollageName;
                                            userRoleDetail.field_group_image = CollageImage;
                                            userRoleDetail.CollageLogo = CollageLogo;
                                            userRoleDetail.CollageRole = CollageRole;
                                            userRoleDetail.CollageEmail = CollageEmail;
                                            userRoleDetail.CollageDesignation = CollageDesignation;
                                            userRoleDetail.Collagedepartment = Collagedepartment;



                                            SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                            CollageLists lists = new CollageLists();
                                            lists.setTitle(CollageName);
                                            lists.setTnid(CollageId);
                                            lists.setField_group_image(CollageImage);
                                            lists.setField_groups_logo(CollageLogo);
                                            lists.setCollageMemberShipId(CollageMemberShipId);
                                            lists.setLat(lati);
                                            lists.setLng(longi);
                                            lists.setMultiple(multiple);
                                            lists.setAffiliation(CollageAffiliation);
                                            lists.setType(CollageType);
                                            SessionManager.getInstance(getActivity()).saveCollage(lists);
                                            UserDetail userDetail = new UserDetail();
                                            userDetail.userprofile_id = user.getString("uid");
                                            userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                            userDetail.email = user.getString("mail");
                                            userDetail.FirstName = user.getString("field_first_name");
                                            userDetail.LastName = user.getString("field_last_name");
                                            userDetail.photo = urlsofphoto;
                                            userDetail.Userwallpic = urlsofWallpic;
                                            //  userDetail.colleges = user.getJSONArray("colleges");
                                            Log.d("TAG", "only name: " + username);
                                            SessionManager.getInstance(getActivity()).saveUser(userDetail);

                                            Utils.setStringPreferences(SignIn.this, "login", "0");
                                            hideIndicator();
                                            Intent upanel = new Intent(SignIn.this, QuestinSite.class);
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putInt(Constants.USER_ROLE, Constants.ROLE_FACULTY).apply();

                                            upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(upanel);
                                            finish();




                                        } else if (MainRole.matches("15")) {

                                            CollageId = CampusData.getJSONObject(0).getString("tnid");
                                            CollageName = CampusData.getJSONObject(0).getString("title");
                                            CollageImage = CampusData.getJSONObject(0).getString("field_group_image");
                                            CollageLogo  = CampusData.getJSONObject(0).getString("field_groups_logo");
                                            CollageRole = CampusData.getJSONObject(0).getString("field_i_am_a");
                                            //  CollageEmail = CampusData.getJSONObject(0).getString("field_college_email");
                                            CollageBatch = CampusData.getJSONObject(0).getString("field_batch_year");
                                            CollageBranch = CampusData.getJSONObject(0).getString("field_branch");
                                            CollageEnrollment  = CampusData.getJSONObject(0).getString("field_enrollment_number");
                                            CollageMemberShipId= CampusData.getJSONObject(0).getString("membership_id");
                                            CollageAffiliation = CampusData.getJSONObject(0).getString("affiliation");
                                            CollageType = CampusData.getJSONObject(0).getString("type");




                                            CollageRoleArray userRoleDetail = new CollageRoleArray();
                                            userRoleDetail.role = MainRole;
                                            userRoleDetail.tnid = CollageId;
                                            userRoleDetail.title = CollageName;
                                            userRoleDetail.field_group_image = CollageImage;
                                            userRoleDetail.CollageLogo = CollageLogo;
                                            userRoleDetail.CollageRole = CollageRole;
                                            userRoleDetail.CollageBranch = CollageBranch;
                                            userRoleDetail.CollageBatch =CollageBatch;
                                            //  userRoleDetail.CollageEmail =CollageEmail;
                                            userRoleDetail.CollageEnrollment =CollageEnrollment;


                                            SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                            CollageLists lists = new CollageLists();
                                            lists.setTitle(CollageName);
                                            lists.setTnid(CollageId);
                                            lists.setField_group_image(CollageImage);
                                            lists.setField_groups_logo(CollageLogo);
                                            lists.setLat(lati);
                                            lists.setLng(longi);
                                            lists.setMultiple(multiple);
                                            lists.setAffiliation(CollageAffiliation);
                                            lists.setCollageMemberShipId(CollageMemberShipId);
                                            lists.setType(CollageType);
                                            SessionManager.getInstance(getActivity()).saveCollage(lists);
                                            UserDetail userDetail = new UserDetail();
                                            userDetail.userprofile_id = user.getString("uid");
                                            userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                            userDetail.email = user.getString("mail");
                                            userDetail.FirstName = user.getString("field_first_name");
                                            userDetail.LastName = user.getString("field_last_name");
                                            userDetail.photo = urlsofphoto;
                                            userDetail.Userwallpic = urlsofWallpic;
                                            //  userDetail.colleges = user.getJSONArray("colleges");
                                            Log.d("TAG", "only name: " + username);
                                            SessionManager.getInstance(getActivity()).saveUser(userDetail);

                                            Utils.setStringPreferences(SignIn.this, "login", "0");
                                            hideIndicator();
                                            Intent upanel = new Intent(SignIn.this, QuestinSite.class);
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putInt(Constants.USER_ROLE, Constants.ROLE_ALUMNI).apply();

                                            upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(upanel);
                                            finish();



                                        } else if (MainRole.matches("16")) {

                                            JSONObject jsonObject = CampusData.getJSONObject(0);
                                            try{
                                                JSONObject UserChild = jsonObject.getJSONObject("child_student");
                                                if (UserChild != null && UserChild.length() > 0) {

                                                    ParentUserChildId = UserChild.getString("uid");
                                                    ParentUserChildImage = UserChild.getString("picture");
                                                    Log.e("UserChild", ParentUserChildId+".."+ParentUserChildImage);

                                                }


                                            }catch (JSONException e){

                                            }



                                            CollageId = CampusData.getJSONObject(0).getString("tnid");
                                            CollageName = CampusData.getJSONObject(0).getString("title");
                                            CollageImage = CampusData.getJSONObject(0).getString("field_group_image");
                                            CollageLogo  = CampusData.getJSONObject(0).getString("field_groups_logo");
                                            CollageRole = CampusData.getJSONObject(0).getString("field_i_am_a");
                                            CollageBatch = CampusData.getJSONObject(0).getString("field_batch_year");
                                            CollageStudentName = CampusData.getJSONObject(0).getString("field_student_name");
                                            CollageEnrollment  = CampusData.getJSONObject(0).getString("field_enrollment_number");
                                            CollageMemberShipId= CampusData.getJSONObject(0).getString("membership_id");
                                            CollageAffiliation = CampusData.getJSONObject(0).getString("affiliation");
                                            CollageType = CampusData.getJSONObject(0).getString("type");



                                            CollageRoleArray userRoleDetail = new CollageRoleArray();
                                            userRoleDetail.role = MainRole;
                                            userRoleDetail.tnid = CollageId;
                                            userRoleDetail.title = CollageName;
                                            userRoleDetail.field_group_image = CollageImage;
                                            userRoleDetail.CollageLogo = CollageLogo;
                                            userRoleDetail.CollageRole = CollageRole;
                                            userRoleDetail.CollageBatch = CollageBatch;
                                            userRoleDetail.CollageStudentNamev = CollageStudentName;
                                            userRoleDetail.CollageEnrollment = CollageEnrollment;



                                            SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                            CollageLists lists = new CollageLists();
                                            lists.setTitle(CollageName);
                                            lists.setTnid(CollageId);
                                            lists.setField_group_image(CollageImage);
                                            lists.setField_groups_logo(CollageLogo);
                                            lists.setLat(lati);
                                            lists.setLng(longi);
                                            lists.setMultiple(multiple);
                                            lists.setAffiliation(CollageAffiliation);
                                            lists.setCollageMemberShipId(CollageMemberShipId);
                                            lists.setType(CollageType);
                                            SessionManager.getInstance(getActivity()).saveCollage(lists);
                                            UserDetail userDetail = new UserDetail();
                                            userDetail.userprofile_id = ParentUserChildId;
                                            userDetail.ParentUid = user.getString("uid");
                                            userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                            userDetail.email = user.getString("mail");
                                            userDetail.FirstName = user.getString("field_first_name");
                                            userDetail.LastName = user.getString("field_last_name");
                                            userDetail.photo = urlsofphoto;
                                            UserDetail.childImage =ParentUserChildImage;
                                            userDetail.Userwallpic = urlsofWallpic;
                                            //  userDetail.colleges = user.getJSONArray("colleges");
                                            Log.d("TAG", "onlynameParentUserChildId " + ParentUserChildId+".."+user.getString("uid"));
                                            SessionManager.getInstance(getActivity()).saveUser(userDetail);
                                            Utils.setStringPreferences(SignIn.this, "login", "0");
                                            hideIndicator();
                                            Intent upanel = new Intent(SignIn.this, QuestinSite.class);
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putInt(Constants.USER_ROLE, Constants.ROLE_PARENT).apply();
                                            upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(upanel);
                                            finish();


                                        } else if (MainRole.matches("2")) {
                                            CollageRoleArray userRoleDetail = new CollageRoleArray();
                                            userRoleDetail.role = MainRole;
                                            userRoleDetail.tnid = CollageId;
                                            userRoleDetail.title = CollageName;
                                            userRoleDetail.field_group_image = CollageImage;
                                            SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                            CollageLists lists = new CollageLists();
                                            lists.setTitle(CollageName);
                                            lists.setTnid(CollageId);
                                            lists.setField_group_image(CollageImage);
                                            lists.setField_groups_logo(CollageLogo);
                                            lists.setLat(lati);
                                            lists.setLng(longi);
                                            lists.setMultiple(multiple);
                                            lists.setAffiliation(CollageAffiliation);
                                            lists.setType(CollageType);
                                            SessionManager.getInstance(getActivity()).saveCollage(lists);
                                            UserDetail userDetail = new UserDetail();
                                            userDetail.userprofile_id = user.getString("uid");
                                            userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                            userDetail.email = user.getString("mail");
                                            userDetail.FirstName = user.getString("field_first_name");
                                            userDetail.LastName = user.getString("field_last_name");
                                            userDetail.photo = urlsofphoto;
                                            userDetail.Userwallpic = urlsofWallpic;
                                            //  userDetail.colleges = user.getJSONArray("colleges");
                                            Log.d("TAG", "only name: " + username);
                                            SessionManager.getInstance(getActivity()).saveUser(userDetail);

                                            Utils.setStringPreferences(SignIn.this, "login", "0");
                                            hideIndicator();
                                            Intent upanel = new Intent(SignIn.this, QuestinSite.class);
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putInt(Constants.USER_ROLE, Constants.ROLE_VISITOR).apply();
                                            upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(upanel);
                                            finish();



                                        } else if (MainRole.matches("")) {
                                            CollageRoleArray userRoleDetail = new CollageRoleArray();
                                            userRoleDetail.role = MainRole;
                                            userRoleDetail.tnid = CollageId;
                                            userRoleDetail.title = CollageName;
                                            userRoleDetail.field_group_image = CollageImage;
                                            SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                            CollageLists lists = new CollageLists();
                                            lists.setTitle(CollageName);
                                            lists.setTnid(CollageId);
                                            lists.setField_group_image(CollageImage);
                                            lists.setField_groups_logo(CollageLogo);
                                            lists.setLat(lati);
                                            lists.setLng(longi);
                                            lists.setMultiple(multiple);
                                            lists.setAffiliation(CollageAffiliation);
                                            lists.setType(CollageType);
                                            SessionManager.getInstance(getActivity()).saveCollage(lists);
                                            UserDetail userDetail = new UserDetail();
                                            userDetail.userprofile_id = user.getString("uid");
                                            userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                            userDetail.email = user.getString("mail");
                                            userDetail.FirstName = user.getString("field_first_name");
                                            userDetail.LastName = user.getString("field_last_name");
                                            userDetail.photo = urlsofphoto;
                                            userDetail.Userwallpic = urlsofWallpic;
                                            //  userDetail.colleges = user.getJSONArray("colleges");
                                            Log.d("TAG", "only name: " + username);
                                            SessionManager.getInstance(getActivity()).saveUser(userDetail);

                                            Utils.setStringPreferences(SignIn.this, "login", "0");
                                            hideIndicator();
                                            Intent upanel = new Intent(SignIn.this, QuestinSite.class);
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putInt(Constants.USER_ROLE, Constants.ROLE_VISITOR).apply();
                                            upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(upanel);
                                            finish();


                                        }
                                        else if (MainRole.matches("3")) {

                                            CollageId = CampusData.getJSONObject(0).getString("tnid");
                                            CollageName = CampusData.getJSONObject(0).getString("title");
                                            CollageImage = CampusData.getJSONObject(0).getString("field_group_image");
                                            CollageLogo  = CampusData.getJSONObject(0).getString("field_groups_logo");
                                            CollageRole = CampusData.getJSONObject(0).getString("field_i_am_a");
                                            CollageBatch = CampusData.getJSONObject(0).getString("field_batch_year");
                                            CollageEmail = CampusData.getJSONObject(0).getString("field_college_email");
                                            CollageDesignation = CampusData.getJSONObject(0).getString("field_faculty_designation");
                                            Collagedepartment = CampusData.getJSONObject(0).getString("field_department");
                                            CollageMemberShipId= CampusData.getJSONObject(0).getString("membership_id");
                                            CollageAffiliation = CampusData.getJSONObject(0).getString("affiliation");
                                            CollageType = CampusData.getJSONObject(0).getString("type");





                                            CollageRoleArray userRoleDetail = new CollageRoleArray();
                                            userRoleDetail.role = MainRole;
                                            userRoleDetail.tnid = CollageId;
                                            userRoleDetail.title = CollageName;
                                            userRoleDetail.field_group_image = CollageImage;
                                            userRoleDetail.CollageLogo = CollageLogo;
                                            userRoleDetail.CollageRole = CollageRole;
                                            userRoleDetail.CollageEmail = CollageEmail;
                                            userRoleDetail.CollageDesignation = CollageDesignation;
                                            userRoleDetail.Collagedepartment = Collagedepartment;




                                            SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                            CollageLists lists = new CollageLists();
                                            lists.setTitle(CollageName);
                                            lists.setTnid(CollageId);
                                            lists.setField_group_image(CollageImage);
                                            lists.setField_groups_logo(CollageLogo);
                                            lists.setLat(lati);
                                            lists.setLng(longi);
                                            lists.setMultiple(multiple);
                                            lists.setAffiliation(CollageAffiliation);
                                            lists.setCollageMemberShipId(CollageMemberShipId);
                                            lists.setType(CollageType);
                                            SessionManager.getInstance(getActivity()).saveCollage(lists);
                                            UserDetail userDetail = new UserDetail();
                                            userDetail.userprofile_id = user.getString("uid");
                                            userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                            userDetail.email = user.getString("mail");
                                            userDetail.FirstName = user.getString("field_first_name");
                                            userDetail.LastName = user.getString("field_last_name");
                                            userDetail.photo = urlsofphoto;
                                            userDetail.Userwallpic = urlsofWallpic;
                                            //  userDetail.colleges = user.getJSONArray("colleges");
                                            Log.d("TAG", "only name: " + username);
                                            SessionManager.getInstance(getActivity()).saveUser(userDetail);

                                            Utils.setStringPreferences(SignIn.this, "login", "0");
                                            hideIndicator();
                                            Intent upanel = new Intent(SignIn.this, QuestinSite.class);
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putInt(Constants.USER_ROLE, Constants.ROLE_FACULTY).apply();
                                            upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(upanel);
                                            finish();


                                        }else if(MainRole.matches("27")){


                                            CollageId = CampusData.getJSONObject(0).getString("tnid");
                                            CollageName = CampusData.getJSONObject(0).getString("title");
                                            CollageImage = CampusData.getJSONObject(0).getString("field_group_image");
                                            CollageLogo  = CampusData.getJSONObject(0).getString("field_groups_logo");
                                            // CollageEmail = CampusData.getJSONObject(0).getString("field_college_email");
                                            CollageMemberShipId= CampusData.getJSONObject(0).getString("membership_id");
                                            CollageAffiliation = CampusData.getJSONObject(0).getString("affiliation");
                                            CollageType = CampusData.getJSONObject(0).getString("type");


                                            CollageRoleArray userRoleDetail = new CollageRoleArray();
                                            userRoleDetail.role = MainRole;
                                            userRoleDetail.tnid = CollageId;
                                            userRoleDetail.title = CollageName;
                                            userRoleDetail.field_group_image = CollageImage;
                                            userRoleDetail.CollageLogo = CollageLogo;

                                            SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                            CollageLists lists = new CollageLists();
                                            lists.setTitle(CollageName);
                                            lists.setTnid(CollageId);
                                            lists.setField_group_image(CollageImage);
                                            lists.setField_groups_logo(CollageLogo);
                                            lists.setLat(lati);
                                            lists.setLng(longi);
                                            lists.setMultiple(multiple);
                                            lists.setAffiliation(CollageAffiliation);
                                            lists.setCollageMemberShipId(CollageMemberShipId);
                                            lists.setType(CollageType);
                                            SessionManager.getInstance(getActivity()).saveCollage(lists);
                                            UserDetail userDetail = new UserDetail();
                                            userDetail.userprofile_id = user.getString("uid");
                                            userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                            userDetail.email = user.getString("mail");
                                            userDetail.FirstName = user.getString("field_first_name");
                                            userDetail.LastName = user.getString("field_last_name");
                                            userDetail.photo = urlsofphoto;
                                            userDetail.Userwallpic = urlsofWallpic;
                                            //  userDetail.colleges = user.getJSONArray("colleges");
                                            Log.d("TAG", "only name: " + username);
                                            SessionManager.getInstance(getActivity()).saveUser(userDetail);

                                            Utils.setStringPreferences(SignIn.this, "login", "0");
                                            hideIndicator();
                                            Intent upanel = new Intent(SignIn.this, QuestinSite.class);
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putInt(Constants.USER_ROLE, Constants.ROLE_DRIVER).apply();
                                            upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(upanel);
                                            finish();
                                        }

                                    }
                                    else {

                                        UserDetail userDetail = new UserDetail();
                                        userDetail.userprofile_id = user.getString("uid");
                                        userDetail.username = user.getString("field_first_name")+" "+user.getString("field_last_name");
                                        userDetail.email = user.getString("mail");
                                        userDetail.FirstName = user.getString("field_first_name");
                                        userDetail.LastName = user.getString("field_last_name");
                                        userDetail.photo = urlsofphoto;
                                        userDetail.Userwallpic = urlsofWallpic;
                                        //  userDetail.colleges = user.getJSONArray("colleges");
                                        Log.d("TAG", "only name: " + username);
                                        SessionManager.getInstance(getActivity()).saveUser(userDetail);

                                        Utils.setStringPreferences(SignIn.this,"login","0");
                                        hideIndicator();
                                        Intent upanel = new Intent(SignIn.this, SelectYourType.class);
                                        Utils.getSharedPreference(SignIn.this).edit()
                                                .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                        upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(upanel);

                                    }

                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showServerSnackbar(R.string.error_responce);

                        } else {
                            showConnectionSnackbar();

                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username",email);
                params.put("password", password);
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);
    }

}

