package co.questin.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import co.questin.R;
import co.questin.questinsitecontent.QuestinSite;
import co.questin.utils.Constants;
import co.questin.utils.Utils;


public class SpalashScreen extends AppCompatActivity {
    private static boolean isLoggedIn;
    private SharedPreferences preferences;
    private int userRole;
    private boolean isCollegeAdded;
    private  static  boolean isExit =false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spalash_screen);
        Utils.setBadge(this, 0);



        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        isExit= pref.getBoolean("isExit",false);


        preferences = Utils.getSharedPreference(SpalashScreen.this);
        isLoggedIn = preferences.getBoolean(Constants.IS_LOGGED_IN, false);
        userRole = preferences.getInt(Constants.USER_ROLE, Constants.ROLE_NULL);
        isCollegeAdded = preferences.getBoolean(Constants.IS_COLLEGE_ADDED, false);
        // UpdateToken();



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // Create an Intent that will start the Menu-Activity.

                switch (userRole){
                    case Constants.ROLE_STUDENT :
                        startActivity(new Intent(SpalashScreen.this, QuestinSite.class));
                        break;
                    case Constants.ROLE_ALUMNI :
                        startActivity(new Intent(SpalashScreen.this, QuestinSite.class));
                        break;
                    case Constants.ROLE_FACULTY :
                        startActivity(new Intent(SpalashScreen.this, QuestinSite.class));
                        break;
                    case Constants.ROLE_PARENT :
                        startActivity(new Intent(SpalashScreen.this, QuestinSite.class));
                        break;
                    case Constants.ROLE_VISITOR :
                        startActivity(new Intent(SpalashScreen.this, QuestinSite.class));

                        break;
                    case Constants.ROLE_DRIVER :
                        startActivity(new Intent(SpalashScreen.this, QuestinSite.class));

                        break;

                    case Constants.ROLE_NULL :


                        if (isExit ){
                            startActivity(new Intent(SpalashScreen.this, SignIn.class));
                        }else{


                            startActivity(new Intent(SpalashScreen.this, AppIntroScreen.class));

                        }

                        break;
                }
                overridePendingTransition(R.animator.fadein, R.animator.fadeout);
                finish();



            }
        }, 2000);
    }



}