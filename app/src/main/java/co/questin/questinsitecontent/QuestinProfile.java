package co.questin.questinsitecontent;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import co.questin.R;
import co.questin.calendersection.CalenderMain;
import co.questin.campusfeedsection.MyCampusFeeds;
import co.questin.chat.ChatTabbedActivity;
import co.questin.studentprofile.FriendsSection;
import co.questin.studentprofile.MyEvents;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.TextUtils;
import co.questin.utils.Utils;




public class QuestinProfile extends BaseAppCompactActivity implements View.OnClickListener{
    LinearLayout ll_my_feeds, ll_myLocation, ll_friends;
    TextView tv_username, tv_useremail,tv_userbatch;
    ImageButton imgbtn_edit_profile;
    ImageView mainheader,profileImage;
    String ActivityCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questin_profile);
        ll_my_feeds = findViewById(R.id.ll_my_feeds);
        ll_myLocation = findViewById(R.id.ll_myLocation);
        ll_friends = findViewById(R.id.ll_friends);
        tv_username = findViewById(R.id.tv_username);
        tv_useremail = findViewById(R.id.tv_useremail);
        profileImage = findViewById(R.id.imageProfile);
        mainheader = findViewById(R.id.mainheader);
        tv_userbatch = findViewById(R.id.tv_userbatch);
        imgbtn_edit_profile = findViewById(R.id.imgbtn_edit_profile);
        imgbtn_edit_profile.setVisibility(View.GONE);
        ll_my_feeds.setOnClickListener(this);
        ll_myLocation.setOnClickListener(this);
        ll_friends.setOnClickListener(this);
        profileImage.setOnClickListener(this);
        imgbtn_edit_profile.setOnClickListener(this);
        mainheader.setOnClickListener(this);
        displayUserData();
        Intent intent = getIntent();
        ActivityCheck = intent.getStringExtra("open");

        Setuponbottombar();

    }


    private void Setuponbottombar() {

        TextView dashboard_child = findViewById(R.id.dashboard_child);
        final TextView calendar = findViewById(R.id.calendar);
        TextView campus_feeds = findViewById(R.id.campus_feeds);
        TextView message_feeds = findViewById(R.id.message_feeds);
        TextView profile = findViewById(R.id.profile);

        dashboard_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent y = new Intent(QuestinProfile.this,QuestinSite.class);
                startActivity(y);
                finish();


            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent p = new Intent(QuestinProfile.this,CalenderMain.class)
                        .putExtra("open", "AsQuestinSite");
                startActivity(p);
                finish();


            }
        });

        campus_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent y = new Intent(QuestinProfile.this,QuestinFeeds.class);
                startActivity(y);
                finish();


            }
        });

        message_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent z = new Intent(QuestinProfile.this,ChatTabbedActivity.class)
                        .putExtra("open", "AsQuestinSite");
                startActivity(z);
                finish();


            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent j = new Intent(QuestinProfile.this,QuestinProfile.class);
                startActivity(j);
*/
            }
        });


    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_my_feeds:
                startActivity (new Intent(this, QuestinMyPost.class));

                break;

            case R.id.ll_myLocation:

                selectyourarea_dialog(this);

                break;

                case R.id.ll_friends:
                startActivity (new Intent (this, FriendsSection.class));
                //Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                break;

        }
    }



    private void displayUserData() {
        if (getUser()!=null){
            if (!TextUtils.isNullOrEmpty(getUser().username)) {
                tv_username.setText(getUser().username);
            }

            if (SessionManager.getInstance(getActivity()).getUserClgRole()!=null) {

                if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole()) && !TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageDesignation())) {
                    tv_useremail.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole() + " " + SessionManager.getInstance(getActivity()).getUserClgRole().getCollageDesignation());
                }
            }
            if (SessionManager.getInstance(getActivity()).getUserClgRole().getCollagedepartment()!=null) {
                if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollagedepartment())) {
                    tv_userbatch.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollagedepartment());
                }

            }
            if (!TextUtils.isNullOrEmpty(getUser().photo)) {
                Glide.with(this).load(getUser().photo)
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .fitCenter().into(profileImage);


            }
            if (!TextUtils.isNullOrEmpty(getUser().Userwallpic)) {
                Glide.with(this).load(getUser().Userwallpic)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .fitCenter().into(mainheader);


            }
        }
    }




    @Override
    public void onBackPressed() {
        Utils.getSharedPreference(QuestinProfile.this).edit()
                .putInt(Constants.RUNNIN_FIRST_PROFILE, Constants.ROLE_RUNNING_FALSE_PROFILE).apply();
        Utils.removeStringPreferences(QuestinProfile.this,"0");
        finish();
    }



}
