package co.questin.questinsitecontent;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.questin.R;
import co.questin.activities.AllAppNotification;
import co.questin.activities.ChangeCollage;
import co.questin.activities.ForceUpdateChecker;
import co.questin.activities.MainActivity;
import co.questin.activities.ProfileEdit;
import co.questin.activities.StartupGuide;
import co.questin.activities.SwitchYourType;
import co.questin.adapters.NavigationItemsAdapter;
import co.questin.alumni.AluminiMain;
import co.questin.calendersection.CalenderMain;
import co.questin.chat.ChatTabbedActivity;
import co.questin.chat.FriendsListService;
import co.questin.models.NavItems;
import co.questin.settings.AboutUs;
import co.questin.settings.EmailUs;
import co.questin.settings.FAQ;
import co.questin.settings.PrivacyPolicy;
import co.questin.settings.Settings;
import co.questin.settings.TermsAndConditions;
import co.questin.teacher.TeacherMain;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.TextUtils;
import co.questin.utils.Utils;
import co.questin.visitor.VisitorMain;

import static co.questin.fcm.MessagingService.All_App_Notification;
import static co.questin.fcm.MessagingService.college_classroom;
import static co.questin.fcm.MessagingService.college_feeds;
import static co.questin.fcm.MessagingService.singlechat;

public class QuestinSite extends BaseAppCompactActivity implements  NavigationView.OnNavigationItemSelectedListener,ForceUpdateChecker.OnUpdateNeededListener {
    Toolbar toolbar;
    TextView mToolbarTitle,toolbar_title;
    ExpandableListView expandableListView;
    DrawerLayout mDrawerlayout;
    TextView mEmail;
    FrameLayout parent_content;
    private NavigationItemsAdapter mNavigationItemsAdapter;
    private List<NavItems> listNavigationItem;
    private HashMap<String, List<NavItems>> listDataChild;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private Fragment mFragment = null;
    private Class mFragmentClass = null;
    TextView tv_username, tv_useremail,tv_userbatch,badge_notificationCalender,badge_notificationcampus,badge_notificationMsg;
    ImageButton imgbtn_edit_profile,imgbtn_account;
    ImageView mainheader,profileImage;
    Dialog imageDialog;
    private TextView badge_notification_icon,bell_icon;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questinsite);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Utils.setBadge(this, 0);

        //  ForceUpdateChecker.with(this).onUpdateNeeded( this).check();

        badge_notification_icon = findViewById(R.id.badge_notification_icon);
        bell_icon = findViewById(R.id.bell_icon);
        badge_notification_icon.setVisibility(View.GONE);
        bell_icon.setVisibility(View.GONE);

        pushFragment(new QuestinDasbord());
        toolbar_title = findViewById(R.id.toolbar_title);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(SessionManager.getInstance(getActivity()).getCollage().getTitle() !=null){

            toolbar_title.setText("Questin");
            toolbar_title.setSelected(true);
            getSupportActionBar().setTitle(null);

        }

        mDrawerlayout = findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerlayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        mDrawerlayout.setDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();

        expandableListView = findViewById(R.id.nav_expand_listview);

        //  ForceUpdateChecker.with(this).onUpdateNeeded(this).check();
        prepareNavigationList ();
        Setuponbottombar();
        Utils.init(QuestinSite.this);

        if (Utils.getFCMId()==null){
            UpdateFCMTokenToServer();
        }

        startService(new Intent(this, FriendsListService.class));

        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.navigation_header, null, false);
        expandableListView.addHeaderView(listHeaderView);
        tv_username = findViewById(R.id.tv_username);
        tv_useremail = findViewById(R.id.tv_useremail);
        profileImage = findViewById(R.id.imageProfile);
        tv_userbatch = findViewById(R.id.tv_userbatch);
        mainheader = findViewById(R.id.mainheader);
        imgbtn_account=findViewById(R.id.imgbtn_account);
        imgbtn_account.setVisibility(View.VISIBLE);
        imgbtn_edit_profile = findViewById(R.id.imgbtn_edit_profile);
        parent_content =findViewById(R.id.parent_content);

        displayUserData();






        imgbtn_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent n = new Intent(QuestinSite.this,ProfileEdit.class);
                startActivity(n);
            }
        });


        imgbtn_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent n = new Intent(QuestinSite.this,SwitchYourType.class);
                startActivity(n);
            }
        });



        mNavigationItemsAdapter = new NavigationItemsAdapter (this,
                listNavigationItem,listDataChild);

        expandableListView.setAdapter (mNavigationItemsAdapter);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

                int len = mNavigationItemsAdapter.getGroupCount();
                for (int i = 0; i < len; i++) {
                    if (i != groupPosition) {
                        expandableListView.collapseGroup(i);
                    }
                }


            }
        });
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener () {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View clickedView, int groupPosition, long rowId) {
                selectFragments(groupPosition,10);
                return false;
            }
        });

        expandableListView.setOnChildClickListener (new ExpandableListView.OnChildClickListener () {
            @Override public boolean onChildClick (ExpandableListView parent, View v, int groupPosition,
                                                   int childPosition, long id) {
                selectFragments(groupPosition,childPosition);
                v.setSelected (true);
                return false;
            }
        });




    }




    private void displayUserData() {
        if (getUser()!=null){
            if (!TextUtils.isNullOrEmpty(getUser().username)) {
                tv_username.setText(getUser().username);
            }
            if (SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole()!=null) {

                if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole()) && !TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageDesignation())) {
                    tv_useremail.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole() + " " + SessionManager.getInstance(getActivity()).getUserClgRole().getCollageDesignation());
                    Log.d("TAG", "secciontokenmainacctivity: " + SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole() + SessionManager.getInstance(getActivity()).getUserClgRole().getCollageDesignation());

                }
            }

            if (SessionManager.getInstance(getActivity()).getUserClgRole().getCollagedepartment()!=null) {
                if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollagedepartment())) {
                    tv_userbatch.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollagedepartment());
                }
            }
            if (!TextUtils.isNullOrEmpty(getUser().photo)) {
                Glide.with(this).load(getUser().photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(profileImage);

            }
            if (!TextUtils.isNullOrEmpty(getUser().Userwallpic)) {
                Glide.with(this).load(getUser().Userwallpic)
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .fitCenter().into(mainheader);




            }
        }
    }

    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version to continue reposting.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create();
        dialog.show();
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    private void Setuponbottombar() {

        TextView dashboard_child = findViewById(R.id.dashboard_child);
        final TextView calendar = findViewById(R.id.calendar);
        TextView campus_feeds = findViewById(R.id.campus_feeds);
        TextView message_feeds = findViewById(R.id.message_feeds);
        TextView profile = findViewById(R.id.profile);
        badge_notificationCalender =findViewById(R.id.badge_notificationCalender);
        badge_notificationcampus =findViewById(R.id.badge_notificationcampus);
        badge_notificationMsg =findViewById(R.id.badge_notificationMsg);


        if(singlechat >0){
            badge_notificationMsg.setVisibility(View.VISIBLE);
            badge_notificationMsg.setText(String.valueOf(singlechat));

        }else if (singlechat ==0){
            badge_notificationMsg.setVisibility(View.GONE);
        }

        if(college_classroom >0){
            badge_notificationCalender.setVisibility(View.VISIBLE);
            badge_notificationCalender.setText(String.valueOf(college_classroom));

        }else if (college_classroom ==0){
            badge_notificationCalender.setVisibility(View.GONE);
        }


        if(college_feeds >0){
            badge_notificationcampus.setVisibility(View.VISIBLE);
            badge_notificationcampus.setText(String.valueOf(college_feeds));

        }else if (college_feeds ==0){
            badge_notificationcampus.setVisibility(View.GONE);
        }





        dashboard_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                college_classroom=0;
                badge_notificationCalender.setVisibility(View.GONE);
                Intent p = new Intent(QuestinSite.this,CalenderMain.class)
                 .putExtra("open", "AsQuestinSite");
                startActivity(p);



            }
        });

        campus_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                college_feeds=0;
                badge_notificationcampus.setVisibility(View.GONE);
                Intent y = new Intent(QuestinSite.this,QuestinFeeds.class);
                startActivity(y);


            }
        });

        message_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                singlechat=0;
                badge_notificationMsg.setVisibility(View.GONE);
                Intent z = new Intent(QuestinSite.this,ChatTabbedActivity.class)
                        .putExtra("open", "AsQuestinSite");
                startActivity(z);



            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent j = new Intent(QuestinSite.this,QuestinProfile.class);
                startActivity(j);


            }
        });


    }


    protected void pushFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.parent_content, fragment);
            ft.commit();
        }
    }






    private void prepareNavigationList () {

        listNavigationItem = new ArrayList<NavItems>();
        listDataChild = new HashMap<String, List<NavItems>> ();

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.community),
                getResources ().getString (R.string.Colleges)));

        List<NavItems> collageList = new ArrayList<> ();

        if(SessionManager.getInstance(getActivity()).getCollage().getTitle() !=null){
            if (SessionManager.getInstance(getActivity()).getCollage().getTnid().matches("258144")){

            }else {
                collageList.add (new NavItems (getResources ().getDrawable (R.mipmap.community),
                        SessionManager.getInstance(getApplicationContext()).getCollage().getTitle()));
            }

        }else {

        }

        collageList.add (new NavItems (getResources ().getDrawable (R.mipmap.colleges),
                getResources ().getString (R.string.colleges)));





        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.need_help),
                getResources ().getString (R.string.need_help)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.read_more),
                getResources ().getString (R.string.read_more)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.setting),
                getResources ().getString (R.string.setting)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.log_out),
                getResources ().getString (R.string.log_out)));

        List<NavItems> needHelp = new ArrayList<> ();
        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.email_us)));
        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.faq)));
        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.faq1)));

        List<NavItems> readMore = new ArrayList<> ();
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.read_more),
                getResources ().getString (R.string.about_us)));
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.term_condition)));
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.privacy_policy)));

        List<NavItems> empty = new ArrayList<NavItems> ();

        listDataChild.put (listNavigationItem.get (0).getNavItemName (), collageList);
        listDataChild.put (listNavigationItem.get (1).getNavItemName (), needHelp);
        listDataChild.put (listNavigationItem.get (2).getNavItemName (), readMore);
        listDataChild.put (listNavigationItem.get (3).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (4).getNavItemName (), empty);
    }

    private void selectFragments(int groupPosition,int childPosition){


        switch (groupPosition){
            case 0:
                switch (childPosition) {
                    case 0:
                        if(SessionManager.getInstance(getActivity()).getCollage().getTitle() !=null){
                            if (SessionManager.getInstance(getActivity()).getCollage().getTnid().matches("258144")){
                                Intent j = new Intent(QuestinSite.this, ChangeCollage.class);
                                startActivity(j);
                                mDrawerlayout.closeDrawer (Gravity.LEFT);
                                mDrawerlayout.setStatusBarBackground(R.color.white);

                            }else {

                                if (SessionManager.getInstance(getActivity()).getUserClgRole()!=null) {

                                    if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("13")) {
                                        Intent j = new Intent(QuestinSite.this,MainActivity.class);
                                        startActivity(j);
                                        finish();


                                    } else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("14")) {
                                        Intent j = new Intent(QuestinSite.this,TeacherMain.class);
                                        startActivity(j);
                                        finish();


                                    } else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("15")) {
                                        Intent j = new Intent(QuestinSite.this,AluminiMain.class);
                                        startActivity(j);
                                        finish();


                                    }
                                    else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("2")) {
                                        Intent j = new Intent(QuestinSite.this,VisitorMain.class);
                                        startActivity(j);
                                        finish();

                                    }
                                }

                                mDrawerlayout.closeDrawer (Gravity.LEFT);
                                mDrawerlayout.setStatusBarBackground(R.color.white);
                            }

                        }else {

                        }



                        break;

                    case 1:
                        Intent j = new Intent(QuestinSite.this, ChangeCollage.class);
                        startActivity(j);
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;

                }
                break;

            case 1:

                switch (childPosition){
                    case 0:

                        Intent backIntent = new Intent(QuestinSite.this, EmailUs.class)
                                .putExtra("TITLE"," Email Us");
                        startActivity(backIntent);
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                    case 1:

                        startActivity (new Intent (this, FAQ.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;

                    case 2:

                        startActivity (new Intent (this, StartupGuide.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                }
                break;

            case 2:
                switch (childPosition){
                    case 0:

                        startActivity (new Intent (this, AboutUs.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                    case 1:

                        Intent o = new Intent(QuestinSite.this, TermsAndConditions.class);
                        startActivity(o);
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;

                    case 2:

                        startActivity (new Intent (this, PrivacyPolicy.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                }
                break;

            case 3:

                startActivity (new Intent (this, Settings.class));
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;

            case 4:
                showNoticeDialog ();
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;

        }


        try {
            mFragment = (Fragment) mFragmentClass.newInstance();
            FragmentManager fragmentManager = getSupportFragmentManager ();
            fragmentManager.beginTransaction ()
                    .replace (R.id.parent_content,mFragment)
                    .commit ();
        } catch (Exception e) {
            e.printStackTrace ();
        }

    }


    private void showNoticeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(QuestinSite.this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Logout ?");
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                DeleteFireBaseToken();
            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        displayUserData();

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to close this application ?");
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                Utils.getSharedPreference(QuestinSite.this).edit()
                        .putInt(Constants.RUNNIN_FIRST, Constants.ROLE_RUNNING_FALSE).apply();
                Utils.removeStringPreferences(QuestinSite.this,"0");
                finishAffinity();
            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }




}




