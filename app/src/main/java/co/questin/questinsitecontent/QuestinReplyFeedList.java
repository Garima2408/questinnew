package co.questin.questinsitecontent;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.activities.UserDisplayProfile;
import co.questin.adapters.ReplyCampusFeedAdapter;
import co.questin.adapters.ReplyQuestinFeedAdapter;
import co.questin.buttonanimation.LikeButton;
import co.questin.buttonanimation.OnAnimationEndListener;
import co.questin.buttonanimation.OnLikeListener;
import co.questin.campusfeedsection.AllCampusFeeds;
import co.questin.campusfeedsection.CommentScreen;
import co.questin.campusfeedsection.ReplyOnCampusFeeds;
import co.questin.campusfeedsection.UpdateMyFeeds;
import co.questin.library.StateBean;
import co.questin.models.ReplayOnPostArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

import static co.questin.utils.Utils.isStatusSuccess;

public class QuestinReplyFeedList extends BaseAppCompactActivity {
    String post_id,parentname,Pimage,Pdate,parentcomment,comments,TransferId,noOfComments,noOfLikes,IsLiked,Content_url;
    static String parent_id;
    ImageView circleView,comment,options,inaprooptions,imagePost,imagepreview,share;
    private TextView blog_content,publisher_name,dateTime,commentNo,likeNo,tv_comment;
    static RecyclerView rv_reply_comment;
    public static ArrayList<ReplayOnPostArray> commentsList;
    static ReplyQuestinFeedAdapter mcommentAdapter;
    private RecyclerView.LayoutManager layoutManager;
    static ImageView ReloadProgress;
    static Activity activity;
    LikeButton like;
    static String tittle,URLDoc;
    RelativeLayout previewlayout;
    LinearLayout box1,box3;
    private static boolean isLoading= true;
    private static int PAGE_SIZE = 0;
    Bundle extras;
    String Extension;
    int  int_condition =0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questin_reply_feed_list);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        activity =getActivity();
        Intent intent = getIntent();
        extras = intent.getExtras();
        post_id =intent.getStringExtra("POST_ID");
        parent_id= intent.getStringExtra("POST_ID");
        TransferId = intent.getStringExtra("USERSELECTED_ID");
        parentname = intent.getStringExtra("NAME");
        Pimage = intent.getStringExtra("IMAGE_URL");
        Pdate = intent.getStringExtra("DATE");
        parentcomment = intent.getStringExtra("BLOG_CONTENT");
        noOfComments = intent.getStringExtra("NOCOMMENT");
        noOfLikes = intent.getStringExtra("NOLIKED");
        IsLiked = intent.getStringExtra("IS_LIKED");
        Content_url = intent.getStringExtra("POSTCONENT");

        commentsList = new ArrayList<ReplayOnPostArray>();
        blog_content = findViewById(R.id.blog_content);
        tv_comment = findViewById(R.id.tv_comment);
        dateTime = findViewById(R.id.dateTime);
        publisher_name = findViewById(R.id.publisher_name);
        circleView = findViewById(R.id.circleView);
        rv_reply_comment =findViewById(R.id.rv_reply_comment);
        comment=findViewById(R.id.comment);
        like =findViewById(R.id.like);
        share  =findViewById(R.id.share);
        commentNo =findViewById(R.id.commentNo);
        likeNo =findViewById(R.id.likeNo);
        options =findViewById(R.id.options);
        inaprooptions =findViewById(R.id.inaprooptions);
        imagePost =findViewById(R.id.imagePost);
        imagepreview =findViewById(R.id.imagepreview);
        previewlayout =findViewById(R.id.previewlayout);
        box1=findViewById(R.id.box1);
        box3=findViewById(R.id.box3);
        layoutManager = new LinearLayoutManager(activity);
        rv_reply_comment.setHasFixedSize(true);
        rv_reply_comment.setLayoutManager(layoutManager);
        ReloadProgress = findViewById(R.id.ReloadProgress);
        mcommentAdapter = new ReplyQuestinFeedAdapter(rv_reply_comment, commentsList ,activity);
        rv_reply_comment.setAdapter(mcommentAdapter);
        rv_reply_comment.setNestedScrollingEnabled(false);



        blog_content. setText(Html.fromHtml(parentcomment));
        publisher_name.setText(parentname);
        dateTime.setText(Pdate);
        commentNo.setText(noOfComments);
        likeNo.setText(noOfLikes);


        if(Pimage != null ) {

            Glide.with(this).load(Pimage)
                    .placeholder(R.mipmap.place_holder).dontAnimate()
                    .fitCenter().into(circleView);

        }else {
            circleView.setImageResource(R.mipmap.place_holder);

        }
        Log.d("TAG", "print: " + TransferId+","+SessionManager.getInstance(getActivity()).getUser().getParentUid());





        if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {
            if(TransferId.equals(SessionManager.getInstance(getActivity()).getUser().getParentUid())){
                options.setVisibility(View.VISIBLE);
                inaprooptions.setVisibility(View.INVISIBLE);
            }else {
                options.setVisibility(View.INVISIBLE);
                inaprooptions.setVisibility(View.VISIBLE);
            }


        }else {

            if(TransferId.equals(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id())){
                options.setVisibility(View.VISIBLE);
                inaprooptions.setVisibility(View.INVISIBLE);
            }else {
                options.setVisibility(View.INVISIBLE);
                inaprooptions.setVisibility(View.VISIBLE);
            }
        }







        if(IsLiked.contains("False")){
            like.setLiked(false);
        }else if(IsLiked.contains("True"))  {
            like.setLiked(true);
        }

        if(Content_url != null  ) {

            Extension = Content_url.substring(Content_url.lastIndexOf("."));

            if (Extension.matches(".jpeg")) {

                previewlayout.setVisibility(View.VISIBLE);
                imagePost.setVisibility(View.VISIBLE);
                Glide.clear(imagePost);
                Glide.with(this).load(Content_url)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(imagePost);
            }else if (Extension.matches(".png"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagePost.setVisibility(View.VISIBLE);
                Glide.clear(imagePost);
                Glide.with(this).load(Content_url)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(imagePost);
            }
            else if (Extension.matches(".jpg"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagePost.setVisibility(View.VISIBLE);

                Glide.clear(imagePost);
                Glide.with(this).load(Content_url)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(imagePost);
            }else if (Extension.matches(".doc"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_doc_download);
            }
            else if (Extension.matches(".docx"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_doc_download);
            }
            else if (Extension.matches(".pdf"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_pdf_download);
            }
            else if (Extension.matches(".txt"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_text_download);
            }
            else if (Extension.matches(".xls"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_xls_download);
            }
            else if (Extension.matches(".xlsx"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_xls_download);
            }


        }else {
            previewlayout.setVisibility(View.GONE);



        }

        /*CALLING LISTS OF REPLY COMMENTS*/
        CollageCommentsList();


        inItView();



        tv_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(QuestinReplyFeedList.this, QuestinReplyComment.class)
                        .putExtra("POST_ID", post_id)
                        .putExtra("open","CommentFromReply");
                startActivity(intent);
            }
        });



        like.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                if (!IsLiked.isEmpty()) {
                    if (IsLiked.contains("False")) {
                        like.setLiked(true);

                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) + 1));
                        String NewLikeNo = likeNo.getText().toString();
                        LikeFeedComments(post_id,"flag");

                        IsLiked ="True";
                        noOfLikes =NewLikeNo;

                    } else if (IsLiked.contains("True")) {
                        like.setLiked(false);
                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) - 1));
                        String NewLikeNo = likeNo.getText().toString();
                        LikeFeedComments(post_id,"unflag");

                        IsLiked ="False";
                        noOfLikes =NewLikeNo;

                    }
                }
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                if (!IsLiked.isEmpty()) {
                    if (IsLiked.contains("False")) {
                        like.setLiked(true);
                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) + 1));
                        String NewLikeNo = likeNo.getText().toString();
                        LikeFeedComments(post_id,"flag");
                        IsLiked ="True";
                        noOfLikes =NewLikeNo;

                    } else if (IsLiked.contains("True")) {
                        like.setLiked(false);
                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) - 1));
                        String NewLikeNo = likeNo.getText().toString();
                        LikeFeedComments(post_id,"unflag");

                        IsLiked ="False";
                        noOfLikes =NewLikeNo;

                    }
                }
            }
        });


        like.setOnAnimationEndListener(new OnAnimationEndListener() {
            @Override
            public void onAnimationEnd(LikeButton likeButton) {

            }
        });












        publisher_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent backIntent = new Intent(QuestinReplyFeedList.this, UserDisplayProfile.class)
                        .putExtra("USERPROFILE_ID",TransferId);
                startActivity(backIntent);


            }
        });
        box3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String shareBody = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "APP NAME (Open it in Google Play Store to Download the Application)");

                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody+parentcomment);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));




            }
        });


        circleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent backIntent = new Intent(QuestinReplyFeedList.this, UserDisplayProfile.class)
                        .putExtra("USERPROFILE_ID",TransferId);
                startActivity(backIntent);


            }
        });
        inaprooptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), android.app.AlertDialog.THEME_HOLO_DARK)
                        .setTitle("Inappropriate")
                        .setMessage(R.string.Inappropriate)
                        .setCancelable(false)
                        .setPositiveButton("Yes", new Dialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                /*  MARKED IT INAPPROPRIATE ON POST*/
                                InappropriateFeedComments(post_id);




                            }
                        })
                        .setNegativeButton("No", new Dialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                builder.create().show();



            }
        });

        options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPopupMenu();


            }

            private void showPopupMenu() {


                PopupMenu popup = new PopupMenu(getActivity(), options);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.optionpopupmenu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.Editoption:


                                Intent backIntent = new Intent(QuestinReplyFeedList.this, UpdateQuestinFeed.class)
                                        .putExtra("POST_ID", post_id)
                                        .putExtra("NAME",parentname)
                                        .putExtra("BLOG_CONTENT", parentcomment)
                                        .putExtra("DATE", Pdate)
                                        .putExtra("IMAGE_URL",  Pimage)
                                        .putExtra("POSTCONENT", Content_url)
                                        .putExtra("open", "CameFromReplyonQuestin");
                                startActivity(backIntent);
                                finish();
                                return true;
                            case R.id.deleteoption:

                                AlertDialog.Builder builder = new AlertDialog.Builder(QuestinReplyFeedList.this, android.app.AlertDialog.THEME_HOLO_DARK)
                                        .setTitle("Delete My Feed")
                                        .setMessage(R.string.delete)
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                /*  DELETE COMMENTS ON POST*/
                                                DeleteFeedComments(post_id);


                                            }
                                        })
                                        .setNegativeButton("No", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        });
                                builder.create().show();



                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popup.show();

            }
        });
        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallAgainList();
            }
        });

    }

    private void inItView(){
        rv_reply_comment.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {
                if(!isLoading   ){
                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    mcommentAdapter.addProgress();
                    CollageCommentsList();

                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }


    /*LISTS OF REPLY COMMENTS ON SUBJECTS*/

    private static void CollageCommentsList() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_REPLYCOMMENTS+"?"+"&"+"cid=258144"+"&"+"pid"+"="+parent_id+"&offset="+PAGE_SIZE+"limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    ArrayList<ReplayOnPostArray> list = new ArrayList<>();

                                    if(jsonArrayData != null && jsonArrayData.length() > 0 ) {

                                        for (int i = 0; i < jsonArrayData.length(); i++) {

                                            ReplayOnPostArray subjects = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), ReplayOnPostArray.class);
                                            list.add(subjects);
                                            JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                                            if (jsonObject.has("field_comment_attachments")) {
                                                JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                                if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0) {
                                                    URLDoc = AttacmentsFeilds.getString("value");
                                                    tittle = AttacmentsFeilds.getString("title");
                                                    subjects.title = tittle;
                                                    subjects.url = URLDoc;

                                                } else {

                                                }
                                            } else {

                                            }

                                        }

                                    }
                                    if (list!=null &&  list.size()>0){
                                        if (PAGE_SIZE == 0) {


                                            rv_reply_comment.setVisibility(View.VISIBLE);
                                            mcommentAdapter.setInitialData(list);

                                        } else {
                                            isLoading = false;
                                            mcommentAdapter.removeProgress();

                                            mcommentAdapter.addData(list);


                                        }
                                    }else{
                                        mcommentAdapter.removeProgress();
                                    }

                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }



    public static void CallAgainList() {
        commentsList.clear();
        CollageCommentsList();




    }

    /*ADD LIKES ON COMMENTS */

    private void LikeFeedComments(final String id, final String flag) {
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_LIKEDCOMMENTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {

                                }else {

                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {

                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {

                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("flag_name","comments_like");
                params.put("entity_id",id);
                params.put("action",flag);
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);

    }



    /*MARKED IN APPROPRIATE ON COMMENTS */

    private void InappropriateFeedComments(final String id) {

        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_MARKEDINAPPROPRIATECOMMENT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    Check();
                                    finish();

                                }else {

                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {

                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {

                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("flag_name","comments_inappropriate");
                params.put("entity_id",id);
                params.put("action","flag");
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);

    }




    /*LISTS OF DELETE COMMENTS ON POSTS*/

    private void DeleteFeedComments(String id) {

        StringRequest mStrRequest = new StringRequest(Request.Method.DELETE, URLS.URL_DELETECOMMENTS+"/"+id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    Check();
                                    finish();

                                }
                            } else {
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            showServerSnackbar(R.string.error_responce);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                        } else {
                        }
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }




        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);
    }






    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Check();
        ActivityCompat.finishAfterTransition(this);


    }

    private void Check() {

        if (extras.containsKey("open")) {
            if (extras.getString("open").equals("CameFromCampusFeed")) {

                AllCampusFeeds.RefreshWorked();

            }else if (extras.getString("open").equals("CameFromparentCampusFeed")) {

                AllCampusFeeds.RefreshWorked();


            }




        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Check();
                ActivityCompat.finishAfterTransition(this);

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }


}
