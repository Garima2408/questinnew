package co.questin.questinsitecontent;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.QuestinFeedsAdapters;
import co.questin.calendersection.CalenderMain;
import co.questin.campusfeedsection.CampusFeedNotifications;
import co.questin.chat.ChatTabbedActivity;
import co.questin.library.StateBean;
import co.questin.models.CampusFeedArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

import static co.questin.fcm.MessagingService.college_Allfeeds;
import static co.questin.utils.Utils.isStatusSuccess;

public class QuestinFeeds extends BaseAppCompactActivity {
    static FloatingActionButton fab;
    static RecyclerView rv_campus_news;
    public static ArrayList<CampusFeedArray> mcampusfeedList;
    private static QuestinFeedsAdapters campusfeedadapter;
    private RecyclerView.LayoutManager layoutManager;
    static String tittle,URLDoc,FilterTittle,FilterId;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    static TextView ErrorText;
    private boolean _hasLoadedOnce= false; // your boolean field
    private static ShimmerFrameLayout mShimmerViewContainer;
    static SwipeRefreshLayout swipeContainer;
    private ArrayList<String> filterlist;
    static ImageView ReloadProgress;
    static Activity activity;

    boolean isPressed = false;
    SharedPreferences sharedLocationpreferences;
    static String filterid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questin_feeds);
        sharedLocationpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        if (sharedLocationpreferences.contains("id")) {
            filterid =(sharedLocationpreferences.getString("id", ""));

        }else {
            selectyourarea_dialog(this);
        }

        activity = getActivity();
        fab = findViewById(R.id.fab);
        ReloadProgress = findViewById(R.id.ReloadProgress);
        filterlist = new ArrayList<String>();
        ErrorText =findViewById(R.id.ErrorText);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();
        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));
        mcampusfeedList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        rv_campus_news = findViewById(R.id.rv_campus_news);
        campusfeedadapter = new QuestinFeedsAdapters(rv_campus_news, mcampusfeedList ,activity);
        rv_campus_news.setLayoutManager(layoutManager);
        rv_campus_news.setItemAnimator(new DefaultItemAnimator());
        rv_campus_news.setAdapter(campusfeedadapter);
        inItView();
        Setuponbottombar();


        CollageAllNewFeedsDisplay();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backIntent = new Intent(QuestinFeeds.this, QuestinCreateFeed.class)
                        .putExtra("open", "QuestinFeed");
                startActivity(backIntent);




            }
        });

        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RefreshWorked();
            }
        });







        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh

                        if (!isLoading){
                            RefreshWorked();
                        }else{
                            isLoading=false;
                            swipeContainer.setRefreshing(false);
                        }


                    }
                }, 1000);
            }
        });



    }


    private void inItView(){
        rv_campus_news.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){

                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    campusfeedadapter.addProgress();
                    CollageAllNewFeedsDisplay();


                }else
                    Log.i("loadinghua", "im else now");
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }




    public static void RefreshWorked() {

        PAGE_SIZE=0;
        isLoading=true;
        mShimmerViewContainer.startShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        CollageAllNewFeedsDisplay();

    }







    private void Setuponbottombar() {

        TextView dashboard_child = findViewById(R.id.dashboard_child);
        final TextView calendar = findViewById(R.id.calendar);
        TextView campus_feeds = findViewById(R.id.campus_feeds);
        TextView message_feeds = findViewById(R.id.message_feeds);
        TextView profile = findViewById(R.id.profile);

        dashboard_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();


            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent p = new Intent(QuestinFeeds.this,CalenderMain.class)
                        .putExtra("open", "AsQuestinSite");
                startActivity(p);
                finish();


            }
        });

        campus_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });

        message_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent z = new Intent(QuestinFeeds.this,ChatTabbedActivity.class)
                        .putExtra("open", "AsQuestinSite");
                startActivity(z);
                finish();


            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent x = new Intent(QuestinFeeds.this,QuestinProfile.class);
                startActivity(x);
                finish();
            }});

    }


    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");
        _hasLoadedOnce= false;
        super.onPause();
    }





    @Override
    public void onDestroy() {
        super.onDestroy();
        PAGE_SIZE=0;
    }




    private static void CollageAllNewFeedsDisplay() {
        swipeContainer.setRefreshing(false);
        ReloadProgress.setVisibility(View.GONE);
        rv_campus_news.setVisibility(View.VISIBLE);
        if (filterid ==null){
            filterid ="881";
        }
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_SHORTCOMMENTS+"?"+"cid=258144"+"&"+"channel="+filterid+"&offset="+PAGE_SIZE+"&limit=20",

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {

                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");
                                    ArrayList<CampusFeedArray> arrayList = new ArrayList<>();
                                    arrayList.clear();


                                    for (int i = 0; i < jsonArrayData.length(); i++) {

                                        CampusFeedArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CampusFeedArray.class);
                                        arrayList.add(CourseInfo);
                                        JSONObject jsonObject = jsonArrayData.getJSONObject(i);

                                        JSONArray jsoncategoryArray = jsonArrayData.getJSONObject(i).getJSONArray("category");
                                        if (jsoncategoryArray != null && jsoncategoryArray.length() > 0) {

                                            for (int j = 0; j < jsoncategoryArray.length(); j++) {
                                                String categoryid = jsoncategoryArray.getJSONObject(j).getString("id");
                                                String categoryName = jsoncategoryArray.getJSONObject(j).getString("name");
                                                CourseInfo.categoryId =categoryid;
                                                CourseInfo.categoryName =categoryName;
                                            }
                                        }




                                        if (jsonObject.has("field_comment_attachments")){
                                            JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                            if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0 ){
                                                URLDoc = AttacmentsFeilds.getString("value");
                                                tittle = AttacmentsFeilds.getString("title");
                                                CourseInfo.title =tittle;
                                                CourseInfo.url =URLDoc;

                                            }else {

                                            }
                                        }
                                    }



                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {

                                            rv_campus_news.setVisibility(View.VISIBLE);
                                            campusfeedadapter.setInitialData(arrayList);
                                            // stop animating Shimmer and hide the layout
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);

                                        } else {
                                            rv_campus_news.setVisibility(View.VISIBLE);
                                            campusfeedadapter.removeProgress();
                                            campusfeedadapter.addData(arrayList);
                                            swipeContainer.setRefreshing(false);
                                            // stop animating Shimmer and hide the layout
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);
                                        }
                                        isLoading = false;
                                    }
                                    else{
                                        if (PAGE_SIZE==0){
                                            // stop animating Shimmer and hide the layout
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);
                                            // swipeContainer.setVisibility(View.GONE);
                                            rv_campus_news.setVisibility(View.GONE);
                                            ErrorText.setVisibility(View.VISIBLE);
                                            ErrorText.setText("No Post");
                                        }else
                                            campusfeedadapter.removeProgress();

                                    }



                                } else {
                                    showSnackbarMessage(stateResponse.getMessage());
                                    mShimmerViewContainer.stopShimmerAnimation();
                                    mShimmerViewContainer.setVisibility(View.GONE);

                                }
                            } else {
                                showServerSnackbar(R.string.error_responce);

                            }
                        } catch (Exception e) {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);
                return params;
            }
        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);
    }


    @Override
    public void onBackPressed() {
        Utils.getSharedPreference(this).edit()
                .putInt(Constants.RUNNIN_FIRST_CAMPUSFEED, Constants.ROLE_RUNNING_FALSE_CAMPUSFEED).apply();
        Utils.removeStringPreferences(QuestinFeeds.this,"0");

        super.onBackPressed();
    }
}
