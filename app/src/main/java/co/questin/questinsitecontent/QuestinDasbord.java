package co.questin.questinsitecontent;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import co.questin.R;
import co.questin.adapters.QuestinSiteAdapter;
import co.questin.college.StandardCodes;
import co.questin.settings.EmailUs;
import co.questin.utils.BaseFragment;
import co.questin.utils.SessionManager;
import co.questin.weathersection.Function;

import static co.questin.fcm.MessagingService.college_news;


public class QuestinDasbord extends BaseFragment {
    GridView GridView_questin;
    TextView cityField,  currentTemperatureField, humidity_field, pressure_field, updatedField;
    RelativeLayout layoutrelay;
    ImageView mainheader,imageView3,wetherIcon;
    Button shareapp,providefeedback;
    TextView badge_notification;
    LinearLayout LinearAnnounce;

    public static int[] QuestinResourcesImages = {
            R.mipmap.groups_community,
            R.mipmap.q_a,
            R.mipmap.events,
            R.mipmap.artclecommunity,
            R.mipmap.document_community,
            R.mipmap.disertation_projects,
            R.mipmap.iscode,
            R.mipmap.intlcode,

    };

    public QuestinDasbord() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_questin_dasbord, container, false);
        GridView_questin =view.findViewById(R.id.GridView_questin);
        mainheader = view.findViewById(R.id.mainheader);
        imageView3 = view.findViewById(R.id.imageView3);
        layoutrelay = view.findViewById(R.id.layoutrelay);
        badge_notification =view.findViewById(R.id.badge_notification);
        shareapp = view.findViewById(R.id.inviteFriend);
        providefeedback = view.findViewById(R.id.provideFeeddback);
        cityField = view.findViewById(R.id.city_field);
        updatedField = view.findViewById(R.id.updated_field);
        currentTemperatureField = view.findViewById(R.id.current_temperature_field);
        humidity_field = view.findViewById(R.id.humidity_field);
        pressure_field = view.findViewById(R.id.pressure_field);
        wetherIcon = view.findViewById(R.id.wetherIcon);
        LinearAnnounce =view.findViewById(R.id.LinearAnnounce);
        LinearAnnounce.setVisibility(View.GONE);

        if(college_news >0){
            badge_notification.setVisibility(View.VISIBLE);
            badge_notification.setText(String.valueOf(college_news));

        }else if (college_news ==0){
            badge_notification.setVisibility(View.GONE);
        }



        Function.placeIdTask asyncTask = new Function.placeIdTask(new Function.AsyncResponse() {
            public void processFinish(String weather_city, String weather_description, String weather_temperature, String weather_humidity, String weather_pressure, String weather_updatedOn, String weather_iconText, String sun_rise, String MainIcon) {
                String iconurl ="http://openweathermap.org/img/w/"+MainIcon+ ".png";

                cityField.setText(weather_city);
                updatedField.setText(weather_updatedOn);
                String numWihoutDecimal = String.valueOf(weather_temperature).split("\\.")[0];
                currentTemperatureField.setText(numWihoutDecimal +"°" +" "+ "C");
                humidity_field.setText("Humidity: " + weather_humidity);
                pressure_field.setText("Pressure: " + weather_pressure);

                try {

                    Glide.with(getActivity()).load(iconurl)
                            .placeholder(R.mipmap.clouds).dontAnimate()
                            .fitCenter().into(wetherIcon);
                }catch (Exception e){

                }
            }
        });


        if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null) {
            String Lat = String.valueOf(Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat()));
            String   Long = String.valueOf(Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng()));

            asyncTask.execute(Lat, Long);
            Log.e("TAG", "LatLong " +Lat + Long);


        }
        else {


        }





        Glide.with(getActivity()).load(R.mipmap.questin_college)
                .placeholder(R.mipmap.header_image).dontAnimate()
                .fitCenter().into(mainheader);



        Glide.with(getActivity())
                    .load(R.mipmap.appicon)
                    .placeholder(R.mipmap.appicon).dontAnimate()
                    .fitCenter().into(imageView3);






        shareapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT,"insert subject here");
                String app_url = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";
                intent.putExtra(Intent.EXTRA_TEXT,app_url);
                startActivity(Intent.createChooser(intent,"share via"));
            }
        });

        providefeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent backIntent = new Intent(getActivity(), EmailUs.class)
                        .putExtra("TITLE","Provide Feedback");
                startActivity(backIntent);


            }
        });




        GridView_questin.setAdapter(new QuestinSiteAdapter(getActivity(), QuestinResourcesImages));


        GridView_questin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id){



                switch (position){
                    case 0:

                        startActivity (new Intent(getActivity(), QuestinsiteGroups.class));

                        break;
                    case 1:
                        startActivity (new Intent (getActivity(), QuestinsiteQnA.class));



                        break;
                    case 2:

                        startActivity (new Intent (getActivity(), QuestinsiteEvents.class));


                        break;
                    case 3:
                        startActivity (new Intent (getActivity(), QuestinsiteArticles.class));

                        break;
                    case 4:

                        startActivity (new Intent (getActivity(), QuestinsiteDocuments.class));

                        break;
                    case 5:
                        startActivity (new Intent (getActivity(), QuestinsiteDissertation.class));

                        break;
                    case 6:
                        Intent intent = new Intent(getActivity(), StandardCodes.class);
                        intent.putExtra("url", "http://questin.org/is-code");
                        startActivity(intent);
                        break;
                    case 7:
                        Intent intent2 = new Intent(getActivity(), StandardCodes.class);
                        intent2.putExtra("url", "http://questin.org/intl-code");
                        startActivity(intent2);
                        break;

                }
            }
        });
        return view;
    }


}
