package co.questin.questinsitecontent;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.MyCampusFeedsAdapters;
import co.questin.adapters.MyQuestinFeedsAdapters;
import co.questin.campusfeedsection.AddCampusFeeds;
import co.questin.library.StateBean;
import co.questin.models.CampusFeedArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class QuestinMyPost extends BaseAppCompactActivity {
    static RecyclerView rv_mycampus_news;
    public static ArrayList<CampusFeedArray> mcampusfeedList;
    private static MyQuestinFeedsAdapters campusfeedadapter;
    private RecyclerView.LayoutManager layoutManager;
    static Activity activity;
    FloatingActionButton fab;
    static String tittle,URLDoc;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    static SwipeRefreshLayout swipeContainer;
    static TextView ErrorText;
    static ImageView ReloadProgress;
    private static ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questin_my_post);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        mcampusfeedList = new ArrayList<>();
        activity =getActivity();
        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        ErrorText =findViewById(R.id.ErrorText);
        ReloadProgress =findViewById(R.id.ReloadProgress);
        layoutManager = new LinearLayoutManager(this);
        rv_mycampus_news = findViewById(R.id.rv_mycampus_news);
        rv_mycampus_news.setHasFixedSize(true);
        campusfeedadapter = new MyQuestinFeedsAdapters(rv_mycampus_news, mcampusfeedList ,activity);
        rv_mycampus_news.setAdapter(campusfeedadapter);


        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();

        rv_mycampus_news.setLayoutManager(layoutManager);

        fab = findViewById(R.id.fab);

        inItView();
        MyCollageNewFeedsDisplay();
        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RefreshWorkedFromMycampusAdapter();
            }
        });
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh


                        if (!isLoading){
                            RefreshWorkedFromMycampusAdapter();
                        }else{
                            isLoading=false;
                            swipeContainer.setRefreshing(false);
                        }


                    }
                }, 1000);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent backIntent = new Intent(activity, QuestinCreateFeed.class)
                        .putExtra("open", "CameQuestinMyFeed");
                activity.startActivity(backIntent);

            }
        });


        rv_mycampus_news.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    fab.hide();
                } else if (dy < 0) {
                    fab.show();
                }
            }
        });

    }


    @Override
    protected void onPause() {
        super.onPause();
        PAGE_SIZE=0;
        isLoading=false;
        mShimmerViewContainer.stopShimmerAnimation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
        swipeContainer.setRefreshing(false);

    }


    public static void RefreshWorkedFromMycampusAdapter() {
        Log.e("myfeed","refresh");
        PAGE_SIZE=0;
        isLoading=true;
        swipeContainer.setRefreshing(true);
        MyCollageNewFeedsDisplay();




    }


    private void inItView(){


        swipeContainer.setRefreshing(false);


        rv_mycampus_news.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager)layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    campusfeedadapter.addProgress();

                    MyCollageNewFeedsDisplay();


                    //fetchData(fYear,,);
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    private static void MyCollageNewFeedsDisplay() {
        ShowIndicator();
        swipeContainer.setRefreshing(false);
        rv_mycampus_news.setVisibility(View.VISIBLE);
        ErrorText.setVisibility(View.GONE);
        ReloadProgress.setVisibility(View.GONE);

        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_MYCAMPUSFEED+"?"+"cid=258144"+"&uid="+SessionManager.getInstance(activity).getUser().userprofile_id +"&offset="+PAGE_SIZE+"&limit=20",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");


                                    ArrayList<CampusFeedArray> arrayList = new ArrayList<>();

                                    for (int i = 0; i < jsonArrayData.length(); i++) {


                                        CampusFeedArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CampusFeedArray.class);
                                        arrayList.add(CourseInfo);

                                        JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                                        if (jsonObject.has("field_comment_attachments")){
                                            JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                            if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0 ){
                                                URLDoc = AttacmentsFeilds.getString("value");
                                                tittle = AttacmentsFeilds.getString("title");
                                                Log.d("TAG", "titleurl: " + tittle +URLDoc);
                                                CourseInfo.title =tittle;
                                                CourseInfo.url =URLDoc;


                                            }else {

                                            }
                                        }else {

                                        }

                                    }

                                    if (arrayList!=null &&  arrayList.size()>0){
                                        if (PAGE_SIZE == 0) {


                                            rv_mycampus_news.setVisibility(View.VISIBLE);
                                            campusfeedadapter.setInitialData(arrayList);

                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);


                                        } else {
                                            campusfeedadapter.removeProgress();
                                            swipeContainer.setRefreshing(false);

                                            campusfeedadapter.addData(arrayList);

                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);



                                        }

                                        isLoading = false;

                                    }else{
                                        if (PAGE_SIZE==0){
                                            rv_mycampus_news.setVisibility(View.GONE);
                                            ErrorText.setVisibility(View.VISIBLE);
                                            ErrorText.setText("No Post");
                                            swipeContainer.setRefreshing(false);
                                            mShimmerViewContainer.stopShimmerAnimation();
                                            mShimmerViewContainer.setVisibility(View.GONE);

                                        }else
                                            campusfeedadapter.removeProgress();

                                    }


                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());
                                    swipeContainer.setRefreshing(false);
                                    ReloadProgress.setVisibility(View.VISIBLE);
                                    mShimmerViewContainer.stopShimmerAnimation();
                                    mShimmerViewContainer.setVisibility(View.GONE);



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }
}
