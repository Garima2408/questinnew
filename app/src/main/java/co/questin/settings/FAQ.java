package co.questin.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ExpandableListView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.FAQExpandableAdapter;
import co.questin.library.StateBean;
import co.questin.models.FaqExpandableChildArray;
import co.questin.models.FaqExpandableparentArray;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class FAQ extends BaseAppCompactActivity {
    ExpandableListView expandableListView;
    FAQExpandableAdapter expandableListAdapter;
    private ArrayList<FaqExpandableChildArray> goalList;
    private List<FaqExpandableparentArray> expandableList;
    FloatingActionButton fab;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        fab = findViewById(R.id.fab);
        expandableListView = findViewById(R.id.expandableListView);

        expandableList = new ArrayList<FaqExpandableparentArray>();
        ArrayList<FaqExpandableChildArray> goalList = null;


        ProgressFAQInfo();




        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity (new Intent(FAQ.this, EmailUs.class));

            }
        });
    }

    private void ProgressFAQInfo() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_FAQ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray jsonArrayData = obj.getJSONArray("data");

                                    for (int i = 0; i < jsonArrayData.length(); i++) {

                                        ArrayList<FaqExpandableChildArray> goalList = new ArrayList<>();

                                        FaqExpandableChildArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), FaqExpandableChildArray.class);
                                        goalList.add(CourseInfo);
                                        FaqExpandableparentArray habitParentBean = new FaqExpandableparentArray();
                                        habitParentBean.parentTitle = jsonArrayData .getJSONObject(i).getString("question");
                                        habitParentBean.childsTaskList.addAll(goalList);
                                        expandableList.add(habitParentBean);


                                        expandableListAdapter = new FAQExpandableAdapter(getActivity(), expandableList);
                                        expandableListView.setIndicatorBounds(0, 20);
                                        expandableListView.setAdapter(expandableListAdapter);
                                        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                                            @Override
                                            public void onGroupExpand(int groupPosition) {
                                                int len = expandableListAdapter.getGroupCount();
                                                for (int i = 0; i < len; i++) {
                                                    if (i != groupPosition) {
                                                        //  expandableListAdapter.collapseGroup(i);
                                                    }
                                                }
                                            }
                                        });
                                    }



                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(FAQ.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(FAQ.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(FAQ.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);

}


    @Override
    public void onBackPressed() {

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


}
