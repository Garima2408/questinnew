package co.questin.calendersection;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.library.StateBean;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class EventDetailCollage extends BaseAppCompactActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener {
    Double Lat, Long;
    ImageView profileImage, backone;
    TextView Tittle, datte, eventdetailsText, eventlocation, dattestart;
    String Event_id, Eventtittle, EventLocations, EventDetails, EventTimes, EventEndDate;
    String eventstartdate, eventsatartime, eventenddate, eventendtime, eventSubscribe,Event_logo;
    //Google ApiClient
    private GoogleApiClient googleApiClient;
    //Our Map
    private GoogleMap mMap;

    //To store longitude and latitude from map
    private double longitude;
    private double latitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail_collage);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        backone = findViewById(R.id.backone);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        Bundle b = getActivity().getIntent().getExtras();
        Event_id = b.getString("EVENT_ID");
       // Event_logo = b.getString("EVENT_LOGO");


        Tittle = findViewById(R.id.Tittle);
        datte = findViewById(R.id.datte);
        eventdetailsText = findViewById(R.id.discription);
        eventlocation = findViewById(R.id.eventlocation);
        dattestart = findViewById(R.id.dattestart);
        profileImage = findViewById(R.id.profileImage);

        GetAllEventDetails();


        backone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void GetAllEventDetails() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_COLLAGEEVENTDETAILS + "/" + Event_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONObject data = obj.getJSONObject("data");

                                    Eventtittle = data.getString("title");
                                    EventDetails = data.getString("body");
                                    EventLocations = data.getString("field_discipline");
                                    eventSubscribe = data.getString("field_is_subscribe");

                                    JSONArray picture = data.getJSONArray("field_groups_logo");
                                    if(picture != null && picture.length() > 0 ) {
                                        for (int j = 0; j < picture.length(); j++) {
                                            Log.e("Media pictire", picture.getString(j));
                                            Event_logo =(picture.getString(0));

                                        }
                                    }

                                    JSONArray EventdatesArrays = data.getJSONArray("field_time");

                                    JSONObject lastObj = EventdatesArrays.getJSONObject(EventdatesArrays.length() - 1);
                                    EventEndDate = lastObj.getString("endDate");
                                    eventendtime = lastObj.getString("endTime");

                                    if (EventdatesArrays != null && EventdatesArrays.length() > 0) {

                                        for (int j = 0; j < EventdatesArrays.length(); j++) {
                                            String startDate = EventdatesArrays.getJSONObject(j).getString("startDate");
                                            eventsatartime = EventdatesArrays.getJSONObject(j).getString("startTime");
                                            String endDate = EventdatesArrays.getJSONObject(j).getString("endDate");
                                            //  eventendtime = EventdatesArrays.getJSONObject(j).getString("endTime");
                                            String Week = EventdatesArrays.getJSONObject(j).getString("Week");

                                            DateFormat inputFormat1 = new SimpleDateFormat("MMM-dd-yyyy");
                                            DateFormat outputFormat1 = new SimpleDateFormat("dd-MM-yyyy");

                                            Date datemainend;
                                            try {
                                                datemainend = inputFormat1.parse(startDate);
                                                String outputDateStr2 = outputFormat1.format(datemainend);
                                                eventstartdate = outputDateStr2;
                                                System.out.println(eventstartdate);


                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                            DateFormat inputFormat2 = new SimpleDateFormat(" MMM-dd-yyyy");
                                            DateFormat outputForma2 = new SimpleDateFormat("dd-MM-yyyy");

                                            Date datema;
                                            try {
                                                datema = inputFormat2.parse(EventEndDate);
                                                String outputDateStr3 = outputForma2.format(datema);
                                                eventenddate = outputDateStr3;
                                                System.out.println(eventenddate);


                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                            dattestart.setText(eventstartdate + "  " + eventsatartime);
                                            datte.setText(eventenddate + "  " + eventendtime);

                                            }
                                    }

                                    if (data.has("field_dept_location")) {
                                        JSONObject locate = data.getJSONObject("field_dept_location");
                                        if (locate != null && locate.length() > 0 ){
                                            Lat = Double.valueOf(locate.getString("lat"));
                                            Long = Double.valueOf(locate.getString("lng"));

                                            Log.d("TAG", "location: " + Lat + Long );

                                            }else {

                                        }
                                        }

                                    Tittle.setText(Html.fromHtml(Eventtittle));
                                    eventdetailsText.setText(Html.fromHtml("<p><b>"+Eventtittle+" "+ "</b><br/><br/>"+eventdetailsText));
                                    eventlocation.setText(Html.fromHtml(EventLocations));

                                    if(Event_logo != null && Event_logo.length() > 0 ) {

                                        Glide.with(EventDetailCollage.this).load(Event_logo)
                                                .placeholder(R.mipmap.college_events).dontAnimate()
                                                .skipMemoryCache(true)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .fitCenter().into(profileImage);

                                    }else {
                                        profileImage.setImageResource(R.mipmap.college_events);

                                    }


                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());

                                    }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(EventDetailCollage.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(EventDetailCollage.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(EventDetailCollage.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //adding the string request to request queue
        requestQueue.add(mStrRequest);

    }

    @Override
    protected void onStart() {
        googleApiClient.connect();

        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    //Getting current location
    private void getCurrentLocation() {
       // mMap.clear();
        //Creating a location object
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            Log.d("TAG", "latlong: " + longitude + " " + latitude);


            //moving the map to location
            moveMap();
        }
    }

    //Function to move the map
    private void moveMap() {
        //String to display current latitude and longitude
        String msg = latitude + ", " + longitude;
        Log.d("TAG", "msg: " + msg);


        //Creating a LatLng Object to store Coordinates


        if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null & SessionManager.getInstance(getActivity()).getCollage().getLng() != null) {
            Lat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat());
            Long = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng());
            LatLng latLng = new LatLng(Lat, Long);


            //Adding marker to map
            mMap.addMarker(new MarkerOptions()
                    .position(latLng) //setting position
                    .draggable(true) //Making the marker draggable
                    .title(SessionManager.getInstance(getActivity()).getCollage().getTitle())); //Adding a title
            //Moving the camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            //Animating the camera
            mMap.animateCamera(CameraUpdateFactory.zoomTo(20));

            //Displaying current coordinates in toast
            //  Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        } else {

            LatLng latLng = new LatLng(latitude, longitude);


            //Adding marker to map
            mMap.addMarker(new MarkerOptions()
                    .position(latLng) //setting position
                    .draggable(true) //Making the marker draggable
                    .title("My Location")); //Adding a title
            //Moving the camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            //Animating the camera
            mMap.animateCamera(CameraUpdateFactory.zoomTo(20));

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);
        mMap.getUiSettings().setScrollGesturesEnabled(false);

        getCurrentLocation();
        moveMap();
    }

    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        //Getting the coordinates
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;

        //Moving the map
        moveMap();
    }


    public void onSearch() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
              /*  if (Classlocation != null) {
                    Log.d("TAG", "recource3: " + Lat + Long);

                    LatLng latLng = new LatLng(Lat, Long);
                    mMap.addMarker(new MarkerOptions().position(latLng).title(SessionManager.getInstance(getActivity()).getCollage().getTitle()));
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                }*/
    }

    @Override
    public void onBackPressed() {
        finish();
    }






}