package co.questin.calendersection;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.alumni.AluminiProfile;
import co.questin.campusfeedsection.AllCampusFeeds;
import co.questin.chat.ChatTabbedActivity;
import co.questin.database.QuestinSQLiteHelper;
import co.questin.library.StateBean;
import co.questin.models.CalenderMonthlyArray;
import co.questin.network.URLS;
import co.questin.questinsitecontent.QuestinFeeds;
import co.questin.questinsitecontent.QuestinProfile;
import co.questin.studentprofile.ProfileDash;
import co.questin.teacher.TeacherProfile;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

public class CalenderMain extends BaseAppCompactActivity {
    RelativeLayout MainLayout;
    static String RunningMonth;
    static TextView TextError;
    static MenuItem itemevent,itemmydues,itemmyclass,itemmyevent,itemmyexam,itemweek,itemmonth,campus;
    private static MyDynamicCalendar myCalendar;
    public ArrayList<CalenderMonthlyArray> calenderdates;
    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2, floatingActionButton3,floatingActionButton4;
    public ArrayList<String> calenderList;
    public ArrayList<CalenderMonthlyArray> dates;
    public ArrayList<String>WeekDaylist ;
    private static QuestinSQLiteHelper questinSQLiteHelper;
    static Boolean CampusEvents,PersonalClass,PersonalExam,PersonalDues,PersonalEvent;
    static int id =1;
    private static ProgressBar spinner;
    static Activity activity;


    BottomNavigationView mBottomNavigationView;

    //private ListView listView;
    private SimpleDateFormat sdfMonthYear = new SimpleDateFormat("MMM - yyyy");
    private static List<EventModel>  eventModelListAssignment;
    private static List<EventModel>  eventModelListClass;
    private static List<EventModel>  eventModelListExam;
    private static List<EventModel>  eventModelListGroupEvent;
    public  static String  dateTimeCustomize,dateStartCustomize;
    private static long mRequestStartTime;
    String ActivityCheck;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        ActivityCheck = intent.getStringExtra("open");


        eventModelListAssignment=new ArrayList<>();
        eventModelListClass=new ArrayList<>();
        eventModelListExam=new ArrayList<>();
        eventModelListGroupEvent=new ArrayList<>();
        CampusEvents=true;
        questinSQLiteHelper = new QuestinSQLiteHelper(this);
        myCalendar = findViewById(R.id.myCalendar);
        TextError =findViewById(R.id.TextError);
        spinner= findViewById(R.id.progressBar);
        MainLayout = findViewById(R.id.MainLayout);


        calenderdates=new ArrayList<>();
        calenderList=new ArrayList<>();
        activity =getActivity();
        Setuponbottombar();
        myCalendar.deleteAllEvent();

        myCalendar.goToCurrentDate();
        myCalendar.goToCurrentDateMain();
        questinSQLiteHelper.deleteRecord();
        questinSQLiteHelper.deleteRecordAssignment();
        questinSQLiteHelper.deleteRecordExams();
        questinSQLiteHelper.deleteRecordFromMyCollageEvents();



        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM");
        RunningMonth = sdf.format(c.getTime());

        AppConstants.calendarFormate.set(Calendar.DATE, AppConstants.calendarFormate.getActualMinimum(Calendar.DATE));
        Date date = AppConstants.calendarFormate.getTime();
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
        dateStartCustomize= DATE_FORMAT.format(date);

        AppConstants.calendarFormate.set(Calendar.DATE, AppConstants.calendarFormate.getActualMaximum(Calendar.DATE));
        Date dateEnd = AppConstants.calendarFormate.getTime();
        dateTimeCustomize= DATE_FORMAT.format(dateEnd);

        //  myCalendar. setCurrentMonthDate();

        mRequestStartTime = System.currentTimeMillis();
        ProgressMonthlyDatesList(RunningMonth);


        myCalendar.showMonthViewWithBelowEvents();
        showMonthViewWithBelowEvents();



        myCalendar.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onClick(Date date) {
                Log.e("date", String.valueOf(date));
            }

            @Override
            public void onLongClick(Date date) {
                Log.e("date", String.valueOf(date));
            }
        });


        myCalendar.setCalendarBackgroundColor("#eeeeee");
        myCalendar.setHeaderBackgroundColor("#4fa8ce");
        myCalendar.setHeaderTextColor("#ffffff");
        myCalendar.setNextPreviousIndicatorColor("#245675");
        myCalendar.setWeekDayLayoutBackgroundColor("#ffffff");
        myCalendar.setWeekDayLayoutTextColor("#808080");
        myCalendar.setExtraDatesOfMonthBackgroundColor("#eaeaea");
        myCalendar.setExtraDatesOfMonthTextColor("#808080");
        myCalendar.setDatesOfMonthBackgroundColor("#ffffff");
        myCalendar.setDatesOfMonthTextColor("#000000");
        myCalendar.setCurrentDateTextColor("#F44336");
        myCalendar.setEventCellBackgroundColor("#4fa8ce");
        myCalendar.setEventCellTextColor("#425684");
        myCalendar.setBelowMonthEventTextColor("#425684");
        myCalendar.setBelowMonthEventDividerColor("#635478");
        myCalendar.setHolidayCellBackgroundColor("#F9BB9C");
        myCalendar.setHolidayCellTextColor("#d590bb");
        myCalendar.setHolidayCellClickable(false);

        materialDesignFAM = findViewById(R.id.material_design_android_floating_action_menu);
        floatingActionButton1 = findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = findViewById(R.id.material_design_floating_action_menu_item2);
        floatingActionButton3 = findViewById(R.id.material_design_floating_action_menu_item3);
        floatingActionButton4 = findViewById(R.id.material_design_floating_action_menu_item4);





        MainLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }
                return false;

            }
        });



        floatingActionButton1.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                startActivity(new Intent(CalenderMain.this, CalenderClasses.class));


            }
        });
        floatingActionButton2.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                startActivity(new Intent(CalenderMain.this, CalenderExam.class));

            }
        });
        floatingActionButton3.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                startActivity(new Intent(CalenderMain.this, CalenderDues.class));

            }
        });
        floatingActionButton4.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                startActivity(new Intent(CalenderMain.this, CelenderEvents.class));

            }
        });

    }

    public static void GetTheRunningMonth(String format) {

        // tv_month_year.setText(sdfMonthYear.format(AppConstants.main_calendar.getTime()));
        // Log.d("TAG", "currentmonth  " + ""+sdfMonthYear.format(AppConstants.main_calendar.getTime()));


        Log.d("TAG", "currentmonthget  " + ""+ format);

        RunningMonth =format;

        myCalendar.deleteAllEvent();
        questinSQLiteHelper.deleteRecord();
        questinSQLiteHelper.deleteRecordAssignment();
        questinSQLiteHelper.deleteRecordExams();
        questinSQLiteHelper.deleteRecordFromMyCollageEvents();


        if(CampusEvents==true){
            myCalendar.deleteAllEvent();
            ProgressMonthlyDatesList(RunningMonth);


        }else if(PersonalClass==true) {
            myCalendar.deleteAllEvent();
            if (SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {
                ProgressMonthlyMyClassesDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getParentUid());

            } else{

                ProgressMonthlyMyClassesDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getUserprofile_id());

            }




        }else if(PersonalExam==true){
            myCalendar.deleteAllEvent();
            if (SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {
                ProgressMonthlyMyExamsDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getParentUid());

            } else{

                ProgressMonthlyMyExamsDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getUserprofile_id());

            }



        }else if(PersonalDues==true){
            myCalendar.deleteAllEvent();
            if (SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {
                ProgressMonthlyMyduesDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getParentUid());

            } else{

                ProgressMonthlyMyduesDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getUserprofile_id());

            }

        }else if(PersonalEvent==true){
            myCalendar.deleteAllEvent();

            if (SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {
                ProgressMonthlyMyEventsDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getParentUid());

            } else{

                ProgressMonthlyMyEventsDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getUserprofile_id());

            }


        }





    }


    private void Setuponbottombar() {


        TextView dashboard_child = findViewById(R.id.dashboard_child);
        final TextView calendar = findViewById(R.id.calendar);
        TextView campus_feeds = findViewById(R.id.campus_feeds);
        TextView message_feeds = findViewById(R.id.message_feeds);
        TextView profile = findViewById(R.id.profile);

        dashboard_child.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCheck.matches("AsQuestinSite")){
                    finish();
                }else if (ActivityCheck.matches("AsSelectedSite")){
                    finish();
                }







            }
        });

        calendar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });

        campus_feeds.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCheck.matches("AsQuestinSite")){
                    Intent y = new Intent(CalenderMain.this,QuestinFeeds.class);
                    startActivity(y);
                    finish();

                }else if (ActivityCheck.matches("AsSelectedSite")){
                    Intent y = new Intent(CalenderMain.this,AllCampusFeeds.class);
                    startActivity(y);
                    finish();

                }



            }
        });

        message_feeds.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCheck.matches("AsQuestinSite")){
                    Intent y = new Intent(CalenderMain.this,ChatTabbedActivity.class)
                            .putExtra("open", "AsQuestinSite");
                    startActivity(y);
                    finish();

                }else if (ActivityCheck.matches("AsSelectedSite")){
                    Intent y = new Intent(CalenderMain.this,ChatTabbedActivity.class)
                            .putExtra("open", "AsSelectedSite");
                    startActivity(y);
                    finish();

                }


            }
        });

        profile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCheck.matches("AsQuestinSite")){
                    Intent j = new Intent(CalenderMain.this, QuestinProfile.class);
                    startActivity(j);
                    finish();


                }else if (ActivityCheck.matches("AsSelectedSite")){

                    if (SessionManager.getInstance(getActivity()).getUserClgRole()!=null) {

                        if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("13")) {
                            Intent j = new Intent(CalenderMain.this, ProfileDash.class);
                            startActivity(j);
                            finish();

                        } else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("14")) {
                            Intent j = new Intent(CalenderMain.this, TeacherProfile.class);
                            startActivity(j);
                            finish();

                        } else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("15")) {
                            Intent j = new Intent(CalenderMain.this, AluminiProfile.class);
                            startActivity(j);
                            finish();

                        }  else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("3")) {
                            Intent j = new Intent(CalenderMain.this, TeacherProfile.class);
                            startActivity(j);
                            finish();

                        }
                    }
                }










            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.calender_menu, menu);
        itemmonth = menu.findItem(R.id.month);
        itemweek = menu.findItem(R.id.week);
        itemevent = menu.findItem(R.id.action_collageevent);
        campus =menu.findItem(R.id.campus);
        itemmyevent = menu.findItem(R.id.event_menupersonal);
        itemmyclass = menu.findItem(R.id.class_menupersonal);
        itemmydues = menu.findItem(R.id.Dues_menupersonal);
        itemmyexam = menu.findItem(R.id.exams_menupersonal);


        if (ActivityCheck.matches("AsQuestinSite")){
            itemevent.setEnabled(false);
            itemevent.setVisible(false);
            campus.setVisible(false);

        }else if (ActivityCheck.matches("AsSelectedSite")){


        }

         if(CampusEvents==true){
            itemevent.setChecked(true);


        }else if(PersonalClass==true){
            itemmyclass.setChecked(true);

        }else if(PersonalExam==true){
            itemmyexam.setChecked(true);

        }else if(PersonalDues==true){
            itemmydues.setChecked(true);

        }else if(PersonalEvent==true){

        }



        return true;
    }


    public void onColorGroupItemClick1(MenuItem item) {
        // If red color selected
        if (item.getItemId() == R.id.month) {

            item.setChecked(true);

        }else {

            // Do nothing

        }
    }


    public void onColorGroupItemClick2(MenuItem item) {
        // If red color selected
        if (item.getItemId() == R.id.week) {
           /* Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

            myCalendar.deleteAllEvent();*/
           /* if (item.isChecked()) item.setChecked(false);
            else item.setChecked(true);
*/
            Intent intent = new Intent(getActivity(), WeekViewDisplay.class);
            startActivity(intent);
        }else {
            // Do nothing
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                myCalendar.deleteAllEvent();
                questinSQLiteHelper.deleteRecord();
                questinSQLiteHelper.deleteRecordAssignment();
                questinSQLiteHelper.deleteRecordExams();
                questinSQLiteHelper.deleteRecordFromMyCollageEvents();
                item.setChecked(false);
                finish();
                return true;

            case R.id.action_today:

                myCalendar.deleteAllEvent();
                myCalendar.goToCurrentDate();

                 if(CampusEvents==true){
                    ProgressMonthlyDatesList(RunningMonth);


                }else if(PersonalClass==true){

                    if (SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {
                        ProgressMonthlyMyClassesDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getParentUid());

                    } else{

                        ProgressMonthlyMyClassesDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getUserprofile_id());

                    }
                }else if(PersonalExam==true){

                    if (SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {
                        ProgressMonthlyMyExamsDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getParentUid());

                    } else{

                        ProgressMonthlyMyExamsDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getUserprofile_id());

                    }
                }else if(PersonalDues==true){

                    if (SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {
                        ProgressMonthlyMyduesDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getParentUid());

                    } else{

                        ProgressMonthlyMyduesDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getUserprofile_id());

                    }

                }else if(PersonalEvent==true){

                    if (SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {
                        ProgressMonthlyMyEventsDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getParentUid());

                    } else{

                        ProgressMonthlyMyEventsDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getUserprofile_id());

                    }
                }


                return true;


            case R.id.action_collageevent:
                CampusEvents =true;
                PersonalClass =false;
                PersonalExam=false;
                PersonalDues=false;
                PersonalEvent=false;


                myCalendar.deleteAllEvent();

                getDataFromCollageEvents();
                if (item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                return true;

            case R.id.event_menupersonal:
                PersonalEvent=true;
                PersonalClass =false;
                PersonalExam=false;
                PersonalDues=false;
                CampusEvents =false;

                myCalendar.deleteAllEvent();
                if (item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                if (SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {
                    ProgressMonthlyMyEventsDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getParentUid());

                } else{

                    ProgressMonthlyMyEventsDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getUserprofile_id());

                }
                return true;

            case R.id.exams_menupersonal:
                PersonalExam=true;
                PersonalEvent=false;
                PersonalClass =false;
                PersonalDues=false;
                CampusEvents =false;

                myCalendar.deleteAllEvent();
                if (item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                if (SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {
                    ProgressMonthlyMyExamsDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getParentUid());

                } else{

                    ProgressMonthlyMyExamsDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getUserprofile_id());

                }
                return true;
            case R.id.Dues_menupersonal:
                PersonalDues=true;
                PersonalExam=false;
                PersonalEvent=false;
                PersonalClass =false;
                CampusEvents =false;


                myCalendar.deleteAllEvent();
                if (item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                if (SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {
                    ProgressMonthlyMyduesDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getParentUid());

                } else{

                    ProgressMonthlyMyduesDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getUserprofile_id());

                }
                return true;

            case R.id.class_menupersonal:
                PersonalClass =true;
                PersonalExam=false;
                PersonalDues=false;
                PersonalEvent=false;
                CampusEvents =false;

                myCalendar.deleteAllEvent();
                if (item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                if (SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {
                    ProgressMonthlyMyClassesDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getParentUid());

                } else{

                    ProgressMonthlyMyClassesDatesList(RunningMonth,SessionManager.getInstance(activity).getUser().getUserprofile_id());

                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }



    public static void showMonthViewWithBelowEvents() {

        myCalendar.showMonthViewWithBelowEvents();

        myCalendar.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onClick(Date date) {
                Log.e("date", String.valueOf(date));




            }

            @Override
            public void onLongClick(Date date) {
                Log.e("date", String.valueOf(date));
            }
        });

    }



    /*collage calender...*/



    private static void ProgressMonthlyDatesList(String RunningMonth) {
        if (Build.VERSION.SDK_INT >= 11)
        {
            activity.invalidateOptionsMenu();
        }
        ShowIndicator();

        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_CALENDERMONTLYDATES+"/"+SessionManager.getInstance(activity).getUser().getUserprofile_id()+"?"+"month="+RunningMonth+"&type=month",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    if (eventModelListExam.size()>0){
                                        eventModelListExam.clear();
                                    }
                                    if (eventModelListAssignment.size()>0){
                                        eventModelListAssignment.clear();
                                    }
                                    if (eventModelListClass.size()>0){
                                        eventModelListClass.clear();
                                    }
                                    if (eventModelListGroupEvent.size()>0){
                                        eventModelListGroupEvent.clear();
                                    }
                                    JSONArray data = obj.getJSONArray("data");

                                    for (int i = 0; i < data.length(); i++) {


                                        String Eventtittle = data.getJSONObject(i).getString("title");
                                        String MainIdDisplay = data.getJSONObject(i).getString("nid");
                                        String type = data.getJSONObject(i).getString("type");
                                        String EventBody = data.getJSONObject(i).getString("body");
                                        Log.d("TAG", "type: " + type);
                                       /* if(type.contains("classes")){

                                            JSONArray jsonArrayDates = new JSONArray(data.getJSONObject(i).getString("field_class_time"));

                                            for (int j = 0; j <jsonArrayDates.length(); j++) {
                                                String EventStartDate = jsonArrayDates.getJSONObject(j).getString("startDate");
                                                String   EventTimeStart = jsonArrayDates.getJSONObject(j).getString("startTime");
                                                String EventendDate = jsonArrayDates.getJSONObject(j).getString("endDate");
                                                String   EventtimeEnd = jsonArrayDates.getJSONObject(j).getString("endTime");
                                                String  EventWeek  = jsonArrayDates.getJSONObject(j).getString("Week");


                                                List<String> myList = new ArrayList<String>(Arrays.asList(EventWeek.split(",")));
                                                System.out.println(myList);
                                                try {
                                                    getTheEventRange(Eventtittle,EventStartDate,EventendDate,EventTimeStart,EventtimeEnd,EventBody,myList,MainIdDisplay);

                                                }catch (Exception e){
                                                }


                                            }



                                        }else if(type.contains("assignment")) {
                                            JSONArray jsonArrayAssignments = new JSONArray(data.getJSONObject(i).getString("field_date_of_birth"));

                                            for (int j = 0; j < jsonArrayAssignments.length(); j++) {
                                                String AssignmentStartDate = jsonArrayAssignments.getJSONObject(j).getString("startDate");
                                                String  AssignmentTimeStart = jsonArrayAssignments.getJSONObject(j).getString("startTime");
                                                String AssignmentENDDate = jsonArrayAssignments.getJSONObject(j).getString("startDate");
                                                String AssignmenttimeEnd = jsonArrayAssignments.getJSONObject(j).getString("startTime");

                                                questinSQLiteHelper.insertcalenderAssignment(new EventModel(AssignmentStartDate,AssignmentENDDate,AssignmentTimeStart,AssignmenttimeEnd,Eventtittle,EventBody,MainIdDisplay));

                                                Log.d("TAG", "insertAssignment: "+AssignmentStartDate+AssignmentENDDate+AssignmentTimeStart+AssignmenttimeEnd+Eventtittle+EventBody);

                                                eventModelListAssignment.add(new EventModel(AssignmentStartDate,AssignmentENDDate,AssignmentTimeStart,AssignmenttimeEnd,Eventtittle,EventBody,MainIdDisplay));




                                            }
                                        }else if(type.contains("exam")) {
                                            String Duration = data.getJSONObject(i).getString("field_order");
                                            JSONArray jsonArrayExams = new JSONArray(data.getJSONObject(i).getString("field_exam_time"));

                                            for (int j = 0; j < jsonArrayExams.length(); j++) {
                                                String ExamStartDate = jsonArrayExams.getJSONObject(j).getString("startDate");
                                                String ExamTimeStart = jsonArrayExams.getJSONObject(j).getString("startTime");
                                                String ExamEndDate = jsonArrayExams.getJSONObject(j).getString("startDate");
                                                String ExamtimeEnd = jsonArrayExams.getJSONObject(j).getString("startTime");

                                                questinSQLiteHelper.insertcalenderExams(new EventModel(ExamStartDate,ExamEndDate,ExamTimeStart,Duration,Eventtittle,EventBody,MainIdDisplay));

                                                Log.d("TAG", "insertExams: "+ExamStartDate+ExamTimeStart+ExamEndDate+Duration+Eventtittle+EventBody);

                                                eventModelListExam.add(new EventModel(ExamStartDate,
                                                        ExamEndDate,ExamTimeStart,ExamtimeEnd,Eventtittle,EventBody,MainIdDisplay));

                                            }*/
                                        if(type.contains("group_events")) {


                                            JSONArray jsonArrayCollageEvent = new JSONArray(data.getJSONObject(i).getString("field_time"));

                                            for (int j = 0; j < jsonArrayCollageEvent.length(); j++) {
                                                String CollageEventStartDate = jsonArrayCollageEvent.getJSONObject(j).getString("startDate");
                                                String CollagEventTimeStart = jsonArrayCollageEvent.getJSONObject(j).getString("startTime");
                                                String CollagEventendDate = jsonArrayCollageEvent.getJSONObject(j).getString("endDate");
                                                String CollagEventtimeEnd = jsonArrayCollageEvent.getJSONObject(j).getString("endTime");
                                                String CollagEventWeek = jsonArrayCollageEvent.getJSONObject(j).getString("Week");


                                                List<String> myEventList = new ArrayList<String>(Arrays.asList(CollagEventWeek.split(",")));
                                                System.out.println(myEventList);

                                                getTheCollageEventRange(Eventtittle, CollageEventStartDate, CollagEventendDate, CollagEventTimeStart, CollagEventtimeEnd, EventBody, myEventList, MainIdDisplay);

                                                Log.d("TAG", "insertCollageEvent: "+CollageEventStartDate+CollagEventendDate+CollagEventTimeStart+CollagEventtimeEnd+Eventtittle+EventBody);

                                            }
                                        }

                                    }


                                    if(CampusEvents==true){
                                        getDataFromCollageEvents();


                                    }else if(PersonalClass==true){

                                    }else if(PersonalExam==true){

                                    }else if(PersonalDues==true){

                                    }else if(PersonalEvent==true){

                                    }



                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);

    }





    /*classes list*/

    private static void getTheEventRange(String eventtittle, String eventStartDate, String eventendDate, String eventTimeStart, String eventtimeEnd, String eventBody, List<String> eventWeek, String mainIdDisplay) {

        List<Date> dates = new ArrayList<Date>();
        dates.clear();
        SimpleDateFormat formatter ;

        formatter = new SimpleDateFormat("dd-MM-yyyy");

        Date  startDate = null;
        try {
//            date1.compareTo(date2); //date1 < date2, returns less than 0
//            date2.compareTo(date1); //date2 > date1, returns greater than 0

//            if (formatter.parse(eventStartDate).compareTo(formatter.parse(dateStartCustomize))<0){
//                startDate = (Date)formatter.parse(dateStartCustomize);
//            } else{
//                startDate = (Date)formatter.parse(eventStartDate);
//            }
            try{
                if (formatter.parse(eventStartDate).before(formatter.parse(dateStartCustomize))){
                    startDate = formatter.parse(dateStartCustomize);
                } else{
                    startDate = formatter.parse(eventStartDate);
                }
            }catch (NullPointerException e){
                e.printStackTrace();
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }



        Date  endDate = null;
        try {
            try{
                if (formatter.parse(eventendDate).before(formatter.parse(dateTimeCustomize))){
                    endDate = formatter.parse(eventendDate);
                } else{
                    endDate = formatter.parse(dateTimeCustomize);
                }
            }catch (NullPointerException e) {
                e.printStackTrace();

            }

            //  endDate = (Date)formatter.parse(dateTimeCustomize);

            // endDate = (Date)formatter.parse(eventendDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }




        long interval = 24*1000 * 60 * 60; // 1 hour in millis
        long endTime =endDate.getTime() ; // create your endtime here, possibly using Calendar or Date
        long curTime = startDate.getTime();
        while (curTime <= endTime) {
            dates.add(new Date(curTime));
            curTime += interval;
        }



        String[] outputStrArr = new String[eventWeek.size()];

        for(int i=0;i<dates.size();i++) {
            Date lDate = dates.get(i);
            String SelectedDate = formatter.format(lDate);

            DateFormat format2 = new SimpleDateFormat("EEE");
            String finalDay1 = format2.format(lDate);
            Log.d("TAG", "dateweek1: "+eventtittle + SelectedDate + finalDay1);



            for (int x = 0; x < eventWeek.size(); x++) {
                outputStrArr[x] = eventWeek.get(x);
                Log.d("TAG", " outputStrArr[x]: "+ outputStrArr[x]);

                if(outputStrArr[x].contains(finalDay1)){

                    Log.d("TAG", "dateweek2: "+eventtittle + SelectedDate + finalDay1+"true");

                    questinSQLiteHelper.insertcalenderevent(new EventModel(SelectedDate,eventendDate,eventTimeStart,eventtimeEnd,eventtittle,eventBody,mainIdDisplay));

                    Log.d("TAG", "insert: "+SelectedDate+eventendDate+eventTimeStart+eventtimeEnd+eventtittle+eventBody);

                    eventModelListClass.add(new EventModel(SelectedDate,eventendDate,eventTimeStart,eventtimeEnd,eventtittle,eventBody,mainIdDisplay));
                }


                else  {



                }


            }

        }

    }




    /*insert data in collage database*/


    private static void getTheCollageEventRange(String eventtittle, String collageEventStartDate, String collagEventendDate, String collagEventTimeStart, String collagEventtimeEnd, String eventBody, List<String> myEventList, String mainIdDisplay) {

        List<Date> dates = new ArrayList<Date>();
        dates.clear();
        DateFormat formatter ;

        formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date  startDate = null;
        try {
            startDate = formatter.parse(collageEventStartDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }



        Date  endDate = null;
        try {
            endDate = formatter.parse(collagEventendDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }




        long interval = 24*1000 * 60 * 60; // 1 hour in millis
        long endTime =endDate.getTime() ; // create your endtime here, possibly using Calendar or Date
        long curTime = startDate.getTime();
        while (curTime <= endTime) {
            dates.add(new Date(curTime));
            curTime += interval;
        }



        String[] outputStrArr = new String[myEventList.size()];

        for(int i=0;i<dates.size();i++) {
            Date lDate = dates.get(i);
            String SelectedDate = formatter.format(lDate);

            DateFormat format2 = new SimpleDateFormat("EEE");
            String finalDay1 = format2.format(lDate);
            Log.d("TAG", "dateweek1: "+eventtittle + SelectedDate + finalDay1);



            for (int x = 0; x < myEventList.size(); x++) {
                outputStrArr[x] = myEventList.get(x);
                Log.d("TAG", " outputStrArr[x]: "+ outputStrArr[x]);



                Log.d("TAG", "dateweek2: "+eventtittle + SelectedDate + finalDay1+"true");

                questinSQLiteHelper.insertcollagecalenderevent(new EventModel(SelectedDate,collagEventendDate,collagEventTimeStart,collagEventtimeEnd,eventtittle,eventBody,mainIdDisplay));


                eventModelListGroupEvent.add(new EventModel(SelectedDate,collagEventendDate,collagEventTimeStart,
                        collagEventtimeEnd,eventtittle,eventBody,mainIdDisplay));
                Log.d("TAG", "insertcollageevent: "+SelectedDate+collagEventendDate+collagEventTimeStart+collagEventtimeEnd+eventtittle+eventBody);





            }

        }
    }








    /*eventlist*/

    private static void getDataFromDatabaseEvents(List<EventModel> eventModelList) {


        Log.d("Reading: ", "Reading all data..");
        // List<EventModel> val = questinSQLiteHelper.getAllvalues();
        List<EventModel> val = eventModelList;

        for (EventModel cn : val) {

            String log = "Id: " + cn.getId() + " ,values: "
                    + cn.getStrDate() + cn.getStrDateEnd()+cn.getStrStartTime()+cn.getStrEndTime()+cn.getStrName()+cn.getStrDetails()+cn.getDisplayid();

            String log2 = cn.getStrDate()+cn.getStrStartTime()+cn.getStrEndTime()+cn.getStrName();
            Log.d("value: ", log);
            Log.d("value: ", log2);

            AddEvents(cn.getStrDate(),cn.getStrStartTime(),cn.getStrEndTime(),cn.getStrName(),cn.getDisplayid(),"true");


        }
    }

    private static void AddEvents(String eventStartDate, String eventTimeStart, String eventtimeEnd, String eventtittle, String displayid, String aTrue) {
        myCalendar.addEvent(id,eventStartDate, eventTimeStart, eventtimeEnd, eventtittle,displayid,aTrue,"class");

        myCalendar.getEventList(new GetEventListListener() {
            @Override
            public void eventList(ArrayList<EventModel> eventList) {

                /* Log.e("tag", "eventList.size():-" + eventList.size());
                for (int i = 0; i < eventList.size(); i++) {
                }*/

            }
        });

        myCalendar.showMonthViewWithBelowEvents();
        showMonthViewWithBelowEvents();

    }

    /*collageevent*/




    private static void getDataFromCollageEvents() {

        Log.d("Reading: ", "Reading all datafromcollage..");
        //   List<EventModel> val = questinSQLiteHelper.getAllCollageEvent();
        List<EventModel> val = eventModelListGroupEvent;


        for (EventModel cn : val) {
            String log = "Id: " + cn.getId() + " ,values: "
                    + cn.getStrDate() + cn.getStrDateEnd()+cn.getStrStartTime()+cn.getStrEndTime()+cn.getStrName()+cn.getStrDetails()+cn.getDisplayid();

            String log2 = cn.getStrDate()+cn.getStrStartTime()+cn.getStrEndTime()+cn.getStrName();
            Log.d("value: ", log);
            Log.d("value: ", log2);

            AddCollageEvents(cn.getStrDate(),cn.getStrStartTime(),cn.getStrEndTime(),cn.getStrName(),cn.getDisplayid(),"true");


        }
    }



    private static void AddCollageEvents(String eventStartDate, String eventTimeStart, String eventtimeEnd, String eventtittle, String displayid, String aTrue) {
        myCalendar.addEvent(id,eventStartDate, eventTimeStart, eventtimeEnd, eventtittle,displayid,aTrue,"collageevent");

        myCalendar.getEventList(new GetEventListListener() {
            @Override
            public void eventList(ArrayList<EventModel> eventList) {

                /* Log.e("tag", "eventList.size():-" + eventList.size());
                for (int i = 0; i < eventList.size(); i++) {
                }*/

            }
        });

        myCalendar.showMonthViewWithBelowEvents();
        showMonthViewWithBelowEvents();

    }









    /*assignment list*/


    private static void getDataFromDatabaseAssignment() {

        Log.d("Readingfromassignment: ", "Reading all data..");
        // List<EventModel> val = questinSQLiteHelper.getAllvaluesAssignment();
        List<EventModel> val = eventModelListAssignment;

        for (EventModel cn : val) {
            String log = "Id: " + cn.getId() + " ,valuesassign1: "
                    + cn.getStrDate() + cn.getStrDateEnd()+cn.getStrStartTime()+cn.getStrEndTime()+cn.getStrName()+cn.getStrDetails()+cn.getDisplayid();

            String log2 = cn.getStrDate()+cn.getStrStartTime()+cn.getStrEndTime()+cn.getStrName();
            Log.d("valueassign2: ", log);
            Log.d("valueassign3: ", log2);

            AddEventsInAssignment(cn.getStrDate(),cn.getStrStartTime(),cn.getStrEndTime(),cn.getStrName(),cn.getDisplayid(),"true");


        }
    }





    private static void AddEventsInAssignment(String assignmentStartDate, String assignmentTimeStart, String assignmenttimeEnd, String eventtittle, String displayid, String aTrue) {
        myCalendar.addEvent(id,assignmentStartDate, assignmentTimeStart, assignmenttimeEnd, eventtittle,displayid,aTrue,"assign");

        myCalendar.getEventList(new GetEventListListener() {
            @Override
            public void eventList(ArrayList<EventModel> eventList) {

                // Log.e("tag", "eventList.size():-" + eventList.size());
                for (int i = 0; i < eventList.size(); i++) {
                    //  Log.e("tag", "eventList.getStrName:-" + eventList.get(i).getStrName());
                }

            }
        });

        myCalendar.showMonthViewWithBelowEvents();
        showMonthViewWithBelowEvents();

    }





    private static void getDataFromDatabaseExams() {

        Log.d("Reading: ", "Reading all data..");
        // List<EventModel> val = questinSQLiteHelper.getAllvaluesExams();
        List<EventModel> val = eventModelListExam;
        for (EventModel cn : val) {
            String log = "Id: " + cn.getId() + " ,values: "
                    + cn.getStrDate() + cn.getStrDateEnd()+cn.getStrStartTime()+cn.getStrEndTime()+cn.getStrName()+cn.getStrDetails()+cn.getDisplayid();

            String log2 = cn.getStrDate()+cn.getStrStartTime()+cn.getStrEndTime()+cn.getStrName();
            Log.d("value: ", log);
            Log.d("value: ", log2);

            AddEventsInExams(cn.getStrDate(),cn.getStrStartTime(),cn.getStrEndTime(),cn.getStrName(),cn.getDisplayid(),"true");


        }
    }



    private static void AddEventsInExams(String examStartDate, String examTimeStart, String examtimeEnd, String eventtittle, String displayid, String aTrue) {
        myCalendar.addEvent(id,examStartDate, examTimeStart, examtimeEnd, eventtittle,displayid,aTrue,"exams");

        myCalendar.getEventList(new GetEventListListener() {
            @Override
            public void eventList(ArrayList<EventModel> eventList) {

                // Log.e("tag", "eventList.size():-" + eventList.size());
                for (int i = 0; i < eventList.size(); i++) {
                    //  Log.e("tag", "eventList.getStrName:-" + eventList.get(i).getStrName());
                }

            }
        });

        myCalendar.showMonthViewWithBelowEvents();
        showMonthViewWithBelowEvents();

    }










    /*my personal events  in calender*/


    /*mypersonal classesss*/

    private static void ProgressMonthlyMyClassesDatesList(String runningMonth, String userprofile_id) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_MYCALENDERMONTLYDATES+"/"+userprofile_id+"?"+"month="+runningMonth+"&type=month",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);

                                    JSONArray data = obj.getJSONArray("data");

                                    for (int i = 0; i < data.length(); i++) {


                                        String Eventtittle = data.getJSONObject(i).getString("title");
                                        String MainIdDisplay = data.getJSONObject(i).getString("nid");
                                        String type = data.getJSONObject(i).getString("field_my_content_type");
                                        String EventBody = data.getJSONObject(i).getString("body");

                                        Log.d("TAG", "type: " + type);
                                        if (type.contains("class")) {
                                            JSONArray jsonArrayDates = new JSONArray(data.getJSONObject(i).getString("field_class_time"));
                                            JSONObject lastObj = jsonArrayDates.getJSONObject(jsonArrayDates.length() - 1);
                                            String ClassEndDate = lastObj.getString("endDate");
                                            Log.d("TAG", "ClassEndDatedddd: " + ClassEndDate);

                                            for (int j = 0; j < jsonArrayDates.length(); j++) {
                                                String startdate = jsonArrayDates.getJSONObject(j).getString("startDate");
                                                String EventTimeStart = jsonArrayDates.getJSONObject(j).getString("startTime");
                                                String enddate = jsonArrayDates.getJSONObject(j).getString("endDate");
                                                String EventtimeEnd = jsonArrayDates.getJSONObject(j).getString("endTime");
                                                String EventWeek = jsonArrayDates.getJSONObject(j).getString("Week");


                                                List<String> myList = new ArrayList<String>(Arrays.asList(EventWeek.split(",")));
                                                System.out.println(myList);

                                                GetMyCalenderRange(Eventtittle, startdate, enddate, EventTimeStart, EventtimeEnd, EventBody, myList,MainIdDisplay,"false");
                                                getTheEventRange(Eventtittle,startdate,enddate,EventTimeStart,EventtimeEnd,EventBody,myList,MainIdDisplay);


                                            }
                                        }
                                    }



                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);

    }


    /*get the range between events my personal*/
    private static void GetMyCalenderRange(String eventtittle, String startdate, String enddate, String eventTimeStart, String eventtimeEnd, String body, List<String> myList, String nid, String aTrue) {


        List<Date> dates = new ArrayList<Date>();
        dates.clear();
        DateFormat formatter ;

        formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date  startDate = null;
        try {
            startDate = formatter.parse(startdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }



        Date  endDate = null;
        try {
            endDate = formatter.parse(enddate);
        } catch (ParseException e) {
            e.printStackTrace();
        }




        long interval = 24*1000 * 60 * 60; // 1 hour in millis
        long endTime =endDate.getTime() ; // create your endtime here, possibly using Calendar or Date
        long curTime = startDate.getTime();
        while (curTime <= endTime) {
            dates.add(new Date(curTime));
            curTime += interval;
        }



        String[] outputStrArr = new String[myList.size()];

        for(int i=0;i<dates.size();i++) {
            Date lDate = dates.get(i);
            String SelectedDate = formatter.format(lDate);

            DateFormat format2 = new SimpleDateFormat("EEE");
            String finalDay1 = format2.format(lDate);
            Log.d("TAG", "dateweek1: "+eventtittle + SelectedDate + finalDay1);



            for (int x = 0; x < myList.size(); x++) {
                outputStrArr[x] = myList.get(x);
                Log.d("TAG", " outputStrArr[x]: "+ outputStrArr[x]);

                if(outputStrArr[x].contains(finalDay1)){

                    Log.d("TAG", "dateweek2: "+eventtittle + SelectedDate + finalDay1+"true");

                    AddClassMypersonal(SelectedDate, eventTimeStart, eventtimeEnd, eventtittle,nid,aTrue);


                }else  {

                }
            }
        }
    }


    /*add class in my personal calender*/


    private static void AddClassMypersonal(String eventStartDate, String eventTimeStart, String eventtimeEnd, String eventtittle, String nid, String aTrue) {
        myCalendar.addEvent(id,eventStartDate, eventTimeStart, eventtimeEnd, eventtittle,nid,aTrue,"classpersonal");

        myCalendar.getEventList(new GetEventListListener() {
            @Override
            public void eventList(ArrayList<EventModel> eventList) {

                // Log.e("tag", "eventList.size():-" + eventList.size());
                for (int i = 0; i < eventList.size(); i++) {
                    //  Log.e("tag", "eventList.getStrName:-" + eventList.get(i).getStrName());
                }

            }
        });

        myCalendar.showMonthViewWithBelowEvents();
        showMonthViewWithBelowEvents();

    }




    /*my personal events in calender*/



    private static void ProgressMonthlyMyEventsDatesList(String runningMonth, String userprofile_id) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_MYCALENDERMONTLYDATES+"/"+userprofile_id+"?"+"month="+runningMonth+"&type=month",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray data = obj.getJSONArray("data");

                                    for (int i = 0; i < data.length(); i++) {
                                        String Eventtittle = data.getJSONObject(i).getString("title");
                                        String nid = data.getJSONObject(i).getString("nid");
                                        String type = data.getJSONObject(i).getString("field_my_content_type");
                                        String body = data.getJSONObject(i).getString("body");

                                        Log.d("TAG", "type: " + type);
                                        if (type.contains("event")) {
                                            JSONArray jsonArrayDates = new JSONArray(data.getJSONObject(i).getString("field_class_time"));

                                            for (int j = 0; j < jsonArrayDates.length(); j++) {
                                                String startdate = jsonArrayDates.getJSONObject(j).getString("startDate");
                                                String EventTimeStart = jsonArrayDates.getJSONObject(j).getString("startTime");
                                                String enddate = jsonArrayDates.getJSONObject(j).getString("endDate");
                                                String EventtimeEnd = jsonArrayDates.getJSONObject(j).getString("endTime");
                                                String EventWeek = jsonArrayDates.getJSONObject(j).getString("Week");


                                                List<String> myListEvent = new ArrayList<String>(Arrays.asList(EventWeek.split(",")));
                                                System.out.println(myListEvent);

                                                GetMyEventCalenderRange(Eventtittle, startdate, enddate, EventTimeStart, EventtimeEnd, body, myListEvent,nid,"false");


                                            }
                                        }
                                    }


                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);

    }







    /*get the range between events my personal*/
    private static void GetMyEventCalenderRange(String eventtittle, String startdate, String enddate, String eventTimeStart, String eventtimeEnd, String body, List<String> myList, String nid, String aTrue) {


        List<Date> datesEvent = new ArrayList<Date>();
        datesEvent.clear();
        DateFormat formatter ;

        formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date  startDate = null;
        try {
            startDate = formatter.parse(startdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }



        Date  endDate = null;
        try {
            endDate = formatter.parse(enddate);
        } catch (ParseException e) {
            e.printStackTrace();
        }




        long interval = 24*1000 * 60 * 60; // 1 hour in millis
        long endTime =endDate.getTime() ; // create your endtime here, possibly using Calendar or Date
        long curTime = startDate.getTime();
        while (curTime <= endTime) {
            datesEvent.add(new Date(curTime));
            curTime += interval;
        }



        String[] outputStrArr = new String[myList.size()];

        for(int i=0;i<datesEvent.size();i++) {
            Date lDate = datesEvent.get(i);
            String SelectedDate = formatter.format(lDate);

            DateFormat format2 = new SimpleDateFormat("EEE");
            String finalDay1 = format2.format(lDate);
            Log.d("TAG", "dateweek1: "+eventtittle + SelectedDate + finalDay1);



            for (int x = 0; x < myList.size(); x++) {
                outputStrArr[x] = myList.get(x);
                Log.d("TAG", " outputStrArr[x]: "+ outputStrArr[x]);

                if(outputStrArr[x].contains(finalDay1)){

                    Log.d("TAG", "dateweek2: "+eventtittle + SelectedDate + finalDay1+"true");

                    AddEventsMypersonal(SelectedDate, eventTimeStart, eventtimeEnd, eventtittle,nid,aTrue);



                }else  {

                }
            }
        }
    }


    /*add events in my personal calender*/


    private static void AddEventsMypersonal(String eventStartDate, String eventTimeStart, String eventtimeEnd, String eventtittle, String nid, String aTrue) {
        myCalendar.addEvent(id,eventStartDate, eventTimeStart, eventtimeEnd, eventtittle,nid,aTrue,"eventpersonal");

        myCalendar.getEventList(new GetEventListListener() {
            @Override
            public void eventList(ArrayList<EventModel> eventList) {

                // Log.e("tag", "eventList.size():-" + eventList.size());
                for (int i = 0; i < eventList.size(); i++) {
                    //  Log.e("tag", "eventList.getStrName:-" + eventList.get(i).getStrName());
                }

            }
        });

        myCalendar.showMonthViewWithBelowEvents();
        showMonthViewWithBelowEvents();

    }








    /*my personal dues in calender*/
    private static void ProgressMonthlyMyduesDatesList(String runningMonth, String userprofile_id) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_MYCALENDERMONTLYDATES+"/"+userprofile_id+"?"+"month="+runningMonth+"&type=month",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray data = obj.getJSONArray("data");
                                    for (int i = 0; i < data.length(); i++) {
                                        String Eventtittle = data.getJSONObject(i).getString("title");
                                        String MainIdDisplay = data.getJSONObject(i).getString("nid");
                                        String type = data.getJSONObject(i).getString("field_my_content_type");
                                        String EventBody = data.getJSONObject(i).getString("body");

                                        Log.d("TAG", "type: " + type);
                                        if (type.contains("due")) {
                                            JSONArray jsonArrayAssignments = new JSONArray(data.getJSONObject(i).getString("field_class_time"));

                                            for (int j = 0; j < jsonArrayAssignments.length(); j++) {
                                                String startdate = jsonArrayAssignments.getJSONObject(j).getString("startDate");
                                                String TimeStart = jsonArrayAssignments.getJSONObject(j).getString("startTime");
                                                String endDate = jsonArrayAssignments.getJSONObject(j).getString("startDate");
                                                String timeEnd = jsonArrayAssignments.getJSONObject(j).getString("startTime");

                                                //  AddEvents();
                                                AddDuesinMy(startdate, TimeStart, endDate, Eventtittle,MainIdDisplay,"false");
                                                questinSQLiteHelper.insertcalenderAssignment(new EventModel(startdate,endDate,TimeStart,timeEnd,Eventtittle,EventBody,MainIdDisplay));
                                                eventModelListAssignment.add(new EventModel(startdate,endDate,TimeStart,timeEnd,Eventtittle,EventBody,MainIdDisplay));


                                            }
                                        }
                                    }

                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);

    }



    private static void AddDuesinMy(String startdate, String timeStart, String endDate, String eventtittle, String nid, String aTrue) {

        myCalendar.addEvent(id,startdate, timeStart, endDate, eventtittle,nid,aTrue,"myduespersonal");

        myCalendar.getEventList(new GetEventListListener() {
            @Override
            public void eventList(ArrayList<EventModel> eventList) {

                // Log.e("tag", "eventList.size():-" + eventList.size());
                for (int i = 0; i < eventList.size(); i++) {
                    //  Log.e("tag", "eventList.getStrName:-" + eventList.get(i).getStrName());
                }

            }
        });

        myCalendar.showMonthViewWithBelowEvents();
        showMonthViewWithBelowEvents();


    }

    /*my personal exams in calender*/

    private static void ProgressMonthlyMyExamsDatesList(String runningMonth, String userprofile_id) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_MYCALENDERMONTLYDATES+"/"+userprofile_id+"?"+"month="+runningMonth+"&type=month",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);

                                    JSONArray data = obj.getJSONArray("data");
                                    for (int i = 0; i < data.length(); i++) {


                                        String Eventtittle = data.getJSONObject(i).getString("title");
                                        String MainIdDisplay = data.getJSONObject(i).getString("nid");
                                        String type = data.getJSONObject(i).getString("field_my_content_type");
                                        String EventBody = data.getJSONObject(i).getString("body");

                                        Log.d("TAG", "type: " + type);


                                        if (type.contains("exam")) {
                                            JSONArray jsonArrayExams = new JSONArray(data.getJSONObject(i).getString("field_class_time"));

                                            for (int j = 0; j < jsonArrayExams.length(); j++) {
                                                String myExamstartdate = jsonArrayExams.getJSONObject(j).getString("startDate");
                                                String myExamTimeStart = jsonArrayExams.getJSONObject(j).getString("startTime");
                                                String myExamendDate = jsonArrayExams.getJSONObject(j).getString("endDate");
                                                String myExamtimeEnd = jsonArrayExams.getJSONObject(j).getString("endTime");
                                                String myExamEventWeek = jsonArrayExams.getJSONObject(j).getString("Week");


                                                List<String> myListEvent = new ArrayList<String>(Arrays.asList(myExamEventWeek.split(",")));
                                                System.out.println(myListEvent);

                                                GetMyExamCalenderRange(Eventtittle, myExamstartdate, myExamendDate, myExamTimeStart, myExamtimeEnd, EventBody, myListEvent,MainIdDisplay,"false");
                                                questinSQLiteHelper.insertcalenderExams(new EventModel(myExamstartdate,myExamendDate,myExamTimeStart,myExamtimeEnd,Eventtittle,EventBody,MainIdDisplay));
                                                eventModelListExam.add(new EventModel(myExamstartdate,
                                                        myExamendDate,myExamTimeStart,myExamtimeEnd,Eventtittle,EventBody,MainIdDisplay));




                                            }
                                        }
                                    }


                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);

    }


    /*  /*get the range between exam my personal*/
    private static void GetMyExamCalenderRange(String eventtittle, String startdate, String enddate, String eventTimeStart, String eventtimeEnd, String body, List<String> myList, String nid, String aTrue) {


        List<Date> datesEvent = new ArrayList<Date>();
        datesEvent.clear();
        DateFormat formatter ;

        formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date  startDate = null;
        try {
            startDate = formatter.parse(startdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }



        Date  endDate = null;
        try {
            endDate = formatter.parse(enddate);
        } catch (ParseException e) {
            e.printStackTrace();
        }




        long interval = 24*1000 * 60 * 60; // 1 hour in millis
        long endTime =endDate.getTime() ; // create your endtime here, possibly using Calendar or Date
        long curTime = startDate.getTime();
        while (curTime <= endTime) {
            datesEvent.add(new Date(curTime));
            curTime += interval;
        }



        String[] outputStrArr = new String[myList.size()];

        for(int i=0;i<datesEvent.size();i++) {
            Date lDate = datesEvent.get(i);
            String SelectedDate = formatter.format(lDate);

            DateFormat format2 = new SimpleDateFormat("EEE");
            String finalDay1 = format2.format(lDate);
            Log.d("TAG", "dateweek1: "+eventtittle + SelectedDate + finalDay1);



            for (int x = 0; x < myList.size(); x++) {
                outputStrArr[x] = myList.get(x);
                Log.d("TAG", " outputStrArr[x]: "+ outputStrArr[x]);

                if(outputStrArr[x].contains(finalDay1)){

                    Log.d("TAG", "dateweek2: "+eventtittle + SelectedDate + finalDay1+"true");

                    // AddEventsMypersonal(SelectedDate, eventTimeStart, eventtimeEnd, eventtittle,nid,aTrue);
                    AddExamssMyClasses(SelectedDate, eventTimeStart, eventtimeEnd, eventtittle,nid,"false");



                }else  {

                }
            }
        }
    }



    private static void AddExamssMyClasses(String examStartDate, String examTimeStart, String examtimeEnd, String eventtittle, String nid, String aTrue) {
        myCalendar.addEvent(id,examStartDate, examTimeStart, examtimeEnd, eventtittle,nid,aTrue,"myexampersonal");

        myCalendar.getEventList(new GetEventListListener() {
            @Override
            public void eventList(ArrayList<EventModel> eventList) {

                // Log.e("tag", "eventList.size():-" + eventList.size());
                for (int i = 0; i < eventList.size(); i++) {
                    //  Log.e("tag", "eventList.getStrName:-" + eventList.get(i).getStrName());
                }

            }
        });

        myCalendar.showMonthViewWithBelowEvents();
        showMonthViewWithBelowEvents();

    }


    @Override
    protected void onPause() {
        super.onPause();

        RunningMonth="";
//         myCalendar.refreshCalendar();
        dateTimeCustomize="";
        dateStartCustomize="";

    }

    @Override
    public void onBackPressed() {
        myCalendar.deleteAllEvent();

        RunningMonth="";
        questinSQLiteHelper.deleteRecord();
        questinSQLiteHelper.deleteRecordAssignment();
        questinSQLiteHelper.deleteRecordExams();
        questinSQLiteHelper.deleteRecordFromMyCollageEvents();
        finish();
    }




}

