package co.questin.calendersection;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import co.questin.R;
import co.questin.library.StateBean;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;

import static co.questin.utils.Utils.isStatusSuccess;

public class UpdateCalenderEvent extends BaseAppCompactActivity  implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener, View.OnClickListener {
    TextView TV_StartDate,TV_StartTime,TV_endDate,TV_EndTime,TV_location;
    Switch AllDayOn;
    EditText edittittle,editDetails;
    String  Classes_Id,tittle,Details, timeEnd, StartDate, TimeStart,EndDate, startDate,  startfrom,format, weekmon, weektues, weekwedns, weekthur, weekfri, weeksat, weeksun, classrepeat;
    private int CalendarHour, CalendarMinute;
    TimePickerDialog timepickerdialog;
    private Calendar mcalender;
    CheckBox repeat,mon, tue, wed, thur, fri, sat, sun;
    LinearLayout Weeklayoutlayout;
    boolean isPressed = false;
    StringBuilder Weekselect;
    String time24format,time24formarend,
            DetailclassDate,DetailClassStart,DetailClassEndDate,DetailClassEnd,Detailweeknos,ClassEndDate;
    private int year, month, day, week;
    private int startDay, startMonth, startYear;
    Double Lat,Long;
    Double SelectedLat, SelectedLong;
    //Google ApiClient
    private GoogleApiClient googleApiClient;
    //Our Map
    private GoogleMap mMap;
    MenuItem shareItem;
    //To store longitude and latitude from map
    private double longitude;
    private double latitude;
    JSONObject ClassDetailsJson;
    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private boolean mLocationPermissionGranted;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    // Used for selecting the current place.
    private static final int PLACE_PICKER_REQUEST = 1000;
    RelativeLayout MapLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender_events_details);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();

        Bundle b = getActivity().getIntent().getExtras();
        Classes_Id = b.getString("CLASSES_ID");
        edittittle = findViewById(R.id.edittittle);
        editDetails = findViewById(R.id.Details);
        TV_StartDate = findViewById(R.id.StartDate);
        TV_endDate = findViewById(R.id.endDate);
        TV_StartTime = findViewById(R.id.StartTime);
        TV_EndTime = findViewById(R.id.EndTime);
        TV_location = findViewById(R.id.location);
        mon = findViewById(R.id.mon);
        tue = findViewById(R.id.tue);
        wed = findViewById(R.id.wed);
        thur = findViewById(R.id.thu);
        fri = findViewById(R.id.fri);
        sat = findViewById(R.id.sat);
        sun = findViewById(R.id.sun);
        repeat = findViewById(R.id.repeat);
        Weeklayoutlayout = findViewById(R.id.Weeklayoutlayout);
        TV_StartDate.setOnClickListener(this);
        TV_endDate.setOnClickListener(this);
        TV_StartTime.setOnClickListener(this);
        TV_EndTime.setOnClickListener(this);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String  formattedDate = sdf.format(c.getTime());

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate2 = sdf2.format(c.getTime());


        StartDate = formattedDate2;
        EndDate = formattedDate2;



        TV_StartDate.setText(formattedDate);
        TV_endDate.setText(formattedDate);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        String time = simpleDateFormat.format(c.getTime());

        TV_StartTime.setText(time);
        TV_EndTime.setText(time);
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date = null;
        try {
            date = parseFormat.parse(time);


        } catch (ParseException e) {
            e.printStackTrace();
        }
        String time24format = displayFormat.format(date);

        System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));

        TimeStart =time24format;
        timeEnd =time24format;
        repeat.setOnClickListener(this);
        mon.setOnClickListener(this);
        tue.setOnClickListener(this);
        wed.setOnClickListener(this);
        thur.setOnClickListener(this);
        fri.setOnClickListener(this);
        sat.setOnClickListener(this);
        sun.setOnClickListener(this);
        Weekselect =new StringBuilder();
        edittittle.setEnabled(false);
        editDetails.setEnabled(false);
        TV_StartDate.setEnabled(false);
        TV_StartTime.setEnabled(false);
        TV_endDate.setEnabled(false);
        TV_EndTime.setEnabled(false);
        TV_location.setEnabled(false);
        repeat.setClickable(false);
        GetAllServicesDetails();
        TV_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null & SessionManager.getInstance(getActivity()).getCollage().getLng() != null) {
                    SelectedLat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat());
                    SelectedLong = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng());

                }else {
                    SelectedLat = latitude;
                    SelectedLong = longitude;


                }


                LatLngBounds latLngBounds = new LatLngBounds(new LatLng(SelectedLat, SelectedLong),
                        new LatLng(SelectedLat, SelectedLong));
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                builder.setLatLngBounds(latLngBounds);

                try {
                    startActivityForResult(builder.build(UpdateCalenderEvent.this), PLACE_PICKER_REQUEST);
                } catch (Exception e) {
                    Log.e("TAG", e.getStackTrace().toString());
                }



            }
        });
    }
    private void GetAllServicesDetails() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.GET, URLS.URL_MYCLASSESDETAILS+ "/" +Classes_Id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {

                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    JSONObject obj = new JSONObject(response);
                                    JSONObject data = obj.getJSONObject("data");
                                    tittle =data.getString("title");
                                    Details=data.getString("body");

                                    JSONArray ClassdateArrays = data.getJSONArray("field_class_time");
                                    JSONArray ar = data.getJSONArray("field_class_time");




                                    if(ClassdateArrays != null && ClassdateArrays.length() > 0 ) {


                                        for (int j = 0; j < ClassdateArrays.length(); j++) {
                                            DetailclassDate = ClassdateArrays.getJSONObject(0).getString("startDate");
                                            DetailClassStart = ClassdateArrays.getJSONObject(j).getString("startTime");
                                            //  DetailClassEndDate  = ClassdateArrays.getJSONObject(j).getString("endDate");
                                            DetailClassEnd = ClassdateArrays.getJSONObject(j).getString("endTime");
                                            Detailweeknos = ClassdateArrays.getJSONObject(j).getString("Week");

                                            Log.d("TAG", "recource: " + DetailclassDate + DetailClassStart);

                                            JSONObject lastObj = ClassdateArrays.getJSONObject(ClassdateArrays.length()-1);
                                            ClassEndDate = lastObj.getString("endDate");
                                            DetailClassEndDate =ClassEndDate;

                                            Log.d("TAG", "ClassEndDatedddd: " + ClassEndDate +" "+ DetailClassEndDate);


                                        }


                                    }

                                    JSONObject Local = data.getJSONObject("field_dept_location");
                                    if (Local!=null && Local.length() >0){
                                        MapLayout.setVisibility(View.VISIBLE);
                                        Lat = Double.valueOf(Local.getString("lat"));
                                        Long = Double.valueOf(Local.getString("lng"));
                                        Log.d("TAG", "recource3: " + Lat + Long);

                                    }else {
                                        if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null & SessionManager.getInstance(getActivity()).getCollage().getLng() != null) {
                                            Lat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat());
                                            Long = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng());

                                        }else {
                                            Lat = latitude;
                                            Long = longitude;


                                        }
                                    }

                                    edittittle.setText(tittle);
                                    editDetails.setText(Details);
                                    TV_StartDate.setText(DetailclassDate);

                                    DateFormat inputFormat = new SimpleDateFormat("MMM-dd-yyyy");
                                    DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");



                                    Date datemain;
                                    try {
                                        datemain = inputFormat.parse(DetailclassDate);
                                        String outputDateStr = outputFormat.format(datemain);

                                        StartDate =outputDateStr;

                                        System.out.println(StartDate);

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }


                                    TV_endDate.setText(ClassEndDate);
                                    String convert =TV_endDate.getText().toString();

                                    // String  convert ="Mar-05-2018";

                                    DateFormat inputFormat1 = new SimpleDateFormat(" MMM-dd-yyyy");
                                    DateFormat outputFormat1 = new SimpleDateFormat("yyyy-MM-dd");

                                    Date datemainend;
                                    try {
                                        datemainend = inputFormat1.parse(convert);
                                        String outputDateStr2 = outputFormat1.format(datemainend);
                                        EndDate =outputDateStr2;
                                        System.out.println(EndDate);

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    TV_StartTime.setText(DetailClassStart);

                                    SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                    SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a",Locale.US);
                                    Date date;
                                    try {
                                        date = parseFormat.parse(DetailClassStart);
                                        time24format = displayFormat.format(date);
                                        System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
                                        TimeStart =time24format;


                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }





                                    TV_EndTime.setText(DetailClassEnd);


                                    SimpleDateFormat displayFormat1 = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                    SimpleDateFormat parseFormat1 = new SimpleDateFormat("hh:mm a",Locale.US);
                                    Date date1;
                                    try {
                                        date1 = parseFormat1.parse(DetailClassEnd);
                                        time24formarend = displayFormat1.format(date1);
                                        System.out.println(parseFormat1.format(date1) + " = " + displayFormat1.format(date1));

                                        timeEnd =time24formarend;

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    onSearch(Lat,Long, SessionManager.getInstance(getActivity()).getCollage().getTitle());


                                } else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());



                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);



                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String Cookies= SessionManager.getInstance(UpdateCalenderEvent.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(UpdateCalenderEvent.this).getaccesstoken().sessionID;
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(UpdateCalenderEvent.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);



                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(mStrRequest);



    }





    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.StartDate:
                hideKeyBoard(v);

                StartdateDialog();
                break;

            case R.id.repeat:
                hideKeyBoard(v);


                if (((CheckBox) v).isChecked()) {

                    classrepeat = "1";
                    Weeklayoutlayout.setVisibility(View.VISIBLE);

                }else {
                    classrepeat = "0";
                    Weeklayoutlayout.setVisibility(View.GONE);

                }
                break;

            case R.id.endDate:
                hideKeyBoard(v);

                StartEnddateDialog();
                break;

            case R.id.StartTime:
                hideKeyBoard(v);


                timepickerdialog = new TimePickerDialog(UpdateCalenderEvent.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                if (hourOfDay == 0) {

                                    hourOfDay += 12;

                                    format = "am";
                                }
                                else if (hourOfDay == 12) {

                                    format = "pm";

                                }
                                else if (hourOfDay > 12) {

                                    hourOfDay -= 12;

                                    format = "pm";

                                }
                                else {

                                    format = "am";
                                }


                                TV_StartTime.setText(hourOfDay + ":" + minute + format);
                                String Slow =(hourOfDay + ":" + minute + format);



                                DateFormat f1 = new SimpleDateFormat("h:mma", Locale.US);

                                try {
                                    Date d = f1.parse(Slow);
                                    DateFormat f2 = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                    String FF = f2.format(d).toLowerCase();
                                    Log.d("TAG", "f2 " + FF);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }




                                SimpleDateFormat inFormatx = new SimpleDateFormat("hh:mma",Locale.US);
                                SimpleDateFormat outFormatx = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                String Time = null;
                                try {
                                    Time = outFormatx.format(inFormatx.parse(Slow));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                System.out.println("time in 24 hour formatS : " + Time);

                                TimeStart=Time;


                            }
                        },
                        CalendarHour, CalendarMinute, false);
                timepickerdialog.show();
                break;
            case R.id.EndTime:
                hideKeyBoard(v);
                timepickerdialog = new TimePickerDialog(UpdateCalenderEvent.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                if (hourOfDay == 0) {

                                    hourOfDay += 12;

                                    format = "am";
                                }
                                else if (hourOfDay == 12) {

                                    format = "pm";

                                }
                                else if (hourOfDay > 12) {

                                    hourOfDay -= 12;

                                    format = "pm";

                                }
                                else {

                                    format = "am";
                                }


                                String SlowEnd =(hourOfDay + ":" + minute + format);
                                TV_EndTime.setText(hourOfDay + ":" + minute + format);

                                Log.d("TAG", "EndTime " + TV_EndTime);


                                SimpleDateFormat inFormatx = new SimpleDateFormat("hh:mma",Locale.US);
                                SimpleDateFormat outFormatx = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                String time24 = null;
                                try {
                                    time24 = outFormatx.format(inFormatx.parse(SlowEnd));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                System.out.println("time in 24 hour formatE : " + time24);

                                timeEnd=time24;


                            }
                        },
                        CalendarHour, CalendarMinute, false);
                timepickerdialog.show();

                break;



        }
    }


    private void StartdateDialog() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {

                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateStart(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, listener, year, month, day);
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dpDialog.show();

    }


    private void StartEnddateDialog() {

        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {

                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateEnd(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, listener, year, month, day);
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dpDialog.show();

    }

    private void showDateStart(int year, int month, int day) {
        Log.d("TAG", "Start Date: " + day + "-" + month + "-" + year);
        StartDate = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        TV_StartDate.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));

        TV_endDate.setText(TV_StartDate.getText().toString());
        EndDate =StartDate;
    }

    private void showDateEnd(int year, int month, int day) {
        Log.d("TAG", "End Date: " + day + "-" + month + "-" + year);
        EndDate = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        TV_endDate.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.calender_edit_menu, menu);
        shareItem = menu.findItem(R.id.Submit);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Submit:

                AddEventinCalenderProcess();

                return true;

            case R.id.Editoption:
                shareItem.setVisible(true);
                edittittle.setEnabled(true);
                editDetails.setEnabled(true);
                TV_StartDate.setEnabled(true);
                TV_StartTime.setEnabled(true);
                TV_endDate.setEnabled(true);
                TV_EndTime.setEnabled(true);
                TV_location.setEnabled(true);
                repeat.setClickable(true);
                return true;


            case R.id.deleteoption:

               DeleteEventsFromCalender(Classes_Id);

               return true;

            case android.R.id.home:

                finish();



                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void selectdays() {

        int totalselect = 0;

        if (mon.isChecked()) {
            weekmon ="MO";
            Weekselect.append(weekmon+","+" " );
            totalselect += 1;
        }

        if (tue.isChecked()) {
            weektues ="TU";
            Weekselect.append(weektues+","+" " );
            totalselect += 1;
        }
        if (wed.isChecked()) {
            weekwedns ="WE";
            Weekselect.append(weekwedns+","+" " );
            totalselect += 1;
        }

        if (thur.isChecked()) {
            weekthur ="TH";
            Weekselect.append(weekthur+","+" ");
            totalselect += 1;
        }
        if (fri.isChecked()) {
            weekfri ="FR";
            Weekselect.append(weekfri+","+" ");
            totalselect += 1;
        }

        if (sat.isChecked()) {
            weeksat ="SA";
            Weekselect.append(weeksat+","+" ");
            totalselect += 1;
        }
        if (sun.isChecked()) {
            weeksun ="SU";
            Weekselect.append(weeksun+","+" ");
            totalselect += 1;
        }

        // Weekselect.append("\nTotal: " + totalselect);
        //   Toast.makeText(getApplicationContext(), Weekselect.toString(), Toast.LENGTH_LONG).show();

    }


    private void AddEventinCalenderProcess() {
        selectdays();
        edittittle.setError(null);
        editDetails.setError(null);



        // Store values at the time of the login attempt.
        tittle = edittittle.getText().toString().trim();
        Details = editDetails.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;


        if (android.text.TextUtils.isEmpty(tittle)) {
            focusView = edittittle;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }
        else if (co.questin.utils.TextUtils.isNullOrEmpty(Details)) {
            // check for First Name
            focusView = editDetails;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            ClassDetailsJson = new JSONObject();

            try {
                ClassDetailsJson.put("start_date", StartDate);
                ClassDetailsJson.put("end_date", EndDate);
                ClassDetailsJson.put("start_time", TimeStart);
                ClassDetailsJson.put("end_time", timeEnd);
                ClassDetailsJson.put("week", Weekselect);
                ClassDetailsJson.put("repeat", classrepeat);


                Log.d("TAG", "classdetails: " + ClassDetailsJson);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            EventCreateCalenderTask();


        }
    }

    private void EventCreateCalenderTask() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.PUT,  URLS.URL_TEACHERCREATECLASS+"/"+Classes_Id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    hideIndicator();
                                    Intent i = new Intent(UpdateCalenderEvent.this,CalenderMain.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                    finish();


                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);

                        } else {
                            hideIndicator();
                            showConnectionSnackbar();

                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                String Cookies= SessionManager.getInstance(UpdateCalenderEvent.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(UpdateCalenderEvent.this).getaccesstoken().sessionID;
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(UpdateCalenderEvent.this).getaccesstoken().accesstoken);
                params.put("Cookie",Cookies);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("title",tittle);
                params.put("body",Details);
                params.put("field_dept_location[lat]", String.valueOf(Lat));
                params.put("field_dept_location[lng]", String.valueOf(Long));
                params.put("field_class_time",ClassDetailsJson.toString());

                return params;
            }

        };
        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);

    }




    @Override
    public void onBackPressed() {
        Intent i = new Intent(UpdateCalenderEvent.this,CalenderMain.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();

    }

    public void onSearch(Double lat, Double aLong, String placename) {
        mMap.clear();
        LatLng latLng = new LatLng(lat, aLong);
        MapLayout.setVisibility(View.VISIBLE);
        mMap.addMarker(new MarkerOptions()
                .position(latLng) //setting position
                .draggable(true) //Making the marker draggable
                .title(placename)); //Adding a title
        //Moving the camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        //Animating the camera
        mMap.animateCamera(CameraUpdateFactory.zoomTo(20));


    }


    @Override
    protected void onStart() {
        googleApiClient.connect();

        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    //Getting current location
    private void getCurrentLocation() {
        mMap.clear();
        //Creating a location object
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            Log.d("TAG", "latlong: " + longitude+ " "+latitude);

        }
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);
        mMap.getUiSettings().setAllGesturesEnabled(false);
        getCurrentLocation();

    }

    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

    private void DeleteEventsFromCalender(String classes_id) {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.DELETE, URLS.URL_DELETESUBJECTS+"/"+classes_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (isStatusSuccess(stateResponse.getStatus())) {
                                    Intent i = new Intent(UpdateCalenderEvent.this,CalenderMain.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                    finish();

                                }else {

                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {

                            showServerSnackbar(R.string.error_responce);

                        } else {

                            showConnectionSnackbar();

                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(UpdateCalenderEvent.this).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(UpdateCalenderEvent.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(UpdateCalenderEvent.this).getaccesstoken().sessionID);

                return params;
            }




        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                StringBuilder stBuilder = new StringBuilder();
                String placename = String.format("%s", place.getName());
                String latitude = String.valueOf(place.getLatLng().latitude);
                String longitude = String.valueOf(place.getLatLng().longitude);
                String address = String.format("%s", place.getAddress());
                stBuilder.append("Name: ");
                stBuilder.append(placename);
                stBuilder.append("\n");
                stBuilder.append("Latitude: ");
                stBuilder.append(latitude);
                stBuilder.append("\n");
                stBuilder.append("Logitude: ");
                stBuilder.append(longitude);
                stBuilder.append("\n");
                stBuilder.append("Address: ");
                stBuilder.append(address);
                TV_location.setText(placename);
                Lat= place.getLatLng().latitude;
                Long = place.getLatLng().longitude;
                onSearch(place.getLatLng().latitude, place.getLatLng().longitude,placename);


            }
        }
    }

}
