package co.questin.calendersection;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.DisplayMainCalenderAdapter;
import co.questin.library.StateBean;
import co.questin.models.CalendarCollection;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

import static co.questin.models.CalendarCollection.date_collection_arr;

public class DisplayMainCalender extends BaseAppCompactActivity {
    String Tittle,Exampercent,Class_id,split_second;
    public GregorianCalendar cal_month, cal_month_copy;
    private DisplayMainCalenderAdapter cal_adapter;
    private TextView tv_month;
    GridView gridview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_main_calender);
        gridview = findViewById(R.id.gv_calendar);


        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
        cal_month_copy = (GregorianCalendar) cal_month.clone();
        /*  cal_adapter = new CalendarAdapter(this, cal_month, date_collection_arr);*/
        tv_month = findViewById(R.id.tv_month);
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
        date_collection_arr = new ArrayList<CalendarCollection>();
        ProgressMonthlyDatesList();


        ImageButton previous = findViewById(R.id.ib_prev);

        previous.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setPreviousMonth();
                refreshCalendar();
            }
        });

        ImageButton next = findViewById(R.id.Ib_next);
        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setNextMonth();
                refreshCalendar();

            }
        });





    }



    protected void setNextMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1),
                    cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) + 1);
        }

    }



    protected void setPreviousMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1),
                    cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) - 1);
        }

    }

    public void refreshCalendar() {
        cal_adapter.refreshDays();
        cal_adapter.notifyDataSetChanged();
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
    }



    private void ProgressMonthlyDatesList() {
        ShowIndicator();
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_CALENDERMONTLYDATES+"?"+"month=2018-02&type=month",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    JSONObject obj = new JSONObject(response);
                                    JSONArray data = obj.getJSONArray("data");

                                    for (int i = 0; i < data.length(); i++) {
                                        String Eventtittle = data.getJSONObject(i).getString("title");
                                        String nid = data.getJSONObject(i).getString("nid");
                                        String type = data.getJSONObject(i).getString("type");
                                        String body = data.getJSONObject(i).getString("body");


                                        Log.d("TAG", "type: " + type);
                                        if (type.contains("classes")) {
                                            JSONArray jsonArrayDates = new JSONArray(data.getJSONObject(i).getString("field_class_time"));

                                            for (int j = 0; j < jsonArrayDates.length(); j++) {
                                                String startdate = jsonArrayDates.getJSONObject(j).getString("startDate");
                                                String EventTimeStart = jsonArrayDates.getJSONObject(j).getString("startTime");
                                                String endDate = jsonArrayDates.getJSONObject(j).getString("endDate");
                                                String EventtimeEnd = jsonArrayDates.getJSONObject(j).getString("endTime");

                                                SimpleDateFormat sdf=new SimpleDateFormat("MMM-dd-yyyy");
                                                Date dateclass=sdf.parse(startdate);
                                                sdf=new SimpleDateFormat("yyyy-MM-dd");
                                                System.out.println(sdf.format(dateclass));
                                                split_second =sdf.format(dateclass);

                                                Log.d("TAG", "split_one: " + split_second + Eventtittle);
                                                CalendarCollection.date_collection_arr.add(new CalendarCollection(split_second,Eventtittle));
                                                cal_adapter = new DisplayMainCalenderAdapter(DisplayMainCalender.this, cal_month, date_collection_arr);
                                                gridview.setAdapter(cal_adapter);


                                                gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                                    public void onItemClick(AdapterView<?> parent, View v,
                                                                            int position, long id) {

                                                        ((DisplayMainCalenderAdapter) parent.getAdapter()).setSelected(v,position);
                                                        String selectedGridDate = DisplayMainCalenderAdapter.day_string
                                                                .get(position);

                                                        String[] separatedTime = selectedGridDate.split("-");
                                                        String gridvalueString = separatedTime[2].replaceFirst("^0*","");
                                                        int gridvalue = Integer.parseInt(gridvalueString);

                                                        if ((gridvalue > 10) && (position < 8)) {
                                                            setPreviousMonth();
                                                            refreshCalendar();
                                                        } else if ((gridvalue < 7) && (position > 28)) {
                                                            setNextMonth();
                                                            refreshCalendar();
                                                        }
                                                        ((DisplayMainCalenderAdapter) parent.getAdapter()).setSelected(v,position);


                                                        ((DisplayMainCalenderAdapter) parent.getAdapter()).getPositionList(selectedGridDate, DisplayMainCalender.this);
                                                    }

                                                });




                                            }


                                        } else if (type.contains("assignment")) {
                                            JSONArray jsonArrayAssignments = new JSONArray(data.getJSONObject(i).getString("field_date_of_birth"));

                                            for (int j = 0; j < jsonArrayAssignments.length(); j++) {


                                            }
                                        } else if (type.contains("exam")) {
                                            JSONArray jsonArrayAssignments = new JSONArray(data.getJSONObject(i).getString("field_exam_time"));

                                            for (int j = 0; j < jsonArrayAssignments.length(); j++) {


                                            }
                                        }


                                        Log.d("TAG", "type: " + type);
                                        if (type.contains("classes")) {
                                            JSONArray jsonArrayDates = new JSONArray(data.getJSONObject(i).getString("field_class_time"));

                                            for (int j = 0; j < jsonArrayDates.length(); j++) {
                                                String startdate = jsonArrayDates.getJSONObject(j).getString("startDate");
                                                String EventTimeStart = jsonArrayDates.getJSONObject(j).getString("startTime");
                                                String endDate = jsonArrayDates.getJSONObject(j).getString("endDate");
                                                String EventtimeEnd = jsonArrayDates.getJSONObject(j).getString("endTime");
                                            }
                                        }
                                    }
                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(DisplayMainCalender.this).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(DisplayMainCalender.this).getaccesstoken().sessionName+"="+SessionManager.getInstance(DisplayMainCalender.this).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(mStrRequest);


    }
}

