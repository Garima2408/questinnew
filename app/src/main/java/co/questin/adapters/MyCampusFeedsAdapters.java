package co.questin.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.activities.UserDisplayProfile;
import co.questin.buttonanimation.LikeButton;
import co.questin.buttonanimation.OnAnimationEndListener;
import co.questin.buttonanimation.OnLikeListener;
import co.questin.campusfeedsection.ReplyOnMyFeeds;
import co.questin.campusfeedsection.UpdateMyFeeds;
import co.questin.library.StateBean;
import co.questin.models.CampusFeedArray;
import co.questin.network.URLS;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

import static co.questin.models.CampusFeedArray.COARSE_TYPE;
import static co.questin.models.CampusFeedArray.PROGRESS_;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;

/**
 * Created by Dell on 23-08-2017.
 */

public class MyCampusFeedsAdapters extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<CampusFeedArray> lists;
    private boolean isLoading;
    private Activity activity;
    Dialog imageDialog;
    Bitmap thumbnail = null;
    String Extension;

    public MyCampusFeedsAdapters(RecyclerView recyclerView, ArrayList<CampusFeedArray> lists, Activity activity) {
        this.lists = lists;
        this.activity = activity;


    }

    public void addData(List<CampusFeedArray> list){

        int size= this.lists.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ this.lists.size());
        this.lists.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.lists.size());
        notifyItemRangeInserted(size, this.lists.size());        //existing size, aur new size
    }

    public void removeProgress(){

        int size= lists!=null && lists.size()>0?lists.size():0;

        if(size>0){

            lists.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }


    @Override
    public int getItemViewType(int position) {

        if(lists!=null && lists.size()>0)
            return lists.get(position).getType();
        return 0;
    }
    public void setInitialData( ArrayList<CampusFeedArray> list){
        lists.clear();
        this.lists=list;
        notifyDataSetChanged();
    }

    public void addProgress(){
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                if(lists!=null && lists.size()>0){
                    lists.add(new CampusFeedArray(1));
                    notifyItemInserted(lists.size()-1);
                }
            }
        };
        handler.post(r);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COARSE_TYPE) {
            View view = LayoutInflater.from(activity).inflate(R.layout.list_of_mycampusfeed, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == PROGRESS_) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof UserViewHolder) {
            final CampusFeedArray course = lists.get(position);
            final UserViewHolder userViewHolder = (UserViewHolder) holder;

            userViewHolder.publisher_name.setText(lists.get(position).getUserName());
            String s =lists.get(position).getPost();
            userViewHolder.dateTime.setText(lists.get(position).getDate());
            userViewHolder.blog_content.setText(Html.fromHtml(s));
            userViewHolder.commentNo.setText(lists.get(position).getReplies_count());
            userViewHolder.likeNo.setText(lists.get(position).getLikes());



            if(lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0 ) {
                Glide.clear(userViewHolder.circleView);
                Glide.with(activity).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(userViewHolder.circleView);

            }else {
                userViewHolder.circleView.setImageResource(R.mipmap.place_holder);
                Glide.clear(userViewHolder.circleView);
            }


            if(lists.get(position).getIs_liked().contains("False")){
                userViewHolder.like.setLiked(false);

            }else if(lists.get(position).getIs_liked().contains("True"))  {
                userViewHolder.like.setLiked(true);
            }



            if(lists.get(position).getUrl() != null && lists.get(position).getUrl().length() > 0 ) {
                Extension = lists.get(position).getUrl().substring(lists.get(position).getUrl().lastIndexOf("."));
                Log.d("TAG", "Extension: " + Extension);
                userViewHolder.previewlayout.setVisibility(View.VISIBLE);

                if (Extension.matches(".jpg")) {
                    userViewHolder.imagePost.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setVisibility(View.GONE);

                    Glide.clear(userViewHolder.imagePost);
                    Glide.with(activity).load(lists.get(position).getUrl())
                            .dontAnimate()
                            .placeholder(R.color.black).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(userViewHolder.imagePost);


                } else if (Extension.matches(".png")) {

                    userViewHolder.imagePost.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setVisibility(View.GONE);

                    Glide.clear(userViewHolder.imagePost);
                    Glide.with(activity).load(lists.get(position).getUrl())
                            .dontAnimate()
                            .placeholder(R.color.black).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(userViewHolder.imagePost);

                }  else if (Extension.matches(".jpeg")) {

                    userViewHolder.imagePost.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setVisibility(View.GONE);

                    Glide.clear(userViewHolder.imagePost);
                    Glide.with(activity).load(lists.get(position).getUrl())
                            .dontAnimate()
                            .placeholder(R.color.black).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(userViewHolder.imagePost);

                }else if (Extension.matches(".docx")) {

                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_doc_download);
                    userViewHolder.imagePost.setVisibility(View.GONE);




                } else if (Extension.matches(".pdf")) {

                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_pdf_download);

                    userViewHolder.imagePost.setVisibility(View.GONE);




                } else if (Extension.matches(".txt")) {
                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_text_download);
                    userViewHolder.imagePost.setVisibility(View.GONE);



                }else if (Extension.matches(".doc")) {
                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_doc_download);
                    userViewHolder.imagePost.setVisibility(View.GONE);



                }else if (Extension.matches(".xls")) {
                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_xls_download);
                    userViewHolder.imagePost.setVisibility(View.GONE);



                }else if (Extension.matches(".xlsx")) {
                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_xls_download);
                    userViewHolder.imagePost.setVisibility(View.GONE);


                }

            }else {
                userViewHolder.previewlayout.setVisibility(View.GONE);
                userViewHolder.imagePost.setImageDrawable(null);
                Glide.clear(userViewHolder.imagePost);
            }


            userViewHolder.imagepreview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                        Utils.downloadFile(activity,lists.get(position).getUrl());
                    //    Utils.downloadFile(activity,lists.get(position).getUrl());

                }
            });


            userViewHolder.publisher_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(activity, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getUid());
                    Pair<ImageView, String> p1 = Pair.create(userViewHolder.circleView, "transitionProfile");
                    Pair <TextView, String> p2 = Pair.create(userViewHolder.publisher_name, "publisherName");


                    ActivityOptionsCompat transitionActivityOptions = null;

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, new Pair[]{p1,p2});
                    }

                    activity.startActivity(backIntent,transitionActivityOptions.toBundle());




                }
            });


            userViewHolder.circleView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(activity, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getUid());
                    Pair<ImageView, String> p1 = Pair.create(userViewHolder.circleView, "transitionProfile");
                    Pair <TextView, String> p2 = Pair.create(userViewHolder.publisher_name, "publisherName");


                    ActivityOptionsCompat transitionActivityOptions = null;

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, new Pair[]{p1,p2});
                    }

                    activity.startActivity(backIntent,transitionActivityOptions.toBundle());




                }
            });



            userViewHolder.imagePost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Utils.ImageDialogOpen(activity,lists.get(position).getUrl());

                }
            });



            userViewHolder.like.setOnLikeListener(new OnLikeListener() {
                @Override
                public void liked(LikeButton likeButton) {
                    if (!lists.get(position).getIs_liked().isEmpty()) {
                        if (lists.get(position).getIs_liked().contains("False")) {
                            // userViewHolder.like.setBackgroundResource(R.mipmap.likeblue);
                            userViewHolder.like.setLiked(true);
                            userViewHolder.likeNo.setText(Integer.toString(Integer.parseInt(lists.get(position).getLikes()) + 1));
                            String NewLikeNo = userViewHolder.likeNo.getText().toString();

                            LikeFeedComments(lists.get(position).getId(),"flag");
                            lists.get(position).setIs_liked("True");
                            lists.get(position).setLikes(NewLikeNo);


                        } else if (lists.get(position).getIs_liked().contains("True")) {
                            //  userViewHolder.like.setBackgroundResource(R.mipmap.like);
                            userViewHolder.like.setLiked(false);
                            userViewHolder.likeNo.setText(Integer.toString(Integer.parseInt(lists.get(position).getLikes()) - 1));
                            String NewLikeNo = userViewHolder.likeNo.getText().toString();
                            LikeFeedComments(lists.get(position).getId(),"unflag");
                            lists.get(position).setIs_liked("False");
                            lists.get(position).setLikes(NewLikeNo);
                        }
                    }
                }

                @Override
                public void unLiked(LikeButton likeButton) {
                    if (!lists.get(position).getIs_liked().isEmpty()) {
                        if (lists.get(position).getIs_liked().contains("False")) {
                            // userViewHolder.like.setBackgroundResource(R.mipmap.likeblue);
                            userViewHolder.like.setLiked(true);

                            userViewHolder.likeNo.setText(Integer.toString(Integer.parseInt(lists.get(position).getLikes()) + 1));
                            String NewLikeNo = userViewHolder.likeNo.getText().toString();

                            LikeFeedComments(lists.get(position).getId(),"flag");
                            lists.get(position).setIs_liked("True");

                            lists.get(position).setLikes(NewLikeNo);


                        } else if (lists.get(position).getIs_liked().contains("True")) {
                            //  userViewHolder.like.setBackgroundResource(R.mipmap.like);
                            userViewHolder.like.setLiked(false);
                            userViewHolder.likeNo.setText(Integer.toString(Integer.parseInt(lists.get(position).getLikes()) - 1));
                            String NewLikeNo = userViewHolder.likeNo.getText().toString();
                            LikeFeedComments(lists.get(position).getId(),"unflag");
                            lists.get(position).setIs_liked("False");
                            lists.get(position).setLikes(NewLikeNo);
                        }
                    }
                }
            });




            userViewHolder.like.setOnAnimationEndListener(new OnAnimationEndListener() {
                @Override
                public void onAnimationEnd(LikeButton likeButton) {

                }
            });





            userViewHolder.box1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(activity, ReplyOnMyFeeds.class)
                            .putExtra("POST_ID", lists.get(position).getId())
                            .putExtra("USERSELECTED_ID",lists.get(position).getUid())
                            .putExtra("NAME", lists.get(position).getUserName())
                            .putExtra("BLOG_CONTENT", lists.get(position).getPost())
                            .putExtra("DATE", lists.get(position).getDate())
                            .putExtra("IMAGE_URL", lists.get(position).getPicture())
                            .putExtra("IS_LIKED", lists.get(position).getIs_liked())
                            .putExtra("NOLIKED", lists.get(position).getLikes())
                            .putExtra("NOCOMMENT", lists.get(position).getReplies_count())
                            .putExtra("POSTCONENT", lists.get(position).getUrl())
                            .putExtra("open", "CameFromMyCampusFeedAdapter");
                    activity.startActivity(backIntent);


                }
            });
            userViewHolder.box3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String shareBody = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";

                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Questin (Open it in Google Play Store to Download the Application)");

                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody+lists.get(position).getPost());
                    activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));




                }
            });


            userViewHolder.box1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(activity, ReplyOnMyFeeds.class)
                            .putExtra("POST_ID", lists.get(position).getId())
                            .putExtra("USERSELECTED_ID",lists.get(position).getUid())
                            .putExtra("NAME", lists.get(position).getUserName())
                            .putExtra("BLOG_CONTENT", lists.get(position).getPost())
                            .putExtra("DATE", lists.get(position).getDate())
                            .putExtra("IMAGE_URL", lists.get(position).getPicture())
                            .putExtra("IS_LIKED", lists.get(position).getIs_liked())
                            .putExtra("NOLIKED", lists.get(position).getLikes())
                            .putExtra("NOCOMMENT", lists.get(position).getReplies_count())
                            .putExtra("POSTCONENT", lists.get(position).getUrl())
                            .putExtra("open", "CameFromMyCampusFeedAdapter");
                    activity.startActivity(backIntent);

                }
            });


            userViewHolder.options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showPopupMenu();
                }

                private void showPopupMenu() {


                    PopupMenu popup = new PopupMenu(activity, userViewHolder.options);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.optionpopupmenu, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.Editoption:


                                    Intent backIntent = new Intent(activity, UpdateMyFeeds.class)
                                            .putExtra("POST_ID", lists.get(position).getId())
                                            .putExtra("NAME", lists.get(position).getUserName())
                                            .putExtra("BLOG_CONTENT", lists.get(position).getPost())
                                            .putExtra("DATE", lists.get(position).getDate())
                                            .putExtra("IMAGE_URL", lists.get(position).getPicture())
                                            .putExtra("POSTCONENT", lists.get(position).getUrl())
                                            .putExtra("open", "CameFromMyCampusFeedAdapter");
                                    activity.startActivity(backIntent);


                                    return true;
                                case R.id.deleteoption:

                                    AlertDialog.Builder builder = new AlertDialog.Builder(activity, android.app.AlertDialog.THEME_HOLO_DARK)
                                            .setTitle("Delete My Feed")
                                            .setMessage(R.string.delete)
                                            .setCancelable(false)
                                            .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    /* *//**//*  DELETE COMMENTS ON POST*//**//**/
                                                    DeleteFeedComments(lists.get(position).getId());
                                                    lists.remove(position);
                                                    notifyDataSetChanged();


                                                }
                                            })
                                            .setNegativeButton("No", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                    builder.create().show();



                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();




                }
            });

            } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return lists == null ? 0 : lists.size();
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }
    private class UserViewHolder extends RecyclerView.ViewHolder {
        ImageView Imagecomment, options, share,inaprooptions,imagePost,imagepreview;
        ImageView circleView;
        LikeButton like;
        RelativeLayout previewlayout;
        LinearLayout box1,box3;
        private TextView commentNo, likeNo,blog_content,publisher_name,dateTime;


        public UserViewHolder(View view) {
            super(view);
            commentNo = itemView.findViewById(R.id.commentNo);
            likeNo = itemView.findViewById(R.id.likeNo);
            blog_content = itemView.findViewById(R.id.blog_content);
            dateTime = itemView.findViewById(R.id.dateTime);
            publisher_name = itemView.findViewById(R.id.publisher_name);
            circleView = itemView.findViewById(R.id.circleView);
            options=itemView.findViewById(R.id.options);
            Imagecomment =itemView.findViewById(R.id.comment);
            share=itemView.findViewById(R.id.share);
            inaprooptions =itemView.findViewById(R.id.inaprooptions);
            like =itemView.findViewById(R.id.like);
            imagePost =itemView.findViewById(R.id.imagePost);
            imagepreview =itemView.findViewById(R.id.imagepreview);
            previewlayout =itemView.findViewById(R.id.previewlayout);
            box1=itemView.findViewById(R.id.box1);
            box3=itemView.findViewById(R.id.box3);
        }
    }


    /*ADD LIKES ON COMMENTS */

    private void LikeFeedComments(final String id, final String flag) {
        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_LIKEDCOMMENTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {

                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {

                                }else {

                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {

                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {

                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("flag_name","comments_like");
                params.put("entity_id",id);
                params.put("action",flag);
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);

    }


    /*LISTS OF DELETE COMMENTS ON POSTS*/

    private void DeleteFeedComments(String id) {

        StringRequest mStrRequest = new StringRequest(Request.Method.DELETE, URLS.URL_DELETECOMMENTS+"/"+id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {

                                }
                            } else {
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            showServerSnackbar(R.string.error_responce);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                        } else {
                        }
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }




        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);
    }

}


