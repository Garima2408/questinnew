package co.questin.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

import co.questin.R;

/**
 * Created by Dell on 15-09-2017.
 */

public class CalenderPagerDetailsAdapter extends PagerAdapter {

    Context contextm;
    List<String> calendereventList;
    private static LayoutInflater inflater = null;
    ImageView view;
    private CalenderlistviewdisplayAdapter addadapter;


    public CalenderPagerDetailsAdapter(Context context, List<String> calendereventList) {
        contextm = context;
        this.calendereventList = calendereventList;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return calendereventList.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {


        return view == object;
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {


        inflater = (LayoutInflater) contextm
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.calenderlist_view, container,
                false);
       ListView EventDisplay = itemView.findViewById(R.id.EventDisplay);

        addadapter = new CalenderlistviewdisplayAdapter(contextm, calendereventList);
        EventDisplay.setAdapter(addadapter);


        itemView.setTag(position);

        // Add viewpager_item.xml to ViewPager
        container.addView(itemView);


        return itemView;
    }





    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        container.removeView((LinearLayout) object);

    }

}
