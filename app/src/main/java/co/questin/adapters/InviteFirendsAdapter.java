package co.questin.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.library.StateBean;
import co.questin.models.AllUser;
import co.questin.network.URLS;
import co.questin.studentprofile.AnotherUserDisplay;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;

import static co.questin.models.AllUser.COARSE_TYPE;
import static co.questin.models.AllUser.PROGRESS_;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;

/**
 * Created by Dell on 21-08-2017.
 */

public class InviteFirendsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<AllUser> lists;
    Activity activity;

    public InviteFirendsAdapter( RecyclerView recyclerView,Activity activity, ArrayList<AllUser> lists) {
        this.lists = lists;
        this.activity = activity;
    }

    public void addData(List<AllUser> list){

        int size= this.lists.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ this.lists.size());
        this.lists.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.lists.size());
        notifyItemRangeInserted(size, this.lists.size());        //existing size, aur new size
    }

    public void removeProgress(){

        int size= lists!=null && lists.size()>0?lists.size():0;

        if(size>0){

            lists.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }


    @Override
    public int getItemViewType(int position) {

        if(lists!=null && lists.size()>0)
            return lists.get(position).getType();
        return 0;
    }
    public void setInitialData( ArrayList<AllUser> list){

        this.lists=list;
        notifyDataSetChanged();
    }

    public void addProgress(){
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                if(lists!=null && lists.size()>0){
                    lists.add(new AllUser(1));
                    notifyItemInserted(lists.size()-1);
                }
            }
        };

        handler.post(r);

    }





    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COARSE_TYPE) {
            View view = LayoutInflater.from(activity).inflate(R.layout.list_of_invitefriends, parent, false);
            return new CustomVholder(view);
        } else if (viewType == PROGRESS_) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof CustomVholder) {
            final CustomVholder userViewHolder = (CustomVholder) holder;

            try {
                userViewHolder.tv_friendname.setText(lists.get(position).getName());
                userViewHolder.tv_email.setText(lists.get(position).getCollageName());

                if (lists.get(position).getMainRole() != null && lists.get(position).getMainRole().length() > 0) {

                    if (lists.get(position).getMainRole().matches("13")) {
                        userViewHolder.tv_departmentrole.setText(lists.get(position).CollageRole);
                        userViewHolder.tv_department.setText(lists.get(position).CollageBranch);


                    } else if (lists.get(position).getMainRole().matches("14")) {
                        userViewHolder.tv_departmentrole.setText(lists.get(position).CollageRole);
                        userViewHolder.tv_department.setText(lists.get(position).getCollagedepartment());


                    } else if (lists.get(position).getMainRole().matches("15")) {
                        userViewHolder.tv_departmentrole.setText(lists.get(position).CollageRole);
                        userViewHolder.tv_department.setText(lists.get(position).CollageBranch);


                    }
                    else if (lists.get(position).getMainRole().matches("3")) {
                        userViewHolder.tv_departmentrole.setText(lists.get(position).CollageRole);
                        userViewHolder.tv_department.setText(lists.get(position).getCollagedepartment());


                    }


                } else {

                }


                if (lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0) {
                    Glide.clear(userViewHolder.friendIcon);
                    Glide.with(activity).load(lists.get(position).getPicture())
                            .placeholder(R.mipmap.place_holder).dontAnimate()
                            .fitCenter().into(userViewHolder.friendIcon);

                } else {
                    userViewHolder.friendIcon.setImageResource(R.mipmap.place_holder);
                    Glide.clear(userViewHolder.friendIcon);
                }
                userViewHolder.mainlay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent backIntent = new Intent(activity, AnotherUserDisplay.class)
                                .putExtra("USERPROFILE_ID",lists.get(position).getUid())
                                .putExtra("COLLAGE_ID",lists.get(position).getCollageId());
                        activity.startActivity(backIntent);

                    }
                });

                userViewHolder.friendIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent backIntent = new Intent(activity, AnotherUserDisplay.class)
                                .putExtra("USERPROFILE_ID",lists.get(position).getUid())
                                .putExtra("COLLAGE_ID",lists.get(position).getCollageId());
                        activity.startActivity(backIntent);

                    }
                });


                if(lists.get(position).getIs_friend_request_sent().matches("1")){
                    userViewHolder.AlreadyInviteIcon.setVisibility(View.VISIBLE);
                    userViewHolder.AlreadyFriendIcon.setVisibility(View.GONE);
                    userViewHolder.InviteIcon.setVisibility(View.GONE);

                }else if (lists.get(position).getIs_friend_request_sent().matches("")){

                }


                if(lists.get(position).getIs_friend().matches("1")){
                    userViewHolder.AlreadyFriendIcon.setVisibility(View.VISIBLE);
                    userViewHolder.AlreadyInviteIcon.setVisibility(View.GONE);
                    userViewHolder.InviteIcon.setVisibility(View.GONE);
                }else if (lists.get(position).getIs_friend().matches("")){


                }

                userViewHolder.InviteIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        lists.get(position).setIs_friend_request_sent("1");
                        userViewHolder.AlreadyInviteIcon.setVisibility(View.VISIBLE);
                        userViewHolder.AlreadyFriendIcon.setVisibility(View.GONE);
                        userViewHolder.InviteIcon.setVisibility(View.GONE);
                        SendInviteToFriends(lists.get(position).getUid());



                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }



    @Override
    public int getItemCount() {
        return lists.size();
    }






    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }

    /**
     * Viewholder for Adapter
     */
    class CustomVholder extends RecyclerView.ViewHolder {

        ImageView friendIcon;

        private TextView tv_friendname, tv_email,InviteIcon,tv_department,tv_departmentrole,AlreadyFriendIcon,AlreadyInviteIcon;
        RelativeLayout mainlay;
        public CustomVholder(View itemView) {
            super(itemView);

            tv_friendname = itemView.findViewById(R.id.tv_friendname);
            tv_email = itemView.findViewById(R.id.tv_email);
            friendIcon =  itemView.findViewById(R.id.friendIcon);
            InviteIcon = itemView.findViewById(R.id.InviteIcon);
            tv_department = itemView.findViewById(R.id.tv_department);
            tv_departmentrole = itemView.findViewById(R.id.tv_departmentrole);
            AlreadyFriendIcon = itemView.findViewById(R.id.AlreadyFriendIcon);
            AlreadyInviteIcon = itemView.findViewById(R.id.AlreadyInviteIcon);
            mainlay =itemView.findViewById(R.id.mainlay);

        }



    }
    /*
    SEND FRIEND REQUEST*/


    private void SendInviteToFriends(String uid) {

        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_SENDINFITE+"/"+uid+"/"+"relationship/friends",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {

                                }else {

                                    showSnackbarMessage(stateResponse.getMessage());
                                }
                            } else {

                                showServerSnackbar(R.string.error_responce);

                            }
                        } catch (Exception e) {

                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(activity).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(activity).getaccesstoken().sessionName+"="+SessionManager.getInstance(activity).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(mStrRequest);

    }

}


