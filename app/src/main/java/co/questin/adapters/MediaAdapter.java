package co.questin.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import co.questin.R;
import co.questin.college.CollageMediaDetails;
import co.questin.models.MediaArray;

import static co.questin.models.MediaArray.COARSE_TYPE;
import static co.questin.models.MediaArray.PROGRESS_;

/**
 * Created by Dell on 17-08-2017.
 */

public class MediaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private SparseIntArray sparseIntArray;
    private Activity activity;
    private ArrayList<MediaArray> lists;


    public MediaAdapter( Activity activity, ArrayList<MediaArray> lists) {
        this.lists = lists;
        this.activity = activity;

        sparseIntArray = new SparseIntArray(5);
        sparseIntArray.put(0, R.mipmap.ic_media);
        sparseIntArray.put(1, R.mipmap.ic_mediab);
        sparseIntArray.put(2, R.mipmap.ic_mediac);
        sparseIntArray.put(3, R.mipmap.ic_mediad);
        sparseIntArray.put(4, R.mipmap.ic_mediae);
    }

    public void addData(ArrayList<MediaArray> lists){

        int size= this.lists.size();
        Log.i("sizeing", "size of new: "+ lists.size()+ " "+ "old: "+ this.lists.size());
        this.lists.addAll(lists);             //add kara
        Log.i("sizeing", "size: "+ this.lists.size());
        notifyItemRangeInserted(size, this.lists.size());        //existing size, aur new size
    }

    public void removeProgress(){

        int size= lists!=null && lists.size()>0?lists.size():0;

        if(size>0){

            lists.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }



    @Override
    public int getItemViewType(int position) {

        if(lists!=null && lists.size()>0)
            return lists.get(position).getType();
        return 0;
    }
    public void setInitialData( ArrayList<MediaArray> list){

        this.lists=list;
        notifyDataSetChanged();
    }

    public void addProgress(){
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                if(lists!=null && lists.size()>0){
                    lists.add(new MediaArray(1));
                    notifyItemInserted(lists.size()-1);
                }
            }
        };

        handler.post(r);

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COARSE_TYPE) {
            View view = LayoutInflater.from(activity).inflate(R.layout.list_of_medialist, parent, false);
            return new CustomVholder(view);
        } else if (viewType == PROGRESS_) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


        if (holder instanceof CustomVholder) {
            final MediaArray contact = lists.get(position);
            final CustomVholder userViewHolder = (CustomVholder) holder;





            try {
                userViewHolder.tv_title.setText(lists.get(position).getTitle());

                userViewHolder.mediaIcon.setBackground(ContextCompat.getDrawable(activity, sparseIntArray.get(position%sparseIntArray.size())));

                userViewHolder.link.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Intent intent=new Intent(activity,CollageMediaDetails.class);
                        Bundle bundle=new Bundle();
                        bundle.putString("MEDIA_ID",contact.getTnid());

                        intent.putExtras(bundle);
                        activity.startActivity(intent);





                    }
                });
                userViewHolder.MainLay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Intent intent=new Intent(activity,CollageMediaDetails.class);
                        Bundle bundle=new Bundle();
                        bundle.putString("MEDIA_ID",contact.getTnid());
                        intent.putExtras(bundle);
                        activity.startActivity(intent);





                    }
                });



            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return lists.size();
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }




    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
        ImageView mediaIcon;
        private TextView tv_title,tv_discription;
        LinearLayout link;
        RelativeLayout MainLay;


        public CustomVholder(View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.tv_title);
            mediaIcon = itemView.findViewById(R.id.mediaIcon);
            link = itemView.findViewById(R.id.link);
            MainLay= itemView.findViewById(R.id.MainLay);


        }


    }


}
