package co.questin.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import co.questin.R;
import co.questin.college.ServicesDetailedPage;
import co.questin.models.CampusServiceArray;

import static co.questin.models.CampusServiceArray.COARSE_TYPE;
import static co.questin.models.CampusServiceArray.PROGRESS_;


/**
 * Created by Dell on 17-08-2017.
 */

public class CampusServicesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity activity;
    private ArrayList<CampusServiceArray> lists;

    public CampusServicesAdapter(RecyclerView recyclerView, ArrayList<CampusServiceArray> lists, Activity activity) {
        this.lists = lists;
        this.activity = activity;


    }


    public void addData(ArrayList<CampusServiceArray> list){

        int size= this.lists.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ this.lists.size());
        this.lists.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.lists.size());
        notifyItemRangeInserted(size, this.lists.size());        //existing size, aur new size
    }

    public void removeProgress(){

        int size= lists!=null && lists.size()>0?lists.size():0;

        if(size>0){

            lists.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }


    @Override
    public int getItemViewType(int position) {

        if(lists!=null && lists.size()>0)
            return lists.get(position).getType();
        return 0;
    }
    public void setInitialData( ArrayList<CampusServiceArray> lists){

        this.lists=lists;
        notifyDataSetChanged();
    }

    public void addProgress(){
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {

                if(lists!=null && lists.size()>0){
                    lists.add(new CampusServiceArray(1));
                    notifyItemInserted(lists.size()-1);
                }
            }
        };

        handler.post(r);
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COARSE_TYPE) {
            View view = LayoutInflater.from(activity).inflate(R.layout.list_of_campusservices, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == PROGRESS_) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof UserViewHolder) {
            final CampusServiceArray course = lists.get(position);
            final UserViewHolder userViewHolder = (UserViewHolder) holder;

            userViewHolder.tv_title.setText(lists.get(position).getTitle());


            if(lists.get(position).getLogo() != null && lists.get(position).getLogo().length() > 0 ) {

                Glide.with(activity).load(lists.get(position).getLogo())

                        .placeholder(R.mipmap.campus_service_icon).dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .skipMemoryCache(true)

                        .fitCenter().into(userViewHolder.image);


            }else {
                userViewHolder.image.setImageResource(R.mipmap.campus_service_icon);

            }

            //  holder.tv_discription.setText(lists.get(position).getDescription());
            String s =lists.get(position).getDescription();
            userViewHolder.tv_discription.setText(Html.fromHtml(s));

            userViewHolder.link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent(activity,ServicesDetailedPage.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("SERVICE_ID",lists.get(position).getTnid());
                    bundle.putString("SERVICE_TITLE",lists.get(position).getTitle());
                    intent.putExtras(bundle);


                    Pair<ImageView, String> p1 = Pair.create(userViewHolder.image, "transitionProfile");
                    Pair <TextView, String> p2 = Pair.create(userViewHolder.tv_discription, "Servicedetail");
                    Pair <TextView, String> p3 = Pair.create(userViewHolder.tv_discription, "Servicedetail");

                    ActivityOptionsCompat transitionActivityOptions = null;

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, new Pair[]{p1,p2,p3});
                    }

                    activity.startActivity(intent,transitionActivityOptions.toBundle());
                }
            });
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return lists == null ? 0 : lists.size();
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }

    private class UserViewHolder extends RecyclerView.ViewHolder {

        TextView tv_title, tv_discription;
        LinearLayout link;
        ImageView image;


        public UserViewHolder(View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(co.questin.R.id.tv_title);
            tv_discription = itemView.findViewById(co.questin.R.id.tv_discription);
            link = itemView.findViewById(co.questin.R.id.link);
            image = itemView.findViewById(co.questin.R.id.image);


        }

    }
}