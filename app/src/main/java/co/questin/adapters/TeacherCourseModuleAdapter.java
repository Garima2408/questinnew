package co.questin.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import co.questin.R;
import co.questin.models.CoursemoduleArray;
import co.questin.teacher.TeacherCourseModuleDetails;

import static co.questin.models.CoursemoduleArray.COARSE_TYPE;
import static co.questin.models.CoursemoduleArray.PROGRESS_;

/**
 * Created by Dell on 28-09-2017.
 */

public class TeacherCourseModuleAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;
    private ArrayList<CoursemoduleArray> SubjectList;
    String Faculty_Value,Student_Value;
    String Join_Value;
    String ValueCame;

    public TeacherCourseModuleAdapter(RecyclerView recyclerView, ArrayList<CoursemoduleArray> SubjectList, Activity activity,String ValueCame) {
        this.SubjectList = SubjectList;
        this.activity = activity;
        this.ValueCame =ValueCame;


    }


    public void addData(List<CoursemoduleArray> list){

        int size= this.SubjectList.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ this.SubjectList.size());
        this.SubjectList.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.SubjectList.size());
        notifyItemRangeInserted(size, this.SubjectList.size());        //existing size, aur new size
    }

    public void removeProgress(){

        int size= SubjectList!=null && SubjectList.size()>0?SubjectList.size():0;

        if(size>0){

            SubjectList.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }



    @Override
    public int getItemViewType(int position) {

        if(SubjectList!=null && SubjectList.size()>0)
            return SubjectList.get(position).getType();
        return 0;
    }
    public void setInitialData( ArrayList<CoursemoduleArray> list){
        SubjectList.clear();
        this.SubjectList=list;
        notifyDataSetChanged();
    }

    public void addProgress(){
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                if(SubjectList!=null && SubjectList.size()>0){
                    SubjectList.add(new CoursemoduleArray(1));
                    notifyItemInserted(SubjectList.size()-1);
                }
            }
        };

        handler.post(r);

    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COARSE_TYPE) {
            View view = LayoutInflater.from(activity).inflate(R.layout.list_of_coursemodule, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == PROGRESS_) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof UserViewHolder) {
            final CoursemoduleArray subjectlist = SubjectList.get(position);
            final UserViewHolder userViewHolder = (UserViewHolder) holder;


            userViewHolder.tv_title.setText(Html.fromHtml(subjectlist.getTitle()));
            userViewHolder.code.setText(subjectlist.getCode());
            userViewHolder.tv_teacher.setText(subjectlist.getTeacher());

            if(subjectlist.getLogo() != null && subjectlist.getLogo().length() > 0 ) {

                Glide.with(activity).load(subjectlist.getLogo())
                        .placeholder(R.mipmap.clasrrominfo).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(userViewHolder.circleView);

            }




            if(subjectlist.getIs_faculty().contains("TRUE")) {
                Faculty_Value ="TRUE";
                Log.d("TAG", "Faculty_Value" + Faculty_Value);


            }else if(subjectlist.getIs_faculty().contains("FALSE")){
                Faculty_Value ="FALSE";
                Log.d("TAG", "Faculty_Value" + Faculty_Value);

            }

            if(subjectlist.getIs_member().contains("member")) {
                Join_Value ="member";
                Log.d("TAG", "Join_Value" + Join_Value);


            }else if(subjectlist.getIs_member().contains("pending")){
                Join_Value ="pending";
                Log.d("TAG", "Join_Value" + Join_Value);

            }
            else if(subjectlist.getIs_member().contains("nomember")){
                Join_Value ="nomember";
                Log.d("TAG", "Join_Value" + Join_Value);

            }else if (subjectlist.getIs_student().contains("TRUE"))
            {
                Faculty_Value="FALSE";
                Student_Value="TRUE";

            }


            userViewHolder.link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent backIntent = new Intent(activity, TeacherCourseModuleDetails.class)
                            .putExtra("COURSE_ID",subjectlist.getTnid())
                            .putExtra("TITTLE",subjectlist.getTitle())
                            .putExtra("COURSE_IMAGE",subjectlist.getLogo())
                            .putExtra("CHECKEDVALUETRUE",subjectlist.getIs_faculty())
                            .putExtra("VALUECHANGED",ValueCame)
                            .putExtra("ISStudent",subjectlist.getIs_student())
                            .putExtra("JOINREQUEST",subjectlist.getIs_member());



                    Pair<ImageView, String> p1 = Pair.create(userViewHolder.circleView, "transitionProfile");
                    Pair <TextView, String> p2 = Pair.create(userViewHolder.tv_title, "UserName");

                    ActivityOptionsCompat transitionActivityOptions = null;

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, new Pair[]{p1,p2});
                    }

                    activity.startActivity(backIntent,transitionActivityOptions.toBundle());




                }
            });





        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return SubjectList == null ? 0 : SubjectList.size();
    }



    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }

    private class UserViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title, tv_teacher, tv_cradits, code;
        ImageView circleView;
        FrameLayout link;

        public UserViewHolder(View view) {
            super(view);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_teacher = itemView.findViewById(R.id.tv_teacher);
            tv_cradits = itemView.findViewById(R.id.tv_cradits);
            code = itemView.findViewById(R.id.code);
            circleView = itemView.findViewById(R.id.circleView);
            link = itemView.findViewById(R.id.link);
        }
    }
}