package co.questin.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import co.questin.R;
import co.questin.models.ResourceAddList;
import co.questin.utils.Utils;

/**
 * Created by Dell on 18-12-2017.
 */

public class ResourceAddAdapter extends ArrayAdapter<ResourceAddList> {
    ArrayList<ResourceAddList> versionList;
    LayoutInflater vi;
    int Resource;
    Dialog imageDialog;
    String Extension;
    Bitmap thumbnail = null;

    String Filename;
    private Activity mcontext;

    public ResourceAddAdapter(Activity mcontext, int resource, ArrayList<ResourceAddList> objects) {
        super(mcontext, resource, objects);
        vi = (LayoutInflater) mcontext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource = resource;
        versionList = objects;
        this.mcontext = mcontext;
    }



    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 500;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // convert view = design
        View v = convertView;
        final ViewHolder holder;

        if (v == null) {
            v = vi.inflate(R.layout.list_of_addresource, null);
            holder = new ViewHolder();
            holder.tv_title = v.findViewById(R.id.tv_title);
            holder.Value = v.findViewById(R.id.Value);
            holder.imagepreview=v.findViewById(R.id.imagepreview);
            holder.options = v.findViewById(R.id.options);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        if(versionList.get(position).getValue() != null && versionList.get(position).getValue().length() > 0 ) {
            Extension = versionList.get(position).getValue().substring(versionList.get(position).getValue().lastIndexOf("."));
            Log.d("TAG", "Extension: " + Extension);

            if (Extension.matches(".jpg")) {


                Glide.clear(holder.imagepreview);
                Glide.with(mcontext).load(versionList.get(position).getValue())
                        .dontAnimate()
                        .placeholder(R.color.black).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(holder.imagepreview);


            }
            else if (Extension.matches(".jpeg")) {


                Glide.clear(holder.imagepreview);
                Glide.with(mcontext).load(versionList.get(position).getValue())
                        .dontAnimate()
                        .placeholder(R.color.black).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(holder.imagepreview);


            } else if (Extension.matches(".png")) {


                Glide.clear(holder.imagepreview);
                Glide.with(mcontext).load(versionList.get(position).getValue())
                        .dontAnimate()
                        .placeholder(R.color.black).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(holder.imagepreview);


            } else if (Extension.matches(".docx")) {

                holder.imagepreview.setImageResource(R.mipmap.ic_doc_download);



            } else if (Extension.matches(".pdf")) {

                holder.imagepreview.setImageResource(R.mipmap.ic_pdf_download);



            } else if (Extension.matches(".txt")) {
                holder.imagepreview.setImageResource(R.mipmap.ic_text_download);



            } else if (Extension.matches(".zip")) {
                holder.imagepreview.setImageResource(R.mipmap.ic_zip_download);



            } else if (Extension.matches(".doc")) {
                holder.imagepreview.setImageResource(R.mipmap.ic_doc_download);



            }
            else if (Extension.matches(".xls")) {
                holder.imagepreview.setImageResource(R.mipmap.ic_xls_download);



            }
            else if (Extension.matches(".xlsx")) {
                holder.imagepreview.setImageResource(R.mipmap.ic_xls_download);

            }

        }else {
            holder.imagepreview.setImageDrawable(null);
        }

        holder.imagepreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.ImageDialogOpen(mcontext,versionList.get(position).getValue());
            }
        });

        holder.tv_title.setText(versionList.get(position).getTitle());
        holder.Value.setText("Value :"+versionList.get(position).getValue());


        holder.options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPopupMenu();


            }

            private void showPopupMenu() {


                PopupMenu popup = new PopupMenu(mcontext, holder.options);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.optionpopupmenu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.Editoption:




                                return true;
                            case R.id.deleteoption:

                                AlertDialog.Builder builder = new AlertDialog.Builder(mcontext, android.app.AlertDialog.THEME_HOLO_DARK)
                                        .setTitle("Delete Resource")
                                        .setMessage(R.string.delete)
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {

                                                versionList.remove(versionList.get(position));
                                                notifyDataSetChanged();

                                            }
                                        })
                                        .setNegativeButton("No", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        });
                                builder.create().show();



                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popup.show();




            }
        });
        return v;
    }

    private  class ViewHolder {

        public TextView tv_title,Value;
        ImageView options,imagepreview;


    }





}


