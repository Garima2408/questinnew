package co.questin.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.models.AllFriends;

import co.questin.network.URLS;
import co.questin.studentprofile.AnotherUserDisplay;
import co.questin.teacher.CourseModuleTeacherInfo;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * Created by Dell on 21-08-2017.
 */

public class UsesFriendsDisplayAdapter extends RecyclerView.Adapter<UsesFriendsDisplayAdapter.CustomVholder> {
    private ArrayList<AllFriends> lists;
    private Activity activity;


    public UsesFriendsDisplayAdapter(Activity activity, ArrayList<AllFriends> lists) {
        this.lists = lists;
        this.activity = activity;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_myfriends, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.tv_friendname.setText(lists.get(position).getField_firstname());
            holder.tv_friendlastname.setText(lists.get(position).getField_lastname());


            if (lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0) {
                Glide.clear(holder.friendIcon);
                Glide.with(activity).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(holder.friendIcon);

            } else {
                holder.friendIcon.setImageResource(R.mipmap.place_holder);
                Glide.clear(holder.friendIcon);
            }


            if (lists.get(position).getMainRole() != null && lists.get(position).getMainRole().length() > 0) {

                if (lists.get(position).getMainRole().matches("13")) {
                    holder.tv_departmentrole.setText(lists.get(position).CollageRole);
                    holder.tv_department.setText(lists.get(position).CollageBranch);


                } else if (lists.get(position).getMainRole().matches("14")) {
                    holder.tv_departmentrole.setText(lists.get(position).CollageRole);
                    holder.tv_department.setText(lists.get(position).getCollagedepartment());


                } else if (lists.get(position).getMainRole().matches("15")) {
                    holder.tv_departmentrole.setText(lists.get(position).CollageRole);
                    holder.tv_department.setText(lists.get(position).CollageBranch);


                } else if (lists.get(position).getMainRole().matches("16")) {
                    holder.tv_departmentrole.setText(lists.get(position).CollageRole);
                    holder.tv_department.setText(lists.get(position).CollageStudentName);

                }


            } else {

            }


            holder.tv_friendname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(activity, AnotherUserDisplay.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getUid())
                            .putExtra("COLLAGE_ID",lists.get(position).getCollageId());
                    Pair<ImageView, String> p1 = Pair.create(holder.friendIcon, "transitionProfile");
                    Pair <TextView, String> p2 = Pair.create(holder.tv_friendname, "publisherName");


                    ActivityOptionsCompat transitionActivityOptions = null;

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, new Pair[]{p1,p2});
                    }

                    activity.startActivity(backIntent,transitionActivityOptions.toBundle());




                }
            });


            holder.friendIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(activity, AnotherUserDisplay.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getUid())
                            .putExtra("COLLAGE_ID",lists.get(position).getCollageId());
                    Pair<ImageView, String> p1 = Pair.create(holder.friendIcon, "transitionProfile");
                    Pair <TextView, String> p2 = Pair.create(holder.tv_friendname, "publisherName");


                    ActivityOptionsCompat transitionActivityOptions = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, new Pair[]{p1,p2});
                    }

                    activity.startActivity(backIntent,transitionActivityOptions.toBundle());




                }
            });

            holder.MainHead.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(activity, AnotherUserDisplay.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getUid())
                            .putExtra("COLLAGE_ID",lists.get(position).getCollageId());
                    Pair<ImageView, String> p1 = Pair.create(holder.friendIcon, "transitionProfile");
                    Pair <TextView, String> p2 = Pair.create(holder.tv_friendname, "publisherName");


                    ActivityOptionsCompat transitionActivityOptions = null;

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, new Pair[]{p1,p2});
                    }

                    activity.startActivity(backIntent,transitionActivityOptions.toBundle());




                }
            });





            holder.options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showPopupMenu();


                }

                private void showPopupMenu() {


                    PopupMenu popup = new PopupMenu(activity, holder.options);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.removepopupmenu, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {

                                case R.id.deleteoption:

                                    AlertDialog.Builder builder = new AlertDialog.Builder(activity, android.app.AlertDialog.THEME_HOLO_DARK)
                                            .setTitle("Remove friend")
                                            .setMessage(R.string.Remove_Student)
                                            .setCancelable(false)
                                            .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {

                                                    /*deletememberfromlist = new DeleteMemberFromList();
                                                    deletememberfromlist.execute(course_id,lists.get(position).getUid());*/
                                                }
                                            })
                                            .setNegativeButton("No", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                    builder.create().show();


                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder  {
        ImageView friendIcon;
        RelativeLayout MainHead;
        private TextView tv_friendname,tv_friendlastname,tv_department,tv_departmentrole;
        ImageView options;

        public CustomVholder(View itemView) {
            super(itemView);

            tv_friendname = itemView.findViewById(R.id.tv_friendname);
            tv_friendlastname = itemView.findViewById(R.id.tv_friendlastname);
            friendIcon = itemView.findViewById(R.id.friendIcon);
            options = itemView.findViewById(R.id.options);
            tv_department = itemView.findViewById(R.id.tv_department);
            tv_departmentrole = itemView.findViewById(R.id.tv_departmentrole);
            MainHead =itemView.findViewById(R.id.MainHead);


        }

    }

}


