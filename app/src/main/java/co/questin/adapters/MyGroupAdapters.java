package co.questin.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;

import co.questin.R;
import co.questin.models.MyGroupItem;

/**
 * Created by Dell on 28-07-2017.
 */

public class MyGroupAdapters extends RecyclerView.Adapter<MyGroupAdapters.CustomVholder> {

    private ArrayList<MyGroupItem> lists;
    private Context mcontext;


    public MyGroupAdapters(Context mcontext, ArrayList<MyGroupItem> lists) {
        this.lists = lists;
        this.mcontext = mcontext;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_group, null);

        return new CustomVholder(view);
    }

    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        final MyGroupItem mgroups = lists.get(position);


        try {
            holder.tv_title.setText(lists.get(position).getTitle());
            Log.v("chechUri", lists.get(position).getImage());

            if(lists.get(position).getImage().isEmpty()){
                holder.iv_groupIcon.setImageResource(R.mipmap.groups_placeholder);

            }else {
                new DownloadImageTask(holder.iv_groupIcon).execute(lists.get(position).getImage());

            }



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView iv_groupIcon;
        private TextView tv_title;


        public CustomVholder(View itemView) {
            super(itemView);
            iv_groupIcon = itemView.findViewById(R.id.iv_groupIcon);
            tv_title = itemView.findViewById(R.id.tv_title);


        }

        @Override
        public void onClick(View view) {

        }
    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }
        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}











