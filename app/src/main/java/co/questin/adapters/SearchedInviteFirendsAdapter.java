package co.questin.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.library.StateBean;
import co.questin.models.AllUser;

import co.questin.network.URLS;
import co.questin.studentprofile.AnotherUserDisplay;
import co.questin.studentprofile.InviteFriends;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static co.questin.utils.BaseAppCompactActivity.ShowIndicator;
import static co.questin.utils.BaseAppCompactActivity.hideIndicator;
import static co.questin.utils.BaseAppCompactActivity.showConnectionSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showServerSnackbar;
import static co.questin.utils.BaseAppCompactActivity.showSnackbarMessage;

/**
 * Created by Dell on 21-08-2017.
 */

public class SearchedInviteFirendsAdapter extends RecyclerView.Adapter<SearchedInviteFirendsAdapter.CustomVholder> {
    private ArrayList<AllUser> lists;
    private Context mcontext;


    public SearchedInviteFirendsAdapter(Context mcontext, ArrayList<AllUser> lists) {
        this.lists = lists;
        this.mcontext = mcontext;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_invitefriends, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.tv_friendname.setText(lists.get(position).getName());
            holder.tv_email.setText(lists.get(position).getCollageName());

            if(lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0 ) {
                Glide.clear(holder.friendIcon);
                Glide.with(mcontext).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(holder.friendIcon);

            }else {
                holder.friendIcon.setImageResource(R.mipmap.place_holder);
                Glide.clear(holder.friendIcon);
            }

            if (lists.get(position).getMainRole() != null &&lists.get(position).getMainRole().length()> 0){

                if (lists.get(position).getMainRole().matches("13")) {
                    holder.tv_departmentrole.setText(lists.get(position).CollageRole);
                    holder.tv_department.setText(lists.get(position).CollageBranch);


                } else if (lists.get(position).getMainRole().matches("14")) {
                    holder.tv_departmentrole.setText(lists.get(position).CollageRole);
                    holder.tv_department.setText(lists.get(position).getCollagedepartment());


                } else if (lists.get(position).getMainRole().matches("15")) {
                    holder.tv_departmentrole.setText(lists.get(position).CollageRole);
                    holder.tv_department.setText(lists.get(position).CollageBranch);


                }else if (lists.get(position).getMainRole().matches("3")) {
                    holder.tv_departmentrole.setText(lists.get(position).CollageRole);
                    holder.tv_department.setText(lists.get(position).getCollagedepartment());


                }


            }else {

            }


            if(lists.get(position).getIs_friend_request_sent().matches("1")){
                holder.AlreadyInviteIcon.setVisibility(View.VISIBLE);
                holder.AlreadyFriendIcon.setVisibility(View.GONE);
                holder.InviteIcon.setVisibility(View.GONE);


            }else if (lists.get(position).getIs_friend_request_sent().matches("")){

            }


            if(lists.get(position).getIs_friend().matches("1")){
                holder.AlreadyFriendIcon.setVisibility(View.VISIBLE);
                holder.AlreadyInviteIcon.setVisibility(View.GONE);
                holder.InviteIcon.setVisibility(View.GONE);
            }else if (lists.get(position).getIs_friend().matches("")){


            }



            if(lists.get(position).getUid().equals(SessionManager.getInstance(mcontext).getUser().getUserprofile_id())){
                holder.AlreadyFriendIcon.setVisibility(View.GONE);
                holder.AlreadyInviteIcon.setVisibility(View.GONE);
                holder.InviteIcon.setVisibility(View.GONE);
            }else {

            }




            holder.InviteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    lists.get(position).setIs_friend_request_sent("1");
                    holder.AlreadyInviteIcon.setVisibility(View.VISIBLE);
                    holder.AlreadyFriendIcon.setVisibility(View.GONE);
                    holder.InviteIcon.setVisibility(View.GONE);
                  SendInviteToFriends(lists.get(position).getUid());



                }
            });


            holder.tv_friendname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(mcontext, AnotherUserDisplay.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getUid())
                            .putExtra("COLLAGE_ID",lists.get(position).getCollageId());
                    mcontext.startActivity(backIntent);

                }
            });

            holder.friendIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(mcontext, AnotherUserDisplay.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getUid())
                            .putExtra("COLLAGE_ID",lists.get(position).getCollageId());
                    mcontext.startActivity(backIntent);

                }
            });

            holder.mainlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(mcontext, AnotherUserDisplay.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getUid())
                            .putExtra("COLLAGE_ID",lists.get(position).getCollageId());
                    mcontext.startActivity(backIntent);

                }
            });



        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
       ImageView friendIcon;

        private TextView tv_friendname, tv_email,InviteIcon,tv_department,tv_departmentrole,AlreadyFriendIcon,AlreadyInviteIcon;
        RelativeLayout mainlay;

        public CustomVholder(View itemView) {
            super(itemView);

            tv_friendname = itemView.findViewById(R.id.tv_friendname);
            tv_email = itemView.findViewById(R.id.tv_email);
            friendIcon =  itemView.findViewById(R.id.friendIcon);
            InviteIcon = itemView.findViewById(R.id.InviteIcon);
            tv_department = itemView.findViewById(R.id.tv_department);
            tv_departmentrole = itemView.findViewById(R.id.tv_departmentrole);
            AlreadyFriendIcon = itemView.findViewById(R.id.AlreadyFriendIcon);
            AlreadyInviteIcon = itemView.findViewById(R.id.AlreadyInviteIcon);
            mainlay =itemView.findViewById(R.id.mainlay);
        }



    }
    /*
    SEND FRIEND REQUEST*/

    private void SendInviteToFriends(String uid) {
        ShowIndicator();

        StringRequest mStrRequest = new StringRequest(Request.Method.POST, URLS.URL_SENDINFITE+"/"+uid+"/"+"relationship/friends",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                        try {
                            Gson gson = new GsonBuilder().create();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonResp = jsonParser.parse(response).getAsJsonObject();
                            StateBean stateResponse = gson.fromJson(jsonResp, StateBean.class);
                            if (stateResponse != null) {
                                hideIndicator();
                                if (Utils.isStatusSuccess(stateResponse.getStatus())) {
                                    showSnackbarMessage(stateResponse.getMessage());
                                }else {
                                    hideIndicator();
                                    showSnackbarMessage(stateResponse.getMessage());


                                }
                            } else {
                                hideIndicator();
                                showServerSnackbar(R.string.error_responce);


                            }
                        } catch (Exception e) {
                            hideIndicator();
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideIndicator();
                        if (error instanceof NoConnectionError) {
                            showConnectionSnackbar();
                        } else {
                            showServerSnackbar(R.string.error_responce);
                        }
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ");
                params.put("Accept-Language", "application/json");
                params.put("X-CSRF-Token", SessionManager.getInstance(mcontext).getaccesstoken().accesstoken);
                params.put("Cookie", SessionManager.getInstance(mcontext).getaccesstoken().sessionName+"="+SessionManager.getInstance(mcontext).getaccesstoken().sessionID);

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }


        };

        mStrRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        requestQueue.add(mStrRequest);
    }


}


