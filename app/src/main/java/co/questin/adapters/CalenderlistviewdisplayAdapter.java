package co.questin.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import co.questin.R;
import co.questin.calendersection.EventDetailCollage;
import co.questin.calendersection.UpdateCalenderClasses;
import co.questin.calendersection.UpdateCalenderDues;
import co.questin.calendersection.UpdateCalenderEvent;
import co.questin.calendersection.UpdateCalenderExam;

/**
 * Created by Dell on 03-10-2017.
 */

public class CalenderlistviewdisplayAdapter extends BaseAdapter {
    private Context context;
    List<String> attendancelist;

    public CalenderlistviewdisplayAdapter(Context context, List<String> attendancelist) {

        this.context = context;
        this.attendancelist = attendancelist;
        }

    @Override
    public int getCount() {
        return attendancelist.size();
    }

    @Override
    public String getItem(int position) {
        return attendancelist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView
            , ViewGroup parent) {
        View row;
        final ListViewHolder listViewHolder;
        if(convertView == null)
        {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.list_of_calenderevent,parent,false);
            listViewHolder = new ListViewHolder();
            listViewHolder.tv_event_name = row.findViewById(R.id.tv_event_name);
            listViewHolder.ll_parrent= row.findViewById(R.id.ll_parrent);

            row.setTag(listViewHolder);
        }
        else
        {
            row=convertView;
            listViewHolder= (ListViewHolder) row.getTag();
        }

        listViewHolder.id = position;

        String[] terms = attendancelist.get(position).split("\\$");
        final String split_one=terms[0];
        final String split_classid =terms[1];
        final String split_value =terms[2];
        final String split_Type =terms[3];


        listViewHolder.tv_event_name.setText(split_one);




        listViewHolder.ll_parrent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if(split_value.contains("true") && split_Type.contains("collageevent")){
                    Intent intent=new Intent(context,EventDetailCollage.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("EVENT_ID",split_classid);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }







                else if(split_value.contains("false") && split_Type.contains("classpersonal")){
                    Intent intent=new Intent(context,UpdateCalenderClasses.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("CLASSES_ID",split_classid);
                    intent.putExtras(bundle);
                    context.startActivity(intent);



                }else  if(split_value.contains("false") && split_Type.contains("eventpersonal")){
                    Intent intent=new Intent(context,UpdateCalenderEvent.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("CLASSES_ID",split_classid);
                    intent.putExtras(bundle);
                    context.startActivity(intent);



                }
                else  if(split_value.contains("false") && split_Type.contains("myduespersonal")){
                    Intent intent=new Intent(context,UpdateCalenderDues.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("CLASSES_ID",split_classid);
                    intent.putExtras(bundle);
                    context.startActivity(intent);

                } else  if(split_value.contains("false") && split_Type.contains("myexampersonal")){
                    Intent intent=new Intent(context,UpdateCalenderExam.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("CLASSES_ID",split_classid);
                    intent.putExtras(bundle);
                    context.startActivity(intent);



                }

            }
        });



        return row;
    }



}


