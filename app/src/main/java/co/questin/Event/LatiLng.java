package co.questin.Event;

/**
 * Created by HP on 3/28/2018.
 */

public class LatiLng {

    private String lat;
    private String lng;
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LatiLng(String lat, String lng, String address) {
        this.lat = lat;
        this.lng = lng;
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
